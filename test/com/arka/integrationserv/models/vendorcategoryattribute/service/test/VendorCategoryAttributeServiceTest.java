package com.arka.integrationserv.models.vendorcategoryattribute.service.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.DataType;
import com.arka.integrationserv.models.vendorcategoryattribute.repository.VendorCategoryAttributeRepository;
import com.arka.integrationserv.models.vendorcategoryattribute.service.VendorCategoryAttributeService;
import com.arka.integrationserv.models.vendorcategoryattribute.service.impl.VendorCategoryAttributeServiceImpl;
import com.arka.integrationserv.test.Base;
import com.arka.integrationserv.test.Constants;
import com.google.common.base.Supplier;

import play.libs.Json;

public class VendorCategoryAttributeServiceTest extends Base implements Constants {

	@InjectMocks
	private VendorCategoryAttributeService<String> vendorCategoryAttributeService = new VendorCategoryAttributeServiceImpl();

	@Mock
	private VendorCategoryAttributeRepository vendorCategoryAttributeRepoMock;

	@Mock
	private CacheService cacheMock;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	VendorCategoryAttribute vendorCategoryAttribute;

	private static final String NAME = "test_name";
	private static final String PRODUCT_ID = "57dfbcecbffa5307fc308c7f";
	private static final String VENDOR_ID = "EqTraX5StRb4FiQjWzL90g";
	private static final Long USER = 1L;
	private static final String CODE = "Password_Test";
	private static final String DESCRIPTION = "Password of the user for unit test";

	private static final Integer WEIGHT = 1;
	private static final String PATH = "path,test";
	private static final String CATEGORY_ID = "cat_57a9bfeca81b981";
	public static final ObjectId tagId = new ObjectId("572759b690e5461f00a0a8f9");

	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		MockitoAnnotations.initMocks(this);
		createModel();
	}

	private void createModel() {
		vendorCategoryAttribute = new VendorCategoryAttribute();
		vendorCategoryAttribute.setId(objectId.toHexString());
		vendorCategoryAttribute.setCode(CODE);
		vendorCategoryAttribute.setCategoryAttributeId(CATEGORY_ID);
		vendorCategoryAttribute.setCategoryId(CATEGORY_ID);
		vendorCategoryAttribute.setDataType(DataType.STRING);
		// vendorCategoryAttribute.setDataTypeTemplate(null);
		// vendorCategoryAttribute.setDefaultValue(null);
		vendorCategoryAttribute.setMandatory(false);
		vendorCategoryAttribute.setPath(PATH);
		// vendorCategoryAttribute.setRestrictionId(null);
		// vendorCategoryAttribute.setTransformationToCatalog(null);
		// vendorCategoryAttribute.setTransformToVendor(null);
		vendorCategoryAttribute.setWeight(WEIGHT);
		vendorCategoryAttribute.setDescription(DESCRIPTION);
		vendorCategoryAttribute.setFlags(1);
		vendorCategoryAttribute.setName(NAME);
		vendorCategoryAttribute.setUpdatedBy(updatedUser);
		vendorCategoryAttribute.setVendor_id(VENDOR_ID);
		vendorCategoryAttribute.setProductId(Arrays.asList(PRODUCT_ID));
		vendorCategoryAttribute.setStatus(Status.ACTIVE);
		vendorCategoryAttribute.setCreatedBy(USER);
	}

	@Test
	public void testSaveVendorCategoryAttributeException() throws ArkaValidationException {
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(null);
	}

	@Test
	public void testSaveVendorCategoryAttributeException2() throws ArkaValidationException {
		Mockito.doReturn(1L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		vendorCategoryAttribute.setCode(null);
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testSaveVendorCategoryAttributeException3() throws ArkaValidationException {
		Mockito.doReturn(1L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		vendorCategoryAttribute.setCode("");
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testSaveVendorCategoryAttributeException4() throws ArkaValidationException {
		Mockito.doReturn(2L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		vendorCategoryAttribute.setName(null);
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testSaveVendorCategoryAttributeException5() throws ArkaValidationException {
		Mockito.doReturn(2L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		vendorCategoryAttribute.setStatus(null);
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testSaveVendorCategoryAttributeException6() throws ArkaValidationException {
		Mockito.doReturn(2L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		vendorCategoryAttribute.setName("");
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testSaveVendorCategoryAttributeException7() throws ArkaValidationException {
		Mockito.doReturn(2L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		vendorCategoryAttribute.setId(null);
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}
	/*
	 * @Test public void testSaveVendorCategoryAttributeException8() throws
	 * ArkaValidationException {
	 * Mockito.doReturn(0L).when(vendorCategoryAttributeRepoMock).count(Matchers
	 * .anyMap()); vendorCategoryAttribute.setId(null);
	 * expectedException.expect(ArkaValidationException.class);
	 * vendorCategoryAttributeService.saveVendorCategoryAttribute(
	 * vendorCategoryAttribute); }
	 */

	@Test
	public void testSaveVendorCategoryAttributeException9() throws ArkaValidationException {
		Mockito.doReturn(2L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		Mockito.doNothing().when(vendorCategoryAttributeRepoMock).insert(vendorCategoryAttribute);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testSaveVendorCategoryAttribute() throws ArkaValidationException {

		Mockito.doNothing().when(vendorCategoryAttributeRepoMock).insert(vendorCategoryAttribute);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorCategoryAttribute);
	}

	@Test
	public void testUpdateVendorCategoryAttributeException() throws ArkaValidationException {
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService.updateVendorCategoryAttribute(null);
	}

	@Test
	public void testUpdateVendorCategoryAttribute() throws ArkaValidationException {
		Mockito.doReturn(true).when(vendorCategoryAttributeRepoMock).updateOne(vendorCategoryAttribute);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		boolean isUpdated = vendorCategoryAttributeService.updateVendorCategoryAttribute(vendorCategoryAttribute);
		Assert.assertEquals(true, isUpdated);

	}

	@Test
	public void testUpdateVendorCategoryAttribute2() throws ArkaValidationException {
		Mockito.doReturn(false).when(vendorCategoryAttributeRepoMock).updateOne(vendorCategoryAttribute);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		boolean isUpdated = vendorCategoryAttributeService.updateVendorCategoryAttribute(vendorCategoryAttribute);
		Assert.assertEquals(false, isUpdated);

	}

	@Test
	public void testFindVendorCategoryAttributeById() {
		getVendorMotorRestrictionById();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		VendorCategoryAttribute va = vendorCategoryAttributeService
				.findVendorCategoryAttributeById(objectId.toHexString()).toCompletableFuture().join();
		Assert.assertNotNull(va);
		Assert.assertEquals(va.getId(), objectId.toHexString());
		vendorCategoryAttributeService.findVendorCategoryAttributeById(objectId.toHexString());
	}

	@Test
	public void testFindVendorCategoryAttributeByIdWithNull() {
		getVendorCategoryAttrByIdWithNull();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		VendorCategoryAttribute va = vendorCategoryAttributeService
				.findVendorCategoryAttributeById(objectId.toHexString()).toCompletableFuture().join();
		Assert.assertNull(va);
	}

	@Test
	public void testFindVendorCategoryAttributeByIdWithException() {
		getVendorCategoryAttrByIdWithException();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService
				.findVendorCategoryAttributeById(objectId.toHexString()).toCompletableFuture().join();
		
	}

	@Test
	public void testFindVendorRestrictions() {
		getOrElse(vendorCategoryAttribute);
		Collection<VendorCategoryAttribute> cvr = vendorCategoryAttributeService
				.findVendorCategoryAttributes(Collections.emptyMap()).toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictionsWithNull() {
		getAllVendorCategoryAttrWithNull();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Collection<VendorCategoryAttribute> cvr = vendorCategoryAttributeService
				.findVendorCategoryAttributes(Collections.emptyMap()).toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictionsWithException() {
		getAllVendorCategoryAttrWithException();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		expectedException.expect(ArkaValidationException.class);
		vendorCategoryAttributeService
				.findVendorCategoryAttributes(Collections.emptyMap()).toCompletableFuture().join();
		
	}

	@Test
	public void testFindVendorRestrictions2() {
		getOrElse(vendorCategoryAttribute);
		Collection<VendorCategoryAttribute> cvr = vendorCategoryAttributeService.findVendorCategoryAttributes(null)
				.toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictions3() {
		getOrElse(vendorCategoryAttribute);
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		Collection<VendorCategoryAttribute> cvr = vendorCategoryAttributeService
				.findVendorCategoryAttributes(criteriaMap).toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testDeleteVendorRestrictionById() throws ArkaValidationException {
		Mockito.doReturn(true).when(vendorCategoryAttributeRepoMock).deleteOneById(Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorCategoryAttributeService.deleteVendorCategoryAttributeById(objectId.toHexString());
		Assert.assertEquals(true, isDeleted);
	}

	@Test
	public void testDeleteVendorRestrictionById2() throws ArkaValidationException {
		Mockito.doReturn(false).when(vendorCategoryAttributeRepoMock).deleteOneById(Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorCategoryAttributeService.deleteVendorCategoryAttributeById(null);
		Assert.assertEquals(false, isDeleted);
	}

	@Test
	public void testDeleteVendorRestrictions() throws ArkaValidationException {
		Mockito.doReturn(true).when(vendorCategoryAttributeRepoMock).deleteMany(Matchers.anyMap());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorCategoryAttributeService.deleteVendorCategoryAttributes(Collections.emptyMap());
		Assert.assertEquals(true, isDeleted);
	}

	@Test
	public void testDeleteVendorRestrictions2() throws ArkaValidationException {
		Mockito.doReturn(false).when(vendorCategoryAttributeRepoMock).deleteMany(Matchers.anyMap());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorCategoryAttributeService.deleteVendorCategoryAttributes(null);
		Assert.assertEquals(false, isDeleted);
	}

	@Test
	public void testdeleteTagFromVendorCategoryAttribute() throws ArkaValidationException {
		Mockito.doReturn(true).when(vendorCategoryAttributeRepoMock).deleteFromArray(Matchers.anyString(),
				Matchers.anyString(), Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorCategoryAttributeService.deleteTagFromVendorCategoryAttribute(objectId.toHexString(),
				tagId.toHexString());
		Assert.assertEquals(true, isDeleted);
	}

	@Test
	public void testdeleteTagFromVendorCategoryAttribute2() throws ArkaValidationException {
		Mockito.doReturn(false).when(vendorCategoryAttributeRepoMock).deleteFromArray(Matchers.anyString(),
				Matchers.anyString(), Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorCategoryAttributeService.deleteTagFromVendorCategoryAttribute(objectId.toHexString(),
				tagId.toHexString());
		Assert.assertEquals(false, isDeleted);
	}

	@Test
	public void testgetVendorCategoryAttributeCount() {
		Mockito.doReturn(1L).when(vendorCategoryAttributeRepoMock).count();
		long count = vendorCategoryAttributeService.getVendorCategoryAttributeCount();
		Assert.assertEquals(1L, count);
	}

	@Test
	public void testgetVendorCategoryAttributeCount2() throws ArkaValidationException {
		Mockito.doReturn(1L).when(vendorCategoryAttributeRepoMock).count(Matchers.anyMap());
		long count = vendorCategoryAttributeService.getVendorCategoryAttributeCount(Collections.emptyMap());
		Assert.assertEquals(1L, count);
	}

	public void getOrElse(VendorCategoryAttribute vendorCategoryAttr) {
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(vendorCategoryAttr))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
	}

	public void inValidateCacheWithPrefix() {
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
	}

	private void getVendorCategoryAttrByIdWithNull() {
		Mockito.doReturn(null).when(vendorCategoryAttributeRepoMock).findOneById(Matchers.anyString());
	}

	private void getVendorCategoryAttrByIdWithException() {
		Mockito.doThrow(ArkaValidationException.class).when(vendorCategoryAttributeRepoMock)
				.findOneById(Matchers.anyString());
	}

	private void getAllVendorCategoryAttrWithNull() {
		Mockito.doReturn(null).when(vendorCategoryAttributeRepoMock).findAll(Matchers.anyMap());
	}

	private void getAllVendorCategoryAttrWithException() {
		Mockito.doThrow(ArkaValidationException.class).when(vendorCategoryAttributeRepoMock).findAll(Matchers.anyMap());
	}

	private void getVendorMotorRestrictionById() {
		Mockito.doReturn(vendorCategoryAttribute).when(vendorCategoryAttributeRepoMock)
				.findOneById(objectId.toHexString());
	}
}
