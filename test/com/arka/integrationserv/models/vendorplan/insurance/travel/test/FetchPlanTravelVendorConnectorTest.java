package com.arka.integrationserv.models.vendorplan.insurance.travel.test;

import java.lang.reflect.InvocationTargetException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.travel.constants.TravelAttributeCode;
import com.arka.integrationserv.models.vendorplan.insurance.travel.TravelVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;

public class FetchPlanTravelVendorConnectorTest extends BaseTravelVendorConnectorTest {

	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());

	}

	@Test
	public void fetchPlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (TravelVendorConnector tarvelConnector : tarvelVendorConnectorList) {
			VendorResponse vendorResponse = tarvelConnector.fetchPlanFromVendor(vendorRequest);
		}

	}

	@Test
	public void fetchPlanWithNoCategory() {
		Mockito.doReturn(null).when(dbUtilMock).getCatgeoryAttributeFieldNameMap(Matchers.anyString(),
				Matchers.anyString(), Matchers.anyBoolean());
		VendorResponse vendorResponse = tarvelVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
/*		processErrorMsg(enquiryQuoteRes.formatResponse(), VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(),
				FetchPlanMessage.CATEGORY_ATTRIBUTES_DOES_NOT_EXISTS.value());*/

	}

	@Test
	public void fetchPlanWithProductDoc() {
		Mockito.doReturn(getProduct("abcd")).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = tarvelVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		

	}

	@Test
	public void fetchPlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = tarvelVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
/*		processErrorMsg(enquiryQuoteRes.formatResponse(), VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());*/

	}

	@Test
	public void fetchPlanWithInsuarnceMedicalCover() {
		vendorRequest.removeInputQuoteParam(TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode());
		VendorResponse vendorResponse = tarvelVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
/*		processErrorMsg(enquiryQuoteRes.formatResponse(), TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode(),
				TravelInsuranceMessage.ATR_MEDICAL_COVER_MANDATORY.value());*/

	}

	@Test
	public void fetchPlanWithInsuarnceMedicalCurrency() {
		vendorRequest.removeInputQuoteParam(TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode());
		VendorResponse vendorResponse = tarvelVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		/*processErrorMsg(enquiryQuoteRes.formatResponse(), TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode(),
				TravelInsuranceMessage.ATR_MEDICAL_COVER_CURRENCY_MANDATORY.value());*/

	}

	@Test
	public void fetchPlanWithInsuarnceType() {
		vendorRequest.removeInputQuoteParam(TravelAttributeCode.ATR_TRAVEL_INSURANCE_TYPE.getCode());
		VendorResponse vendorResponse = tarvelVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
/*		processErrorMsg(enquiryQuoteRes.formatResponse(), TravelAttributeCode.ATR_TRAVEL_INSURANCE_TYPE.getCode(),
				TravelInsuranceMessage.ATR_TRAVEL_INSURANCE_TYPE_MANDATORY.value());*/

	}

}
