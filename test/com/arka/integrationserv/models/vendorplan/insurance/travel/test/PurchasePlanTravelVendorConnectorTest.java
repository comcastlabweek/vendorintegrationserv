package com.arka.integrationserv.models.vendorplan.insurance.travel.test;

import java.lang.reflect.InvocationTargetException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.travel.TravelVendorConnector;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class PurchasePlanTravelVendorConnectorTest extends BaseTravelVendorConnectorTest {
	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());

	}

	@Test
	public void purchasePlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (TravelVendorConnector tarvelConnector : tarvelVendorConnectorList) {
			ObjectNode policyJson = tarvelConnector.purchasePlan(getProposalJson());
		}

	}

	@Test
	public void purchasePlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = tarvelVendorConnectorList.get(0).purchasePlan(getProposalJson());
		processErrorMsg(policyJson, VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}

}
