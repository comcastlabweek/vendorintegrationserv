package com.arka.integrationserv.models.vendorplan.insurance.travel.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryFields;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.travel.constants.TravelAttributeCode;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.insurance.travel.TravelVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class BaseTravelVendorConnectorTest {

	protected Random random = new Random();

	@Mock
	public VendorProdMgmtServ vendorProdMgmtServMock;

	@Mock
	public DBUtil dbUtilMock;

	@Mock
	public PropUtils propUtilsMock;

	List<TravelVendorConnector> tarvelVendorConnectorList = new ArrayList<TravelVendorConnector>();

	VendorRequest vendorRequest = new VendorRequest();

	@Before
	public void doBeforeEachTest() {
		MockitoAnnotations.initMocks(this);
		initTravelConnectors();
		initEnquiryQuoteRequest();

	}

	private void initEnquiryQuoteRequest() {
		vendorRequest = new  VendorRequest();
		vendorRequest.setEnquiryId("EqTraX5StRb4FiQjWzL90g");
		vendorRequest.setProductId(getMongoDBId());
		vendorRequest.setCategoryId(getMongoDBId());
		vendorRequest.setVendorId("EqTraX5StRb4FiQjWzL90g");
		vendorRequest.setVendorName("vendorName");
		vendorRequest.setInput_quote_params(getInputParamsLst());

	}

	private void initTravelConnectors() {
//		tarvelVendorConnectorList.add(bajajAllianaceTravelCareInsuranceConnector);
//		tarvelVendorConnectorList.add(bajajAllianceTravelInsuranceConnector);
//		tarvelVendorConnectorList.add(bajajAllianzTravelEliteSilverConnector);
//		tarvelVendorConnectorList.add(bhartiAXASmartTravellerPlanConnector);
//		tarvelVendorConnectorList.add(relianceTravelInsuranceConnector);
//		tarvelVendorConnectorList.add(religareExplorePlatinumInsuranceConnector);
//		tarvelVendorConnectorList.add(hdfcErgoTravelInsuranceConnector);
//		tarvelVendorConnectorList.add(apolloMunichTravelInsuranceConnector);
	}

	private List<InputPolicyParams> getInputParamsLst() {

		List<InputPolicyParams> inputParams = new ArrayList<InputPolicyParams>();
		inputParams.add(
				getInputParam(TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode(), "50000", "", new String[] {}));
		inputParams.add(
				getInputParam(TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode(), "$", "", new String[] {}));
		inputParams.add(
				getInputParam(TravelAttributeCode.ATR_TRAVEL_INSURANCE_TYPE.getCode(), "Family", "", new String[] {}));
		inputParams.add(
				getInputParam(TravelAttributeCode.ATR_SELF_AGE.getCode(), "34", "", new String[] { "self", "age" }));
		inputParams.add(getInputParam(TravelAttributeCode.ATR_SPOUSE_AGE.getCode(), "23", "",
				new String[] { "spouse", "age" }));
		inputParams.add(getInputParam(TravelAttributeCode.ATR_SPOUSE_DOB.getCode(), "13-05-1992", "",
				new String[] { "spouse" }));
		inputParams.add(
				getInputParam(TravelAttributeCode.ATR_SELF_DOB.getCode(), "14-09-1989", "", new String[] { "self" }));
		inputParams.add(getInputParam(TravelAttributeCode.ATR_SELF_DOB_ENTRY_DATE.getCode(), "17-02-2017", "",
				new String[] { "self" }));
		inputParams.add(getInputParam(TravelAttributeCode.ATR_SPOUSE_DOB_ENTRY_DATE.getCode(), "17-02-2017", "",
				new String[] { "spouse" }));
		return inputParams;

	}

	private InputPolicyParams getInputParam(String catAttr, String value, String additionalValue, String[] tags) {
		InputPolicyParams inputQuoteParams = new InputPolicyParams();
		inputQuoteParams.setValue(value);
		inputQuoteParams.setAdditionalValue(additionalValue);
		inputQuoteParams.setCategoryAttributeCode(catAttr);
		inputQuoteParams.setTag(Arrays.asList(tags));
		return inputQuoteParams;
	}

	private String getMongoDBId() {
		return RandomStringUtils.randomAlphanumeric(16);
	}

	public String getVendor() {
		return RandomStringUtils.randomAlphanumeric(6);
	}

	public Map<String, JsonNode> getCategoryAttributeIdMap() {
		HashMap<String, JsonNode> categoryAttributeFieldNameMap = new HashMap<String, JsonNode>();

		categoryAttributeFieldNameMap.put("57f5231308df09217caafYWF",
				getAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_COVER.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df09217caauYWF",
				getAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_CURRENCY.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df08217caauYWF",
				getAttribute(TravelAttributeCode.ATR_SELF_AGE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df07217caauYWF",
				getAttribute(TravelAttributeCode.ATR_TRAVEL_INSURANCE_TYPE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caauYWF",
				getAttribute(TravelAttributeCode.ATR_SPOUSE_AGE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(TravelAttributeCode.ATR_SELF_DOB.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caeuYWF",
				getAttribute(TravelAttributeCode.ATR_SPOUSE_DOB.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217cafuYWF",
				getAttribute(TravelAttributeCode.ATR_SELF_DOB_ENTRY_DATE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217cbauYWF",
				getAttribute(TravelAttributeCode.ATR_SPOUSE_DOB_ENTRY_DATE.getCode()));


		return categoryAttributeFieldNameMap;

	}

	private ObjectNode getAttribute(String categoryAttributeCode) {
		ObjectNode categoryAttributeJson = Json.newObject();

		categoryAttributeJson.put("_id", "57f5231308df09217caaf" + RandomStringUtils.randomAlphabetic(3));
		categoryAttributeJson.put("code", categoryAttributeCode);
		categoryAttributeJson.put("name", categoryAttributeCode.toLowerCase());
		categoryAttributeJson.put("mandatory", false);
		categoryAttributeJson.put("default_value", "");
		categoryAttributeJson.put("restriction", "");
		categoryAttributeJson.put("restriction_id", "");
		categoryAttributeJson.put("data_type", "STRING");
		categoryAttributeJson.put("usage_type", "I");
		categoryAttributeJson.put("data_entry_type", "CONSUMER");
		categoryAttributeJson.put("max_occurrence", "1");
		categoryAttributeJson.put("category_id", "57f5231308df09217caaf809");
		
		String tags= categoryAttributeCode.split("ATR-")[1];
		tags=tags.toLowerCase();
		
		if (categoryAttributeCode.contains("SELF")|| categoryAttributeCode.contains("SPOUSE")|| categoryAttributeCode.contains("CURRENCY")) {
			ArrayNode tagArray = categoryAttributeJson.putArray(CategoryFields.CATEGORY_TAGS.value());
			ObjectNode tagJson = tagArray.addObject();
			if (categoryAttributeCode.contains("AGE")) {

				tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "age");
			}
			if (categoryAttributeCode.contains("DOB")) {

				ObjectNode pricetagJson = tagArray.addObject();
				pricetagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "dob");
			}
			if(categoryAttributeCode.contains("CURRENCY")) {
				tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "currency");
			}
			

		}
		
		

		return categoryAttributeJson;

	}

	protected ObjectNode getProduct(String productCode) {
		
		
		ObjectNode productJson = Json.newObject();
		productJson.put("category_id", "57f5231308df09217caaf809");
		productJson.put("vendor_id", random.nextInt(10));
		productJson.put("_id", "57f5231308df09217caaf" + RandomStringUtils.randomAlphabetic(3));
		productJson.put("code", productCode);
		productJson.put("name", productCode.toLowerCase());
		productJson.put("image_url", "http://cdn/product/image1");
		
		if(productCode.length()%2==0)
		{
			productJson.put("productDocUrl", "http://cdn/product/image1/doc");
		}

		return productJson;
	}

	protected JsonNode getProposalJson() {
		ObjectNode proposalJson = Json.newObject();
		proposalJson.put(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), 1L);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_USER_ID.value(), 2L);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_ID.value(), 3L);		
		

		return proposalJson;
	}

	
	
	protected void processErrorMsg(JsonNode errorJson, String errorKey, String errorMsg) {
		assertTrue(errorJson.get(JsonUtils.ERROR).get(errorKey).asText().contains(errorMsg));

	}
	
}
