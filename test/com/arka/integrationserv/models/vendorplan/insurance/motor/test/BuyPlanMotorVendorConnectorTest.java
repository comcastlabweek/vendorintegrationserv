package com.arka.integrationserv.models.vendorplan.insurance.motor.test;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.motor.MotorVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;

public class BuyPlanMotorVendorConnectorTest extends BaseMotorVendorConnectorTest {
	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());
		Mockito.doReturn(new HashMap<String, List<String>>()).when(mandatoryParams).getMandatoryQueryParameter();
		Mockito.doReturn(new HashMap<String, List<String>>()).when(mandatoryParams)
				.getMandatoryHeaderFromArgs(Matchers.any(Set.class));
		Mockito.doReturn(CompletableFuture.completedFuture(getVehicleJson())).when(vendorProdMgmtServMock)
				.getMotor(Matchers.anyString(), Matchers.anyMap(), Matchers.anyMap());

	}

	@Test
	public void buyPlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (MotorVendorConnector motorConnector : motorVendorConnectorList) {
			VendorResponse vendorResponse = motorConnector.buyPlanFromVendor(vendorRequest);
		}

	}
	
	@Test
	public void buyPlanForAllVendorsHasPreviouslyExpiredPolicy() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {
		vendorRequest.getInput_quote_params().add(getInputParam(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(), "2017-07-01", "", new String[] {}));
		for (MotorVendorConnector motorConnector : motorVendorConnectorList) {
			VendorResponse vendorResponse = motorConnector.buyPlanFromVendor(vendorRequest);
		}

	}
	
	@Test
	public void buyPlanForAllVendorsExpiredPolicyBefore90Days() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {
		vendorRequest.getInput_quote_params().add(getInputParam(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(), "2017-01-01", "", new String[] {}));
		for (MotorVendorConnector motorConnector : motorVendorConnectorList) {
			VendorResponse vendorResponse = motorConnector.buyPlanFromVendor(vendorRequest);
		}

	}

	@Test
	public void buyPlanWithProductDoc() {
		Mockito.doReturn(getProduct("abcd")).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
	}

	@Test
	public void buyPlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}

	@Test
	public void buyPlanWithNoVehicle() {
		Mockito.doThrow(new RuntimeException("test", new IllegalArgumentException("abc"))).when(vendorProdMgmtServMock)
				.getMotor(Matchers.anyString(), Matchers.anyMap(), Matchers.anyMap());
		expectedException.expect(RuntimeException.class);
		motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);

	}

	@Test
	public void buyPlanWithAllCovers() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		setupInput_quote_params(InputPolicyParams,true,true,true,false);
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull("error"));

	}
	@Test
	public void isPassengerCoverSelected() {
		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		setupInput_quote_params(InputPolicyParams,true,true,true,true);
		InputPolicyParams.add(getInputParam("AC-COVER-PASSENGER_COVER", "false", "", new String[] {}));
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull("error"));

	}
	
	@Test
	public void testPassengerCoverSelectedMissingCase() {
		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		setupInput_quote_params(InputPolicyParams,true,null,true,true);
		InputPolicyParams.add(getInputParam("AC-COVER-PASSENGER_COVER", "false", "", new String[] {}));
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull("error"));

	}
	
	@Test
	public void hasPassengerCoverVal() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		setupInput_quote_params(InputPolicyParams,false,true,true,true);
		InputPolicyParams.remove(CarAttributeCode.AC_COVER_PASSENGER_COVER_VAL.getCode());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull("error"));

	}
	
	@Test
	public void hasPLpgFitValVal() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		setupInput_quote_params(InputPolicyParams,true,true,false,false);
		InputPolicyParams.remove(CarAttributeCode.AC_COVER_PASSENGER_COVER_VAL.getCode());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertTrue(vendorResponse.getErrorJson().hasNonNull("error"));

	}

	@Test
	public void buyPlanWithoutCovers() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();

		InputPolicyParams.add(getInputParam("AC-DATE_OF_REGISTRATION", "2017-02-01", "", new String[] {}));

		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull("error"));

	}
	
	@Test
	public void buyPlanInvalidRegDate() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();

		InputPolicyParams.add(getInputParam("AC-DATE_OF_REGISTRATION", "02-1899", "", new String[] {}));

		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertTrue(vendorResponse.getErrorJson().hasNonNull("error"));

	}
	
	@Test
	public void buyPlanIsCategoryAttributeIdMapNull() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		InputPolicyParams.add(getInputParam("AC-DATE_OF_REGISTRATION", "2017-02-01", "", new String[] {}));
		Mockito.doReturn(null).when(dbUtilMock)
		.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());


		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertTrue(vendorResponse.getErrorJson().get("error").get("categoryId").asText().equals("category.attributes.does.not.exists"));

	}
	
	@Test
	public void buyPlanIsCategoryAttributeIdMaEmpty() {

		List<InputPolicyParams> InputPolicyParams = vendorRequest.getInput_quote_params();
		
		InputPolicyParams.add(getInputParam("AC-DATE_OF_REGISTRATION", "2017-02-01", "", new String[] {}));
		Mockito.doReturn(Collections.emptyMap()).when(dbUtilMock)
		.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());


		VendorResponse vendorResponse = motorVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		Assert.assertTrue(vendorResponse.getErrorJson().get("error").get("categoryId").asText().equals("category.attributes.does.not.exists"));

	}

	
	
	private void setupInput_quote_params(List<InputPolicyParams> InputPolicyParams,boolean hasPassengersCoverValue,
			Boolean isPassengersCoverd,boolean hasLPGValue,Boolean isPrevPolicyExpired){
		
		InputPolicyParams.add(getInputParam("AC-COVER-INVOICE_COVER", "true", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-COVER-ENGINE_PROTECTOR", "true", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-COVER-NCB_PROTECTION", "true", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-COVER-ZERO_DEPRICIATION", "true", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-COVER-24X7_ROADSIDE_ASSISTANCE", "true", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-COVER-DRIVER_COVER", "true", "", new String[] {}));
		
		if(isPassengersCoverd!=null){
			InputPolicyParams.add(getInputParam("AC-COVER-PASSENGER_COVER", isPassengersCoverd.toString(), "", new String[] {}));
		}
		InputPolicyParams.add(getInputParam("AC-PRODUCT_DRIVER_COVER", "3000", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-PRODUCT_PASSENGER_COVER", "4000", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-PRODUCT_ZERO_DEPRECIATION", "2000", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-PRODUCT_NCB_PROTECTION", "1290", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-PRODUCT_INVOICE_COVER", "2900", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-PRODUCT_24X7_ROADSIDE_ASSISTANCE", "2908", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-PRODUCT_ENGINE_PROTECTOR", "2000", "", new String[] {}));
		InputPolicyParams.add(getInputParam("AC-DATE_OF_REGISTRATION", "2017-02-01", "", new String[] {}));
		//if(true){
			InputPolicyParams.add(getInputParam(CarAttributeCode.AC_PREVIOUS_POLICY_EXPIRED_ON.getCode(), "2017-07-01", "", new String[] {}));
		//}
		
		InputPolicyParams.add(getInputParam(CarAttributeCode.AC_IS_CNG_OR_LPG_EXTERNALLY_FITTED.getCode(), "true", "",
				new String[] {}));
		if(hasLPGValue){
		InputPolicyParams.add(getInputParam(CarAttributeCode.AC_CNG_OR_LPG_EXTERNALLY_FITTED_VALUE.getCode(), "2800", "",
				new String[] {}));
		}
		InputPolicyParams.add(getInputParam(CarAttributeCode.AC_COVER_ELECTRICAL_ACCESSORIES_VALUE.getCode(), "1800", "",
				new String[] {}));
		InputPolicyParams.add(getInputParam(CarAttributeCode.AC_COVER_NON_ELECTRICAL_ACCESSORIES_VALUE.getCode(), "1200",
				"", new String[] {}));
		if(hasPassengersCoverValue){
		InputPolicyParams.add(
				getInputParam(CarAttributeCode.AC_COVER_PASSENGER_COVER_VAL.getCode(), "2000", "", new String[] {}));
		}
		InputPolicyParams.add(
				getInputParam(CarAttributeCode.AC_VOLUNTARY_DEDUCTIBLE_AMOUNT.getCode(), "200", "", new String[] {}));
	}
}
