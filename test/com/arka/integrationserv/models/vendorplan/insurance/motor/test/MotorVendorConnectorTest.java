package com.arka.integrationserv.models.vendorplan.insurance.motor.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.integrationserv.models.vendorplan.VendorConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.MotorVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.test.MockBase;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Http.RequestBuilder;

public class MotorVendorConnectorTest extends MockBase {

	private static final Logger LOG = LoggerFactory.getLogger(MotorVendorConnectorTest.class);
	
	@InjectMocks
	VendorConnector vendorConnector = new MotorVendorConnector();
	private VendorRequest vendorRequest;

	@Before
	public void doBeforeEachTest() {
		MockitoAnnotations.initMocks(this);
		createModel();
		RequestBuilder rb = new RequestBuilder();
		play.mvc.Http.Context context = new play.mvc.Http.Context(rb);
		play.mvc.Http.Context.current.set(context);
	}

	private void createModel() {
		vendorRequest = new VendorRequest();
		vendorRequest.setEnquiryId(id);
		vendorRequest.setProductId(objectId.toHexString());
		vendorRequest.setCategoryId(categoryId.toString());
		vendorRequest.setVendorId(vendorId);
		vendorRequest.setVendorName("vendorName");
		vendorRequest.setEnquiryId(id);
		vendorRequest.setInput_quote_params(getInputParamsLst(true, 100));

	}

	private List<InputPolicyParams> getInputParamsLst(boolean b, Integer value) {

		List<InputPolicyParams> ls = new ArrayList<>();
		ls.add(getInputParams(CarAttributeCode.AC_COVER_DRIVER_COVER.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_ELECTRICAL_ACCESSORIES_VALUE.getCode(), value, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_ENGINE_PROTECTOR.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_NCB_PROTECTION.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_ZERO_DEPRICIATION.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_24_7_ROADSIDE_ASSISTANCE.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_INVOICE_COVER.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_PASSENGER_COVER.getCode(), value, null));
		ls.add(getInputParams(CarAttributeCode.AC_PRODUCT_BASE_PRICE.getCode(), value, null));
		ls.add(getInputParams(CarAttributeCode.AC_IS_CNG_OR_LPG_EXTERNALLY_FITTED.getCode(), b, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_PASSENGER_COVER_VAL.getCode(), value, null));
		ls.add(getInputParams(CarAttributeCode.AC_VOLUNTARY_DEDUCTIBLE_AMOUNT.getCode(), value, null));
		ls.add(getInputParams(CarAttributeCode.AC_COVER_NON_ELECTRICAL_ACCESSORIES_VALUE.getCode(), value, null));
		ls.add(getInputParams(CarAttributeCode.AC_PRODUCT_BASE_PRICE.getCode(), value, null));

		ls.add(getInputParams("AC-SERVICE_TAX", value, null));
		ls.add(getInputParams("AC-PRODUCT_DISCOUNTS", value, null));
		ls.add(getInputParams("AC-PRODUCT_PREMIUM", value, null));

		ls.add(getInputParams("AC-PRODUCT_ADDITIONAL_COVERS", value, null));

		return ls;

	}

	private InputPolicyParams getInputParams(String catAttr, Object val, String tags) {
		InputPolicyParams inputQuoteParams = new InputPolicyParams();
		inputQuoteParams.setValue(val.toString());
		inputQuoteParams.setAdditionalValue("additionalValue");
		inputQuoteParams.setCategoryAttributeCode(catAttr);
		inputQuoteParams.setTag(Arrays.asList(tags));
		return inputQuoteParams;
	}

	private JsonNode getProposal() {
		ObjectNode proposalJson = Json.newObject();
		proposalJson.put(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), objectId.toString());
		proposalJson.put(VendorQuoteConstants.PROPOSAL_USER_ID.value(), createdUser);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_ID.value(), proposalId);
		return proposalJson;
	}

	private JsonNode getProduct(String pCategoryId, String pVendorId, String pCode, String pId) {
		ObjectNode productJson = Json.newObject();
		productJson.put(ProductFields.PRODUCT_CATEGORY_ID.value(), pCategoryId);
		productJson.put(ProductFields.PRODUCT_VENDOR_ID.value(), pVendorId);
		productJson.put(ProductFields.PRODUCT_NAME.value(), productName);
		productJson.put(ProductFields.PRODUCT_CODE.value(), pCode);
		productJson.put(ProductFields.PRODUCT_IMAGE_URL.value(), productImgUrl);
		productJson.put(ProductFields.PRODUCT_DOCUMENT_URL.value(), productDocUrl);
		productJson.put(ProductFields.PRODUCT_ID.value(), pId);
		productJson.set(ProductFields.PRODUCT_ATTRIBUTES.value(), getProductAttributes());

		return productJson;
	}

	private JsonNode getProductAttributes() {
		ArrayNode newArray = Json.newArray();
		newArray.add(Json.newObject().put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), 1)
				.put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), objectId.toHexString()));
		return newArray;
	}

	private Map<String, JsonNode> getCatgeoryAttributeFieldNameMapJson() {
		Map<String, JsonNode> map = new HashMap<>();
		ObjectNode json = Json.newObject();
		json.put(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value(),
				HealthAttributeCode.AH_AMBULANCE_COVER.getCode());

		json.set(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAGS.value(), tags());
		map.put(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value(), json);
		map.put(objectId.toHexString(), json);
		return map;
	}

	private JsonNode tags() {
		ArrayNode newArray = Json.newArray();
		newArray.add(Json.newObject().put(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAG_NAME.value(), "CAR"));

		return newArray;
	}

	@Test
	public void purchasePlan() {
		// mockGetCategoryAttributeList();
		mockGetProductJson(getProduct(categoryId.toString(), vendorId.toString(), productCode, objectId.toHexString()));
		ObjectNode purchasePlan = vendorConnector.purchasePlan(getProposal());

		Assert.assertEquals(categoryId.toString(), purchasePlan.get("categoryId").asText());
		Assert.assertEquals(vendorId.toString(), purchasePlan.get("vendorId").asText());
		Assert.assertEquals(objectId.toHexString(), purchasePlan.get("productId").asText());
		Assert.assertTrue( purchasePlan.hasNonNull("policyNumber"));
	}

	@Test
	public void fetchPlanFromVendor() {
		mockGetCatgeoryAttributeFieldNameMap(getCatgeoryAttributeFieldNameMapJson());
		mockGetProductJson(getProduct(categoryId.toString(), vendorId.toString(), productCode, objectId.toHexString()));
		VendorResponse vendorResponse = vendorConnector.fetchPlanFromVendor(vendorRequest);
		Assert.assertEquals(objectId.toHexString(), vendorResponse.getProductId());
		Assert.assertEquals(vendorId, vendorResponse.getVendorId());
		Assert.assertEquals(categoryId.toString(), vendorResponse.getCategoryId());
		Assert.assertEquals(vendorRequest.getEnquiryId(), vendorResponse.getEnquiryId());
	}

	@Test
	public void fetchPlanFromVendorProductEmpty() {
		mockGetCatgeoryAttributeFieldNameMap(getCatgeoryAttributeFieldNameMapJson());
		mockGetProductJson(Json.newObject());
		VendorResponse vendorResponse = vendorConnector.fetchPlanFromVendor(vendorRequest);

		Assert.assertNotNull(vendorResponse.getErrorJson());
		Assert.assertTrue(vendorResponse.getErrorJson().hasNonNull("error"));
		
		Assert.assertTrue(
				vendorResponse.getErrorJson().get("error").hasNonNull("AC-DATE_OF_REGISTRATION"));
		Assert.assertEquals("DATE OF REGISTRATION MANDATORY FOR FETCHING QUOTE",
				vendorResponse.getErrorJson().get("error").get("AC-DATE_OF_REGISTRATION").asText().trim());
		Assert.assertEquals(vendorRequest.getProductId(), vendorResponse.getProductId());
	}

	@Test
	public void fetchPlanFromVendorProductNull() {
		mockGetCatgeoryAttributeFieldNameMap(getCatgeoryAttributeFieldNameMapJson());
		mockGetProductJson(null);
		// expectedException.expect(NullPointerException.class);
		VendorResponse vendorResponse = vendorConnector.fetchPlanFromVendor(vendorRequest);

		Assert.assertNotNull(vendorResponse.getErrorJson());
		Assert.assertTrue(vendorResponse.getErrorJson().hasNonNull("error"));
		
		Assert.assertTrue(
				vendorResponse.getErrorJson().get("error").hasNonNull("AC-DATE_OF_REGISTRATION"));
		Assert.assertEquals("DATE OF REGISTRATION MANDATORY FOR FETCHING QUOTE",
				vendorResponse.getErrorJson().get("error").get("AC-DATE_OF_REGISTRATION").asText().trim());
		Assert.assertEquals(vendorRequest.getProductId(), vendorResponse.getProductId());

	}

	@Test
	public void fetchPlanFromVendorCatgeoryAttributeNull() {
		mockGetCatgeoryAttributeFieldNameMap(null);
		VendorResponse vendorResponse = vendorConnector.fetchPlanFromVendor(vendorRequest);
		Assert.assertNotNull(vendorResponse.getErrorJson());
		Assert.assertTrue(vendorResponse.getErrorJson().hasNonNull("error"));
		Assert.assertTrue(vendorResponse.getErrorJson().get("error").hasNonNull("categoryId"));
		Assert.assertEquals("category.attributes.does.not.exists",
				vendorResponse.getErrorJson().get("error").get("categoryId").asText());
		Assert.assertEquals(vendorRequest.getProductId(), vendorResponse.getProductId());

	}

	@Test
	public void fetchPlanFromVendorCatgeoryAttributeEmpty() {
		mockGetCatgeoryAttributeFieldNameMap(Collections.emptyMap());
		VendorResponse vendorResponse = vendorConnector.fetchPlanFromVendor(vendorRequest);
		Assert.assertNotNull(vendorResponse.getErrorJson());
		Assert.assertTrue(vendorResponse.getErrorJson().hasNonNull("error"));
		Assert.assertTrue(vendorResponse.getErrorJson().get("error").hasNonNull("categoryId"));
		Assert.assertEquals("category.attributes.does.not.exists",
				vendorResponse.getErrorJson().get("error").get("categoryId").asText());
		Assert.assertEquals(vendorRequest.getProductId(), vendorResponse.getProductId());

	}

	/*
	 * @Test public void buyPlanFromVendor() {
	 * mockGetCatgeoryAttributeFieldNameMap(getCatgeoryAttributeFieldNameMapJson
	 * ()); mockGetProductJson(getProduct(categoryId.toString(),
	 * vendorId.toString(), productCode, objectId.toHexString()));
	 * VendorResponse vendorResponse =
	 * vendorConnector.buyPlanFromVendor(vendorRequest);
	 * Assert.assertEquals(objectId.toHexString(),
	 * vendorResponse.getProductId()); Assert.assertEquals(vendorId,
	 * vendorResponse.getVendorId());
	 * Assert.assertEquals(categoryId.toString(),
	 * vendorResponse.getCategoryId());
	 * Assert.assertEquals(vendorRequest.getEnquiryId(),
	 * vendorResponse.getEnquiryId());
	 * Assert.assertTrue(vendorResponse.getAttributeValueList().size() > 0); }
	 */
	@Test
	public void buyPlanFromVendorInputParamsFalse() {
		mockGetCatgeoryAttributeFieldNameMap(getCatgeoryAttributeFieldNameMapJson());
		mockGetProductJson(getProduct(categoryId.toString(), vendorId.toString(), productCode, objectId.toHexString()));
		vendorRequest.setInput_quote_params(new ArrayList());
		VendorResponse vendorResponse = vendorConnector.buyPlanFromVendor(vendorRequest);
		Assert.assertEquals(objectId.toHexString(), vendorResponse.getProductId());
		Assert.assertEquals(vendorId, vendorResponse.getVendorId());
		Assert.assertEquals(categoryId.toString(), vendorResponse.getCategoryId());
		Assert.assertEquals(vendorRequest.getEnquiryId(), vendorResponse.getEnquiryId());
		LOG.debug("msd:"+vendorResponse.getAttributeValueList());
		//Assert.assertTrue(vendorResponse.getAttributeValueList().size() > 0);
	}

	@Test
	public void buyPlanFromVendor3() {
		mockGetCatgeoryAttributeFieldNameMap(getCatgeoryAttributeFieldNameMapJson());
		mockGetProductJson(getProduct(categoryId.toString(), vendorId.toString(), productCode, objectId.toHexString()));
		vendorRequest.setInput_quote_params(new ArrayList());
		VendorResponse vendorResponse = vendorConnector.buyPlanFromVendor(vendorRequest);
		Assert.assertEquals(objectId.toHexString(), vendorResponse.getProductId());
		Assert.assertEquals(vendorId, vendorResponse.getVendorId());
		Assert.assertEquals(categoryId.toString(), vendorResponse.getCategoryId());
		Assert.assertEquals(vendorRequest.getEnquiryId(), vendorResponse.getEnquiryId());
		//Assert.assertTrue(vendorResponse.getAttributeValueList().size() > 0);
	}

	
}