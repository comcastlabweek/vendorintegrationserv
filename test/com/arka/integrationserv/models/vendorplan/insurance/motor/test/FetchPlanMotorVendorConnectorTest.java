package com.arka.integrationserv.models.vendorplan.insurance.motor.test;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.motor.MotorVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;

public class FetchPlanMotorVendorConnectorTest extends BaseMotorVendorConnectorTest {
	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());
		Mockito.doReturn(new HashMap<String, List<String>>()).when(mandatoryParams).getMandatoryQueryParameter();
		Mockito.doReturn(new HashMap<String, List<String>>()).when(mandatoryParams)
				.getMandatoryHeaderFromArgs(Matchers.any(Set.class));
		Mockito.doReturn(CompletableFuture.completedFuture(getVehicleJson())).when(vendorProdMgmtServMock).getMotor(Matchers.anyString(),
				Matchers.anyMap(), Matchers.anyMap());

	}

	@Test
	public void fetchPlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (MotorVendorConnector motorConnector : motorVendorConnectorList) {
			VendorResponse vendorResponse = motorConnector.fetchPlanFromVendor(vendorRequest);
		}

	}

	@Test
	public void fetchPlanWithNoCategory() {
		Mockito.doReturn(null).when(dbUtilMock).getCatgeoryAttributeFieldNameMap(Matchers.anyString(),
				Matchers.anyString(), Matchers.anyBoolean());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).fetchPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(),
				FetchPlanMessage.CATEGORY_ATTRIBUTES_DOES_NOT_EXISTS.value());

	}

	@Test
	public void fetchPlanWithProductDoc() {
		Mockito.doReturn(getProduct("abcd")).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).fetchPlanFromVendor(vendorRequest);
	}

	@Test
	public void fetchPlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = motorVendorConnectorList.get(0).fetchPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}


	
	@Test
	public void fetchPlanWithNoVehicle() {
		Mockito.doThrow(new RuntimeException("test", new IllegalArgumentException("abc"))).when(vendorProdMgmtServMock).getMotor(Matchers.anyString(),
				Matchers.anyMap(), Matchers.anyMap());
		expectedException.expect(RuntimeException.class);
		 motorVendorConnectorList.get(0).fetchPlanFromVendor(vendorRequest);
		
	}
	
}
