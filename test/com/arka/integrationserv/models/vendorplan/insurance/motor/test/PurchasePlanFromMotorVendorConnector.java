package com.arka.integrationserv.models.vendorplan.insurance.motor.test;

import java.lang.reflect.InvocationTargetException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.motor.MotorVendorConnector;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class PurchasePlanFromMotorVendorConnector extends BaseMotorVendorConnectorTest {
	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());

	}

	@Test
	public void purchasePlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (MotorVendorConnector motorConnector : motorVendorConnectorList) {
			ObjectNode policyJson = motorConnector.purchasePlan(getProposalJson());
		}

	}

	@Test
	public void purchasePlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = motorVendorConnectorList.get(0).purchasePlan(getProposalJson());
		processErrorMsg(policyJson, VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}
	
	@Test
	public void purchasePlanWithProductEmpty() {

		Mockito.doReturn(Json.newObject()).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = motorVendorConnectorList.get(0).purchasePlan(getProposalJson());
		processErrorMsg(policyJson, VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}
	
	@Test
	public void purchasePlanHasInvalidInputParams() {
		ObjectNode proposalJson = (ObjectNode)getProposalJson();
		proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray().add(Json.newObject()));
		
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = motorVendorConnectorList.get(0).purchasePlan(proposalJson);
		
	}
	@Test
	public void purchasePlanHasInputParams() {
		ObjectNode proposalJson = (ObjectNode)getProposalJson();
		proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray()
				.add(Json.newObject().put("categoryAttributeCode", "AC-PRODUCT_PREMIUM").put("value", "10")));
		proposalJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), "Test");
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = motorVendorConnectorList.get(0).purchasePlan(proposalJson);
		
	}
	@Test
	public void purchasePlanHasIssuedTimeInProposal() {
		ObjectNode proposalJson = (ObjectNode)getProposalJson();
		proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray()
				.add(Json.newObject().put("categoryAttributeCode", "AC-PRODUCT_PREMIUM").put("value", "10")));
		proposalJson.put(PolicyFields.ISSUED_TIME.getCode(), "2017-05-01");
		proposalJson.put(PolicyFields.COMMENCEMENT_DATE.getCode(), "2017-05-01");
		proposalJson.put(PolicyFields.EXPIRY_DATE.getCode(), "2017-05-01");
		
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = motorVendorConnectorList.get(0).purchasePlan(proposalJson);
		
	}
	
	@Test
	public void purchasePlanHasInputParamsWithoutPremium() {
		ObjectNode proposalJson = (ObjectNode)getProposalJson();
		proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray()
				.add(Json.newObject().put("categoryAttributeCode", "PRODUCT").put("value", "10")));
		
		
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = motorVendorConnectorList.get(0).purchasePlan(proposalJson);
		
	}
	
	

}
