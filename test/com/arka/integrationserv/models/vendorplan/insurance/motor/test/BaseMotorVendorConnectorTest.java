package com.arka.integrationserv.models.vendorplan.insurance.motor.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.services.VendorIntegrationServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.insurance.motor.BajajAllianzInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.HDFCERGOMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.IffcoTokioInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.LibertyVideoconMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.MotorVendorConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.RoyalSundaramMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.USGICMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.mvc.Http.RequestBuilder;

public class BaseMotorVendorConnectorTest {

	protected Random random = new Random();
	
	@InjectMocks
	public MotorVendorConnector iffcoTokioInsuranceConnector = new IffcoTokioInsuranceConnector();
	
	@InjectMocks
	public MotorVendorConnector bajajAllianzInsuranceConnector = new BajajAllianzInsuranceConnector();
	
	@InjectMocks
	public MotorVendorConnector royalSundaramMotorInsuranceConnector = new RoyalSundaramMotorInsuranceConnector();
	
	@InjectMocks
	public MotorVendorConnector usgicMotorInsuranceConnector = new USGICMotorInsuranceConnector();
	
	@InjectMocks
	public MotorVendorConnector hdfcERGOMotorInsuranceConnector = new HDFCERGOMotorInsuranceConnector();
	
	@InjectMocks
	public MotorVendorConnector libertyVideoconMotorInsuranceConnector = new LibertyVideoconMotorInsuranceConnector();

	@Mock
	public VendorProdMgmtServ vendorProdMgmtServMock;
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	@Mock
	public DBUtil dbUtilMock;

	@Mock
	public PropUtils propUtilsMock;

	@Mock
	VendorIntegrationServ vendorIntegrationServMock;

	
	@Mock
	MandatoryParams mandatoryParams;

	List<MotorVendorConnector> motorVendorConnectorList = new ArrayList<MotorVendorConnector>();

	VendorRequest vendorRequest = new VendorRequest();

	@Before
	public void doBeforeEachTest() {
		MockitoAnnotations.initMocks(this);
		initMotorConnectors();
		initEnquiryQuoteRequest();
		RequestBuilder rb = new RequestBuilder();
		play.mvc.Http.Context context = new play.mvc.Http.Context(rb);
		play.mvc.Http.Context.current.set(context);

	}

	private void initEnquiryQuoteRequest() {
		vendorRequest = new VendorRequest();
		vendorRequest.setEnquiryId("EqTraX5StRb4FiQjWzL90g");
		vendorRequest.setProductId(getMongoDBId());
		vendorRequest.setCategoryId(getMongoDBId());
		vendorRequest.setVendorId("EqTraX5StRb4FiQjWzL90g");
		vendorRequest.setVendorName("vendorName");
		vendorRequest.setInput_quote_params(getInputParamsLst());

	}

	private void initMotorConnectors() {
		motorVendorConnectorList.add(iffcoTokioInsuranceConnector);
		motorVendorConnectorList.add(bajajAllianzInsuranceConnector);
		motorVendorConnectorList.add(libertyVideoconMotorInsuranceConnector);
		motorVendorConnectorList.add(hdfcERGOMotorInsuranceConnector);
		motorVendorConnectorList.add(royalSundaramMotorInsuranceConnector);
		motorVendorConnectorList.add(usgicMotorInsuranceConnector);

	}

	private List<InputPolicyParams> getInputParamsLst() {

		List<InputPolicyParams> inputParams = new ArrayList<InputPolicyParams>();
		inputParams.add(getInputParam("AC-VEHCODE", "57f5231308df06217caauYWF", "", new String[] {}));
		/*inputParams.add(getInputParam(CarAttributeCode.AC_IS_CNG_OR_LPG_EXTERNALLY_FITTED.getCode(), "true", "",
				new String[] {}));
		inputParams.add(getInputParam(CarAttributeCode.AC_CNG_OR_LPG_EXTERNALLY_FITTED_VALUE.getCode(), "2800", "",
				new String[] {}));
		inputParams.add(getInputParam(CarAttributeCode.AC_COVER_ELECTRICAL_ACCESSORIES_VALUE.getCode(), "1800", "",
				new String[] {}));
		inputParams.add(getInputParam(CarAttributeCode.AC_COVER_NON_ELECTRICAL_ACCESSORIES_VALUE.getCode(), "1200", "",
				new String[] {}));
		inputParams.add(
				getInputParam(CarAttributeCode.AC_COVER_PASSENGER_COVER_VAL.getCode(), "1000", "", new String[] {}));
		inputParams.add(
				getInputParam(CarAttributeCode.AC_VOLUNTARY_DEDUCTIBLE_AMOUNT.getCode(), "200", "", new String[] {}));*/

		return inputParams;

	}

	protected InputPolicyParams getInputParam(String catAttr, String value, String additionalValue, String[] tags) {
		InputPolicyParams inputQuoteParams = new InputPolicyParams();
		inputQuoteParams.setValue(value);
		inputQuoteParams.setAdditionalValue(additionalValue);
		inputQuoteParams.setCategoryAttributeCode(catAttr);
		inputQuoteParams.setTag(Arrays.asList(tags));
		return inputQuoteParams;
	}

	private String getMongoDBId() {
		return RandomStringUtils.randomAlphanumeric(16);
	}

	public String getVendor() {
		return RandomStringUtils.randomAlphanumeric(6);
	}

	public Map<String, JsonNode> getCategoryAttributeIdMap() {
		HashMap<String, JsonNode> categoryAttributeFieldNameMap = new HashMap<String, JsonNode>();

		categoryAttributeFieldNameMap.put("57f5231308df09217caafYWF", getAttribute("AC-VEHCODE"));
		categoryAttributeFieldNameMap.put("57f5231308df09217caauYWF",
				getAttribute(CarAttributeCode.AC_IS_CNG_OR_LPG_EXTERNALLY_FITTED.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df08217caauYWF",
				getAttribute(CarAttributeCode.AC_CNG_OR_LPG_EXTERNALLY_FITTED_VALUE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df07217caauYWF",
				getAttribute(CarAttributeCode.AC_COVER_ELECTRICAL_ACCESSORIES_VALUE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caauYWF",
				getAttribute(CarAttributeCode.AC_COVER_NON_ELECTRICAL_ACCESSORIES_VALUE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(CarAttributeCode.AC_COVER_PASSENGER_COVER_VAL.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(CarAttributeCode.AC_VOLUNTARY_DEDUCTIBLE_AMOUNT.getCode()));

		return categoryAttributeFieldNameMap;

	}

	private ObjectNode getAttribute(String categoryAttributeCode) {
		ObjectNode categoryAttributeJson = Json.newObject();

		categoryAttributeJson.put("_id", "57f5231308df09217caaf" + RandomStringUtils.randomAlphabetic(3));
		categoryAttributeJson.put("code", categoryAttributeCode);
		categoryAttributeJson.put("name", categoryAttributeCode.toLowerCase());
		categoryAttributeJson.put("mandatory", false);
		categoryAttributeJson.put("default_value", "");
		categoryAttributeJson.put("restriction", "");
		categoryAttributeJson.put("restriction_id", "");
		categoryAttributeJson.put("data_type", "STRING");
		categoryAttributeJson.put("usage_type", "I");
		categoryAttributeJson.put("data_entry_type", "CONSUMER");
		categoryAttributeJson.put("max_occurrence", "1");
		categoryAttributeJson.put("category_id", "57f5231308df09217caaf809");

		String tags = categoryAttributeCode.split("AC-")[1];
		tags = tags.toLowerCase();

		if (categoryAttributeCode.contains("SELF") || categoryAttributeCode.contains("SPOUSE")
				|| categoryAttributeCode.contains("SON") || categoryAttributeCode.contains("DAUGHTER")
				|| categoryAttributeCode.contains("FATHER") || categoryAttributeCode.contains("MOTHER")) {
			ArrayNode tagArray = categoryAttributeJson.putArray(CategoryFields.CATEGORY_TAGS.value());
			ObjectNode tagJson = tagArray.addObject();
			if (categoryAttributeCode.contains("AGE")) {

				tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "age");
				ObjectNode pricetagJson = tagArray.addObject();
				pricetagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "family");
			}
			if (categoryAttributeCode.contains("DOB")) {

				ObjectNode pricetagJson = tagArray.addObject();
				pricetagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "dob");
			}
			if (categoryAttributeCode.contains("CURRENCY")) {
				tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "currency");
			}

		}
		if (categoryAttributeCode.contains("DESCRIPTION")) {
			ArrayNode tagArray = categoryAttributeJson.putArray(CategoryFields.CATEGORY_TAGS.value());
			ObjectNode tagJson = tagArray.addObject();
			tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), tags);
		}

		return categoryAttributeJson;

	}

	protected ObjectNode getProduct(String productCode) {

		ObjectNode productJson = Json.newObject();
		productJson.put("category_id", "57f5231308df09217caaf809");
		productJson.put("vendor_id", random.nextInt(10));
		productJson.put("_id", "57f5231308df09217caaf" + RandomStringUtils.randomAlphabetic(3));
		productJson.put("code", productCode);
		productJson.put("name", productCode.toLowerCase());
		productJson.put("image_url", "http://cdn/product/image1");

		if (productCode.length() % 2 == 0) {
			productJson.put("productDocUrl", "http://cdn/product/image1/doc");
		}

		ArrayNode productAttributes = productJson.putArray(ProductFields.PRODUCT_ATTRIBUTES.value());
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f1231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f2231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f3231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f4231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f5231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57fa231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57fb31308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57fc231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));

		return productJson;
	}

	protected JsonNode getProposalJson() {
		ObjectNode proposalJson = Json.newObject();
		proposalJson.put(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), 1L);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_USER_ID.value(), 2L);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_ID.value(), 3L);

		return proposalJson;
	}

	protected Object getVehicleJson() {
		ObjectNode vehicleJson = Json.newObject();
		vehicleJson.put("_id", "57f5231308df09217caaf809");
//		vehicleJson.put(MotorConstants.MOTOR_CC.value(), "100");
//		vehicleJson.put(MotorConstants.MOTOR_FUEL_TYPE.value(), "petrol");
//		vehicleJson.put(MotorConstants.MOTOR_MAKE.value(), "tata");
//		vehicleJson.put(MotorConstants.MOTOR_MODEL.value(), "g10");
//		vehicleJson.put(MotorConstants.MOTOR_VARIANT.value(), "34");
		return vehicleJson;
	}

	protected void processErrorMsg(JsonNode errorJson, String errorKey, String errorMsg) {
		assertTrue(errorJson.get(JsonUtils.ERROR).get(errorKey).asText().contains(errorMsg));

	}

}
