package com.arka.integrationserv.models.vendorplan.insurance.health.test;

import java.lang.reflect.InvocationTargetException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.health.HealthVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;

public class BuyPlanHealthVendorConnectorTest extends BaseHealthVendorConnectorTest {
	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());

	}

	@Test
	public void buyPlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (HealthVendorConnector healthConnector : healthVendorConnectorList) {
			vendorRequest.setQuoteKey(getVendor());
			VendorResponse vendorResponse = healthConnector.buyPlanFromVendor(vendorRequest);
		}

	}

	@Test
	public void buyPlanWithNoCategory() {
		Mockito.doReturn(null).when(dbUtilMock).getCatgeoryAttributeFieldNameMap(Matchers.anyString(),
				Matchers.anyString(), Matchers.anyBoolean());
		vendorRequest.setQuoteKey(getVendor());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(),
				FetchPlanMessage.CATEGORY_ATTRIBUTES_DOES_NOT_EXISTS.value());

	}

	@Test
	public void buyPlanWithProductDoc() {
		Mockito.doReturn(getProduct("abcd")).when(dbUtilMock).getProductJson(Matchers.anyString());
		vendorRequest.setQuoteKey(getVendor());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);

	}

	@Test
	public void buyPlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		vendorRequest.setQuoteKey(getVendor());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0).buyPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}

}
