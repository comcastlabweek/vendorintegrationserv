package com.arka.integrationserv.models.vendorplan.insurance.health.test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.health.HealthVendorConnector;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class FetchPlanHealthVendorConnectorTest extends BaseHealthVendorConnectorTest {
	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());

	}

	@Test
	public void fetchPlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (HealthVendorConnector tarvelConnector : healthVendorConnectorList) {
			VendorResponse vendorResponse = tarvelConnector.fetchPlanFromVendor(vendorRequest);
		}

	}

	@Test
	public void fetchPlanWithNoCategory() {
		Mockito.doReturn(null).when(dbUtilMock).getCatgeoryAttributeFieldNameMap(Matchers.anyString(),
				Matchers.anyString(), Matchers.anyBoolean());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(),
				FetchPlanMessage.CATEGORY_ATTRIBUTES_DOES_NOT_EXISTS.value());

	}

	@Test
	public void fetchPlanWithProductDoc() {
		Mockito.doReturn(getProduct("abcd")).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);

	}

	@Test
	public void fetchPlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}
	
	@Test
	public void fetchPlanWithEmptyProduct() {

		Mockito.doReturn(Json.newObject()).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		processErrorMsg(vendorResponse.formatResponse(), VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}
	

	@Test
	public void hasEmptyCategoryAtributeMap(){
		Mockito.doReturn(Collections.emptyMap()).when(dbUtilMock)
		.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		Assert.assertEquals("category.attributes.does.not.exists",vendorResponse.getErrorJson().get(JsonUtils.ERROR).get("categoryId").asText().trim());
	}
	
	@Test
	public void hasProductImgUrl(){
		
		ObjectNode product = getProduct("abcd");
		product.remove("image_url");
		Mockito.doReturn(product).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull(JsonUtils.ERROR));
		
	}
	
	@Test
	public void hasProductAttribute(){
		
		ObjectNode product = getProduct("abcd");
		product.remove(ProductFields.PRODUCT_ATTRIBUTES.value());
		Mockito.doReturn(product).when(dbUtilMock).getProductJson(Matchers.anyString());
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull(JsonUtils.ERROR));
		
	}
	@Test
	public void hasSumAssuredTrue(){
		VendorRequest vendorRequest = new VendorRequest();
		vendorRequest.setInput_quote_params(new ArrayList<>());
		List<InputPolicyParams> inputParams = vendorRequest.getInput_quote_params();
		inputParams.add(getInputParam(HealthAttributeCode.AH_SUM_ASSURED.getCode(), "500000", "", new String[] {}));
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull(JsonUtils.ERROR));
		
	}
	
	@Test
	public void hasSumAssuredFalse(){
		VendorRequest vendorRequest = new VendorRequest();
		vendorRequest.setInput_quote_params(new ArrayList<>());
		
		VendorResponse vendorResponse = healthVendorConnectorList.get(0)
				.fetchPlanFromVendor(vendorRequest);
		Assert.assertFalse(vendorResponse.getErrorJson().hasNonNull(JsonUtils.ERROR));
	}

	private InputPolicyParams getInputParam(String catAttr, String value, String additionalValue, String[] tags) {
		InputPolicyParams inputQuoteParams = new InputPolicyParams();
		inputQuoteParams.setValue(value);
		inputQuoteParams.setAdditionalValue(additionalValue);
		inputQuoteParams.setCategoryAttributeCode(catAttr);
		inputQuoteParams.setTag(Arrays.asList(tags));
		return inputQuoteParams;
	}
}
