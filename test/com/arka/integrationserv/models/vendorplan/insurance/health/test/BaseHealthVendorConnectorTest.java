package com.arka.integrationserv.models.vendorplan.insurance.health.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.health.constants.HealthInsuranceType;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.insurance.health.HealthVendorConnector;
import com.arka.integrationserv.models.vendorplan.insurance.health.IffcoTokioHealthInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.health.ReligareNoClaimBonusSuper;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class BaseHealthVendorConnectorTest {

	protected Random random = new Random();

	@InjectMocks
	public HealthVendorConnector religareNoClaimBonusSuper = new ReligareNoClaimBonusSuper();

	@InjectMocks
	public HealthVendorConnector starAllianzMediClassic = new IffcoTokioHealthInsuranceConnector();

	@Mock
	public VendorProdMgmtServ vendorProdMgmtServMock;

	@Mock
	public DBUtil dbUtilMock;

	@Mock
	public PropUtils propUtilsMock;

	List<HealthVendorConnector> healthVendorConnectorList = new ArrayList<HealthVendorConnector>();

	VendorRequest vendorRequest = new VendorRequest();

	@Before
	public void doBeforeEachTest() {
		MockitoAnnotations.initMocks(this);
		initTravelConnectors();
		initEnquiryQuoteRequest();

	}

	private void initEnquiryQuoteRequest() {
		vendorRequest = new VendorRequest();
		vendorRequest.setEnquiryId("EqTraX5StRb4FiQjWzL90g");
		vendorRequest.setProductId(getMongoDBId());
		vendorRequest.setCategoryId(getMongoDBId());
		vendorRequest.setVendorId("EqTraX5StRb4FiQjWzL90g");
		vendorRequest.setVendorName("vendorName");
		vendorRequest.setInput_quote_params(getInputParamsLst());

	}

	private void initTravelConnectors() {
		healthVendorConnectorList.add(religareNoClaimBonusSuper);
		healthVendorConnectorList.add(starAllianzMediClassic);
	}

	private List<InputPolicyParams> getInputParamsLst() {

		List<InputPolicyParams> inputParams = new ArrayList<InputPolicyParams>();
		inputParams.add(getInputParam(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode(),
				HealthInsuranceType.FAMILY.name(), "", new String[] {}));
		inputParams.add(getInputParam(HealthAttributeCode.AH_SUM_ASSURED.getCode(), "500000", "", new String[] {}));
		inputParams.add(getInputParam(HealthAttributeCode.AH_SELF_GENDER.getCode(), "Female", "",
				new String[] { "spouse", "gender" }));
		inputParams.add(getInputParam(HealthAttributeCode.AH_SPOUSE_GENDER.getCode(), "Male", "",
				new String[] { "self", "gender" }));
		inputParams.add(
				getInputParam(HealthAttributeCode.AH_SELF_AGE.getCode(), "23", "", new String[] { "self", "age" }));
		inputParams.add(
				getInputParam(HealthAttributeCode.AH_SPOUSE_AGE.getCode(), "36", "", new String[] { "spouse", "age" }));

		inputParams.add(getInputParam(HealthAttributeCode.AH_SON_AGE.getCode(), "10", "",
				new String[] { "son", "age", "son_1" }));
		inputParams.add(getInputParam(HealthAttributeCode.AH_SON_AGE.getCode(), "13", "",
				new String[] { "son", "age", "son_2" }));

		inputParams.add(getInputParam(HealthAttributeCode.AH_DAUGHTER_AGE.getCode(), "8", "",
				new String[] { "spouse", "age", "duaghter" }));

		inputParams.add(getInputParam(HealthAttributeCode.AH_FATHER_AGE.getCode(), "39", "",
				new String[] { "self", "age", "father" }));
		inputParams.add(getInputParam(HealthAttributeCode.AH_MOTHER_AGE.getCode(), "56", "",
				new String[] { "spouse", "age", "father" }));

		inputParams.add(getInputParam(HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode(), "false",
				"", new String[] {}));
		inputParams.add(getInputParam(HealthAttributeCode.AH_IS_PARENTS_TO_BE_INCLUDED.getCode(), "false", "",
				new String[] {}));
		inputParams.add(getInputParam(HealthAttributeCode.AH_PINCODE_SELF_OR_FAMILY.getCode(), "636702", "",
				new String[] { "family" }));
		return inputParams;

	}

	private InputPolicyParams getInputParam(String catAttr, String value, String additionalValue, String[] tags) {
		InputPolicyParams inputQuoteParams = new InputPolicyParams();
		inputQuoteParams.setValue(value);
		inputQuoteParams.setAdditionalValue(additionalValue);
		inputQuoteParams.setCategoryAttributeCode(catAttr);
		inputQuoteParams.setTag(Arrays.asList(tags));
		return inputQuoteParams;
	}

	private String getMongoDBId() {
		return RandomStringUtils.randomAlphanumeric(16);
	}

	public String getVendor() {
		return RandomStringUtils.randomAlphanumeric(6);
	}

	public Map<String, JsonNode> getCategoryAttributeIdMap() {
		HashMap<String, JsonNode> categoryAttributeFieldNameMap = new HashMap<String, JsonNode>();

		categoryAttributeFieldNameMap.put("57f5231308df09217caafYWF",
				getAttribute(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df09217caauYWF",
				getAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df08217caauYWF",
				getAttribute(HealthAttributeCode.AH_SELF_GENDER.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df07217caauYWF",
				getAttribute(HealthAttributeCode.AH_SPOUSE_GENDER.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caauYWF",
				getAttribute(HealthAttributeCode.AH_SELF_AGE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(HealthAttributeCode.AH_SPOUSE_AGE.getCode()));

		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(HealthAttributeCode.AH_SON_AGE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(HealthAttributeCode.AH_DAUGHTER_AGE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(HealthAttributeCode.AH_FATHER_AGE.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217caduYWF",
				getAttribute(HealthAttributeCode.AH_MOTHER_AGE.getCode()));

		categoryAttributeFieldNameMap.put("57f5231308df06217caeuYWF",
				getAttribute(HealthAttributeCode.AH_IS_PRE_EXISTING_ILLNESS_TO_BE_DECLARED.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217cafuYWF",
				getAttribute(HealthAttributeCode.AH_IS_PARENTS_TO_BE_INCLUDED.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_PINCODE_SELF_OR_FAMILY.getCode()));

		categoryAttributeFieldNameMap.put("57f1231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_ROOM_RENT_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57f2231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_DAY_CARE_TREATMENT_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57f3231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_AMBULANCE_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57f4231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_NO_CLAIM_BONUS_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57f5231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_DOMICILIARY_HOSPITALISATION_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57fa231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57fb231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER_DESCRIPTION.getCode()));
		categoryAttributeFieldNameMap.put("57fc231308df06217cbauYWF",
				getAttribute(HealthAttributeCode.AH_CO_PAY_DESCRIPTION.getCode()));

		return categoryAttributeFieldNameMap;

	}

	private ObjectNode getAttribute(String categoryAttributeCode) {
		ObjectNode categoryAttributeJson = Json.newObject();

		categoryAttributeJson.put("_id", "57f5231308df09217caaf" + RandomStringUtils.randomAlphabetic(3));
		categoryAttributeJson.put("code", categoryAttributeCode);
		categoryAttributeJson.put("name", categoryAttributeCode.toLowerCase());
		categoryAttributeJson.put("mandatory", false);
		categoryAttributeJson.put("default_value", "");
		categoryAttributeJson.put("restriction", "");
		categoryAttributeJson.put("restriction_id", "");
		categoryAttributeJson.put("data_type", "STRING");
		categoryAttributeJson.put("usage_type", "I");
		categoryAttributeJson.put("data_entry_type", "CONSUMER");
		categoryAttributeJson.put("max_occurrence", "1");
		categoryAttributeJson.put("category_id", "57f5231308df09217caaf809");

		String tags = categoryAttributeCode.split("AH-")[1];
		tags = tags.toLowerCase();

		if (categoryAttributeCode.contains("SELF") || categoryAttributeCode.contains("SPOUSE")
				|| categoryAttributeCode.contains("SON") || categoryAttributeCode.contains("DAUGHTER")
				|| categoryAttributeCode.contains("FATHER") || categoryAttributeCode.contains("MOTHER")) {
			ArrayNode tagArray = categoryAttributeJson.putArray(CategoryFields.CATEGORY_TAGS.value());
			ObjectNode tagJson = tagArray.addObject();
			if (categoryAttributeCode.contains("AGE")) {

				tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "age");
				ObjectNode pricetagJson = tagArray.addObject();
				pricetagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "family");
			}
			if (categoryAttributeCode.contains("DOB")) {

				ObjectNode pricetagJson = tagArray.addObject();
				pricetagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "dob");
			}
			if (categoryAttributeCode.contains("CURRENCY")) {
				tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), "currency");
			}

		}
		if (categoryAttributeCode.contains("DESCRIPTION")) {
			ArrayNode tagArray = categoryAttributeJson.putArray(CategoryFields.CATEGORY_TAGS.value());
			ObjectNode tagJson = tagArray.addObject();
			tagJson.put(CategoryFields.CATEGORY_TAG_NAME.value(), tags);
		}

		return categoryAttributeJson;

	}

	protected ObjectNode getProduct(String productCode) {

		ObjectNode productJson = Json.newObject();
		productJson.put("category_id", "dbwqPuzVQkMqjHrNBJdoduVjefZONc6pWqnhBdJMPZY");
		productJson.put("vendor_id", random.nextInt(10));
		productJson.put("_id", "57f5231308df09217caaf" + RandomStringUtils.randomAlphabetic(3));
		productJson.put("code", productCode);
		productJson.put("name", productCode.toLowerCase());
		productJson.put("image_url", "http://cdn/product/image1");

		if (productCode.length() % 2 == 0) {
			productJson.put("productDocUrl", "http://cdn/product/image1/doc");
		}

		ArrayNode productAttributes = productJson.putArray(ProductFields.PRODUCT_ATTRIBUTES.value());
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f1231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f2231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f3231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f4231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57f5231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57fa231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57fb31308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));
		productAttributes.addObject().put(ProductFields.PRODUCT_ATTRIBUTE_ID.value(), "57fc231308df06217cbauYWF")
				.put(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value(), RandomStringUtils.randomAlphabetic(6));

		return productJson;
	}

	protected JsonNode getProposalJson() {
		ObjectNode proposalJson = Json.newObject();
		proposalJson.put(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), 1L);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_USER_ID.value(), 2L);
		proposalJson.put(VendorQuoteConstants.PROPOSAL_ID.value(), 3L);

		return proposalJson;
	}

	protected void processErrorMsg(JsonNode errorJson, String errorKey, String errorMsg) {
		assertTrue(errorJson.get(JsonUtils.ERROR).get(errorKey).asText().contains(errorMsg));

	}

}
