package com.arka.integrationserv.models.vendorplan.insurance.health.test;

import java.lang.reflect.InvocationTargetException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.insurance.health.HealthVendorConnector;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class PurchasePlanHealthVendorConenctorTest extends BaseHealthVendorConnectorTest {

	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		Mockito.doReturn(getCategoryAttributeIdMap()).when(dbUtilMock)
				.getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
		Mockito.doReturn(getProduct("abc")).when(dbUtilMock).getProductJson(Matchers.anyString());

	}

	@Test
	public void purchasePlanForAllVendors() throws NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, InstantiationException {

		for (HealthVendorConnector healthConnector : healthVendorConnectorList) {
			ObjectNode policyJson = healthConnector.purchasePlan(getProposalJson());
		}

	}

	@Test
	public void purchasePlanWithNoProduct() {

		Mockito.doReturn(null).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = healthVendorConnectorList.get(0).purchasePlan(getProposalJson());
		processErrorMsg(policyJson, VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}
	
	@Test
	public void purchasePlanWithEmptyProduct() {

		Mockito.doReturn(Json.newObject()).when(dbUtilMock).getProductJson(Matchers.anyString());
		ObjectNode policyJson = healthVendorConnectorList.get(0).purchasePlan(getProposalJson());
		processErrorMsg(policyJson, VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
				FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value());

	}
	
	@Test
	public void purchasePlanWithIvalidInputParms() {

		ObjectNode proposalJson =(ObjectNode) getProposalJson();
		 proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray().add(Json.newObject()));
		ObjectNode policyJson = healthVendorConnectorList.get(0).purchasePlan(proposalJson);
		
		Assert.assertFalse(policyJson.hasNonNull("error"));

	}
	@Test
	public void hasSumAssuredCategoryAttributeCode() {

		ObjectNode proposalJson =(ObjectNode) getProposalJson();
		 proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray()
				 .add(Json.newObject().put("categoryAttributeCode", HealthAttributeCode.AH_SUM_ASSURED.getCode()).
						 put("value", 200)));
		ObjectNode policyJson = healthVendorConnectorList.get(0).purchasePlan(proposalJson);
		Assert.assertFalse(policyJson.hasNonNull("error"));
		Assert.assertTrue(policyJson.get("sumAssured").asInt()==200);

	}
	@Test
	public void hasPremiumAmntCategoryAttributeCode() {

		ObjectNode proposalJson =(ObjectNode) getProposalJson();
		 proposalJson.set(VendorQuoteConstants.POLICY_INPUT_PARAMS.value(), Json.newArray()
				 .add(Json.newObject().put("categoryAttributeCode", HealthAttributeCode.AH_PREMIUM_AMOUNT.getCode()).
						 put("value", 200)));
		ObjectNode policyJson = healthVendorConnectorList.get(0).purchasePlan(proposalJson);
		Assert.assertTrue(policyJson.get("premium").asInt()==200);
		Assert.assertFalse(policyJson.hasNonNull("error"));

	}
	
	@Test
	public void hasPlaceHolderCategoryAttributeCode() {

		ObjectNode proposalJson =(ObjectNode) getProposalJson();
		 proposalJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), "Name");
		 proposalJson.put(PolicyFields.ISSUED_TIME.getCode(),"2017-05-03");
		 proposalJson.put(PolicyFields.COMMENCEMENT_DATE.getCode(),"2017-05-03");
		 proposalJson.put(PolicyFields.EXPIRY_DATE.getCode(),"2017-05-03");
		ObjectNode policyJson = healthVendorConnectorList.get(0).purchasePlan(proposalJson);
		Assert.assertFalse(policyJson.hasNonNull("error"));
		Assert.assertTrue(policyJson.get("policyHolderName").asText().equals("Name"));

	}

	
	
}
