package com.arka.integrationserv.models.dataconfiguration.service.test;

import static org.mockito.Mockito.verify;

import java.rmi.AccessException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.DataConfigurationField;
import com.arka.integrationserv.models.dataconfiguration.constants.DataType;
import com.arka.integrationserv.models.dataconfiguration.repository.DataConfigurationRepository;
import com.arka.integrationserv.models.dataconfiguration.service.DataConfigurationService;
import com.arka.integrationserv.models.dataconfiguration.service.impl.DataConfigurationServiceImpl;
import com.arka.integrationserv.test.Base;
import com.arka.integrationserv.test.Constants;
import com.google.common.base.Supplier;

import play.libs.Json;

public class DataConfigurationServiceTest extends Base implements Constants {

	@InjectMocks
	private DataConfigurationService<String> dataConfigurationService = new DataConfigurationServiceImpl();

	@Mock
	private DataConfigurationRepository dataConfigRepoMock;

	@Mock
	private CacheService cacheMock;

	private DataConfiguration dataConfiguration;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	private static final String COLLECTION_NAME = "test_collection_new";
	private static final String PRODUCT_ID = "57dfbcecbffa5307fc308c7f";
	private static final String CATALOG_MODEL_RESTRICTION_ID = "57d648a5bffa5319dc08f7f1";
	private static final String VENDOR_ID = "3";
	private static final Long USER = 1L;

	@Before
	public void doBeforeEachTest() {
		super.doBeforeEachTest();
		MockitoAnnotations.initMocks(this);
		makeModel();
	}

	private void makeModel() {
		dataConfiguration = new DataConfiguration();
		dataConfiguration.setId(objectId.toHexString());
		dataConfiguration.setCollectionName(COLLECTION_NAME);
		dataConfiguration.setHasMapping(false);
		dataConfiguration.setProductId(PRODUCT_ID);
		dataConfiguration.setCatalogModelRestrictionId(CATALOG_MODEL_RESTRICTION_ID);
		dataConfiguration.setVendorId(VENDOR_ID);
		dataConfiguration.setStatus(Status.ACTIVE);
		dataConfiguration.setCreatedBy(USER);
		List<DataConfigurationField> fields = new ArrayList<>();
		DataConfigurationField dcf = new DataConfigurationField();
		dcf.setDataType(DataType.STRING);
		dcf.setFieldName("Name");
		dcf.setSearchableField(true);
		dcf.setCreatedBy(USER);
		fields.add(dcf);
		dataConfiguration.setDataConfigurationField(fields);
	}

	@Test
	public void testInsert() throws ArkaValidationException {
		Mockito.doNothing().when(dataConfigRepoMock).insert(dataConfiguration);
		inValidateCacheWithPrefix();
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
		verify(dataConfigRepoMock).insert(dataConfiguration);
	}

	@Test
	public void testInsertExceptionNull() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(null);
	}

	@Test
	public void testInsertExceptionCollectionNameNull() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(dataConfiguration);
		dataConfiguration.setCollectionName(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}
	
	@Test
	public void testInsertExceptionDataConfigNull() throws ArkaValidationException {
		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		dataConfiguration.setCollectionName("");
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}

	@Test
	public void testInsertExceptionCollectionNameEmpty() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		dataConfiguration.setCollectionName("");
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}

	@Test
	public void testInsertExceptionCollectionStatusNull() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		dataConfiguration.setStatus(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}

	@Test
	public void testInsertExceptionCollectionIdNull() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		dataConfiguration.setId(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}

	@Test
	public void testInsertExceptionDataConfigurationFieldNull() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		dataConfiguration.setDataConfigurationField(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}
	
	@Test
	public void testInsertExceptionDataConfigurationFieldEmpty() throws ArkaValidationException {

		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		List<DataConfigurationField> fields = new ArrayList<>();
		dataConfiguration.setDataConfigurationField(fields);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}

	@Test
	public void testInsertExceptionDuplicate() throws ArkaValidationException {
		Mockito.doReturn(2L).when(dataConfigRepoMock).count(Matchers.anyMap());
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).insert(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.saveDataConfiguration(dataConfiguration);
	}

	@Test
	public void testUpdate() throws ArkaValidationException {
		Mockito.doReturn(true).when(dataConfigRepoMock).updateOne(dataConfiguration);
		inValidateCacheWithPrefix();

		boolean updated = dataConfigurationService.updateDataConfiguration(dataConfiguration);
		Assert.assertEquals(true, updated);

	}

	@Test
	public void testUpdateFalse() throws ArkaValidationException {
		Mockito.doReturn(false).when(dataConfigRepoMock).updateOne(dataConfiguration);
		inValidateCacheWithPrefix();
		boolean updated = dataConfigurationService.updateDataConfiguration(dataConfiguration);
		Assert.assertEquals(false, updated);

	}

	@Test
	public void testUpdateException() throws ArkaValidationException {

		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).updateOne(null);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.updateDataConfiguration(null);
	}

	@Test
	public void testUpdateDataConfigurationFromForm() throws ArkaValidationException {

		Mockito.doReturn(true).when(dataConfigRepoMock).updateOne(dataConfiguration);
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Mockito.doReturn(true).when(dataConfigRepoMock).updateOne(dataConfiguration);

		DataConfiguration updateDataConfigurationfromForm = dataConfigurationService
				.updateDataConfigurationfromForm(dataConfiguration);

		Assert.assertEquals(createdUser, updateDataConfigurationfromForm.getCreatedBy());
	}

	@Test
	public void testUpdateDataConfigurationFromFormWithException() throws ArkaValidationException {

		Mockito.doReturn(true).when(dataConfigRepoMock).updateOne(dataConfiguration);
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Map<String, List<String>> errors = new HashMap<String, List<String>>();
		Mockito.doThrow(new ArkaValidationException("error", errors)).when(dataConfigRepoMock)
				.updateOne(dataConfiguration);
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.updateDataConfigurationfromForm(dataConfiguration);
	}

	@Test
	public void testUpdateDataConfigurationFromFormFalse() throws ArkaValidationException {
		Mockito.doReturn(false).when(dataConfigRepoMock).updateOne(dataConfiguration);
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Mockito.doReturn(false).when(dataConfigRepoMock).updateOne(dataConfiguration);
		DataConfiguration updateDataConfigurationfromForm = dataConfigurationService
				.updateDataConfigurationfromForm(dataConfiguration);

		Assert.assertEquals(createdUser, updateDataConfigurationfromForm.getCreatedBy());
	}

	@Test
	public void testUpdateDataConfigurationFromFormException() throws ArkaValidationException, AccessException {

		expectedException.expect(NullPointerException.class);
		dataConfigurationService.updateDataConfigurationfromForm(null);
	}

	@Test
	public void testFindDataConfigurationById() throws ArkaValidationException {
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		DataConfiguration dc = dataConfigurationService.findDataConfigurationById(objectId.toHexString())
				.toCompletableFuture().join();
		Assert.assertNotNull(dc);
		Assert.assertEquals(dc.getId(), objectId.toHexString());
	}

	@Test
	public void testFindDataConfigurationByIdWithNull() throws ArkaValidationException {
		getDataConfigByIdWithNull();
		Mockito.doAnswer(invocation -> {
			String dataConfig = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfig));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		DataConfiguration dc = dataConfigurationService.findDataConfigurationById(objectId.toHexString())
				.toCompletableFuture().join();
		Assert.assertNull(dc);
	}

	@Test
	public void testFindDataConfigurationByIdWithException() throws ArkaValidationException {
		getDataConfigByIdWithException();
		Mockito.doAnswer(invocation -> {
			String dataConfig = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfig));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.findDataConfigurationById(objectId.toHexString())
				.toCompletableFuture().join();
	}

	@Test
	public void testDeleteDataConfigurationById() throws ArkaValidationException {
		Mockito.doReturn(true).when(dataConfigRepoMock).deleteOneById(Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = dataConfigurationService.deleteDataConfigurationById(objectId.toHexString());
		Assert.assertEquals(true, isDeleted);
	}

	@Test
	public void testDeleteDataConfigurationByIdFalse() throws ArkaValidationException {

		Mockito.doReturn(false).when(dataConfigRepoMock).deleteOneById(Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = dataConfigurationService.deleteDataConfigurationById(objectId.toHexString());
		Assert.assertEquals(false, isDeleted);

	}

	@Test
	public void testDeleteDataConfigurations() throws ArkaValidationException {

		Mockito.doReturn(true).when(dataConfigRepoMock).deleteMany(Matchers.anyMap());
		inValidateCacheWithPrefix();

		boolean isDeleted = dataConfigurationService.deleteDataConfigurations(Collections.emptyMap());
		Assert.assertEquals(true, isDeleted);

	}

	@Test
	public void testDeleteDataConfigurationsFalse() throws ArkaValidationException {

		Mockito.doReturn(false).when(dataConfigRepoMock).deleteMany(Matchers.anyMap());
		inValidateCacheWithPrefix();

		boolean isDeleted = dataConfigurationService.deleteDataConfigurations(Collections.emptyMap());
		Assert.assertEquals(false, isDeleted);

	}

	@Test
	public void testDeleteTagFromDataConfiguration() throws ArkaValidationException {

		Mockito.doReturn(true).when(dataConfigRepoMock).deleteFromArray(Matchers.anyString(), Matchers.anyString(),
				Matchers.anyString());
		inValidateCacheWithPrefix();

		boolean isDeleted = dataConfigurationService.deleteTagFromDataConfiguration("", "");
		Assert.assertEquals(true, isDeleted);

	}

	@Test
	public void testDeleteTagFromDataConfigurationFalse() throws ArkaValidationException {

		Mockito.doReturn(false).when(dataConfigRepoMock).deleteFromArray(Matchers.anyString(), Matchers.anyString(),
				Matchers.anyString());
		inValidateCacheWithPrefix();

		boolean isDeleted = dataConfigurationService.deleteTagFromDataConfiguration("", "");
		Assert.assertEquals(false, isDeleted);

	}

	@Test
	public void testGetDataConfigurationCount() throws ArkaValidationException {

		Mockito.doReturn(1L).when(dataConfigRepoMock).count();
		long count = dataConfigurationService.getDataConfigurationCount();
		Assert.assertEquals(1L, count);

	}

	@Test
	public void testGetDataConfigurationCountAgrgMap() throws ArkaValidationException {

		Mockito.doReturn(1L).when(dataConfigRepoMock).count(Matchers.anyMap());
		long count = dataConfigurationService.getDataConfigurationCount(Collections.emptyMap());
		Assert.assertEquals(1L, count);

	}

	@Test
	public void testCheckUniquenessForCollectionName() throws ArkaValidationException {
		Mockito.doReturn(1L).when(dataConfigRepoMock).count(Matchers.anyMap());
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.checkUniquenessForCollectionName(COLLECTION_NAME);
		
	}

	@Test
	public void testCheckUniquenessForCollectionNameEmpty() throws ArkaValidationException {
		Mockito.doReturn(1L).when(dataConfigRepoMock).count(Matchers.anyMap());
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.checkUniquenessForCollectionName("");
	}

	@Test
	public void testCheckUniquenessForCollectionNameNull() throws ArkaValidationException {
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.checkUniquenessForCollectionName(null);
	}

	@Test
	public void testCheckUniquenessForCollectionNameException() throws ArkaValidationException {
		Map<String, List<String>> errors = new HashMap<String, List<String>>();
		Mockito.doThrow(new ArkaValidationException("error", errors)).when(dataConfigRepoMock).count(Matchers.anyMap());
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.checkUniquenessForCollectionName(COLLECTION_NAME);
	}
	
	@Test
	public void testCheckUniquenessForCollectionNameCountNull() throws ArkaValidationException {
		Mockito.doReturn(0L).when(dataConfigRepoMock).count(Matchers.anyMap());
		dataConfigurationService.checkUniquenessForCollectionName(COLLECTION_NAME);
	}

	@Test
	public void testCreateTempCollection() throws ArkaValidationException {
		Mockito.doReturn(false).when(dataConfigRepoMock).createTempCollection(Matchers.any(DataConfiguration.class),
				Matchers.anyBoolean());
		inValidateCacheWithPrefix();

		boolean isCreated = dataConfigurationService.createTempCollection(null, false);
		verify(dataConfigRepoMock).createTempCollection(Matchers.any(DataConfiguration.class), Matchers.anyBoolean());
		Assert.assertEquals(false, isCreated);
	}

	@Test
	public void testCreateTempCollection2() throws ArkaValidationException {
		Mockito.doReturn(true).when(dataConfigRepoMock).createTempCollection(Matchers.any(DataConfiguration.class),
				Matchers.anyBoolean());
		inValidateCacheWithPrefix();

		boolean isCreated = dataConfigurationService.createTempCollection(dataConfiguration, true);
		verify(dataConfigRepoMock).createTempCollection(Matchers.any(DataConfiguration.class), Matchers.anyBoolean());
		Assert.assertEquals(true, isCreated);
	}

	@Test
	public void testCreateTempCollectionException() throws ArkaValidationException {
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock)
				.createTempCollection(Matchers.any(DataConfiguration.class), Matchers.anyBoolean());
		inValidateCacheWithPrefix();
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.createTempCollection(null, false);
	}

	@Test
	public void testCreateTempCollectionEntryInDataConfig() throws ArkaValidationException {
		Mockito.doReturn(true).when(dataConfigRepoMock)
				.createTempCollectionEntryInDataConfig(Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		boolean createTempCollectionEntryInDataConfig = dataConfigurationService
				.createTempCollectionEntryInDataConfig(dataConfiguration);

		verify(dataConfigRepoMock).createTempCollectionEntryInDataConfig(Matchers.any(DataConfiguration.class));

		Assert.assertEquals(true, createTempCollectionEntryInDataConfig);
	}

	@Test
	public void testCreateTempCollectionEntryInDataConfigFalse() throws ArkaValidationException {
		Mockito.doReturn(false).when(dataConfigRepoMock)
				.createTempCollectionEntryInDataConfig(Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		boolean createTempCollectionEntryInDataConfig = dataConfigurationService
				.createTempCollectionEntryInDataConfig(null);

		verify(dataConfigRepoMock).createTempCollectionEntryInDataConfig(Matchers.any(DataConfiguration.class));

		Assert.assertEquals(false, createTempCollectionEntryInDataConfig);
	}

	@Test
	public void testAddVendorUploadedFilesIntoCollection() throws ArkaValidationException {
		Mockito.doNothing().when(dataConfigRepoMock).addVendorUploadedFilesIntoCollection(Matchers.anyList(),
				Matchers.anyString(), Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		dataConfigurationService.addVendorUploadedFilesIntoCollection(Collections.emptyList(), COLLECTION_NAME,
				dataConfiguration);

		verify(dataConfigRepoMock).addVendorUploadedFilesIntoCollection(Matchers.anyList(), Matchers.anyString(),
				Matchers.any(DataConfiguration.class));

	}

	@Test
	public void testCreateTempFromOriginalCollection() throws ArkaValidationException {
		Mockito.doReturn(true).when(dataConfigRepoMock)
				.createTempFromOriginalCollection(Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		dataConfigurationService.createTempFromOriginalCollection(dataConfiguration);

		verify(dataConfigRepoMock).createTempFromOriginalCollection(Matchers.any(DataConfiguration.class));

	}

	@Test
	public void testCreateTempFromOriginalCollection2() throws ArkaValidationException {
		Mockito.doReturn(false).when(dataConfigRepoMock)
				.createTempFromOriginalCollection(Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		boolean isCreated = dataConfigurationService.createTempFromOriginalCollection(null);

		verify(dataConfigRepoMock).createTempFromOriginalCollection(Matchers.any(DataConfiguration.class));
		Assert.assertEquals(false, isCreated);
	}

	@Test
	public void testUpdateCurrentVersionandHistory() throws ArkaValidationException {
		Mockito.doReturn(true).when(dataConfigRepoMock)
				.updateCurrentVersionandHistory(Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		boolean isUpdated = dataConfigurationService.updateCurrentVersionandHistory(dataConfiguration);

		verify(dataConfigRepoMock).updateCurrentVersionandHistory(Matchers.any(DataConfiguration.class));
		Assert.assertEquals(true, isUpdated);

	}

	@Test
	public void testUpdateCurrentVersionandHistoryFalse() throws ArkaValidationException {
		Mockito.doReturn(false).when(dataConfigRepoMock)
				.updateCurrentVersionandHistory(Matchers.any(DataConfiguration.class));
		inValidateCacheWithPrefix();

		boolean isUpdated = dataConfigurationService.updateCurrentVersionandHistory(null);

		verify(dataConfigRepoMock).updateCurrentVersionandHistory(Matchers.any(DataConfiguration.class));
		Assert.assertEquals(false, isUpdated);

	}

	@Test
	public void testGetCollectionFields() throws ArkaValidationException {
		Mockito.doReturn(Collections.emptyList()).when(dataConfigRepoMock).getCollectionFields(Matchers.anyString());
		Collection<String> collectionFields = dataConfigurationService.getCollectionFields(COLLECTION_NAME);
		verify(dataConfigRepoMock).getCollectionFields(Matchers.anyString());
		Assert.assertNotNull(collectionFields);

	}

	@Test
	public void testInsertMany() throws ArkaValidationException {
		Mockito.doNothing().when(dataConfigRepoMock).insertMany(Matchers.anyString(), Matchers.anyList());
		dataConfigurationService.insertMany(COLLECTION_NAME, Collections.emptyList());
		verify(dataConfigRepoMock).insertMany(Matchers.anyString(), Matchers.anyList());

	}

	

	@Test
	public void testDeleteMany() throws ArkaValidationException {
		Mockito.doNothing().when(dataConfigRepoMock).deleteMany(Matchers.anyString(), Matchers.anyMap());
		dataConfigurationService.deleteMany(COLLECTION_NAME, Collections.emptyMap());
		verify(dataConfigRepoMock).deleteMany(Matchers.anyString(), Matchers.anyMap());
	}

	@Test
	public void testFindOneByIdFromCollection() throws ArkaValidationException {
		Mockito.doReturn(Collections.emptyMap()).when(dataConfigRepoMock)
				.findOneByIdFromCollection(Matchers.anyString(), Matchers.anyString());
		dataConfigurationService.findOneByIdFromCollection(COLLECTION_NAME, objectId.toHexString());
		verify(dataConfigRepoMock).findOneByIdFromCollection(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testFindOneAndUpdateByIdFromCollection() throws ArkaValidationException {
		Mockito.doReturn(Collections.emptyMap()).when(dataConfigRepoMock)
				.findOneAndUpdateByIdFromCollection(Matchers.anyString(), Matchers.anyString(), Matchers.anyMap());
		dataConfigurationService.findOneAndUpdateByIdFromCollection(COLLECTION_NAME, objectId.toHexString(),
				Collections.emptyMap());
		verify(dataConfigRepoMock).findOneAndUpdateByIdFromCollection(Matchers.anyString(), Matchers.anyString(),
				Matchers.anyMap());
	}

	@Test
	public void testFindFromCollection() throws ArkaValidationException {
		Mockito.doReturn(Collections.emptyList()).when(dataConfigRepoMock).findFromCollection(Matchers.anyString(),
				Matchers.anyMap());
		dataConfigurationService.findFromCollection(COLLECTION_NAME, Collections.emptyMap());
		verify(dataConfigRepoMock).findFromCollection(Matchers.anyString(), Matchers.anyMap());
	}

	@Test
	public void tesGetCollectionRecordCount() throws ArkaValidationException {
		Mockito.doReturn(1L).when(dataConfigRepoMock).getCollectionRecordCount(Matchers.anyString(), Matchers.anyMap());
		dataConfigurationService.getCollectionRecordCount(COLLECTION_NAME, Collections.emptyMap());
		verify(dataConfigRepoMock).getCollectionRecordCount(Matchers.anyString(), Matchers.anyMap());
	}

	@Test
	public void testFindDataConfigurationsNullMap() throws ArkaValidationException {
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Collection<DataConfiguration> join = dataConfigurationService.findDataConfigurations(null).toCompletableFuture()
				.join();
		Assert.assertNotNull(join);
	}

	@Test
	public void testFindDataConfigurationsEmptyMap() throws ArkaValidationException {
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Collection<DataConfiguration> join = dataConfigurationService.findDataConfigurations(Collections.emptyMap())
				.toCompletableFuture().join();
		Assert.assertNotNull(join);
	}

	@Test
	public void testFindDataConfigurations() throws ArkaValidationException {
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(dataConfiguration))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		Collection<DataConfiguration> join = dataConfigurationService.findDataConfigurations(criteriaMap)
				.toCompletableFuture().join();
		Assert.assertNotNull(join);
	}

	@Test
	public void testFindDataConfigurationsWithNull() throws ArkaValidationException {
		getAllDataConfigWithNull();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		Collection<DataConfiguration> join = dataConfigurationService.findDataConfigurations(criteriaMap)
				.toCompletableFuture().join();
		Assert.assertNotNull(join);
	}

	@Test
	public void testFindDataConfigurationsWithException() throws ArkaValidationException {
		getAllDataConfigWithException();
		Mockito.doAnswer(invocation -> {
			String dataConfigList = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(dataConfigList));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		expectedException.expect(ArkaValidationException.class);
		dataConfigurationService.findDataConfigurations(criteriaMap)
				.toCompletableFuture().join();
	}

	public void inValidateCacheWithPrefix() {
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(Matchers.anyString());
	}

	private void getDataConfigByIdWithNull() {
		Mockito.doReturn(null).when(dataConfigRepoMock).findOneById(Matchers.anyString());
	}

	private void getDataConfigByIdWithException() {
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).findOneById(Matchers.anyString());
	}

	private void getAllDataConfigWithNull() {
		Mockito.doReturn(null).when(dataConfigRepoMock).findAll(Matchers.anyMap());
	}

	private void getAllDataConfigWithException() {
		Mockito.doThrow(ArkaValidationException.class).when(dataConfigRepoMock).findAll(Matchers.anyMap());
	}
}
