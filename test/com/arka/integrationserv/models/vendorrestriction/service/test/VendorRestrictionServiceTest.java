package com.arka.integrationserv.models.vendorrestriction.service.test;

import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorrestriction.VendorRestriction;
import com.arka.integrationserv.models.vendorrestriction.constant.ConstraintType;
import com.arka.integrationserv.models.vendorrestriction.repository.VendorRestrictionRepository;
import com.arka.integrationserv.models.vendorrestriction.service.VendorRestrictionService;
import com.arka.integrationserv.models.vendorrestriction.service.Impl.VendorRestrictionServiceImpl;
import com.arka.integrationserv.test.Base;
import com.arka.integrationserv.test.Constants;
import com.google.common.base.Supplier;

import play.libs.Json;

public class VendorRestrictionServiceTest extends Base implements Constants {

	private static final String NAME = "test_name";
	private static final String PRODUCT_ID = "57dfbcecbffa5307fc308c7f";
	private static final String VENDOR_ID = "EqTraX5StRb4FiQjWzL90g";
	private static final Long USER = 1L;
	private static final String CODE = "code";
	private static final String DESCRIPTION = "description";
	@InjectMocks
	private VendorRestrictionService<String> vendorRestrictionService = new VendorRestrictionServiceImpl();

	@Mock
	private VendorRestrictionRepository vendorRestrictionRepoMock;

	@Mock
	private CacheService cacheMock;

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	VendorRestriction vendorRestriction;

	@Before
	public void doBeforeEachTest() {

		super.doBeforeEachTest();
		MockitoAnnotations.initMocks(this);
		vendorRestriction = new VendorRestriction();
		vendorRestriction.setId(objectId.toHexString());
		vendorRestriction.setCode(CODE);
		vendorRestriction.setConstraintType(ConstraintType.FIELD);
		vendorRestriction.setConstraintValue(Json.newArray());
		vendorRestriction.setDescription(DESCRIPTION);
		vendorRestriction.setFlags(1);
		vendorRestriction.setName(NAME);
		vendorRestriction.setUpdatedBy(updatedUser);
		vendorRestriction.setVendorId(VENDOR_ID);
		vendorRestriction.setVendorProduct(Arrays.asList(PRODUCT_ID));
		vendorRestriction.setStatus(Status.ACTIVE);
		vendorRestriction.setCreatedBy(USER);
	}

	@Test
	public void testsaveVendorRestrictionExceptionObjectNull() throws ArkaValidationException {
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.saveVendorRestriction(null);
	}

	@Test
	public void testsaveVendorRestrictionExceptionIdNull() throws ArkaValidationException {
		saveVendorRestrictionMock(2L);
		vendorRestriction.setId(null);
		saveVendorRestrictionCheckException(vendorRestriction);
	}

	@Test
	public void testsaveVendorRestrictionExceptionCodeNull() throws ArkaValidationException {
		saveVendorRestrictionMock(2L);
		vendorRestriction.setCode(null);
		saveVendorRestrictionCheckException(vendorRestriction);
	}

	@Test
	public void testsaveVendorRestrictionExceptionCodeEmpty() throws ArkaValidationException {
		saveVendorRestrictionMock(2L);
		vendorRestriction.setCode("");
		saveVendorRestrictionCheckException(vendorRestriction);
	}

	@Test
	public void testsaveVendorRestrictionExceptionNameNull() throws ArkaValidationException {
		saveVendorRestrictionMock(2L);
		vendorRestriction.setName(null);
		saveVendorRestrictionCheckException(vendorRestriction);
	}

	@Test
	public void testsaveVendorRestrictionExceptionNameEmty() throws ArkaValidationException {
		saveVendorRestrictionMock(2L);
		vendorRestriction.setName("");
		saveVendorRestrictionCheckException(vendorRestriction);
	}
	
	@Test
	public void testsaveVendorRestrictionException() throws ArkaValidationException {
		saveVendorRestrictionCountWithException();
		Map<String, List<String>> errors = new HashMap<String,List<String>>();
		Mockito.doThrow(new ArkaValidationException("error",errors)).when(vendorRestrictionRepoMock).count(Matchers.anyMap());
		vendorRestrictionService.saveVendorRestriction(vendorRestriction);
	}

	@Test
	public void testsaveVendorRestrictionExceptionThrow() throws ArkaValidationException {
		Mockito.doThrow(NullPointerException.class).when(vendorRestrictionRepoMock).count(Matchers.anyMap());
		Mockito.doNothing().when(vendorRestrictionRepoMock).insert(vendorRestriction);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());

		vendorRestriction.setName(null);

		expectedException.expect(NullPointerException.class);
		vendorRestrictionService.saveVendorRestriction(vendorRestriction);
		verify(vendorRestrictionRepoMock).insert(vendorRestriction);
	}

	@Test
	public void testsaveVendorRestriction() throws ArkaValidationException {

		Mockito.doNothing().when(vendorRestrictionRepoMock).insert(vendorRestriction);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		vendorRestrictionService.saveVendorRestriction(vendorRestriction);
		verify(vendorRestrictionRepoMock).insert(vendorRestriction);

	}

	@Test
	public void testUpdateVendorRestrictionException() throws ArkaValidationException {
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.updateVendorRestriction(null);
	}

	@Test
	public void testUpdateVendorRestrictionExceptionCodeNull() throws ArkaValidationException {
		vendorRestriction.setCode(null);
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.updateVendorRestriction(vendorRestriction);
	}

	@Test
	public void testUpdateVendorRestrictionExceptionStausNull() throws ArkaValidationException {
		vendorRestriction.setStatus(null);
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.updateVendorRestriction(vendorRestriction);
	}

	@Test
	public void testUpdateVendorRestriction() throws ArkaValidationException {

		Mockito.doReturn(true).when(vendorRestrictionRepoMock).updateOne(vendorRestriction);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		boolean isUpdated = vendorRestrictionService.updateVendorRestriction(vendorRestriction);
		verify(vendorRestrictionRepoMock).updateOne(vendorRestriction);
		Assert.assertEquals(true, isUpdated);

	}

	@Test
	public void testUpdateVendorRestrictionFalse() throws ArkaValidationException {

		Mockito.doReturn(false).when(vendorRestrictionRepoMock).updateOne(vendorRestriction);
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		boolean isUpdated = vendorRestrictionService.updateVendorRestriction(vendorRestriction);
		verify(vendorRestrictionRepoMock).updateOne(vendorRestriction);
		Assert.assertEquals(false, isUpdated);

	}

	@Test
	public void testFindVendorRestrictionById() {
		getOrElse(vendorRestriction);
		VendorRestriction vr = vendorRestrictionService.findVendorRestrictionById(objectId.toHexString())
				.toCompletableFuture().join();
		Assert.assertNotNull(vr);
		Assert.assertEquals(vr.getId(), objectId.toHexString());
	}

	@Test
	public void testFindVendorRestrictionByIdWithNull() {
		getVendorRestrictionByIdWithNull();
		Mockito.doAnswer(invocation -> {
			String vendorRestriction = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(vendorRestriction));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		VendorRestriction vr = vendorRestrictionService.findVendorRestrictionById(objectId.toHexString())
				.toCompletableFuture().join();
		Assert.assertNull(vr);
	}

	@Test
	public void testFindVendorRestrictionByIdWithException() {
		getVendorRestrictionByIdWithException();
		Mockito.doAnswer(invocation -> {
			String vendorRestriction = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(vendorRestriction));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.findVendorRestrictionById(objectId.toHexString())
				.toCompletableFuture().join();
		
	}
	
	@Test
	public void testFindVendorRestrictionByIdNotNull() {
		getVendorRestrictionById();
		Mockito.doAnswer(invocation -> {
			String vendorRestriction = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(vendorRestriction));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		VendorRestriction vr = vendorRestrictionService.findVendorRestrictionById(objectId.toHexString())
				.toCompletableFuture().join();
		Assert.assertNotNull(vr);
		Assert.assertEquals(vr.getId(), objectId.toHexString());
	}

	@Test
	public void testFindVendorRestrictionsEmpty() {
		getOrElse(vendorRestriction);
		Collection<VendorRestriction> cvr = vendorRestrictionService.findVendorRestrictions(Collections.emptyMap())
				.toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictionsNull() {
		getOrElse(vendorRestriction);
		Collection<VendorRestriction> cvr = vendorRestrictionService.findVendorRestrictions(null).toCompletableFuture()
				.join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictions() {
		getOrElse(vendorRestriction);
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		Collection<VendorRestriction> cvr = vendorRestrictionService.findVendorRestrictions(criteriaMap)
				.toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictionsWithNull() {
		getAllVendorRestrictionWithNull();
		Mockito.doAnswer(invocation -> {
			String vendorRestriction = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(vendorRestriction));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		Collection<VendorRestriction> cvr = vendorRestrictionService.findVendorRestrictions(criteriaMap)
				.toCompletableFuture().join();
		Assert.assertNotNull(cvr);
	}

	@Test
	public void testFindVendorRestrictionsWithException() {
		getAllVendorRestrictionWithException();
		Mockito.doAnswer(invocation -> {
			String vendorRestriction = (String) ((Supplier) invocation.getArguments()[1]).get();
			return CompletableFuture.completedFuture(JsonUtils.toJson(vendorRestriction));
		}).when(cacheMock).getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
		Map<String, List<String>> criteriaMap = new HashMap<>();
		criteriaMap.put("cacheKeySuffix", new ArrayList<String>());
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.findVendorRestrictions(criteriaMap)
				.toCompletableFuture().join();
	}

	@Test
	public void testDeleteVendorRestrictionById() throws ArkaValidationException {
		Mockito.doReturn(true).when(vendorRestrictionRepoMock).deleteOneById(Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorRestrictionService.deleteVendorRestrictionById(objectId.toHexString());
		Assert.assertEquals(true, isDeleted);
	}

	@Test
	public void testDeleteVendorRestrictionById2() throws ArkaValidationException {
		Mockito.doReturn(false).when(vendorRestrictionRepoMock).deleteOneById(Matchers.anyString());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorRestrictionService.deleteVendorRestrictionById(null);
		Assert.assertEquals(false, isDeleted);
	}

	@Test
	public void testDeleteVendorRestrictions() throws ArkaValidationException {
		Mockito.doReturn(true).when(vendorRestrictionRepoMock).deleteMany(Matchers.anyMap());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorRestrictionService.deleteVendorRestrictions(Collections.emptyMap());
		Assert.assertEquals(true, isDeleted);
	}

	@Test
	public void testDeleteVendorRestrictions2() throws ArkaValidationException {
		Mockito.doReturn(false).when(vendorRestrictionRepoMock).deleteMany(Matchers.anyMap());
		inValidateCacheWithPrefix();
		boolean isDeleted = vendorRestrictionService.deleteVendorRestrictions(null);
		Assert.assertEquals(false, isDeleted);
	}

	@Test
	public void testGetVendorRestrictionCount() {
		Mockito.doReturn(1L).when(vendorRestrictionRepoMock).count();
		long count = vendorRestrictionService.getVendorRestrictionCount();
		Assert.assertEquals(1L, count);
	}

	@Test
	public void testGetVendorRestrictionCount2() throws ArkaValidationException {
		Mockito.doReturn(1L).when(vendorRestrictionRepoMock).count(Matchers.anyMap());
		long count = vendorRestrictionService.getVendorRestrictionCount(Collections.emptyMap());
		Assert.assertEquals(1L, count);
	}

	public void getOrElse(VendorRestriction vendorRestrictn) {
		Mockito.doReturn(CompletableFuture.completedFuture(JsonUtils.toJson(vendorRestrictn))).when(cacheMock)
				.getOrElse(Matchers.anyString(), Matchers.any(Supplier.class));
	}

	public void inValidateCacheWithPrefix() {
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(Matchers.anyString());
	}

	public void saveVendorRestrictionMock(Long count) throws ArkaValidationException {
		Mockito.doReturn(count).when(vendorRestrictionRepoMock).count(Matchers.anyMap());
		insertMock();
	}
	
	public void saveVendorRestrictionCountWithException() throws ArkaValidationException {
		Mockito.doThrow(ArkaValidationException.class).when(vendorRestrictionRepoMock).count(Matchers.anyMap());
		insertMock();
	}

	public void insertMock() throws ArkaValidationException {
		Mockito.doNothing().when(vendorRestrictionRepoMock).insert(Matchers.any(VendorRestriction.class));
		Mockito.doReturn(CompletableFuture.completedFuture(Json.newObject())).when(cacheMock)
				.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
	}

	public void saveVendorRestrictionCheckException(VendorRestriction vendorRestrictn) throws ArkaValidationException {
		expectedException.expect(ArkaValidationException.class);
		vendorRestrictionService.saveVendorRestriction(vendorRestrictn);
		verify(vendorRestrictionRepoMock).insert(Matchers.any(VendorRestriction.class));
	}
	
	private void getVendorRestrictionById(){
		Mockito.doReturn(vendorRestriction).when(vendorRestrictionRepoMock).findOneById(objectId.toHexString());
	}

	private void getVendorRestrictionByIdWithNull() {
		Mockito.doReturn(null).when(vendorRestrictionRepoMock).findOneById(Matchers.anyString());
	}

	private void getVendorRestrictionByIdWithException() {
		Mockito.doThrow(ArkaValidationException.class).when(vendorRestrictionRepoMock)
				.findOneById(Matchers.anyString());
	}

	private void getAllVendorRestrictionWithNull() {
		Mockito.doReturn(null).when(vendorRestrictionRepoMock).findAll(Matchers.anyMap());
	}

	private void getAllVendorRestrictionWithException() {
		Mockito.doThrow(ArkaValidationException.class).when(vendorRestrictionRepoMock).findAll(Matchers.anyMap());
	}
}
