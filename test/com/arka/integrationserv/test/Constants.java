/**
 * 
 */
package com.arka.integrationserv.test;

import org.bson.types.ObjectId;

/**
 * @author himagirinathan
 *
 */
public interface Constants {
	public static final String BASE_TEST = "base_test";
	public static final ObjectId objectId = new ObjectId("572759b690e5461f00a0a8f9");
	public static final ObjectId notPresentObjectId = new ObjectId("572759b690e5461f00a0a8f8");

	public static final Long createdUser = 1L;
	public static final Long updatedUser = 2L;
	public static final Long proposalId = 1L;

	public static final String id = "EqTraX5StRb4FiQjWzL90g";

	public static final Long categoryId = 1L;
	public static final String vendorId = "EqTraX5StRb4FiQjWzL90g";
	public static final String productCode = "product_code";
	public static final String productName = "product_name";
	public static final String productImgUrl = "http://cdn.arkainsure.com/img/img.png";
	public static final String productDocUrl =  "http://cdn.arkainsure.com/doc/doc.png";
	
	
	
	
}
