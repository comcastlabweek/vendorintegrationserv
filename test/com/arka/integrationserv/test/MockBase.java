package com.arka.integrationserv.test;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.services.VendorIntegrationServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.databind.JsonNode;

public class MockBase extends Base implements Constants {

	@Mock
	public VendorProdMgmtServ vendorProdMgmtServMock;

	@Mock
	public VendorIntegrationServ vendorIntegrationServMock;

	@Mock
	public MandatoryParams mandatoryParamsMock;

	@Mock
	public DBUtil dbUtilMock;

	@Mock
	public PropUtils propUtilsMock;

	public void mockGetCategoryAttributeList(JsonNode res) {
		Mockito.doReturn(CompletableFuture.completedFuture(res)).when(vendorProdMgmtServMock)
				.getCategoryAttributeList(Matchers.anyMap(), Matchers.anyMap());
	}

	public void mockGetProduct(JsonNode res) {
		Mockito.doReturn(CompletableFuture.completedFuture(res)).when(vendorProdMgmtServMock)
				.getProduct(Matchers.anyString(), Matchers.anyMap(), Matchers.anyMap());
	}

	public void mockGetProductJson(JsonNode res) {
		Mockito.doReturn(res).when(dbUtilMock).getProductJson(Matchers.anyString());
	}
	public void mockGetCatgeoryAttributeFieldNameMap(Map res) {
		Mockito.doReturn(res).when(dbUtilMock).getCatgeoryAttributeFieldNameMap(Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean());
	}
	

}
