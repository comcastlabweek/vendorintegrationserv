
name := "vendorintegrationserv"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.8"

val cxfVersion: String = "3.1.12"

libraryDependencies ++= Seq(
  cache,
  jdbc,
  javaWs,
  "org.mongodb" % "mongo-java-driver" % "3.2.2",
  "commons-io" % "commons-io" % "2.4",
  "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.6.1",
  "biz.paluch.redis" % "lettuce" % "4.2.1.Final",
  "org.apache.commons" % "commons-pool2" % "2.4.2",
  "org.mockito" % "mockito-all" % "1.9.5" % "test",  
  "org.apache.poi" % "poi" % "3.14",
  "org.apache.poi" % "poi-ooxml" % "3.14",
  "org.apache.commons" % "commons-collections4" % "4.1",
  "ch.qos.logback" % "logback-classic" % "1.1.7",
  "net.logstash.logback" % "logstash-logback-encoder" % "4.7",
  "com.github.fge" % "json-patch" % "1.9",
  //logger
  "org.codehaus.janino" % "janino" % "3.0.6",
  "org.codehaus.groovy" % "groovy-all" % "2.4.8",
  "org.apache.cxf" % "cxf-rt-frontend-jaxws" % cxfVersion,
  "org.apache.cxf" % "cxf-rt-transports-http" % cxfVersion,
  "org.springframework" % "spring-context" % "5.0.0.RELEASE",
  "commonlib" % "commonlib" % "1.0-SNAPSHOT"
)

unmanagedSourceDirectories in Compile += baseDirectory.value/"vendor"
// Compile the project before generating Eclipse files, so that generated .scala or .class files for views and routes are present
EclipseKeys.preTasks := Seq(compile in Compile)
EclipseKeys.projectFlavor := EclipseProjectFlavor.Java           // Java project. Don't expect Scala IDE
EclipseKeys.createSrc := EclipseCreateSrc.ValueSet(EclipseCreateSrc.ManagedClasses, EclipseCreateSrc.ManagedResources)  // Use .class files instead of generated .scala files for views and routes
