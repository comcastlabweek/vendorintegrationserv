package com.arka.integrationserv.models.dataconfiguration.repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.base.repository.MongoBaseRepository;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.repository.impl.DataConfigurationRepositoryImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(DataConfigurationRepositoryImpl.class)
public interface DataConfigurationRepository extends MongoBaseRepository<DataConfiguration> {

	// File Upload Supported methods

	public void addVendorUploadedFilesIntoCollection(List<Map<String, Object>> listDoc, String tempCollectionName,
			DataConfiguration dataConfig);

	public boolean createTempFromOriginalCollection(DataConfiguration dataConfig);

	public boolean createTempCollection(DataConfiguration dataconfig, boolean isHasMapped)
			throws ArkaValidationException;

	public boolean createTempCollectionEntryInDataConfig(DataConfiguration dataconfig) throws ArkaValidationException;

	public boolean updateCurrentVersionandHistory(DataConfiguration currVendorDataConfig)
			throws ArkaValidationException;

	// Collection mapping Supported methods

	public Collection<String> getCollectionFields(String collectionName);

	public Map<String, Object> findOneByIdFromCollection(String collectionName, String id);

	public Map<String, Object> findOneAndUpdateByIdFromCollection(String collectionName, String id,
			Map<String, Object> update);

	public List<HashMap<String, Object>> findFromCollection(String collectionName, Map<String, Object> query);

	public long getCollectionRecordCount(String collectionName, Map<String, Object> query);

	public Document searchFirst(String collectionName, Document searchQuery, Bson orderBy,
			Bson includeFields);

}
