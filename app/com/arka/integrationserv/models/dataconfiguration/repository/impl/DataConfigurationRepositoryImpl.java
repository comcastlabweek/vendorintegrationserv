package com.arka.integrationserv.models.dataconfiguration.repository.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.base.constants.BaseModelConstant;
import com.arka.common.models.base.repository.impl.MongoBaseRepositoryImpl;
import com.arka.common.models.tag.Tag;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.DataConfigurationField;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationConstant;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationDynamicCollectionField;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationErrorMsg;
import com.arka.integrationserv.models.dataconfiguration.constants.MappingStatus;
import com.arka.integrationserv.models.dataconfiguration.repository.DataConfigurationRepository;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.IndexModel;

public class DataConfigurationRepositoryImpl extends MongoBaseRepositoryImpl<DataConfiguration>
		implements DataConfigurationRepository {

	public DataConfigurationRepositoryImpl() {
		super(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
	}

	// File Upload Supported method

	@Override
	public void addVendorUploadedFilesIntoCollection(List<Map<String, Object>> listDoc, String tempCollectionName,
			DataConfiguration dataConfig) {
		MongoCollection<Document> tempCollection = getMongoCollection(tempCollectionName);
		List<Document> docList = listDoc.parallelStream().map(doc -> new Document(doc)).collect(Collectors.toList());
		tempCollection.insertMany(docList);

		if (!dataConfig.isHasMapping() && dataConfig.getTempMappingCollections() != null
				&& dataConfig.getTempMappingCollections().size() == 1) {
			String version = dataConfig.getCurrentVersion();
			docList.parallelStream().forEach(doc -> {
				doc.put(DataConfigurationDynamicCollectionField.VERSION.value(), version);
			});
			MongoCollection<Document> originalCollection = getMongoCollection(dataConfig.getCollectionName());
			originalCollection.insertMany(docList);
		}
	}

	// File Upload Supported method

	@Override
	public boolean createTempCollection(DataConfiguration dataconfig, boolean isHasMapped)
			throws ArkaValidationException {
		boolean updateOne = false;
		if (dataconfig != null) {
			long currentTimeMillis = System.currentTimeMillis();
			String newlyCreatedTempCollection = "temp_mapping_" + dataconfig.getCollectionName() + "_"
					+ currentTimeMillis;
			if (!isHasMapped && dataconfig.getTempMappingCollections() == null) {
				List<String> versionHistory = dataconfig.getVersionHistory();
				if (versionHistory == null || versionHistory.isEmpty()) {
					versionHistory = new ArrayList<>();
				}
				versionHistory.add("" + currentTimeMillis);
				dataconfig.setCurrentVersion("" + currentTimeMillis);
				dataconfig.setVersionHistory(versionHistory);
			}

			List<String> tempMappingCollections = dataconfig.getTempMappingCollections();
			if (tempMappingCollections == null) {
				tempMappingCollections = new ArrayList<>();
			}
			if (!tempMappingCollections.contains(newlyCreatedTempCollection)) {
				tempMappingCollections.add(newlyCreatedTempCollection);
			} else {
				return false;
			}
			dataconfig.setTempMappingCollections(tempMappingCollections);

			// createCollection(dataconfig, true);
			updateOne = updateOne(dataconfig, true);

		}
		return updateOne;
	}

	// File Upload Supported methods

	@Override
	public boolean createTempCollectionEntryInDataConfig(DataConfiguration dataconfig) throws ArkaValidationException {
		// new Temp collection---starting
		DataConfiguration newTempCollection = dataconfig !=null ? dataconfig :new DataConfiguration();
		newTempCollection.setId(generateNewId());
		newTempCollection.setHasMapping(false);
		newTempCollection.setCurrentVersion(null);
		newTempCollection.setVersionHistory(null);
		String newlyCreatedTempCollection="";
		if(dataconfig !=null && dataconfig.getTempMappingCollections()!=null
				&& dataconfig.getTempMappingCollections().size()>0){
			newlyCreatedTempCollection = dataconfig.getTempMappingCollections()
					.get(dataconfig.getTempMappingCollections().size() - 1);
		}
		newTempCollection.setCollectionName(newlyCreatedTempCollection);
		insert(newTempCollection);
		return true;

		// closing
	}

	// File Upload Supported methods

	@Override
	public boolean updateCurrentVersionandHistory(DataConfiguration dataconfig) throws ArkaValidationException {
		boolean updateOne = false;
		if (!dataconfig.isHasMapping()) {
			String latestTempcollectionName = dataconfig.getTempMappingCollections()
					.get(dataconfig.getTempMappingCollections().size() - 1);

			String[] latestTempCollectionNameArray = latestTempcollectionName.split("_");
			String latestTempcollectionNameMillisTimeSeconds = latestTempCollectionNameArray[latestTempCollectionNameArray.length
					- 1];

			List<String> versionHistory = dataconfig.getVersionHistory();
			if (versionHistory == null || versionHistory.isEmpty()) {
				versionHistory = new ArrayList<>();
			}
			versionHistory.add("" + latestTempcollectionNameMillisTimeSeconds);
			dataconfig.setCurrentVersion("" + latestTempcollectionNameMillisTimeSeconds);
			dataconfig.setVersionHistory(versionHistory);
			try {
				updateOne = updateOne(dataconfig, false);
			} catch (ArkaValidationException e) {
				Map<String, List<String>> error = new HashMap<>();
				error.put(DataConfigurationConstant.CURRENT_VERSION.value(),
						Arrays.asList(DataConfigurationErrorMsg.DATA_CONFIGURATION_NOT_UPDATED.value()));
				throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(), error);
			}
			return updateOne;
		}
		return false;
	}

	// File Upload Supported methods

	@Override
	public boolean createTempFromOriginalCollection(DataConfiguration dataConfig) {
		if (dataConfig != null) {
			MongoCollection<Document> collection = getMongoCollection(dataConfig.getCollectionName());
			Document query = new Document(DataConfigurationDynamicCollectionField.VERSION.value(),
					dataConfig.getCurrentVersion());
			FindIterable<Document> findItr = collection.find(query);

			List<Document> copyFromOriginal = findItr.into(new ArrayList<Document>()).parallelStream().map(doc -> {
				if (dataConfig.isHasMapping()) {
					doc.put(DataConfigurationDynamicCollectionField.FOR_MAPPING.value(), false);
				}
				return doc;
			}).collect(Collectors.toList());

			if (copyFromOriginal != null && !copyFromOriginal.isEmpty()) {
				String latestcollectioName = dataConfig.getTempMappingCollections()
						.get(dataConfig.getTempMappingCollections().size() - 1);
				List<String> collections = getAllMongoCollections();
				if (collections.contains(latestcollectioName)) {
					// Todo:
					if (dataConfig.isHasMapping()) {
						getMongoCollection(latestcollectioName).insertMany(copyFromOriginal);
					}
					manageMappedandUnmappedTempCol(dataConfig);
					return true;
				}
			}
		}

		return false;
	}

	// File Upload Supported private method

	private void manageMappedandUnmappedTempCol(DataConfiguration dataConfig) {

		String latestTempcollectionName = dataConfig.getTempMappingCollections()
				.get(dataConfig.getTempMappingCollections().size() - 1);

		String[] latestTempCollectionNameArray = latestTempcollectionName.split("_");
		String latestTempcollectionNameMillisTimeSeconds = latestTempCollectionNameArray[latestTempCollectionNameArray.length
				- 1];

		MongoCollection<Document> mongoCollection = getMongoCollection(latestTempcollectionName);

		List<String> distinctHashList = mongoCollection
				.distinct(DataConfigurationDynamicCollectionField._HASH.value(), String.class)
				.into(new ArrayList<String>());

		distinctHashList.forEach(hashcode -> {
			List<Document> hashFindDocs = mongoCollection
					.find(new Document(DataConfigurationDynamicCollectionField._HASH.value(), hashcode))
					.into(new ArrayList<>());
			hashFindDocs.forEach(doc -> {
				if (dataConfig.isHasMapping()) {

					if (hashFindDocs.size() == 1
							&& doc.containsKey(DataConfigurationDynamicCollectionField.MAPPED_ID.value())
							&& doc.get(DataConfigurationDynamicCollectionField.MAPPED_ID.value()) != null) {
						doc.put(DataConfigurationDynamicCollectionField.MAPPING_STATUS.value(),
								MappingStatus.MISSING.value());
						doc.put(DataConfigurationDynamicCollectionField.FOR_MAPPING.value(), true);
					} else if (hashFindDocs.size() > 1
							&& doc.containsKey(DataConfigurationDynamicCollectionField.MAPPED_ID.value())
							&& doc.get(DataConfigurationDynamicCollectionField.MAPPED_ID.value()) != null) {
						doc.put(DataConfigurationDynamicCollectionField.MAPPING_STATUS.value(),
								MappingStatus.MAPPED.value());
						doc.put(DataConfigurationDynamicCollectionField.FOR_MAPPING.value(), true);
					} else if (hashFindDocs.size() >= 1
							&& (!doc.containsKey(DataConfigurationDynamicCollectionField.MAPPED_ID.value())
									|| doc.get(DataConfigurationDynamicCollectionField.MAPPED_ID.value()) == null)) {
						doc.put(DataConfigurationDynamicCollectionField.MAPPING_STATUS.value(),
								MappingStatus.NEW.value());
						doc.put(DataConfigurationDynamicCollectionField.FOR_MAPPING.value(), true);
						getMongoCollection(latestTempcollectionName).updateOne(
								new Document(BaseModelConstant._ID.value(), doc.get(BaseModelConstant._ID.value())),
								new Document("$set", doc));
						return;
					} else {
						doc.put(DataConfigurationDynamicCollectionField.FOR_MAPPING.value(), false);
					}
					getMongoCollection(latestTempcollectionName).updateOne(
							new Document(BaseModelConstant._ID.value(), doc.get(BaseModelConstant._ID.value())),
							new Document("$set", doc));
				} else {
					if (hashFindDocs.size() == 1 && doc.containsValue(dataConfig.getCurrentVersion())) {
						doc.remove(DataConfigurationDynamicCollectionField.VERSION.value());
						doc.put(DataConfigurationDynamicCollectionField.VERSION.value(),
								latestTempcollectionNameMillisTimeSeconds);
						getMongoCollection(latestTempcollectionName).updateOne(
								new Document(BaseModelConstant._ID.value(), doc.get(BaseModelConstant._ID.value())),
								new Document("$set", doc));
					} else if (hashFindDocs.size() == 1 && !doc.containsValue(dataConfig.getCurrentVersion())) {
						doc.remove(DataConfigurationDynamicCollectionField.VERSION.value());
						doc.put(DataConfigurationDynamicCollectionField.VERSION.value(),
								latestTempcollectionNameMillisTimeSeconds);
						getMongoCollection(latestTempcollectionName).updateOne(
								new Document(BaseModelConstant._ID.value(), doc.get(BaseModelConstant._ID.value())),
								new Document("$set", doc));
					} else if (hashFindDocs.size() > 1 && !doc.containsKey("version")) {
						doc.put(DataConfigurationDynamicCollectionField.VERSION.value(),
								latestTempcollectionNameMillisTimeSeconds);
						getMongoCollection(latestTempcollectionName).updateOne(
								new Document(BaseModelConstant._ID.value(), doc.get(BaseModelConstant._ID.value())),
								new Document("$set", doc));
					} else {
						getMongoCollection(latestTempcollectionName).deleteOne(doc);
					}

				}

			});

		});

		// TODO:check
		if (!dataConfig.isHasMapping() && dataConfig.getTempMappingCollections().size() != 1) {
			List<Document> movedToOriginal = getMongoCollection(latestTempcollectionName).find().into(new ArrayList<>())
					.parallelStream().map(document -> {
						document.remove(BaseModelConstant._ID.value());
						return document;
					}).collect(Collectors.toList());
			getMongoCollection(dataConfig.getCollectionName()).insertMany(movedToOriginal);
		}
	}

	// Override General Method

	@Override
	public void insert(DataConfiguration dataConfig) throws ArkaValidationException {
		setFieldId(dataConfig);
		super.insert(dataConfig);
		createCollection(dataConfig, false);
	}

	// Override General Method

	@Override
	public boolean updateOne(DataConfiguration dataConfiguration) throws ArkaValidationException {
		setFieldId(dataConfiguration);
		String _id = dataConfiguration.getId();
		String existingCollectionName = findOneById(_id).getCollectionName();
		if (isCollectionPresent(existingCollectionName)) {
			deleteCollection(existingCollectionName);
			boolean updated = super.updateOne(dataConfiguration);
			createCollection(dataConfiguration);
			return updated;
		} else {
			Map<String, List<String>> error = new HashMap<>();
			error.put(DataConfigurationConstant.COLLECTION_NAME.value(),
					Arrays.asList(DataConfigurationErrorMsg.COLLECTION_CONTAIN_DATA.value()));
			throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(), error);
		}
	}

	// Data configuration create collection method while save and update

	private boolean createCollection(DataConfiguration dataConfiguration) throws ArkaValidationException {
		String newCollectionName = dataConfiguration.getCollectionName();
		List<String> collections = getAllMongoCollections();
		if (!collections.contains(newCollectionName)) {
			createNewCollection(dataConfiguration.getCollectionName());
			dataConfiguration.getDataConfigurationField().stream()
					.filter(field -> field.getIndex() == -1 || field.getIndex() == 1).forEach(toBeIndexed -> {
						Map<String, Integer> indexMap = new HashMap<>();
						indexMap.put(toBeIndexed.getFieldName(), toBeIndexed.getIndex());
						createIndexes(newCollectionName, indexMap);
					});
			return true;
		}
		Map<String, List<String>> error = new HashMap<>();
		error.put(DataConfigurationConstant.COLLECTION_NAME.value(),
				Arrays.asList(DataConfigurationErrorMsg.COLLECTION_CONTAIN_DATA.value()));
		throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(), error);
	}

	// Data configuration private updateOne method

	private boolean updateOne(DataConfiguration dataConfiguration, boolean createCollectionTemp)
			throws ArkaValidationException {

		setFieldId(dataConfiguration);
		String _id = dataConfiguration.getId();
		String existingCollectionName = findOneById(_id).getCollectionName();

		boolean updated = super.updateOne(dataConfiguration);
		if (createCollectionTemp && !getAllMongoCollections().contains(existingCollectionName)) {
			// TODO:Check
			createCollection(dataConfiguration, false);
			// createCollection(dataConfiguration, true);
		}
		return updated;
	}

	// Data configuration private

	private void createIndexes(String collectionName, Map<String, Integer> indexFields) {
		if (indexFields != null && !indexFields.isEmpty()) {
			List<IndexModel> indexes = new ArrayList<>();
			indexFields.forEach((field, index) -> {
				indexes.add(new IndexModel(new Document(field, index)));
			});

			// MongoCollection<Document> collection =
			// dbService.getDB().getCollection(collectionName);
			getMongoCollection(collectionName).createIndexes(indexes);
		}
	}

	// Collection mapping supported methods

	@Override
	public Collection<String> getCollectionFields(String collectionName) {
		Document firstDoc = getMongoCollection(collectionName).find().first();
		return firstDoc.keySet();
	}

	// Collection mapping supported methods

	@Override
	public Map<String, Object> findOneByIdFromCollection(String collectionName, String id) {
//		Document query = new Document();
//		query.append(_ID, new ObjectId(id));
		Document firstDoc = getMongoCollection(collectionName).find().first();
		return firstDoc;
	}

	// Collection mapping supported method

	@Override
	public Map<String, Object> findOneAndUpdateByIdFromCollection(String collectionName, String id,
			Map<String, Object> update) {
		return getMongoCollection(collectionName).findOneAndUpdate(
				new Document(BaseModelConstant._ID.value(), new ObjectId(id)),
				new Document("$set", new Document(update)));
	}

	// Collection mapping Supported method

	@Override
	public long getCollectionRecordCount(String collectionName, Map<String, Object> query) {
		MongoCollection<Document> collection = getMongoCollection(collectionName);
		if (query != null && !query.isEmpty()) {
			return getMongoCollection(collectionName).count(new Document(query));
		}
		return collection.count();
	}

	// Collection mapping Supported method

	@Override
	public List<HashMap<String, Object>> findFromCollection(String collectionName, Map<String, Object> query) {
		Document queryDoc = new Document(query);
		ArrayList<Document> list = getMongoCollection(collectionName).find(queryDoc).into(new ArrayList<>());
		List<HashMap<String, Object>> mapList = list.parallelStream().map(doc -> new HashMap<String, Object>(doc))
				.collect(Collectors.toList());
		return mapList;
	}

	// Data Configuration Supported private method

	private boolean createCollection(DataConfiguration dataConfiguration, boolean isTemp)
			throws ArkaValidationException {
		String newCollectionName;
		if (!isTemp) {
			newCollectionName = dataConfiguration.getCollectionName();
		} else {
			newCollectionName = dataConfiguration.getTempMappingCollections()
					.get(dataConfiguration.getTempMappingCollections().size() - 1);
		}
		List<String> collections = getAllMongoCollections();
		if (!collections.contains(newCollectionName)) {
			createNewCollection(newCollectionName);
			dataConfiguration.getDataConfigurationField().stream()
					.filter(field -> field.getIndex() == -1 || field.getIndex() == 1).forEach(toBeIndexed -> {
						Map<String, Integer> indexMap = new HashMap<>();
						indexMap.put(toBeIndexed.getFieldName(), toBeIndexed.getIndex());
						createIndexes(newCollectionName, indexMap);
					});
			return true;
		} else {
			// TODO:
			// Map<String, List<String>> error = new HashMap<>();
			// error.put(DataConfigurationConstant.COLLECTION_NAME.value(),
			// Arrays.asList(DataConfigurationErrorMsg.COLLECTION_CONTAIN_DATA.value()));
			// throw new
			// ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
			// error);
			return false;

		}
	}

	// Data Configuration Supported private method

	private void setFieldId(DataConfiguration dataConfig) {
		if (dataConfig.getDataConfigurationField() != null) {
			List<DataConfigurationField> fieldlst = dataConfig.getDataConfigurationField();
			fieldlst.forEach(field -> {
				if (field.getId() == null) {
					field.setId(generateNewId());
					field.setCreatedTime(LocalDateTime.now());
				} else {
					field.setUpdatedTime(LocalDateTime.now());
				}
			});
			List<Tag> tagList = dataConfig.getTags();
			if (tagList != null) {
				tagList.forEach(field -> {
					if (field.getId() == null) {
						field.setId(generateNewId());
						field.setCreatedTime(LocalDateTime.now());
					} else {
						field.setUpdatedTime(LocalDateTime.now());
					}
				});
			}

		}
	}

	@Override
	public Document searchFirst(String collectionName, Document searchQuery, Bson orderBy,
			Bson includeFields) {
		Document countDoc = null;
		try {
			countDoc = dbService.getCollection(collectionName).find(searchQuery).sort(orderBy).projection(includeFields).first();
		} catch (RuntimeException e) {
		Map<String, List<String>> errorMap = new LinkedHashMap<>();
		errorMap.put("error", Arrays.asList(e.getMessage()));
		throw new ArkaValidationException("Can't aggregate Count", errorMap);
		}
		return countDoc;
		
	}

}
