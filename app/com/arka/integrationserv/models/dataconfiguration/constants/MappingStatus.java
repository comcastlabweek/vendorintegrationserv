package com.arka.integrationserv.models.dataconfiguration.constants;

public enum MappingStatus {

	MISSING("missing"),
	
	MAPPED("mapped"),
	
	NEW("new");
	
	private String value;
	
	private MappingStatus(String value){
		this.value = value;
	}
	
	public String value(){
		return value;
	}
	
}
