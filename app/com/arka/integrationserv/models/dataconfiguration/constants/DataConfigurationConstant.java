package com.arka.integrationserv.models.dataconfiguration.constants;

public enum DataConfigurationConstant {

	ID("_id"),

	PRODUCT_ID("product_id"),

	VENDOR_ID("vendor_id"),

	DATA_CONFIGURATION_FIELD("data_configuration_field"),

	CATALOG_MODEL_RESTRICTION_ID("catalog_model_restriction_id"),

	COLLECTION_NAME("collection_name"),

	TAGS("tags"),

	HAS_MAPPING("has_mapping"),

	SEARCHABLE_RESTRICTION_FIELDS_FOR_MAPPING("searchable_restriction_fields_for_mapping"),

	TEMP_MAPPING_COLLECTIONS("temp_mapping_collections"),

	VERSION_HISTORY("version_history"),

	CURRENT_VERSION("current_version"),

	STATUS("status");

	private String value;

	private DataConfigurationConstant(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	// @JsonProperty(value = "data_configuration_field")
	// private List<DataConfigurationField> dataConfigurationField;

}
