package com.arka.integrationserv.models.dataconfiguration.constants;

public enum DataConfigurationDynamicCollectionField {
	
	VERSION("version"),
	
	FOR_MAPPING("for_mapping"),
	
	MAPPED_ID("mapped_id"),
	
	MAPPING_STATUS("mapping_status"),
	
	_HASH("_hash");
	
	private String value;
	
	private DataConfigurationDynamicCollectionField(String value){
		this.value = value;
	}
	
	public String value(){
		return value;
	}

}
