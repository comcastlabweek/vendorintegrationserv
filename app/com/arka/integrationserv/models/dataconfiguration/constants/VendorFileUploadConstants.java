package com.arka.integrationserv.models.dataconfiguration.constants;

public enum VendorFileUploadConstants {

	VENDOR_FILE_UPLOAD_ERROR("error"),

	VENDOR_FILE_UPLOAD_PATH("vendor.file.upload.path"),

	COLLECTION_ID("collection-id"),

	PRODUCT_NAME("name"),

	VENDOR_NAME("name"),
	
	COLLECTION_NOT_ABLE_TO_CREATED("collection.not.able.to.created"),

	VENDOR_UPLOADED_FILE("vendorUploadedFile"),

	SUCCESS("Success"),

	ERROR("Error"),

	DATA_CONFIGURATION_ID_NULL("data.configuration.id.null"),

	FILE_NOT_FOUND_OR_COLLECTIONID_NOT_FOUND("file.not.found.or.collectionid.not.found"),
	
	FIELDNAME_NOT_MATCHED_WITH_ACTUAL_FIELDNAME("fieldname.not matched.with.actual.fieldname"),

	FILE_UPLOAD_ERROR("file.upload.error"),

	FILE_UPLOAD_SUCCESS("file.upload.success"),
	
	FILE_OBJECT_NULL("File.object.null"),
	
	TAB_SEPERATOR("\t"),
	
	ROW("row"),
	
	FILE_IS_EMPTY("file.is.empty");

	private String value;

	private VendorFileUploadConstants(String value) {
		this.value = value;
	}

	public String value() {
		return value;
	}

}
