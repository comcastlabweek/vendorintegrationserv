package com.arka.integrationserv.models.dataconfiguration.constants;

public enum DataConfigurationErrorMsg {

	DATA_CONFIG_ERROR("data.config.error"),

	COLLECTION_CONTAIN_DATA("collection.contain.data"),

	COLLECTION_NAME_NULL("collection.name.null"),

	COLLECTION_NAME_ALREADY_EXISTS("collection.name.already.exists"),

	DATA_CONFIGURATION_NOT_FOUND("data.configuration.not.found"),

	DATA_CONFIGURATION_NULL("data.configuration.null"),

	DATA_CONFIGURATION_FIELD_NULL("data.configuration.fields.null"),

	STATUS_NULL("data.configuration.status.null"),

	STATUS_INVALID("data.configuration.status.invalid"),

	DATA_CONFIGURATION_VALIDATION_FAILED("data.configuration.validation.failed"),

	DATA_CONFIGURATION_NOT_UPDATED("data.configuration.not.updated"),

	PATCH_OPERATION_NOT_SUPPORTED("patch.operation.not.supported"),

	DATA_CONFIGURATION_NOT_UPDATED_OP_OR_PATH_MISSING_IN_PATCH_DATA(
			"data.configuration.not.updated.OP.or.PATH.missing.in.PATCH.data");

	private String value;

	private DataConfigurationErrorMsg(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
