package com.arka.integrationserv.models.dataconfiguration.constants;

public enum DataConfigurationFieldConstant {
	
	FIELD_NAME( "field_name"),
	
	DATA_TYPE( "data_type"),
	
	INDEX("index"),
	
	WEIGHT("weight");
	
	private String value;

	private DataConfigurationFieldConstant(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
