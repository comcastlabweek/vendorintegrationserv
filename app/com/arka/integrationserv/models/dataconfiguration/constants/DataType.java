package com.arka.integrationserv.models.dataconfiguration.constants;

public enum DataType {
	INTEGER, STRING, CHAR, BOOLEAN, FLOAT, DOUBLE, LONG, SHORT, BYTE, DATE, ARRAYNODE
}
