package com.arka.integrationserv.models.dataconfiguration.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.service.impl.DataConfigurationServiceImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(DataConfigurationServiceImpl.class)
public interface DataConfigurationService<ID> {

	public static final String ALL_DATA_CONFIG_CACHE_KEY_SUFFIX = "vendor.catalog.integration.data.config.all";
	
	//Data Configuration General Methods

	public void saveDataConfiguration(DataConfiguration dataConfig) throws ArkaValidationException;

	public boolean updateDataConfiguration(DataConfiguration dataConfig) throws ArkaValidationException;


	public CompletionStage<DataConfiguration> findDataConfigurationById(ID id);

	public CompletionStage<Collection<DataConfiguration>> findDataConfigurations(Map<String, List<String>> criteriaMap);

	public boolean deleteDataConfigurationById(ID id) throws ArkaValidationException;

	public boolean deleteDataConfigurations(Map<String, List<String>> deleteQuery) throws ArkaValidationException;

	public boolean deleteTagFromDataConfiguration(ID dataConfigId, ID tagId) throws ArkaValidationException;

	public long getDataConfigurationCount();

	public long getDataConfigurationCount(Map<String, List<String>> criteriaMap) throws ArkaValidationException;
	
	//Data Configuration Supported methods
	
	public DataConfiguration updateDataConfigurationfromForm(DataConfiguration dataConfig)
			throws ArkaValidationException;
	
	public void checkUniquenessForCollectionName(String collectionName) throws ArkaValidationException;
	
	//File Upload Supported methods

	public boolean createTempCollection(DataConfiguration dataconfig, boolean isHasMapped)
			throws ArkaValidationException;

	public boolean createTempCollectionEntryInDataConfig(DataConfiguration dataconfig) throws ArkaValidationException;

	public void addVendorUploadedFilesIntoCollection(List<Map<String, Object>> docList, String tempCollectionName,
			DataConfiguration dataconfig);

	public boolean createTempFromOriginalCollection(DataConfiguration dataConfig);

	public boolean updateCurrentVersionandHistory(DataConfiguration currVendorDataConfig)
			throws ArkaValidationException;
	
	//Collection Mapping Supported Methods

	public Collection<String> getCollectionFields(String collectionName);

	public Map<String, Object> findOneByIdFromCollection(String collectionName, String id);

	public Map<String, Object> findOneAndUpdateByIdFromCollection(String collectionName, String id,
			Map<String, Object> update);

	public List<HashMap<String, Object>> findFromCollection(String collectionName, Map<String, Object> query);

	public long getCollectionRecordCount(String collectionName, Map<String, Object> query);

	public void insertMany(String collectionName, List<Map<String, Object>> dataList);

	public void deleteMany(String collectionName, Map<String, Object> query);

}
