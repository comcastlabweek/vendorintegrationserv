package com.arka.integrationserv.models.dataconfiguration.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import org.apache.commons.collections4.MapUtils;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationConstant;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationErrorMsg;
import com.arka.integrationserv.models.dataconfiguration.repository.DataConfigurationRepository;
import com.arka.integrationserv.models.dataconfiguration.service.DataConfigurationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;

public class DataConfigurationServiceImpl implements DataConfigurationService<String> {

	@Inject
	private DataConfigurationRepository dataConfigRepo;

	@Inject
	private CacheService cache;

	// Data Configuration General Method

	@Override
	public void saveDataConfiguration(DataConfiguration dataConfiguration) throws ArkaValidationException {

		Map<String, List<String>> errors = validate(dataConfiguration, "new");
		if (MapUtils.isNotEmpty(errors)) {
			throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIGURATION_VALIDATION_FAILED.value(),
					errors);
		}
		dataConfiguration.setVersion(System.currentTimeMillis());
		dataConfigRepo.insert(dataConfiguration);
		cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + ""
				+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
	}

	// Data Configuration Supported Method

	@Override
	public DataConfiguration updateDataConfigurationfromForm(DataConfiguration dataConfig)
			throws ArkaValidationException {
		try {
			String _id = dataConfig.getId();
			JsonNode formdata = JsonUtils.toJson(dataConfig);
			DataConfiguration dataConfiguration = findDataConfigurationById(_id).toCompletableFuture().join();
			ObjectNode dcjson = (ObjectNode) JsonUtils.toJson(dataConfiguration);

			formdata.fields().forEachRemaining(field -> {
				// if (JsonUtils.isValidField(dcjson, field.getKey())) {
				dcjson.set(field.getKey(), field.getValue());
				// }
			});

			DataConfiguration modifieddataConfig = Json.fromJson(dcjson, DataConfiguration.class);
			modifieddataConfig.setId(_id);
			boolean updated = updateDataConfiguration(modifieddataConfig);

			if (updated) {
				return modifieddataConfig;
			} else {
				return dataConfiguration;
			}
		} catch (ArkaValidationException e) {
			throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIGURATION_NOT_UPDATED.value(),
					e.getErrors());
		}
	}

	// Data Configuration General Method

	@Override
	public boolean updateDataConfiguration(DataConfiguration dataConfiguration) throws ArkaValidationException {
		Map<String, List<String>> errors = validate(dataConfiguration, "edit");
		if (MapUtils.isNotEmpty(errors)) {
			throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIGURATION_VALIDATION_FAILED.value(),
					errors);
		}
		boolean updated = dataConfigRepo.updateOne(dataConfiguration);
		if (updated) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataConfiguration.getId());
		}
		return updated;
	}

	// Data Configuration General Method

	@Override
	public CompletionStage<DataConfiguration> findDataConfigurationById(String id) {
		return cache.getOrElse(
				CacheService.getCacheKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value(), null, id), () -> {
						return JsonUtils.stringify(dataConfigRepo.findOneById(id));
				}).thenApplyAsync(json -> {
					return JsonUtils.fromJson(json, DataConfiguration.class);
				});
	}

	// Data Configuration General Method

	@Override
	public CompletionStage<Collection<DataConfiguration>> findDataConfigurations(
			Map<String, List<String>> criteriaMap) {

		String cacheKeySuffix = "";
		if (criteriaMap == null || criteriaMap.isEmpty())
			cacheKeySuffix = ALL_DATA_CONFIG_CACHE_KEY_SUFFIX;

		return cache.getOrElse(CacheService.getCacheKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value(),
				criteriaMap, cacheKeySuffix), () -> {
						return JsonUtils.stringify(dataConfigRepo.findAll(criteriaMap));
				}).thenApplyAsync(dataConfigJson -> {
					return JsonUtils.jsonToList(dataConfigJson, DataConfiguration.class);
				});
	}

	// Data Configuration General Method

	@Override
	public boolean deleteDataConfigurationById(String id) throws ArkaValidationException {
		boolean Inactivated = dataConfigRepo.deleteOneById(id);
		if (Inactivated) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + id);
		}
		return Inactivated;
	}

	// Data Configuration General Method

	@Override
	public boolean deleteDataConfigurations(Map<String, List<String>> deleteQuery) throws ArkaValidationException {
		boolean Inactivated = dataConfigRepo.deleteMany(deleteQuery);
		if (Inactivated) {
			cache.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		}
		return Inactivated;
	}

	// Data Configuration General Method

	@Override
	public boolean deleteTagFromDataConfiguration(String dataConfigId, String tagId) throws ArkaValidationException {

		boolean inactivated = dataConfigRepo.deleteFromArray(dataConfigId, tagId,
				DataConfigurationConstant.TAGS.value());
		if (inactivated) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataConfigId);
		}
		return inactivated;

	}

	// Data Configuration General Method

	@Override
	public long getDataConfigurationCount() {
		return dataConfigRepo.count();
	}

	// Data Configuration General Method

	@Override
	public long getDataConfigurationCount(Map<String, List<String>> criteriaMap) throws ArkaValidationException {
		return dataConfigRepo.count(criteriaMap);
	}

	// Data Configuration Supported method

	@Override
	public void checkUniquenessForCollectionName(String collectionName) throws ArkaValidationException {
		Map<String, List<String>> errorMap = new HashMap<>();

		if (collectionName == null || collectionName.isEmpty()) {
			errorMap.put(DataConfigurationConstant.COLLECTION_NAME.value(),
					Arrays.asList(DataConfigurationErrorMsg.COLLECTION_NAME_NULL.value()));
			throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(), errorMap);
		} else {
			Map<String, List<String>> queryMap = new HashMap<>();
			queryMap.put(DataConfigurationConstant.COLLECTION_NAME.value(), Arrays.asList(collectionName));

				if (dataConfigRepo.count(queryMap) != 0) {
					errorMap.put(DataConfigurationConstant.COLLECTION_NAME.value(),
							Arrays.asList(DataConfigurationErrorMsg.COLLECTION_NAME_ALREADY_EXISTS.value()));
					throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(), errorMap);
				}
		}
	}

	// File Upload Supported methods

	@Override
	public boolean createTempCollection(DataConfiguration dataconfig, boolean isHasMapped)
			throws ArkaValidationException {
		boolean createTempCollection = dataConfigRepo.createTempCollection(dataconfig, isHasMapped);
		if (createTempCollection) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataconfig.getId());
		}
		return createTempCollection;
	}

	// File Upload Supported methods

	@Override
	public boolean createTempCollectionEntryInDataConfig(DataConfiguration dataconfig) throws ArkaValidationException {
		boolean createTempCollectionEntryInDataConfig = dataConfigRepo
				.createTempCollectionEntryInDataConfig(dataconfig);
		if (createTempCollectionEntryInDataConfig) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataconfig.getId());
		}
		return createTempCollectionEntryInDataConfig;
	}

	// File Upload Supported methods

	@Override
	public void addVendorUploadedFilesIntoCollection(List<Map<String, Object>> listDoc, String tempCollectionName,
			DataConfiguration dataconfig) {
		dataConfigRepo.addVendorUploadedFilesIntoCollection(listDoc, tempCollectionName, dataconfig);
		cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
		cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
				+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
		cache.inValidateCacheWithExactKey(
				VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataconfig.getId());
	}

	// File Upload Supported methods

	@Override
	public boolean createTempFromOriginalCollection(DataConfiguration dataConfig) {
		boolean createTempFromOriginalCollection = dataConfigRepo.createTempFromOriginalCollection(dataConfig);
		if (createTempFromOriginalCollection) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataConfig.getId());
		}
		return createTempFromOriginalCollection;
	}

	// File Upload Supported methods

	@Override
	public boolean updateCurrentVersionandHistory(DataConfiguration dataconfig) throws ArkaValidationException {
		boolean updateCurrentVersionandHistory = dataConfigRepo.updateCurrentVersionandHistory(dataconfig);
		if (updateCurrentVersionandHistory) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "."
					+ DataConfigurationService.ALL_DATA_CONFIG_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_DATA_CONFIGURATION.value() + "." + dataconfig.getId());
		}
		return updateCurrentVersionandHistory;
	}

	// Collection Mapping Supported Methods

	@Override
	public Collection<String> getCollectionFields(String collectionName) {
		return dataConfigRepo.getCollectionFields(collectionName);
	}

	// Collection Mapping Supported Methods

	@Override
	public Map<String, Object> findOneByIdFromCollection(String collectionName, String id) {
		return dataConfigRepo.findOneByIdFromCollection(collectionName, id);
	}

	// Collection Mapping Supported Methods

	@Override
	public Map<String, Object> findOneAndUpdateByIdFromCollection(String collectionName, String id,
			Map<String, Object> update) {
		return dataConfigRepo.findOneAndUpdateByIdFromCollection(collectionName, id, update);
	}

	// Collection Mapping Supported Methods

	@Override
	public List<HashMap<String, Object>> findFromCollection(String collectionName, Map<String, Object> query) {
		return dataConfigRepo.findFromCollection(collectionName, query);
	}

	// Collection Mapping Supported Methods

	@Override
	public long getCollectionRecordCount(String collectionName, Map<String, Object> query) {
		return dataConfigRepo.getCollectionRecordCount(collectionName, query);
	}

	// Collection Mapping Supported Methods

	@Override
	public void insertMany(String collectionName, List<Map<String, Object>> dataList) {

		dataConfigRepo.insertMany(collectionName, dataList);

	}

	// Collection Mapping Supported Methods

	@Override
	public void deleteMany(String collectionName, Map<String, Object> query) {
		dataConfigRepo.deleteMany(collectionName, query);
	}

	private Map<String, List<String>> validate(DataConfiguration dataConfiguration, String action)
			throws ArkaValidationException {
		Map<String, List<String>> errorMap = new LinkedHashMap<>();

		if (dataConfiguration == null) {
			errorMap.put(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
					Arrays.asList(DataConfigurationErrorMsg.DATA_CONFIGURATION_NULL.value()));
			return errorMap;
		}

		fillDataConfigurationCollectionNameError(errorMap, dataConfiguration, action);

		fillDataConfigurationFieldError(errorMap, dataConfiguration);

		fillDataConfigurationStatusError(errorMap, dataConfiguration);

		return errorMap;
	}

	private void fillDataConfigurationCollectionNameError(Map<String, List<String>> errorMap,
			DataConfiguration dataConfiguration, String action) throws ArkaValidationException {
		if (dataConfiguration.getCollectionName() == null || dataConfiguration.getCollectionName().trim().isEmpty()) {
			errorMap.put(DataConfigurationConstant.COLLECTION_NAME.value(),
					Arrays.asList(DataConfigurationErrorMsg.COLLECTION_NAME_NULL.value()));
		} else {

			if ("new".equalsIgnoreCase(action)) {

				Map<String, List<String>> query = new HashMap<>();
				if (dataConfiguration.getCollectionName() != null) {
					query.put(DataConfigurationConstant.COLLECTION_NAME.value(),
							Arrays.asList(dataConfiguration.getCollectionName()));
				}

				long count = dataConfigRepo.count(query);

				if (dataConfiguration.getId() != null) {
					if (count > 1)
						errorMap.put(DataConfigurationConstant.COLLECTION_NAME.value(),
								Arrays.asList(DataConfigurationErrorMsg.COLLECTION_NAME_ALREADY_EXISTS.value()));
				} else {
					if (count > 0)
						errorMap.put(DataConfigurationConstant.COLLECTION_NAME.value(),
								Arrays.asList(DataConfigurationErrorMsg.COLLECTION_NAME_ALREADY_EXISTS.value()));
				}
			}
		}
	}

	private void fillDataConfigurationFieldError(Map<String, List<String>> errorMap,
			DataConfiguration dataConfiguration) {
		if (dataConfiguration.getDataConfigurationField() == null
				|| dataConfiguration.getDataConfigurationField().isEmpty()) {
			errorMap.put(DataConfigurationConstant.DATA_CONFIGURATION_FIELD.value(),
					Arrays.asList(DataConfigurationErrorMsg.DATA_CONFIGURATION_FIELD_NULL.value()));
		}
	}

	private void fillDataConfigurationStatusError(Map<String, List<String>> errorMap,
			DataConfiguration dataConfiguration) {
		if (dataConfiguration.getStatus() == null) {
			errorMap.put(DataConfigurationConstant.STATUS.value(),
					Arrays.asList(DataConfigurationErrorMsg.STATUS_NULL.value()));
		} else if (!Status.ACTIVE.equals(dataConfiguration.getStatus())
				&& !Status.INACTIVE.equals(dataConfiguration.getStatus())) {
			errorMap.put(DataConfigurationConstant.STATUS.value(),
					Arrays.asList(DataConfigurationErrorMsg.STATUS_INVALID.value()));
		}
	}

}