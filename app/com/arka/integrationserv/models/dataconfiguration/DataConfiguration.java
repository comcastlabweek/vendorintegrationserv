package com.arka.integrationserv.models.dataconfiguration;

import java.io.Serializable;
import java.util.List;

import com.arka.common.models.base.BaseModel;
import com.arka.common.models.constant.Status;
import com.arka.common.models.tag.Tag;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DataConfiguration extends BaseModel<String> implements Serializable {

	private static final long serialVersionUID = -9153469673408259097L;

	@JsonProperty(value = "product_id")
	private String productId;

	@JsonProperty(value = "vendor_id")
	private String vendorId;

	@JsonProperty(value = "catalog_model_restriction_id")
	private String catalogModelRestrictionId;

	@JsonProperty(value = "collection_name")
	private String collectionName;

	@JsonProperty(value = "tags")
	private List<Tag> tags;

	@JsonProperty(value = "has_mapping")
	private boolean hasMapping;

	@JsonProperty(value = "data_configuration_field")
	private List<DataConfigurationField> dataConfigurationField;

	@JsonProperty(value = "searchable_restriction_fields_for_mapping")
	private List<String> searchableRestrictionFieldsForMapping;

	@JsonProperty(value = "temp_mapping_collections")
	private List<String> tempMappingCollections;

	@JsonProperty(value = "version_history")
	private List<String> versionHistory;

	@JsonProperty(value = "current_version")
	private String currentVersion;

	@JsonProperty(value = "status")
	private Status status;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getCatalogModelRestrictionId() {
		return catalogModelRestrictionId;
	}

	public void setCatalogModelRestrictionId(String catalogModelRestrictionId) {
		this.catalogModelRestrictionId = catalogModelRestrictionId;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public boolean isHasMapping() {
		return hasMapping;
	}

	public void setHasMapping(boolean hasMapping) {
		this.hasMapping = hasMapping;
	}

	public List<DataConfigurationField> getDataConfigurationField() {
		return dataConfigurationField;
	}

	public void setDataConfigurationField(List<DataConfigurationField> dataConfigurationField) {
		this.dataConfigurationField = dataConfigurationField;
	}

	public List<String> getSearchableRestrictionFieldsForMapping() {
		return searchableRestrictionFieldsForMapping;
	}

	public void setSearchableRestrictionFieldsForMapping(List<String> searchableRestrictionFieldsForMapping) {
		this.searchableRestrictionFieldsForMapping = searchableRestrictionFieldsForMapping;
	}

	public List<String> getTempMappingCollections() {
		return tempMappingCollections;
	}

	public void setTempMappingCollections(List<String> tempMappingCollections) {
		this.tempMappingCollections = tempMappingCollections;
	}

	public List<String> getVersionHistory() {
		return versionHistory;
	}

	public void setVersionHistory(List<String> versionHistory) {
		this.versionHistory = versionHistory;
	}

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "DataConfiguration [productId=" + productId + ", vendorId=" + vendorId + ", catalogModelRestrictionId="
				+ catalogModelRestrictionId + ", collectionName=" + collectionName + ", tags=" + tags + ", hasMapping="
				+ hasMapping + ", dataConfigurationField=" + dataConfigurationField
				+ ", searchableRestrictionFieldsForMapping=" + searchableRestrictionFieldsForMapping
				+ ", tempMappingCollections=" + tempMappingCollections + ", versionHistory=" + versionHistory
				+ ", currentVersion=" + currentVersion + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((catalogModelRestrictionId == null) ? 0 : catalogModelRestrictionId.hashCode());
		result = prime * result + ((collectionName == null) ? 0 : collectionName.hashCode());
		result = prime * result + ((currentVersion == null) ? 0 : currentVersion.hashCode());
		result = prime * result + ((dataConfigurationField == null) ? 0 : dataConfigurationField.hashCode());
		result = prime * result + (hasMapping ? 1231 : 1237);
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((searchableRestrictionFieldsForMapping == null) ? 0
				: searchableRestrictionFieldsForMapping.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((tempMappingCollections == null) ? 0 : tempMappingCollections.hashCode());
		result = prime * result + ((vendorId == null) ? 0 : vendorId.hashCode());
		result = prime * result + ((versionHistory == null) ? 0 : versionHistory.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataConfiguration other = (DataConfiguration) obj;
		if (catalogModelRestrictionId == null) {
			if (other.catalogModelRestrictionId != null)
				return false;
		} else if (!catalogModelRestrictionId.equals(other.catalogModelRestrictionId))
			return false;
		if (collectionName == null) {
			if (other.collectionName != null)
				return false;
		} else if (!collectionName.equals(other.collectionName))
			return false;
		if (currentVersion == null) {
			if (other.currentVersion != null)
				return false;
		} else if (!currentVersion.equals(other.currentVersion))
			return false;
		if (dataConfigurationField == null) {
			if (other.dataConfigurationField != null)
				return false;
		} else if (!dataConfigurationField.equals(other.dataConfigurationField))
			return false;
		if (hasMapping != other.hasMapping)
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (searchableRestrictionFieldsForMapping == null) {
			if (other.searchableRestrictionFieldsForMapping != null)
				return false;
		} else if (!searchableRestrictionFieldsForMapping.equals(other.searchableRestrictionFieldsForMapping))
			return false;
		if (status != other.status)
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (tempMappingCollections == null) {
			if (other.tempMappingCollections != null)
				return false;
		} else if (!tempMappingCollections.equals(other.tempMappingCollections))
			return false;
		if (vendorId == null) {
			if (other.vendorId != null)
				return false;
		} else if (!vendorId.equals(other.vendorId))
			return false;
		if (versionHistory == null) {
			if (other.versionHistory != null)
				return false;
		} else if (!versionHistory.equals(other.versionHistory))
			return false;
		return true;
	}

//	@Override
//	public String toString() {
//		return "DataConfiguration [productId=" + productId + ", vendorId=" + vendorId + ", catalogModelRestrictionId="
//				+ catalogModelRestrictionId + ", collectionName=" + collectionName + ", tags="
//				+ (tags != null ? tags.size() : null) + ", hasMapping=" + hasMapping + ", dataConfigurationField="
//				+ (dataConfigurationField != null ? dataConfigurationField.size() : null)
//				+ ", searchableRestrictionFieldsForMapping="
//				+ (searchableRestrictionFieldsForMapping != null ? searchableRestrictionFieldsForMapping.size() : null)
//				+ ", tempMappingCollections=" + (tempMappingCollections != null ? tempMappingCollections.size() : null)
//				+ ", versionHistory=" + (versionHistory != null ? versionHistory.size() : null) + ", currentVersion="
//				+ currentVersion + ", status=" + status + "]";
//	}
	

}
