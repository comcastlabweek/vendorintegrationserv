package com.arka.integrationserv.models.dataconfiguration;

import java.io.Serializable;

import com.arka.common.models.base.BaseModel;
import com.arka.integrationserv.models.dataconfiguration.constants.DataType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DataConfigurationField extends BaseModel<String> implements Serializable {

	private static final long serialVersionUID = -1955550062442987882L;

	@JsonProperty(value = "field_name")
	private String fieldName;

	@JsonProperty(value = "alias_name")
	private String aliasName;

	@JsonProperty(value = "data_type")
	private DataType dataType;

	@JsonProperty(value = "index")
	private Integer index;

	@JsonProperty(value = "weight")
	private Integer weight;

	@JsonProperty(value = "searchable_field")
	private Boolean searchableField;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}
	
	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Boolean getSearchableField() {
		return searchableField;
	}

	public void setSearchableField(Boolean searchableField) {
		this.searchableField = searchableField;
	}

	@Override
	public String toString() {
		return "DataConfigurationField [fieldName=" + fieldName + ", aliseName=" + aliasName + ", dataType=" + dataType
				+ ", index=" + index + ", weight=" + weight + ", searchableField=" + searchableField + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((aliasName == null) ? 0 : aliasName.hashCode());
		result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
		result = prime * result + ((fieldName == null) ? 0 : fieldName.hashCode());
		result = prime * result + ((index == null) ? 0 : index.hashCode());
		result = prime * result + ((searchableField == null) ? 0 : searchableField.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataConfigurationField other = (DataConfigurationField) obj;
		if (aliasName == null) {
			if (other.aliasName != null)
				return false;
		} else if (!aliasName.equals(other.aliasName))
			return false;
		if (dataType != other.dataType)
			return false;
		if (fieldName == null) {
			if (other.fieldName != null)
				return false;
		} else if (!fieldName.equals(other.fieldName))
			return false;
		if (index == null) {
			if (other.index != null)
				return false;
		} else if (!index.equals(other.index))
			return false;
		if (searchableField == null) {
			if (other.searchableField != null)
				return false;
		} else if (!searchableField.equals(other.searchableField))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}
}
