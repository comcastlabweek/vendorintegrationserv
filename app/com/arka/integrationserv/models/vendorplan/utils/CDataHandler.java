package com.arka.integrationserv.models.vendorplan.utils;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Node;

public class CDataHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Logger logger = LoggerFactory.getLogger(CDataHandler.class);
	@Override
	public void close(MessageContext context) {
	}

	@Override
	public boolean handleMessage(SOAPMessageContext soapMessage) {
		try {
			SOAPMessage message = soapMessage.getMessage();
			boolean isOutboundMessage = (Boolean) soapMessage.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			if (isOutboundMessage) {
				SOAPEnvelope envelope;
				envelope = message.getSOAPPart().getEnvelope();
				if (envelope != null) {
					SOAPBody body = envelope.getBody();
					if (body != null) {
						Node node = body.getFirstChild();
						if (null != node && "commBRIDGEFusionMOTOR".equalsIgnoreCase(node.getLocalName())) {
							node = node.getFirstChild();
							if (null != node && "strXdoc".equalsIgnoreCase(node.getLocalName())) {
								
								CDATASection createCDATASection = message.getSOAPPart().createCDATASection(node.getFirstChild().getTextContent());
								
								node.removeChild(node.getFirstChild());
								node.appendChild(createCDATASection);	
								
							}
						}
					}
				}

				message.saveChanges();
				System.out.println("------------------------------------------------------------------------------------");
				message.writeTo(System.out); // Line 3
				ByteArrayOutputStream baos = new ByteArrayOutputStream();  
				message.writeTo(baos);  
				logger.debug("------------------------------------------------------------------------------------" +baos.toString());  
			}	
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
		return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext soapMessage) {
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		return Collections.EMPTY_SET;
	}
}