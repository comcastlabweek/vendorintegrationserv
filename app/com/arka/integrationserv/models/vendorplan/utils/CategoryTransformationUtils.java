package com.arka.integrationserv.models.vendorplan.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.integration.constants.VendorCategoryAttributeFields;
import com.arka.common.models.tag.VendorObjectPatch;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.services.VendorIntegrationServ;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorplan.constants.OpEnum;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorrestriction.constant.ConstraintType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.google.inject.Inject;

import play.Logger;
import play.libs.Json;

public class CategoryTransformationUtils {

	@Inject
	DBUtil dbUtil;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	VendorIntegrationServ vendorIntegrationServ;

	public static ObjectMapper mapper;

	Map<String, JsonNode> categoryAttributeCodeMap;

	private static final Pattern arrayToAttributesPattern = Pattern.compile("^(.*?)\\s*\\[(.*?)\\s*=\\s*'(.*?)'\\s*\\]");
	public static final Pattern replacePlaceholder = Pattern.compile("\\{\\{(.*?)\\}\\}");
	public static final Pattern transformPlaceholder = Pattern.compile("\\[\\[(.*?)\\]\\]");
	public static final Pattern dateTransformPlaceholder = Pattern.compile("\\(\\((.*?)\\)\\)");
	public static final Pattern reverseNumberTransformPlaceholder = Pattern.compile("\\(\\((.*?)\\)\\)");

	private String GUID_GENERATED = "";
	DecimalFormat df = new DecimalFormat("########.##");
	static {
		mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

	}

	public List<PatchRequestVO> createRequestPatch(VendorRequest vendorRequest, String vendorObjectName) {
		List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
		Map<String, String> internalVendorAttributes = new HashMap<>();
		JsonNode vendorCategoryAttributeCodeJson = dbUtil
				.getVendorCategoryAttributesWithProductId(vendorRequest.getProductId(), vendorObjectName);

		categoryAttributeCodeMap = dbUtil.getCategoryAttributesIDMap(vendorRequest.getCategoryId(), Boolean.TRUE);
		if (JsonUtils.isValidField(vendorCategoryAttributeCodeJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)
				&& JsonUtils.isValidIndex(vendorCategoryAttributeCodeJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY), 0)) {
			vendorCategoryAttributeCodeJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(vcAttrItem -> {
				PatchRequestVO patchRequestVO;
				Object patchValue = null;
				VendorCategoryAttribute vcAttr = Json.fromJson(vcAttrItem, VendorCategoryAttribute.class);
				VendorObjectPatch vendorObjectPatch = AttributeUtil.retrieveVendorPatch(vcAttr.getVendorObjectPatches(),
						vendorObjectName);
				if (StringUtils.isEmpty(vendorObjectPatch.getPath())
						|| StringUtils.equalsIgnoreCase("INTERNAL", vendorObjectPatch.getPath())) {
					patchValue = populateValueOnDataType(vendorRequest, internalVendorAttributes, patchValue, vcAttr);
					internalVendorAttributes.put(vcAttr.getCode(), patchValue.toString());
					return;
				}
				patchValue = populateValueOnDataType(vendorRequest, internalVendorAttributes, patchValue, vcAttr);
				for (VendorObjectPatch vendorObjectPatch1 : vcAttr.getVendorObjectPatches()) {
					if (vendorObjectName.equalsIgnoreCase(vendorObjectPatch1.getVendorObject())) {
						patchRequestVO = new PatchRequestVO(OpEnum.getEnum(vendorObjectPatch1.getOperation()),
								vendorObjectPatch1.getPath(), patchValue);
						patchRequestVOList.add(patchRequestVO);
					}
				}
			});
		}
		return patchRequestVOList;

	}

	private Object populateValueOnDataType(VendorRequest vendorRequest,
			Map<String, String> internalVendorAttributes, Object patchValue,
			VendorCategoryAttribute vcAttr) {
		Matcher matchPattern = null;
		StringBuffer sb = null;
		String categoryAttributeValue = null;
		switch (vcAttr.getDataType()) {

			case ARRAY_MAP:
				categoryAttributeValue = getAssociatedCategoryAttributeValue(vcAttr, vendorRequest);
				categoryAttributeValue = categoryAttributeValue.replaceAll("[\\[\\]]", "");
				List<String> categoryAttributeValueitems = new ArrayList<>();
				;
				if (!categoryAttributeValue.isEmpty()) {
					categoryAttributeValueitems = Arrays.asList(categoryAttributeValue.split("\\s*,\\s*"));
				}
				sb = new StringBuffer();
				int index = 0;
				index = Integer.parseInt(vcAttr.getDataTypeTemplate().trim());
				if (!categoryAttributeValueitems.isEmpty() && categoryAttributeValueitems != null) {
					if (categoryAttributeValueitems.size() == 1) {
						index = 0;
					}
					if (categoryAttributeValueitems.size() == 3) { // incase of TN-11-2342 format skipping 2index
						if (index == 2) {
							patchValue = "";
						} else if (index == 3) {
							patchValue = categoryAttributeValueitems.get(--index);
						} else {
							patchValue = categoryAttributeValueitems.get(index);
						}
					} else {
						patchValue = categoryAttributeValueitems.get(index);
					}

				} else {
					patchValue = categoryAttributeValue;
				}
				break;
			case DERIVED_OBJECT:
				matchPattern = replacePlaceholder.matcher(vcAttr.getDataTypeTemplate());
				sb = new StringBuffer();
				while (matchPattern.find()) {
					String attrName = matchPattern.group(1);
					String replaced = "0";
					if (vendorRequest.isAttributePresent(attrName)) {
						if (AttributeUtil
								.isNumeric(vendorRequest.getUniqueInputQuoteForAttribute(attrName).getValue())) {
							replaced = df.format(
									vendorRequest.getUniqueInputQuoteForAttribute(attrName).getValueAsDouble());
						} else {
							replaced = vendorRequest.getUniqueInputQuoteForAttribute(attrName).getValue();
						}
					}
					matchPattern.appendReplacement(sb, replaced);
				}
				matchPattern.appendTail(sb);
				matchPattern = transformPlaceholder.matcher(sb.toString());
				sb = new StringBuffer();
				while (matchPattern.find()) {
					String attrName = matchPattern.group(1);
					if (internalVendorAttributes.containsKey(attrName)) {
						String replaced = internalVendorAttributes.get(attrName);
						matchPattern.appendReplacement(sb, replaced.trim());
					}
				}
				matchPattern.appendTail(sb);
				matchPattern = dateTransformPlaceholder.matcher(sb.toString());
				sb = new StringBuffer();
				while (matchPattern.find()) {
					String attrName = matchPattern.group(1);
					String[] attrNameArray = attrName.split("[,]");
					String replaced = vendorRequest.isAttributePresent(attrNameArray[0])
							? vendorRequest.getUniqueInputQuoteForAttribute(attrNameArray[0]).getValue()
							: "";
					if (attrNameArray[0].toLowerCase().contains("today")) {
						LocalDateTime now = LocalDateTime.now();
						DateTimeFormatter formatter = DateTimeFormatter.ofPattern(attrNameArray[1]);
						replaced = now.format(formatter);
					} else {
						replaced = vendorDateFormatConversion(replaced, attrNameArray[1]);
					}
					matchPattern.appendReplacement(sb, replaced);
				}
				matchPattern.appendTail(sb);
				try {
					if (StringUtils.isNotEmpty(sb.toString()))
						patchValue = mapper.readTree(sb.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			case DERIVED_CSV:
				final String[] attrNameList = vcAttr.getDataTypeTemplate().trim().split("\\s*,\\s*");
				List<String> derivedValues = new ArrayList<>();
				if (ArrayUtils.isNotEmpty(attrNameList)) {
					for (String attrName : attrNameList) {
						String replaced = vendorRequest.isAttributePresent(attrName)
								? vendorRequest.getUniqueInputQuoteForAttribute(attrName).getValue()
								: "";
						if (StringUtils.isNotEmpty(replaced)) {
							derivedValues.add(replaced);
						}
					}
				}
				patchValue = StringUtils.join(derivedValues, ",");
				break;
			case DATE:
				categoryAttributeValue = getAssociatedCategoryAttributeValue(vcAttr, vendorRequest);
				if (!vcAttr.getDataTypeTemplate().isEmpty()) {
					if (vcAttr.getDataTypeTemplate().toLowerCase().contains("date")) {
						patchValue = vendorDateFormatConversion(categoryAttributeValue,
								vcAttr.getDataTypeTemplate().replaceAll(".*\\(|\\).*", "").trim());
					} else if (vcAttr.getDataTypeTemplate().toLowerCase().contains("today")) {
						LocalDateTime now = LocalDateTime.now();
						DateTimeFormatter formatter = DateTimeFormatter
								.ofPattern(vcAttr.getDataTypeTemplate().replaceAll(".*\\(|\\).*", "").trim());
						patchValue = now.format(formatter);
					} else if (vcAttr.getDataTypeTemplate().toLowerCase().contains("year")) {
						patchValue = vendorDateFormatConversion(categoryAttributeValue,
								vcAttr.getDataTypeTemplate().replaceAll(".*\\(|\\).*", "").trim());
					} else if (vcAttr.getDataTypeTemplate().toLowerCase().contains("month")) {
						patchValue = vendorDateFormatConversion(categoryAttributeValue,
								vcAttr.getDataTypeTemplate().replaceAll(".*\\(|\\).*", "").trim());
					}

				} else {
					patchValue = categoryAttributeValue;
				}

				break;
			case GUID:
				if (GUID_GENERATED.isEmpty()) {
					GUID_GENERATED = AttributeUtil.generateGUID();
					patchValue = GUID_GENERATED;
				} else {
					patchValue = GUID_GENERATED;
				}
				break;
			case RANDOM:
				patchValue = AttributeUtil.generateRandom();
				break;
			default:
				categoryAttributeValue = getAssociatedCategoryAttributeValue(vcAttr, vendorRequest);
				if (vcAttr.getTransformToVendor() != null) {
					patchValue = doTransformaToVendor(vcAttr, vendorRequest, categoryAttributeValue);
				} else {
					patchValue = categoryAttributeValue;
				}
				break;
		}
		return patchValue;
	}

	public ObjectNode getValidationError(String errorKey, String errorValue) {
		ObjectNode errorJson = Json.newObject();
		errorJson.put(errorKey, errorValue);
		ObjectNode validationErrorJson = Json.newObject();
		validationErrorJson.set(ResponsePayLoad.ERROR, errorJson);
		return validationErrorJson;
	}

	public String doTransformaToVendor(VendorCategoryAttribute vcAttr, VendorRequest vendorRequest,
									   String categoryAttributeValue) {
		if (vcAttr.getTransformToVendor() == null || Strings.isNullOrEmpty(categoryAttributeValue)) {
			return categoryAttributeValue;
		}
		switch (vcAttr.getTransformToVendor()) {
			case MAP:
				return populateMapTransformative(vcAttr, categoryAttributeValue);
			case LOOKUP:
				return populateLookUpTransformative(vcAttr, vendorRequest, categoryAttributeValue);
			case ARRAY_JOIN_NO_DELIMITER:
				return populateArrayJoinTransformative(vcAttr, categoryAttributeValue);
			case VEHICLE_NUMBER_HYPHEN_DELIMITER:
				return populateVehicleNumberHypenTransformative(vcAttr, categoryAttributeValue);
			case FEET_TO_INCHES:
				return populateFeetInchesTransformative(vcAttr, categoryAttributeValue);
			case PERCENTAGE_TO_DECIMAL:
				return populatePercentageDecimalTransformative(vcAttr, categoryAttributeValue);
			default:
				return categoryAttributeValue;
		}
	}

	public String doTransformaToCategory(VendorCategoryAttribute vcAttr, String categoryAttributeValue) {

		switch (vcAttr.getTransformationToCatalog()) {
			case DISCOUNT:
				return "-" + categoryAttributeValue;
			default:
				break;
		}

		return null;

	}

	public String populateMapTransformative(VendorCategoryAttribute vcAttr, String categoryAttributeValue) {
		String vendorValue = "";
		JsonNode vendorRestrictionJson = dbUtil.fetchVendorRestriction(vcAttr.getCode(), vcAttr.getRestrictionId());
		if (vendorRestrictionJson == null) {
			return vendorValue;
		}
		ConstraintType vendorConstarintType = ConstraintType.valueOf(vendorRestrictionJson
				.get(VendorCategoryAttributeFields.VENDOR_RESTRICTION_CONSTARINT_TYPE.value()).asText());
		JsonNode constraintValueArr = vendorRestrictionJson
				.get(VendorCategoryAttributeFields.VENDOR_RESTRICTION_CONSTARINT_VALUE.value());
		ObjectNode vendorValueNode = Json.newObject();
		if (vendorConstarintType.equals(ConstraintType.SINGLESELECT)) {
			constraintValueArr.elements().forEachRemaining(constarintValue -> {
				String catalogValue = constarintValue.get(VendorQuoteConstants.CATALOG_VALUE.value()).asText();
				if (catalogValue.toLowerCase().equals(categoryAttributeValue.toLowerCase())) {
					vendorValueNode.set("Vendor_value", constarintValue);
				}

			});
			if (vendorValueNode.hasNonNull("Vendor_value")) {
				JsonNode constarintValue = vendorValueNode.get("Vendor_value");
				vendorValue = constarintValue.get(VendorQuoteConstants.VENDOR_VALUE.value()).asText();
			}
		} else if (vendorConstarintType.equals(ConstraintType.MAP)) {
			constraintValueArr.elements().forEachRemaining(constarintValue -> {
				// String catalogValue =
				// constarintValue.get(VendorQuoteConstants.CATALOG_VALUE.value()).asText();
				constarintValue.get("value").forEach(item -> {
					if (item.asText().equalsIgnoreCase(categoryAttributeValue)) {
						vendorValueNode.set("Vendor_value", constarintValue.get("key"));
					}
				});
			});
			if (vendorValueNode.hasNonNull("Vendor_value")) {
				vendorValue = vendorValueNode.get("Vendor_value").asText();
			}
		}

		return vendorValue;

	}

	public String populateLookUpTransformative(VendorCategoryAttribute vcAttr, VendorRequest vendorRequest,
											   String categoryAttributeValue) {
		JsonNode vendorRestrictionJson = dbUtil.fetchVendorRestriction(vcAttr.getCode(), vcAttr.getRestrictionId());
		if (vendorRestrictionJson == null) {
			// System.out.println(Json.prettyPrint(getValidationError(vendorAttributeCode,
			// FetchPlanMessage.VENDOR_RESTRICTION_ID_DOES_NOT_EXISTS.value())));
			//
			// return getValidationError(vendorAttributeCode,
			// FetchPlanMessage.VENDOR_RESTRICTION_ID_DOES_NOT_EXISTS.value());

		}
		ConstraintType vendorConstarintType = ConstraintType.valueOf(vendorRestrictionJson
				.get(VendorCategoryAttributeFields.VENDOR_RESTRICTION_CONSTARINT_TYPE.value()).asText());
		JsonNode constraintValueArr = vendorRestrictionJson
				.get(VendorCategoryAttributeFields.VENDOR_RESTRICTION_CONSTARINT_VALUE.value());
		AtomicBoolean isLookUpValueFound = new AtomicBoolean(false);
		AtomicReference<String> mappedLkpvalue = new AtomicReference<>();
		// case 1 : constraintType Field

		if (vendorConstarintType.equals(ConstraintType.FIELD)) {

			constraintValueArr.elements().forEachRemaining(constarintValue -> {
				String vendorDataConfigurationId = constarintValue.get(VendorQuoteConstants.COLLECTION_KEY.value())
						.asText();
				String valueField = constarintValue.get(VendorQuoteConstants.VALUE_FIELD.value()).asText();
				List<String> lookupField = new ArrayList<>();
				if (constarintValue.get(VendorQuoteConstants.LOOKUP_FIELD.value()).isArray()) {
					for (final JsonNode objNode : constarintValue.get(VendorQuoteConstants.LOOKUP_FIELD.value())) {
						lookupField.add(objNode.asText());
					}
				} else {
					lookupField.add(constarintValue.get(VendorQuoteConstants.LOOKUP_FIELD.value()).asText());
				}
				try {
					Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
					String collectionName = dbUtil.getvendorDataCollectionName(vendorDataConfigurationId);
					if (!collectionName.isEmpty() && collectionName != null) {
						Map<String, List<String>> queryStringMap = new HashMap<>();
						queryStringMap.put("cname", Arrays.asList(collectionName));
						if (lookupField.size() > 1) {
							for (String lkpfield : lookupField) {
								String lkpupvalue = lkpfield.replaceFirst("_", "-");
								String attributeValue = "";
								if (vendorRequest.isAttributePresent(lkpupvalue)) {
									attributeValue = vendorRequest.getUniqueInputQuoteForAttribute(lkpupvalue)
											.getValue();
								} else {
									attributeValue = vcAttr.getDefaultValue();
								}
								queryStringMap.put(lkpfield, Arrays.asList(attributeValue));
							}
						} else {
							queryStringMap.put(lookupField.get(0), Arrays.asList(categoryAttributeValue));
						}
						// System.out.println("Query Map for collection : " + queryStringMap);
						queryStringMap.putAll(mandatQP);
						try {
							JsonNode anyCollectionJson = dbUtil.getUnknownCollectionRecord(queryStringMap);
							JsonNode collectionData = getCollectionData(anyCollectionJson, lookupField, "");
							if (JsonUtils.isValidField(collectionData, valueField)) {
								isLookUpValueFound.set(true);
								mappedLkpvalue.set(collectionData.get(valueField).asText());

							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {
						// System.out.println("vendor Data : " +
						// Json.prettyPrint(vendorDataConfiguration));
					}

				} catch (Exception e) {
					Logger.error(e.getMessage());
					e.printStackTrace();
				}

			});

		}

		if (!isLookUpValueFound.get()) {
			// return getValidationError(vendorAttributeCode,
			// FetchPlanMessage.CANNOT_LOOK_UP_CATALOG_ATTRIBUTE_VALUE.value());
		} else {
			return mappedLkpvalue.get();
		}

		return null;

	}

	private JsonNode getCollectionData(JsonNode anyCollectionJson, List<String> lookupField, String lookupValue) {
		ObjectNode collectionLookUpJson = Json.newObject();
		if (JsonUtils.isValidField(anyCollectionJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
			JsonNode collectionJson = anyCollectionJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY);
			collectionJson.elements().forEachRemaining(collectionItemJson -> {
				if (JsonUtils.isValidField(collectionItemJson, VendorQuoteConstants.COLLECTION_DATA.value())) {
					JsonNode collectionDataJson = collectionItemJson.get(VendorQuoteConstants.COLLECTION_DATA.value());
					if (JsonUtils.isValidField(collectionDataJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
						JsonNode collectionDataArr = collectionDataJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY);
						collectionDataArr.elements().forEachRemaining(collectionData -> {
							collectionLookUpJson.set("data", collectionData);
						});
					} else {
						// System.out.println("inner item not present");
					}

				} else {
					// System.out.println("outer data not present" +
					// Json.prettyPrint(collectionJson));
				}

			});

		} else {
			System.out.println("outer item not present");
		}

		if (JsonUtils.isValidField(collectionLookUpJson, "data")) {
			return collectionLookUpJson.get("data");
		} else {
			return null;
		}

	}

	public String populateArrayJoinTransformative(VendorCategoryAttribute vcAttr, String categoryAttributeValue) {
		categoryAttributeValue = categoryAttributeValue.replaceAll("\\[|\\]", "").replace(",", "").replaceAll("\\s",
				"");
		return categoryAttributeValue;
	}

	public String populateVehicleNumberHypenTransformative(VendorCategoryAttribute vcAttr,
														   String categoryAttributeValue) {
		categoryAttributeValue = categoryAttributeValue.replaceAll("\\[|\\]", "").replace(",", "-").replaceAll("\\s",
				"");
		if (categoryAttributeValue.split("-").length == 3) {
			categoryAttributeValue = replaceCharAt(categoryAttributeValue, categoryAttributeValue.lastIndexOf("-"),
					"-");
		}
		return categoryAttributeValue;
	}

	public String replaceCharAt(String s, int pos, String c) {
		StringBuffer buf = new StringBuffer(s);
		buf.insert(pos, c);
		return buf.toString();
	}

	public String populateFeetInchesTransformative(VendorCategoryAttribute vcAttr, String categoryAttributeValue) {
		if (categoryAttributeValue != null && categoryAttributeValue != "") {
			Double categoryAttributeValueDouble = Double.parseDouble(categoryAttributeValue);
			double fractionalPart = categoryAttributeValueDouble % 1;
			int feet = (int) (categoryAttributeValueDouble - fractionalPart);
			int inches = (int) (Math.ceil(fractionalPart));
			int totalInches = feet * 12 + inches;
			return String.valueOf(totalInches);
		}
		return categoryAttributeValue;

	}

	public String populatePercentageDecimalTransformative(VendorCategoryAttribute vcAttr,
														  String categoryAttributeValue) {
		if (categoryAttributeValue != null && categoryAttributeValue != "") {
			double categoryAttributeValueInt = Double.parseDouble(categoryAttributeValue);
			return String.valueOf(categoryAttributeValueInt / 100);
		}
		return categoryAttributeValue;

	}

	public String vendorDateFormatConversion(String attributeValue, String dateFormat) {
		String formatedDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		if (attributeValue != null && attributeValue != "") {
			try {
				DateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd");
				Date date1 = dateFmt.parse(attributeValue);
				formatedDate = sdf.format(date1);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return formatedDate;

	}

	public String getAssociatedCategoryAttributeValue(VendorCategoryAttribute vcAttr, VendorRequest vendorRequest) {
		String attributeValue = null;
		if (vcAttr.getCategoryAttributeId() != null && !vcAttr.getCategoryAttributeId().isEmpty()) {
			JsonNode vendorCatgeoryAttribute = categoryAttributeCodeMap.get(vcAttr.getCategoryAttributeId());
			String categoryCode = vendorCatgeoryAttribute.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value())
					.asText();
			if (vendorRequest.isAttributePresent(categoryCode)) {
				attributeValue = vendorRequest.getUniqueInputQuoteForAttribute(categoryCode).getValue();
			} else {
				attributeValue = vcAttr.getDefaultValue();
			}
		} else {
			attributeValue = vcAttr.getDefaultValue();
		}
		return attributeValue;

	}

	public VendorResponse mapToCatalogCategory(VendorRequest vendorRequest, JsonNode responseJson,
											   VendorResponse vendorResponse, String vendorObjectName) {
		JsonNode vendorCategoryAttributeCodeJson = dbUtil
				.getVendorCategoryAttributesWithProductId(vendorRequest.getProductId(), vendorObjectName);
		categoryAttributeCodeMap = dbUtil.getCategoryAttributesIDMap(vendorRequest.getCategoryId(), Boolean.TRUE);

		vendorCategoryAttributeCodeJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).forEach(vcAttrItem -> {
			AttributeValue attributeValue = null;
			VendorCategoryAttribute vcAttr = Json.fromJson(vcAttrItem, VendorCategoryAttribute.class);
			VendorObjectPatch vendorObjectPatch = AttributeUtil.retrieveVendorPatch(vcAttr.getVendorObjectPatches(),
					vendorObjectName);

			Matcher matchPattern = null;
			StringBuffer sb = null;
			switch (vcAttr.getDataType()) {

				case ARRAY_OBJECT:
					populateArrayToObject(vcAttr, responseJson, vendorObjectPatch, vendorResponse);
					break;
				case DATE:
					break;

				case ARRAY_TO_ATTRIBUTES:

					String arrayToAttributesTemplate = vcAttr.getDataTypeTemplate();
					JsonNode arrayToAttributesTemplateJsonNode;
					try {
						arrayToAttributesTemplateJsonNode = mapper.readTree(arrayToAttributesTemplate);
						// System.out.println("ArrayToAttributesJsonNode: " +
						// mapper.writeValueAsString(arrayToAttributesTemplateJsonNode));

						Iterator<Entry<String, JsonNode>> attributeNodes = arrayToAttributesTemplateJsonNode.fields();
						while (attributeNodes.hasNext()) {
							Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) attributeNodes.next();
							if (null == entry.getValue() && StringUtils.isEmpty(entry.getValue().asText())) {
								continue;
							}
							String attrValue = entry.getValue().asText();
							String[] parts = attrValue.split("/");
							if (parts != null && parts.length == 3) {

								if (parts[1].indexOf("[") > -1) {

									Matcher matcher = arrayToAttributesPattern.matcher(parts[1]);
									if (matcher.find() && matcher.groupCount() == 3) {

										String locatorNodeName = matcher.group(2);
										String locatorValue = matcher.group(3);
										String requiredNodeName = parts[2];

										JsonNode searchRootNode = responseJson.at(vendorObjectPatch.getPath());
										String locatedValue = locateAndGetFromArray(searchRootNode, locatorNodeName,
												locatorValue, requiredNodeName);

										attributeValue = new AttributeValue();
										attributeValue.setCategoryAttributeCode(entry.getKey());
										attributeValue.setValue(locatedValue);
										vendorResponse.addAttributeValue(attributeValue);
									}
								}
							}
						}

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					break;

				case DERIVED_OBJECT:
					matchPattern = replacePlaceholder.matcher(vcAttr.getDataTypeTemplate());
					sb = new StringBuffer();
					while (matchPattern.find()) {
						String attrName = matchPattern.group(1);
						String replaced = "0";
						if (!responseJson.at(vendorObjectPatch.getPath()).get(attrName).asText().isEmpty()) {
							replaced = responseJson.at(vendorObjectPatch.getPath()).get(attrName).asText();
						}
						matchPattern.appendReplacement(sb, replaced);
					}
					matchPattern.appendTail(sb);
					// format the response value ((value,precision)) in admin web
					matchPattern = reverseNumberTransformPlaceholder.matcher(sb.toString());
					sb = new StringBuffer();
					while (matchPattern.find()) {
						String attrName = matchPattern.group(1);
						String[] attrNameArray = attrName.split("[,]");
						double responseValue = responseJson.at(vendorObjectPatch.getPath()).get(attrNameArray[0])
								.asDouble();
						if (AttributeUtil.isNumeric(attrNameArray[1])) {
							df.setMaximumFractionDigits(Integer.parseInt(attrNameArray[1]));
						}
						String replaced = df.format(responseValue);
						matchPattern.appendReplacement(sb, replaced);
					}
					matchPattern.appendTail(sb);

					JsonNode node;
					attributeValue = new AttributeValue();
					try {
						node = mapper.readTree(sb.toString());
						Iterator<Entry<String, JsonNode>> nodes = node.fields();
						while (nodes.hasNext()) {
							Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodes.next();
							attributeValue.setCategoryAttributeCode(entry.getKey());
							attributeValue.setValue(entry.getValue().toString());
							vendorResponse.addAttributeValue(attributeValue);
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					break;

				case NUMBER:
					if (vcAttr.getCategoryAttributeId() != null && !vcAttr.getCategoryAttributeId().isEmpty()) {
						JsonNode vendorCatgeoryAttribute = categoryAttributeCodeMap.get(vcAttr.getCategoryAttributeId());
						String categoryCode = vendorCatgeoryAttribute
								.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText();
						attributeValue = new AttributeValue();
						attributeValue.setCategoryAttributeCode(categoryCode.toString());

						Double value = responseJson.at(vendorObjectPatch.getPath()).asDouble();
						attributeValue.setValue(df.format(value));
						vendorResponse.addAttributeValue(attributeValue);
					}
					break;
				default:
					if (vcAttr.getCategoryAttributeId() != null && !vcAttr.getCategoryAttributeId().isEmpty()) {
						JsonNode vendorCatgeoryAttribute = categoryAttributeCodeMap.get(vcAttr.getCategoryAttributeId());
						String categoryCode = vendorCatgeoryAttribute
								.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText();
						attributeValue = new AttributeValue();
						attributeValue.setCategoryAttributeCode(categoryCode);
						// for adding internal attribute to response---- addon applicable
						String responseValue = "";
						if (StringUtils.isEmpty(vendorObjectPatch.getPath())
								|| StringUtils.equalsIgnoreCase("INTERNAL", vendorObjectPatch.getPath())) {
							responseValue = vcAttr.getDataTypeTemplate();
						} else {
							responseValue = responseJson.at(vendorObjectPatch.getPath()).asText();
						}
						if (vcAttr.getTransformationToCatalog() != null) {
							responseValue = doTransformaToCategory(vcAttr, responseValue);
						}
						attributeValue.setValue(responseValue);
						vendorResponse.addAttributeValue(attributeValue);
					}
			}

		});

		return vendorResponse;

	}

	public VendorResponse populateArrayToObject(VendorCategoryAttribute vcAttr, JsonNode responseJson,
												VendorObjectPatch vendorObjectPatch, VendorResponse vendorResponse) {
		JsonNode vendorRestrictionJson = null;
		JsonNode constraintValueArr = null;
		ConstraintType vendorConstarintType = null;
		String dataTemplate = vcAttr.getDataTypeTemplate();
		dataTemplate = dataTemplate.replaceAll("[\\{\\}]", "").trim();
		Map<String, String> constraintValueMap = new HashMap<>();
		if (vcAttr.getTransformationToCatalog() != null) {
			vendorRestrictionJson = dbUtil.fetchVendorRestriction(vcAttr.getCode(), vcAttr.getRestrictionId());
			vendorConstarintType = ConstraintType.valueOf(vendorRestrictionJson
					.get(VendorCategoryAttributeFields.VENDOR_RESTRICTION_CONSTARINT_TYPE.value()).asText());
			constraintValueArr = vendorRestrictionJson
					.get(VendorCategoryAttributeFields.VENDOR_RESTRICTION_CONSTARINT_VALUE.value());
			if (vendorConstarintType.equals(ConstraintType.SINGLESELECT)) {
				constraintValueArr.elements().forEachRemaining(constarintValue -> {
					String catalogValue = constarintValue.get(VendorQuoteConstants.CATALOG_VALUE.value()).asText();
					String vendorValue = constarintValue.get(VendorQuoteConstants.VENDOR_VALUE.value()).asText();
					constraintValueMap.put(vendorValue, catalogValue);
				});
			}

		}
		List<String> vendorAttributeitems = Arrays.asList(dataTemplate.split("\\s*,\\s*"));
		JsonNode responseitem = responseJson.at(vendorObjectPatch.getPath());
		responseitem.elements().forEachRemaining(item -> {
			boolean flag = true;
			String parentNode = "";
			boolean keyexist = false;
			// vendorAttributeitems.forEach(vendoritem -> {
			for (String vendoritem : vendorAttributeitems) {
				if (!item.get(vendoritem).isNull()) {
					// System.out.println(vendoritem+"............."+item.get(vendoritem)+"............................"+
					// constraintValueMap.get(vendoritem));
					if (((constraintValueMap.get(item.get(vendoritem).asText()) != null)
							|| constraintValueMap.get(vendoritem) != null) && !keyexist) {
						keyexist = true;
					} else {
						break;
					}
					if (keyexist) {
						AttributeValue attributeValue = new AttributeValue();
						if (flag) {
							parentNode = constraintValueMap.get(item.get(vendoritem).asText());
							attributeValue.setCategoryAttributeCode(parentNode);
							flag = false;
						} else {
							attributeValue.setCategoryAttributeCode(parentNode + constraintValueMap.get(vendoritem));
						}
						attributeValue.setValue(item.get(vendoritem).asText());
						vendorResponse.addAttributeValue(attributeValue);
						keyexist = false;
					}
				}
				// });
			}
		});
		return vendorResponse;

	}

	private static String locateAndGetFromArray(JsonNode jsonNode, String locatorNodeName, String locatorValue,
												String requiredNodeName) {

		String[] locatedValueArray = new String[1];
		locatedValueArray[0] = null;

		jsonNode.forEach(node -> {
			if (node.at("/" + locatorNodeName) != null
					&& StringUtils.equalsIgnoreCase(locatorValue, node.at("/" + locatorNodeName).asText())) {
				if (null != node.at("/" + requiredNodeName)) {
					locatedValueArray[0] = node.at("/" + requiredNodeName).asText();
				}
			}
		});
		return locatedValueArray[0];
	}
}