package com.arka.integrationserv.models.vendorplan.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import com.arka.common.builder.MongoQueryBuilder;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.controllers.utils.ResultWrapperUtils;
import com.arka.common.integration.constants.VendorCategoryAttributeFields;
import com.arka.common.models.base.repository.MongoBaseRepository;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.provider.db.MongoDBService;
import com.arka.common.services.CmsServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.validator.CriteriaValidator;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.service.DataConfigurationService;
import com.arka.integrationserv.models.vendorcategoryattribute.service.VendorCategoryAttributeService;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorrestriction.service.VendorRestrictionService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import play.Logger;
import play.libs.Json;
import play.mvc.Http.Context;

public class DBUtil {

	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	VendorCategoryAttributeService<String> vendorCategoryAttributeService;

	@Inject
	VendorRestrictionService<String> vendorRestrictionService;

	@Inject
	DataConfigurationService<String> dataConfigurationService;
	
	@Inject
	private MongoQueryBuilder mongoQueryBuilder;

	@Inject
	private MongoDBService dbService;

	@Inject
	private CriteriaValidator criteriaValidator;
	
	@Inject
	private CmsServ cmsServ;


	// private final List<String> encDecFields =
	// VendorIntegrationEncryptDecryptFields.getEncAndDecFields();

	public JsonNode getVendorCategoryAttributesWithProductId(String productId,
			String vendorObject) {
		try {
			Map<String, List<String>> mandatQP = mandatoryParams
					.getMandatoryQueryParameter();
			mandatQP.put(ProductFields.VENDOR_PRODUCT_ID.value(),
					Arrays.asList(String.valueOf(productId)));
			mandatQP.put("vendor_object_patches.vendor_object",
					Arrays.asList(String.valueOf(vendorObject)));
			mandatQP.put("nor", Arrays.asList("0"));
			mandatQP.put("asc", Arrays.asList("weight"));
			JsonNode vendorCategoryAttributelistJson = vendorCategoryAttributeService
					.findVendorCategoryAttributes(mandatQP)
					.thenApplyAsync(coll -> ResultWrapperUtils
							.wrapListInJson(coll, null, null))
					.toCompletableFuture().join();
			return vendorCategoryAttributelistJson;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			System.out.println(Json.prettyPrint(
					ResponsePayLoad.getJsonResponseInternalServerError(e)));
			return null;
		}
	}

	public String getvendorDataCollectionName(
			String vendorDataConfigurationId) {
		try {
			if (StringUtils.isNotEmpty(vendorDataConfigurationId)) {
				DataConfiguration dataConfiguration = dataConfigurationService
						.findDataConfigurationById(vendorDataConfigurationId)
						.toCompletableFuture().join();
				return dataConfiguration.getCollectionName();
			}
			return null;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			return null;
		}
	}

	public JsonNode getUnknownCollectionRecord(Map<String, List<String>> queryStringMap) {
		Map<String, String[]> queryStringArray=CriteriaUtils.queryStringListToArray(queryStringMap);
		String[] projectionFields = queryStringArray.get("pfield");
		String[] distinct = queryStringArray.get("distinct");
		String[] regex = queryStringArray.get("regex");

		Document projFieldDoc = new Document();
		List<String> groupByFields = new ArrayList<>();

		if (projectionFields != null && projectionFields.length > 0) {
			groupByFields.addAll(Arrays.asList(projectionFields));
			projFieldDoc.putAll(mongoQueryBuilder.buildProjection(groupByFields));
		}
		List<String> collectionNames = queryStringMap.get("cname");

		Map<String, List<String>> dataConfigQueryStringMap = new HashMap<>();
		dataConfigQueryStringMap.put("collection_name", collectionNames);
		JsonNode dataConfigJson =  JsonUtils.toEncryptedIdJson(ResultWrapperUtils.wrapListInJson(
				dataConfigurationService.findDataConfigurations(dataConfigQueryStringMap).toCompletableFuture().join(), null, null), null);
		ArrayNode respJsonArray = JsonUtils.newJsonArray();
		if (JsonUtils.isValidField(dataConfigJson, JsonUtils.JSON_ARRAY_ITEMS_KEY)) {

			dataConfigJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).elements().forEachRemaining(jsonNode -> {

				if (JsonUtils.isValidField(jsonNode, "data_configuration_field") 
						&& JsonUtils.isValidField(jsonNode, "collection_name")) {

					JsonNode dataConfigFields = jsonNode.get("data_configuration_field");

					Map<String, String> validFieldAndType = getValidFieldsAndDataType(dataConfigFields);

					String collectionName = jsonNode.get("collection_name").asText();

					JsonNode dataJson = processData(null, collectionName, queryStringArray, groupByFields,
							projectionFields, distinct, projFieldDoc, validFieldAndType, regex != null);

					ObjectNode newJsonObject = JsonUtils.newJsonObject();
					newJsonObject.put("collection_name", collectionName);
					newJsonObject.set("data", dataJson);
					respJsonArray.add(newJsonObject);
				} else {
					ObjectNode newJsonObject = JsonUtils.newJsonObject();
					newJsonObject.put("error", "Has no 'data_configuration_field' field");
					respJsonArray.add(newJsonObject);
				}

			});
		}


		return ResultWrapperUtils.wrapListInJson(respJsonArray, null);
	}

	private Map<String, String> getValidFieldsAndDataType(JsonNode dataConfigFields) {

		Map<String, String> validFieldAndType = new HashMap<>();

		StreamSupport
		.stream(Spliterators.spliteratorUnknownSize(dataConfigFields.elements(), Spliterator.CONCURRENT), true)
		.filter(field -> JsonUtils.isValidField(field, "field_name")
				&& JsonUtils.isValidField(field, "data_type"))
		.forEach(field -> {
			validFieldAndType.put(field.get("field_name").asText(), field.get("data_type").asText());
		});

		return validFieldAndType;
	}
	
	private JsonNode processData(String cacheKey, String collectionName, Map<String, String[]> queryString,
			List<String> groupByFields, String[] projectionFields, String[] distinct, Document projFieldDoc,
			Map<String, String> validFieldAndType, boolean isRegex) {
		Map<String, List<Object>> validFilters = criteriaValidator.getValidFilters(queryString, validFieldAndType);

		MongoCollection<Document> collection = dbService.getCollection(collectionName);

		if(collection!=null){

			Document query = new Document();
			if (queryString.containsKey("arr") && queryString.get("arr") != null) {
				query.putAll(mongoQueryBuilder.buildQuery(validFilters, isRegex, queryString));
			} else {
				query.putAll(mongoQueryBuilder.buildQuery(validFilters, isRegex));
			}
			if (queryString.containsKey(CriteriaUtils.STARTS_WITH)) {
				query.putAll(mongoQueryBuilder.buildQuery(validFilters, isRegex, queryString));
			}
			ObjectNode recordCountDetails = Json.newObject();

			if (queryString.containsKey("rcount") && queryString.get("rcount") != null) {

				recordCountDetails.put("total_record_count", collection.count());

				recordCountDetails.put("filtered_record_count", collection.count(query));
			}

			Document sortBy = new Document();
			sortBy.putAll(mongoQueryBuilder.buildSortQuery(CriteriaUtils.queryStringArrayToList(queryString)));

			List<Document> documentList = new ArrayList<>();

			if (distinct != null && distinct.length > 0) {

				Document buildGroupBy = mongoQueryBuilder.buildGroupBy(groupByFields);

				List<Document> aggreList = new ArrayList<>();

				if (query != null && !query.isEmpty()) {
					aggreList.add(new Document("$match", query));
				}
				if (buildGroupBy != null && !buildGroupBy.isEmpty()) {
					aggreList.add(new Document("$group", new Document(MongoBaseRepository._ID, buildGroupBy)));
				}

				if (projFieldDoc != null && !projFieldDoc.isEmpty()) {
					aggreList.add(new Document("$project", projFieldDoc));
				}
				
				if (sortBy != null && !sortBy.isEmpty()) {
					aggreList.add(new Document("$sort", sortBy));
				}

				mongoQueryBuilder.setPaginationFieldsForAggregation(aggreList,
						CriteriaUtils.queryStringArrayToList(queryString));

				List<Document> dataList = collection.aggregate(aggreList).allowDiskUse(true).into(new ArrayList<Document>())
						.parallelStream().map(doc -> ((Document) doc.get(MongoBaseRepository._ID))).collect(Collectors.toList());

				documentList.addAll(dataList);

			} else {

				FindIterable<Document> findItr = collection.find(query);

				if (projFieldDoc != null && !projFieldDoc.isEmpty()) {
					findItr.projection(projFieldDoc);
				}

				if (sortBy != null && !sortBy.isEmpty()) {
					findItr.sort(sortBy);
				}

				mongoQueryBuilder.setPaginationFieldsForFind(findItr, CriteriaUtils.queryStringArrayToList(queryString));

				List<Document> dataList = findItr.into(new ArrayList<Document>());
				documentList.addAll(dataList);
			}
			if (documentList != null && !documentList.isEmpty()) {
				return ResultWrapperUtils.wrapDocumentInJson(documentList, projectionFields, recordCountDetails);
			}
		}
		ObjectNode jsonObject = JsonUtils.newJsonObject();
		jsonObject.put("message", "No data found");
		return jsonObject;

	}

	public Map<String, JsonNode> getCatgeoryAttributeFieldNameMap(
			String categoryId, String fieldName, Boolean isParentToBeincluded) {
		try {
			Map<String, JsonNode> categoryAttributeIdMap = new HashMap<String, JsonNode>();
			JsonNode categoryAttributelistJson = getCategoryAttributes(
					categoryId, isParentToBeincluded);
			if (categoryAttributelistJson != null && categoryAttributelistJson
					.hasNonNull(JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				categoryAttributelistJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY)
						.forEach(categoryAttribute -> {
							if (categoryAttribute.hasNonNull(fieldName)) {
								String fieldValue = categoryAttribute
										.get(fieldName).asText();
								categoryAttributeIdMap.put(fieldValue,
										categoryAttribute);
							}
						});
			}
			return categoryAttributeIdMap;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			return null;
		}
	}

	public JsonNode getCategoryAttributes(String categoryId,
			boolean isParentToBeIncluded) {
		try {
			Map<String, List<String>> mandatQP = mandatoryParams
					.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(
							Context.current().args.entrySet());
			mandatQP.put("parent",
					Arrays.asList(String.valueOf(isParentToBeIncluded)));
			mandatQP.put("nor", Arrays.asList("0"));
			JsonNode categoryAttributelistJson = vendorProdMgmtServ
					.getCategoryAttributeList(categoryId, mandatQP, mandatHP)
					.toCompletableFuture().get();
			return categoryAttributelistJson;

		} catch (Exception e) {
			return ResponsePayLoad.getJsonResponseInternalServerError(e);
		}

	}

	public Map<String, JsonNode> getCategoryAttributesIDMap(String categoryId,
			boolean isParentToBeIncluded) {
		try {
			Map<String, JsonNode> categoryAttributeIdMap = new HashMap<String, JsonNode>();
			JsonNode categoryAttributelistJson = getCategoryAttributes(
					categoryId, isParentToBeIncluded);
			if (categoryAttributelistJson != null && categoryAttributelistJson
					.hasNonNull(JsonUtils.JSON_ARRAY_ITEMS_KEY)) {
				categoryAttributelistJson.get(JsonUtils.JSON_ARRAY_ITEMS_KEY)
						.forEach(categoryAttribute -> {
							categoryAttributeIdMap.put(
									CryptUtils
											.decryptData(
													categoryAttribute
															.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_ID
																	.value())
															.asText()),
									categoryAttribute);
						});
			}
			return categoryAttributeIdMap;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			return null;
		}

	}

	public JsonNode fetchVendorRestriction(String vendorAttributeCode,
			String restrictionId) {
		if (StringUtils.isNotEmpty(restrictionId)) {
			try {
				JsonNode vendorRestrictionJson = JsonUtils
						.toJson(vendorRestrictionService
								.findVendorRestrictionById(restrictionId)
								.toCompletableFuture().join());
				if (!JsonUtils.isValidField(vendorRestrictionJson, VendorCategoryAttributeFields.VENDOR_RESTRICTION_ID
										.value())) {
					return null;
				}
				return vendorRestrictionJson;
			} catch (Exception e) {
				Logger.error(e.getMessage(), e);
			}
		} 
		return null;
	}

	public JsonNode getProductJson(String productId) {
		try {
			Map<String, List<String>> mandatQP = mandatoryParams
					.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(
							Context.current().args.entrySet());
			JsonNode productJson = vendorProdMgmtServ
					.getProduct(productId, mandatQP, mandatHP)
					.toCompletableFuture().get();
			if (productJson != null && productJson
					.hasNonNull(ProductFields.PRODUCT_ID.value())) {
				return productJson;
			}
			return null;
		} catch (Exception e) {
			Logger.error(e.getMessage(), e);
			return null;
		}
	}

	public JsonNode getGlossaryDescriptionList(List<String> queryList) {
		Map<String, List<String>> mandatQP = new HashMap<>();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		mandatQP.put("id", queryList);
		return cmsServ.getGlossaryList(mandatQP, mandatHP).toCompletableFuture().join();
		
	}
}
