package com.arka.integrationserv.models.vendorplan.utils;

import java.security.SecureRandom;
import java.util.List;
import java.util.UUID;

import com.arka.common.integration.constants.VendorCategoryAttributeFields;
import com.arka.common.models.tag.VendorObjectPatch;
import com.fasterxml.jackson.databind.JsonNode;

public class AttributeUtil {

	public static boolean isAssociatedCategoryAttribute(JsonNode vendorCatgeoryAttribute) {

		if (vendorCatgeoryAttribute
				.hasNonNull(VendorCategoryAttributeFields.VENDOR_CATEGORY_ATTRIBUTE_CATEGORY_ATTRIBUTE_ID.value())) {
			return !vendorCatgeoryAttribute
					.get(VendorCategoryAttributeFields.VENDOR_CATEGORY_ATTRIBUTE_CATEGORY_ATTRIBUTE_ID.value()).asText()
					.trim().isEmpty();

		}

		return false;
	}

	public static boolean isTransformationToBeDone(JsonNode vendorCatgeoryAttribute) {

		if (vendorCatgeoryAttribute
				.hasNonNull(VendorCategoryAttributeFields.VENDOR_CATEGORY_ATTRIBUTE_TARNSFORM_TO_VENDOR.value())) {
			return !vendorCatgeoryAttribute
					.get(VendorCategoryAttributeFields.VENDOR_CATEGORY_ATTRIBUTE_TARNSFORM_TO_VENDOR.value()).asText()
					.trim().isEmpty();
		}
		return false;
	}
	
	public static boolean isTransformationToBeDone(JsonNode vendorCatgeoryAttribute, String transformToVendor) {

		if (vendorCatgeoryAttribute
				.hasNonNull(VendorCategoryAttributeFields.VENDOR_CATEGORY_ATTRIBUTE_TARNSFORM_TO_VENDOR.value())) {

			return vendorCatgeoryAttribute
					.get(VendorCategoryAttributeFields.VENDOR_CATEGORY_ATTRIBUTE_TARNSFORM_TO_VENDOR.value()).asText()
					.equals(transformToVendor);
		}

		return false;
	}


	public static VendorObjectPatch retrieveVendorPatch(List<VendorObjectPatch> vendorObjectPatchlist,
			String vendorObjectName) {
		VendorObjectPatch vendorObjectPatch = null;
		for (VendorObjectPatch vendorObjectPatchitem : vendorObjectPatchlist) {
			if (vendorObjectPatchitem.getVendorObject().equalsIgnoreCase(vendorObjectName)) {
				vendorObjectPatch = vendorObjectPatchitem;
			}
		}
		return vendorObjectPatch;

	}
	
	public static String generateGUID() {
		UUID uuid = UUID.randomUUID();
	    String randomUUIDString = uuid.toString();
		return randomUUIDString;
		
	}
	public static String generateRandom() {
        long epochInMillis = System.currentTimeMillis();
        SecureRandom secureRandom = new SecureRandom();
        int secureInt = secureRandom.nextInt(1000000);
        String randomString = String.valueOf(epochInMillis)+String.valueOf(secureInt);
        System.out.println(randomString.length());
        return randomString;
    }
	
	public static boolean isNumeric(String s) {  
	    return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
	}  

}
