package com.arka.integrationserv.models.vendorplan;

import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public interface VendorConnector {
	

	VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest);

	VendorResponse buyPlanFromVendor(VendorRequest vendorRequest);

	ObjectNode purchasePlan(VendorRequest vendorRequest);
	
	ObjectNode policyPdf(String policyNum);

	ObjectNode purchasePlan(JsonNode reqJson);

}
