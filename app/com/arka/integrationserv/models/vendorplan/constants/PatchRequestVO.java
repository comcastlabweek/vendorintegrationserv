package com.arka.integrationserv.models.vendorplan.constants;

/**
 * Created by ganesh on 30/06/17.
 */


public class PatchRequestVO {
    private static final String TEST = "test";
    private static final String COPY = "copy";
    private static final String MOVE = "move";
    private static final String REMOVE = "remove";
    private static final String ADD = "add";
    private static final String REPLACE = "replace";

    public PatchRequestVO() {
    }

    public PatchRequestVO(OpEnum op, String path, Object value) {
        this.op = op;
        this.path = path;
        this.value = value;
    }

    public PatchRequestVO(String op, String path, Object value) {
        setOp(op);
        this.path = path;
        this.value = value;
    }

    public PatchRequestVO(char op, String path, Object value) {
        setOp(op);
        this.path = path;
        this.value = value;
    }

    private OpEnum op;
    private String path;
    private Object value;

    public OpEnum getOp() {
        return op;
    }
    public void setOp(OpEnum op) {
        this.op = op;
    }
    public void setOp(String operation) {
        if(operation.equals(REPLACE)) {
            this.op = OpEnum.REPLACE;
        }
        else if(operation.equals(ADD)) {
            //this.op = OpEnum.ADD;
        }
        else if(operation.equals(REMOVE)) {
          //  this.op = OpEnum.REMOVE;
        }
        else if(operation.equals(MOVE)) {
            this.op = OpEnum.MOVE;
        }
        else if(operation.equals(COPY)) {
            this.op = OpEnum.COPY;
        }
        else if(operation.equals(TEST)) {
            this.op = OpEnum.TEST;
        }
    }
    public void setOp(char operation) {

//        switch (Character.toUpperCase(operation)) {
//            case 'A':
//                this.op = OpEnum.ADD;
//                break;
//            case 'R':
//                this.op = OpEnum.REPLACE;
//                break;
//            case 'M':
//                this.op = OpEnum.MOVE;
//                break;
//            case 'C':
//                this.op = OpEnum.COPY;
//                break;
//            case 'T':
//                this.op = OpEnum.TEST;
//                break;
//
//        }
    }


    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
}
