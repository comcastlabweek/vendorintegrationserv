package com.arka.integrationserv.models.vendorplan.constants;

public enum EnquiryQuoteResponseFields {

	ID ("id"),

	PRODUCT_ID("productId"),

	CATEGORY_ID("categoryId"),

	ENQUIRY_ID("enquiryId"),

	VENDOR_ID ("vendorId"),

	PRODUCT_NAME_KEY("productNameKey"),

	PRODUCT_CODE_KEY("productCodeKey"),

	PRODUCT_IMAGE_KEY("productImageKey"),

	PRODUCT_NAME_VALUE("productNameValue"),

	PRODUCT_CODE_VALUE("productCodeValue"),

	PRODUCT_IMAGE_VALUE("productImageValue"),

	QUOTE_KEY("quoteKey"),	

	GROUP_ATTRIBUTE_LIST ("groupAttributeList"),

	 ADDITIONAL_INFO_JSON("additionalInfoJson"),

	ERROR_JSON ("errorJson")

	;

	private String value;

	private EnquiryQuoteResponseFields(String valueField) {
		this.value = valueField;
	}

	public String getValue() {
		return this.value;
	}
}
