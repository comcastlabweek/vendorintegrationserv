package com.arka.integrationserv.models.vendorplan.constants;

public enum AttributeValueFields {
	ID("id"),

	GROUP_ATTRIBUTE_ID("groupattributeId"),

	GROUP_ATTRIBUTE("groupAttribute"),

	CATEGORY_ATTRIBUTE_CODE("categoryAttributeCode"),

	VALUE("value"),

	DEDUCTIBLE("deductible"),

	TAGS("tags"),

	DESCRIPTION("description"),
	
;
	
	private String value;

	private AttributeValueFields(String valueField) {
		this.value = valueField;
	}

	public String getValue() {
		return this.value;
	}
}
