package com.arka.integrationserv.models.vendorplan.constants;

public enum FetchPlanMessage {
	
	CANNOT_SEEK_VENDOR_CONNECTOR_FOR_PRODUCT("cannot.seek.vendor.connector.for.product"),
	QUOTES_UNATTAINABLE_FROM_VENDOR("quotes.unattainable.from.vendor"),
	QUOTES_TIMED_OUT_FROM_VENDOR("quotes.timedout.from.vendor"),
	QUOTES_NOT_SUPPORTED_FROM_VENDOR("quotes.not.supported.from.vendor"),
	
	QUOTE_INFO_EMPTY("quote.info.empty"),
	QUOTE_KEY_EMPTY("quote.key.empty"),
	ENQUIRY_ID_EMPTY("quote.enquiry_id.empty"),
	PRODUCT_ID_EMPTY("quote.product_id.empty"),
	VENDOR_ID_EMPTY("quote.vendor_id.empty"),
	CATEGORY_ID_EMPTY("quote.category_id.empty"),
	CATEGORY_CODE_EMPTY("quote.category_code.empty"),
	PRODUCT_CODE_EMPTY("quote.product_code.empty"),
	ENQUIRY_CATEGORY_ATTRIBUTE_CODE_IS_EMPTY("enquiry.category.attribute.code.is.empty"),
	ENQUIRY_VALUE_IS_EMPTY("enquiry.value.is.empty"),
	CANNOT_MAP_CATALOG_ATTRIBUTE_VALUE("catalog.value.cannot.be mapped"),
	CANNOT_LOOK_UP_CATALOG_ATTRIBUTE_VALUE("catalog.value.not.found.in lookup.table"),
	
	PRODUCT_DOES_NOT_EXISTS("products.does.not.exists"),
	VENDOR_CATEGORY_ATTRIBUTES_DOES_NOT_EXISTS("vendor.category.attributes.does.not.exists"),
	VENDOR_CATEGORY_ATTRIBUTES_TRANSFORM_TO_VENDOR_INVALID("vendor.category.attributes.transform.to.vendor.invalid"),
	VENDOR_CATEGORY_ATTRIBUTES_CONSTARINT_TYPE_INVALID("vendor.category.attributes.constarint.type.invalid"),
	CATEGORY_ATTRIBUTES_DOES_NOT_EXISTS("category.attributes.does.not.exists"),
	CATEGORY_ATTRIBUTE_ID_DOES_NOT_EXISTS("category.attribute.id.does.not.exists"),
	MANDATORY_PARAM_MISSING("vendor.param.mandatory"),
	RESTRICTION_NOT_SPECIFIED_FOR_MAP_TRANSFORMATION("map.transformTovendor.restriction.empty"),
	VENDOR_RESTRICTION_ID_DOES_NOT_EXISTS("vendor.restriction.id.does.not.exists"), 
	VENDOR_CATEGORY_ATTRIBUTE_CODE_DOES_NOT_EXISTS("vendor.attribute.code.does.not.exists"), 
	PROPOSAL_INFO_EMPTY("proposal.info.empty"),       
	
	BREAK_IN_POICY_NOT_ALLOWED("break.in.policy.not.allowed"),
	NOT_SUPPORTED_MODEL("not.supported.model"),
	IDV_LIMIT_EXCEED_FOR_BREAKINPOLICY("idv.limit.exceed.for.breakinpolicy"),
	MAX_ALLOWED_IDV_EXCEED("max.allowed.idv.exceed"),
	MIN_ALLOWED_IDV_EXCEED("max.allowed.idv.exceed"),
	NO_PREMIUM_RETURNED("no.premium.returned"),
	SUM_ASSURED_NOT_SUPPORTED("sum.assured.not.supported"),
	FAILED_IN_VENDER_SIDE("failed.in.vendor.side"),
	UNABLE_TO_PROCESS("unable.to.process"),
	INVALID_NOMINEE("invalid.nominee"),
	INVALID_REGNO("invalid.regno"),
	INVALID_MANUFACTUREYEAR("invalid.manufactureyear"),
	INVALID_ENGINENO("invalid.engineno"),
	INVALID_CHASISNO("invalid.chasisno"),
	INVALID_DOB("invalid.dob"),
	INVALID_PAN("invalid.pan"),
	INVALID_AADHARNO("invalid.aadharno"),
	INVALID_TITLE("invalid.title"),
	INVALID_NAME("invalid.name"),
	INVALID_GENDER("invalid.gender"),
	INVALID_ADDRESS("invalid.address"),
	INVALID_PINCODE("invalid.pincode"),
	INVALID_CITY("invalid.city"),
	INVALID_STATE("invalid.state"),
	INVALID_COUNTRY("invalid.country"),
	INVALID_OCCUPATION("invalid.occupation"),
	INVALID_MOBILE("invalid.mobile"),
	INVALID_EMAIL("invalid.email"),
	DEFAULT_VALUE_MISSING("default.missing"),
	INVALID_STARTDATE("invalid.startdate"),
	INVALID_ENDDATE("invalid.enddate"),
	INVALID_VEHICLETYPE("invalid.vehicletype"),
	INVALID_VEHICLEMAKE("invalid.vehiclemake"),
	INVALID_VEHICLEMODEL("invalid.vehiclemodel"),
	INVALID_VEHICLEVARIANT("invalid.vehiclevariant"),
	INVALID_FUEL("invalid.fuel"),
	INVALID_ZONE("invalid.zone"),
	INVALID_REGDATE("invalid.regdate"),
	INVALID_REGLOCATION("invalid.regloc"),
	
	//HEALTH Insurance;
	MAX_ADULT_EXCEED("max.adult.exceeds"),
	MAX_CHILDREN_EXCEED("max.children.exceeds"),
	;
	
	private String value;

	private FetchPlanMessage(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
