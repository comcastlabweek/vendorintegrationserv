package com.arka.integrationserv.models.vendorplan.constants;

public enum InputQuoteParamFields {

	CATEGORY_ATTRIBUTE_CODE("categoryAttributeCode"),

	VALUE("value"),

	TAG("tag");

	private String value;

	private InputQuoteParamFields(String valueField) {
		this.value = valueField;
	}

	public String getValue() {
		return this.value;
	}
}
