package com.arka.integrationserv.models.vendorplan.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.data.validation.ValidationError;
import play.libs.Json;

public class ResponsePayLoad {

	// Error keys

	public static final String STATUS = "status";
	public static final String ERROR = "error";
	public static final String ERROR_SOURCE = "source";
	public static final String ERROR_MESSAGE = "message";
	public static final String ERROR_CAUSE = "cause";
	public static final String ERROR_STATUS = "status";

	// client error status

	public static final String NOT_FOUND = "NOT_FOUND";
	public static final String BAD_REQUEST = "BAD_REQUEST";
	public static final String FORBIDDEN = "FORBIDDEN";

	// server error status

	public static final String RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";
	public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
	public static final String ERROR_INVALID = "error.invalid";

	// Date format

	public static final String RESPONSE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public static JsonNode getJsonresponseForBadRequest(ValidationError validateError) {
		ObjectNode responseJson = Json.newObject();
		ObjectNode errorJson = responseJson.putObject(ERROR);
		String errorKey = validateError.key();
		ArrayNode errorMsgArr = errorJson.putArray(errorKey);

		validateError.messages().stream().forEach((message) -> {
			errorMsgArr.add(message);
		});
		return responseJson;
	}

	public static JsonNode getJsonresponseForBadRequest(List<ValidationError> validate) {
		Map<String, List<ValidationError>> validationErrorMap = convertToValidationErrorMap(validate);
		return getJsonresponseForBadRequest(validationErrorMap);
	}

	private static Map<String, List<ValidationError>> convertToValidationErrorMap(List<ValidationError> validate) {
		Map<String, List<ValidationError>> validationErrorMap = new HashMap<String, List<ValidationError>>();

		validate.stream().forEach((err) -> {
			String errorKey = err.key();
			if (!validationErrorMap.containsKey(errorKey)) {
				validationErrorMap.put(errorKey, new ArrayList<ValidationError>());
			}
			validationErrorMap.get(errorKey).add(err);
		});
		return validationErrorMap;
	}

	public static JsonNode getJsonresponseForBadRequest(Map<String, List<ValidationError>> errors) {

		ObjectNode responseJson = Json.newObject();
		ObjectNode errorJson = responseJson.putObject(ERROR);
		errors.entrySet().stream().forEach((err) -> {
			String errorField = err.getKey();
			ArrayNode errorMsgArr = errorJson.putArray(errorField);
			err.getValue().stream().forEach((validateErr) -> {
				validateErr.messages().stream().forEach((validationErrMsg) -> {
					errorMsgArr.add(validationErrMsg);
				});
			});
		});
		return responseJson;
	}

	public static JsonNode getJsonResponseInternalServerError(Throwable throwable) {
		ObjectNode exceptionJson = Json.newObject();
		ObjectNode errorJson = exceptionJson.putObject(ERROR);
		errorJson.put(ERROR_SOURCE, throwable.getClass().getName());
		errorJson.put(ERROR_MESSAGE, throwable.getMessage());
		errorJson.put(ERROR_CAUSE, ExceptionUtils.getStackTrace(throwable));
		exceptionJson.put(STATUS, INTERNAL_SERVER_ERROR);
		return exceptionJson;
	}

	public static JsonNode getJsonResponseForClientError(int status, String message, String cientError) {
		ObjectNode exceptionJson = Json.newObject();
		ObjectNode errorJson = exceptionJson.putObject(ERROR);
		errorJson.put(ERROR_STATUS, status);
		errorJson.put(ERROR_MESSAGE, message);
		exceptionJson.put(STATUS, cientError);
		return exceptionJson;
	}

}
