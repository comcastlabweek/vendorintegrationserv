package com.arka.integrationserv.models.vendorplan.constants;

public enum EnquiryQuoteRequestFields {
	
	ENQUIRY_ID("enquiryId"),
	PRODUCT_ID("productId"),
	 PRODUCT_CODE("productCode"),
	 CATGEORY_ID("categoryId"),
	 CATGEORY_CODE("categoryCode"),
	VENDOR_ID("vendorId"),
	QUOTE_KEY("quoteKey"),

	INPUT_QUOTE_PARAMS("input_quote_params");
	
	private String value;
	
	private EnquiryQuoteRequestFields(String valueField) {
		this.value=valueField;
	}

	public String getValue() {
		return this.value;
	}
}
