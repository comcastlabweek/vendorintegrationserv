package com.arka.integrationserv.models.vendorplan.constants;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OpEnum {

    A("add"),
    R("replace"),
    REPLACE("remove"),
    MOVE("move"),
    COPY("copy"),
    TEST("test");

    private String value;
    private static Map<String, OpEnum> enumMap = new HashMap<String, OpEnum>();

    static {
        for(OpEnum enumType : OpEnum.values()) {
            enumMap.put(enumType.name(), enumType);
        }
    }

    OpEnum(String value) {
        this.value = value;
    }

    public static OpEnum getEnum(String value) {
        return enumMap.get(value);
    }

    @Override
    @JsonValue
    public String toString() {
        return value;
    }
    
}
