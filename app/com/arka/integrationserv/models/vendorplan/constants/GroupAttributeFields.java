package com.arka.integrationserv.models.vendorplan.constants;

public enum GroupAttributeFields {
	ID("id"),

	GROUP_TAG("groupTag"),

	ENQUIRY_QUOTE_RESPONSE_ID("enquiryquoteResponseId"),

	ENQUIRY_QUOTE_RESPONSE("enquiryQuoteResponse"),

	ATTRIBUTE_VALUE_LIST("attributeValueList"),

	DECSRIPTION("description"),

	LONG_DECSRIPTION("longDescription"),

	COVERED("covered"),
	
	;
	
	private String value;

	private GroupAttributeFields(String valueField) {
		this.value = valueField;
	}

	public String getValue() {
		return this.value;
	}

}
