//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.11.08 at 06:33:31 PM IST 
//


package com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.idvcalculationrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policy_start_date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehicle_class_cd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RTOLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vehiclemodelcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="manufacturer_code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="purchaseregndate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prev_policy_end_date" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="typeofbusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyStartDate",
    "vehicleClassCd",
    "rtoLocationCode",
    "vehiclemodelcode",
    "manufacturerCode",
    "purchaseregndate",
    "prevPolicyEndDate",
    "typeofbusiness",
    "manufacturingyear"
})
@XmlRootElement(name = "IDV")
public class IDVREQUEST {

    @XmlElement(name = "policy_start_date")
    protected String policyStartDate;
    @XmlElement(name = "vehicle_class_cd")
    protected String vehicleClassCd;
    @XmlElement(name = "RTOLocationCode")
    protected String rtoLocationCode;
    protected String vehiclemodelcode;
    @XmlElement(name = "manufacturer_code")
    protected String manufacturerCode;
    protected String purchaseregndate;
    @XmlElement(name = "prev_policy_end_date")
    protected String prevPolicyEndDate;
    protected String typeofbusiness;
    protected String manufacturingyear;

    /**
     * Gets the value of the policyStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyStartDate() {
        return policyStartDate;
    }

    /**
     * Sets the value of the policyStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyStartDate(String value) {
        this.policyStartDate = value;
    }

    /**
     * Gets the value of the vehicleClassCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleClassCd() {
        return vehicleClassCd;
    }

    /**
     * Sets the value of the vehicleClassCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleClassCd(String value) {
        this.vehicleClassCd = value;
    }

    /**
     * Gets the value of the rtoLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRTOLocationCode() {
        return rtoLocationCode;
    }

    /**
     * Sets the value of the rtoLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRTOLocationCode(String value) {
        this.rtoLocationCode = value;
    }

    /**
     * Gets the value of the vehiclemodelcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehiclemodelcode() {
        return vehiclemodelcode;
    }

    /**
     * Sets the value of the vehiclemodelcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehiclemodelcode(String value) {
        this.vehiclemodelcode = value;
    }

    /**
     * Gets the value of the manufacturerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturerCode() {
        return manufacturerCode;
    }

    /**
     * Sets the value of the manufacturerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturerCode(String value) {
        this.manufacturerCode = value;
    }

    /**
     * Gets the value of the purchaseregndate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPurchaseregndate() {
        return purchaseregndate;
    }

    /**
     * Sets the value of the purchaseregndate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchaseregndate(String value) {
        this.purchaseregndate = value;
    }

    /**
     * Gets the value of the prevPolicyEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevPolicyEndDate() {
        return prevPolicyEndDate;
    }

    /**
     * Sets the value of the prevPolicyEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevPolicyEndDate(String value) {
        this.prevPolicyEndDate = value;
    }

    /**
     * Gets the value of the typeofbusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeofbusiness() {
        return typeofbusiness;
    }

    /**
     * Sets the value of the typeofbusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeofbusiness(String value) {
        this.typeofbusiness = value;
    }
    

    /**
     * Gets the value of the manufacturingyear property.
     * 
     */
    public String getManufacturingyear() {
        return manufacturingyear;
    }

    /**
     * Sets the value of the manufacturingyear property.
     * 
     */
    public void setManufacturingyear(String value) {
        this.manufacturingyear = value;
    }

}
