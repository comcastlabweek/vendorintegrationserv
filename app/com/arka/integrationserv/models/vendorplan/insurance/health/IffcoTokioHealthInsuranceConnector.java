package com.arka.integrationserv.models.vendorplan.insurance.health;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DefaultMessageHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.google.inject.Inject;

import itgi.health.objects.FHPInsuredResponse;
import itgi.health.objects.IHPInsuredResponse;
import itgi.health.paymentwrapper.fhp.FHPObjectFactory;
import itgi.health.paymentwrapper.fhp.FHPRequest;
import itgi.health.paymentwrapper.fhp.FHPRequest.ListOfFHPInsured;
import itgi.health.paymentwrapper.fhp.FHPRequest.ListOfFHPInsured.FHPInsuredObj;
import itgi.health.paymentwrapper.ihp.IHPObjectFactory;
import itgi.health.paymentwrapper.ihp.IHPRequest;
import itgi.health.paymentwrapper.ihp.IHPRequest.ListOfIHPInsured;
import itgi.health.premiumwrapper.ArrayOfErrorDetails;
import itgi.health.premiumwrapper.ArrayOfFHPInsured;
import itgi.health.premiumwrapper.ArrayOfIHPInsured;
import itgi.health.premiumwrapper.ArrayOfTns1FHPInsuredResponse;
import itgi.health.premiumwrapper.ArrayOfTns1IHPInsuredResponse;
import itgi.health.premiumwrapper.ErrorDetails;
import itgi.health.premiumwrapper.FHPInsured;
import itgi.health.premiumwrapper.FHPPolicy;
import itgi.health.premiumwrapper.FHPPremiumDetails;
import itgi.health.premiumwrapper.FHPPremiumWebService;
import itgi.health.premiumwrapper.FHPPremiumWebServiceService;
import itgi.health.premiumwrapper.GetFHPPremium;
import itgi.health.premiumwrapper.GetIHPPremium;
import itgi.health.premiumwrapper.IHPInsured;
import itgi.health.premiumwrapper.IHPPolicy;
import itgi.health.premiumwrapper.IHPPremiumDetails;
import itgi.health.premiumwrapper.IHPPremiumWebService;
import itgi.health.premiumwrapper.IHPPremiumWebServiceService;
import play.libs.Json;

public class IffcoTokioHealthInsuranceConnector extends HealthVendorConnector {

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;

	private static final int MAX_FAMILY_PARTIES = 10;
	
	private static final Logger logger = LoggerFactory.getLogger(IffcoTokioHealthInsuranceConnector.class);
	
	@Override
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();
		try {
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			if (vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode()).getValue()
					.equalsIgnoreCase("INDIVIDUAL")) {
				patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "getIHPPremium");
				JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
				JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
				System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
				GetIHPPremium getIHPPremium = new GetIHPPremium();
				IHPPolicy iHPPolicy = new IHPPolicy();
				ArrayOfIHPInsured item = new ArrayOfIHPInsured();
				item.getItem().add(new IHPInsured());
				iHPPolicy.setIhpInsured(item);
				getIHPPremium.setIhpPolicy(iHPPolicy);
				JsonNode orgJsonNode = mapper.valueToTree(getIHPPremium);
				JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
				GetIHPPremium getIHPPremiumReq = mapper.treeToValue(patchedJsonNode, GetIHPPremium.class);
				IHPPremiumWebServiceService iHPPremiumWebServiceService = new IHPPremiumWebServiceService();
				IHPPremiumWebService iHPPremiumWebService = iHPPremiumWebServiceService.getIHPPremiumWebService();
				BindingProvider bindingProvider = (BindingProvider) iHPPremiumWebService;
				DefaultMessageHandler.printMessage(bindingProvider);
				IHPPremiumDetails iHPPremiumDetails =  new IHPPremiumDetails();
				ArrayOfErrorDetails erroritem = new ArrayOfErrorDetails();
				erroritem.getItem().add(new ErrorDetails());
				ArrayOfTns1IHPInsuredResponse insureditem = new ArrayOfTns1IHPInsuredResponse();
				insureditem.getIhpInsured().add(new IHPInsuredResponse());
				//insureditem.getIhpInsured().add(new IHPInsuredResponse());
				iHPPremiumDetails.setIhpInsured(insureditem);
				iHPPremiumDetails.setError(erroritem);
				 iHPPremiumDetails = iHPPremiumWebService.getIHPPremium(getIHPPremiumReq.getIhpPolicy());
				 if(iHPPremiumDetails.getError() == null) {
						vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest, Json.toJson(iHPPremiumDetails),vendorResponse, "getIHPPremiumResponse");
						vendorResponse.addAttributeValue(
								getAttributeValue(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValue()));
						vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
						//vendorResponse.setProduct(appendProductInfo(vendorRequest,vendorResponse));
						vendorResponse = appendProductInfoAndBenefits(vendorRequest, vendorResponse);
				    } else{
				    	logger.error(iHPPremiumDetails.getError().getItem().toString());
				    	vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
								FetchPlanMessage.FAILED_IN_VENDER_SIDE.value()));
						return vendorResponse;
				    }
				
			} else if (vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode()).getValue()
					.equalsIgnoreCase("FAMILY")) {
				
				patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "getFHPPremium");
				JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
				JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
				System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));

				GetFHPPremium getFHPPremium = new GetFHPPremium();
				FHPPolicy fHPPolicy = new FHPPolicy();
				ArrayOfFHPInsured item = new ArrayOfFHPInsured();				
				for(int i=0; i< MAX_FAMILY_PARTIES; i++) {					
					item.getItem().add(new FHPInsured());
				}
				fHPPolicy.setFhpInsured(item);
				getFHPPremium.setFhpPolicy(fHPPolicy);

				JsonNode orgJsonNode = mapper.valueToTree(getFHPPremium);
				JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);

				// Removing the item	
				GetFHPPremium getFHPPremiumReq = mapper.treeToValue(patchedJsonNode, GetFHPPremium.class);
				ListIterator<FHPInsured> fhpInsuredItem = getFHPPremiumReq.getFhpPolicy().getFhpInsured().getItem().listIterator();
				while (fhpInsuredItem.hasNext()) {
					FHPInsured fhpInsured = fhpInsuredItem.next();
					if (fhpInsured.getDateOfBirth() == null || fhpInsured.getDateOfBirth().isEmpty()) {
						fhpInsuredItem.remove();
						continue;
					}

				}				
				
				FHPPremiumWebServiceService fHPPremiumWebServiceService = new FHPPremiumWebServiceService();
				FHPPremiumWebService fHPPremiumWebService = fHPPremiumWebServiceService.getFHPPremiumWebService();

				BindingProvider bindingProvider = (BindingProvider) fHPPremiumWebService;
				DefaultMessageHandler.printMessage(bindingProvider);
				FHPPremiumDetails fHPPremiumDetails = new FHPPremiumDetails();
				ArrayOfErrorDetails erroritem = new ArrayOfErrorDetails();
				erroritem.getItem().add(new ErrorDetails());
				ArrayOfTns1FHPInsuredResponse insureditem = new ArrayOfTns1FHPInsuredResponse();
				for(int i=0; i< MAX_FAMILY_PARTIES; i++) {		
					insureditem.getFhpInsured().add(new FHPInsuredResponse());
				}
				fHPPremiumDetails.setFhpInsured(insureditem);
				fHPPremiumDetails.setError(erroritem);
			    fHPPremiumDetails = fHPPremiumWebService.getFHPPremium(getFHPPremiumReq.getFhpPolicy());
				 if(fHPPremiumDetails.getError() == null) {
						vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest, Json.toJson(fHPPremiumDetails),vendorResponse, "getFHPPremiumResponse");
						vendorResponse.addAttributeValue(
								getAttributeValue(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValue()));
						vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
						//vendorResponse.setProduct(appendProductInfo(vendorRequest,vendorResponse));
						vendorResponse = appendProductInfoAndBenefits(vendorRequest, vendorResponse);
				    } else{
				    	logger.error(fHPPremiumDetails.getError().getItem().toString());
				    	vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
								FetchPlanMessage.FAILED_IN_VENDER_SIDE.value()));
						return vendorResponse;
				    }
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
			return vendorResponse;
		}
		return vendorResponse;
	}

	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("ITGI_HEALTH_" + AttributeUtil.generateRandom());
		return vendorResponse;

	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		fetchPlanFromVendor(vendorRequest);
		ObjectNode policyJson = Json.newObject();
		try {
			if (vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode()).getValue()
					.equals("INDIVIDUAL")) {
				List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
				patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "IHPREQUEST");
				JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
				JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
				System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
				IHPObjectFactory ihpObjectFactory = new IHPObjectFactory(); 
				IHPRequest ihpRequest = ihpObjectFactory.createRequest();
				ihpRequest.setPolicy(ihpObjectFactory.createRequestPolicy());
				ihpRequest.setAccount(ihpObjectFactory.createRequestAccount());
				ihpRequest.setContact(ihpObjectFactory.createRequestContact());
				ListOfIHPInsured listOfIHPInsured = ihpObjectFactory.createRequestListOfIHPInsured();	
				listOfIHPInsured.getIHPInsured().add(new IHPRequest.ListOfIHPInsured.IHPInsured());			
				ihpRequest.setListOfIHPInsured(listOfIHPInsured);
				
				JsonNode orgJsonNode = mapper.valueToTree(ihpRequest);
				JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
				IHPRequest getPayRequest = mapper.treeToValue(patchedJsonNode, IHPRequest.class);
				JAXBContext context = JAXBContext.newInstance(IHPRequest.class);
		        Marshaller jaxbMarshaller = context.createMarshaller();
				StringWriter sw = new StringWriter();
				jaxbMarshaller.marshal(getPayRequest, sw);
				String xmlString = sw.toString();
				System.out.println("getPayRequest..................." + xmlString);
				xmlString=xmlString.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
				
				policyJson.put("UNIQUE_QUOTEID", getPayRequest.getPolicy().getUniqueQuoteId());
				policyJson.put("XML_DATA", xmlString);
				policyJson.put("PARTNER_CODE", "ITGIHLT030");
				policyJson.put("FORM_URL", "http://220.227.8.74/portaltest/ihpReq");
	
			} else if (vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_HEALTH_INSURANCE_TYPE.getCode()).getValue()
					.equals("FAMILY")) {
				List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
				patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "FHPREQUEST");
				JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
				JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
				System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
				FHPObjectFactory fhpObjectFactory = new FHPObjectFactory();
				FHPRequest fhpRequest = fhpObjectFactory.createRequest();
				
				fhpRequest.setPolicy(fhpObjectFactory.createRequestPolicy());
				fhpRequest.setAccount(fhpObjectFactory.createRequestAccount());
				fhpRequest.setContact(fhpObjectFactory.createRequestContact());
				ListOfFHPInsured listOfFHPInsured = fhpObjectFactory.createRequestListOfFHPInsured();
				for(int i=0; i< MAX_FAMILY_PARTIES; i++) {	
					listOfFHPInsured.getFHPInsured().add(new FHPRequest.ListOfFHPInsured.FHPInsuredObj());			
				}
				fhpRequest.setListOfFHPInsured(listOfFHPInsured);
				
				JsonNode orgJsonNode = mapper.valueToTree(fhpRequest);
				JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
				FHPRequest getPayRequest = mapper.treeToValue(patchedJsonNode, FHPRequest.class);
				// Removing the item	
				ListIterator<FHPInsuredObj> fhpInsuredItem = getPayRequest.getListOfFHPInsured().getFHPInsured().listIterator();
				while (fhpInsuredItem.hasNext()) {
					FHPInsuredObj fhpInsured = fhpInsuredItem.next();
					if (fhpInsured.getDateOfBirth() == null || fhpInsured.getDateOfBirth().isEmpty()) {
						fhpInsuredItem.remove();
						continue;
					}

				}

				JAXBContext context = JAXBContext.newInstance(FHPRequest.class);
		        Marshaller jaxbMarshaller = context.createMarshaller();
				StringWriter sw = new StringWriter();
				jaxbMarshaller.marshal(getPayRequest, sw);
				String xmlString = sw.toString();
				
				System.out.println("getPayRequest..................." + xmlString);
				xmlString=xmlString.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
				
				policyJson.put("UNIQUE_QUOTEID", getPayRequest.getPolicy().getUniqueQuoteId());
				policyJson.put("XML_DATA", xmlString);
				policyJson.put("PARTNER_CODE", "ITGIHLT030");
				policyJson.put("FORM_URL", "http://220.227.8.74/portaltest/FHPServiceReq");
			}
			
			policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
			policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
			policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
			policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
			policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
			policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
			policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
			policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
			policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
			policyJson.put(PolicyFields.PREMIUM.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_PREMIUM_AMOUNT.getCode()).getValue());
			policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode()).getValue());

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}

		return policyJson;

	}
}
