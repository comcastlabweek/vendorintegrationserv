package com.arka.integrationserv.models.vendorplan.insurance.health;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.bson.Document;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.DateConstants;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.dataconfiguration.repository.DataConfigurationRepository;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DefaultMessageHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import play.libs.Json;
import religare.health.AdditionalCover;
import  religare.health.intf.xsd.IntPartyDO;
import  religare.health.intf.xsd.IntPartyQuestionDO;
import  religare.health.intf.xsd.IntPolicyAdditionalFieldsDO;
import  religare.health.intf.xsd.IntPolicyDO;
import  religare.health.intf.xsd.IntPolicyDataIO;
import  religare.health.relinterface.CreatePolicy;
import  religare.health.relinterface.CreatePolicyServiceInt;
import  religare.health.relinterface.CreatePolicyServiceIntPortType;

public class ReligareNoClaimBonusSuper extends HealthVendorConnector {

	// 1 proposer + 10 people covered
	private static final int MAX_PARTIES = 11;
	public static final String FIELD_INSURED_COUNT_CODE = "insuredCountCode";
	public static final String FIELD_MIN_AGE_IN_BAND = "minAgeInBand";
	public static final String FIELD_AGE_BAND = "ageBand";
	private static final int COPAY_20_PERCENT_MIN_AGE = 61;
	private static final double COPAY_20_PERCENT_DISCOUNT = 0.2;
	private static final double PA_PREMIUM_PER_LAKH = 70.0;
	DecimalFormat df = new DecimalFormat("############");
	private static List<Integer> mandatoryNCBSumAssuredList = Arrays.asList(new Integer[] { 3, 4 });

	@Inject
	private DataConfigurationRepository dataConfigurationRepository;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;


	private static final Logger logger = LoggerFactory.getLogger(ReligareNoClaimBonusSuper.class);
	@Override
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();
		try {
			if ((vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValueAsLong() / 100000) >= 3) {
				vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
				vendorResponse = appendProductInfoAndBenefits(vendorRequest, vendorResponse);
				vendorResponse = formInsuredMembers(vendorRequest, vendorResponse);
				vendorResponse = getPremiumDetails(vendorRequest, vendorResponse);
			} else {
		    	vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.SUM_ASSURED_NOT_SUPPORTED.value()));
				return vendorResponse;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
			return vendorResponse;
		}
		return vendorResponse;
	}

	public static CreatePolicy prepareCreatePolicyRequest() throws JsonProcessingException {
		CreatePolicy createPolicyReq = new CreatePolicy();
		IntPolicyDataIO intPolicyDataIO = new IntPolicyDataIO();
		createPolicyReq.setIntIO(intPolicyDataIO);
		final IntPolicyDO intPolicyDO = new IntPolicyDO();
		for (int i = 0; i < MAX_PARTIES; i++) {
			IntPartyDO partyDO = new IntPartyDO();
			intPolicyDO.getPartyDOList().add(partyDO);
		}

		intPolicyDataIO.setPolicy(intPolicyDO);
		intPolicyDO.getPolicyAdditionalFieldsDOList().add(new IntPolicyAdditionalFieldsDO());

		JsonNode orgJsonNode = mapper.valueToTree(createPolicyReq);
		System.out.println("Original JsonNode: ");
		System.out.println(mapper.writeValueAsString(orgJsonNode));

		return createPolicyReq;
	}

	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("RELIGARE_HEALTH_" + AttributeUtil.generateRandom());
		return vendorResponse;

	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		ObjectNode policyJson = Json.newObject();
		try {

			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "createPolicy");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
			CreatePolicy createPolicyReq = prepareCreatePolicyRequest();
			JsonNode orgJsonNode = mapper.valueToTree(createPolicyReq);
			final JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);

			CreatePolicy patchedCreatePolicy = mapper.treeToValue(patchedJsonNode, CreatePolicy.class);
			System.out.println("Patched Policy: " + mapper.writeValueAsString(patchedCreatePolicy));

			/// TODO: ************************* ::::: DANGER ::::
			/// *************************///
			/// TODO: ************************* HACK START
			/// *************************///
			/// TODO: ************************* ::::: DANGER ::::
			/// *************************///

			// InputStream medPatchStream =
			// this.getClass().getResourceAsStream("/medical-patch.json");
			// List<PatchRequestVO> medPatchRequestVOList =
			// Arrays.asList(mapper.readValue(medPatchStream,
			// PatchRequestVO[].class));
			// JsonNode medPatchJsonNode =
			// mapper.valueToTree(patchRequestVOList);

			String[] relationships = new String[] { "proposer", "self", "spouse", "son1", "son2", "son3", "daughter1", "daughter2", "daughter3", "father", "mother" };


			List<InputPolicyParams> inputQuoteParams = vendorRequest.getInput_quote_params();

			List<IntPartyDO> patchedPartyDOList = patchedCreatePolicy.getIntIO().getPolicy().getPartyDOList();
			ListIterator<IntPartyDO> listIterator = patchedPartyDOList.listIterator();

			// Skip Proposer
			if (listIterator.hasNext()) {
				IntPartyDO partyDO = listIterator.next();

				if ("MALE".equalsIgnoreCase(partyDO.getGenderCd())) {
					partyDO.setTitleCd("MR");
				} else {
					partyDO.setTitleCd("MS");
				}
				partyDO.setGenderCd(StringUtils.upperCase(partyDO.getGenderCd()));

			}
			while (listIterator.hasNext()) {

				IntPartyDO partyDO = listIterator.next();

				if (!"SELF".equalsIgnoreCase(partyDO.getRelationCd())) {
					partyDO.setGuid(AttributeUtil.generateGUID());
				}

				if ("UDTR".equalsIgnoreCase(partyDO.getRelationCd())) {
					partyDO.setGenderCd("FEMALE");
				}
				if ("SONM".equalsIgnoreCase(partyDO.getRelationCd())) {
					partyDO.setGenderCd("MALE");
				}

				if ("MALE".equalsIgnoreCase(partyDO.getGenderCd())) {
					partyDO.setTitleCd("MR");
				} else {
					partyDO.setTitleCd("MS");
				}
				partyDO.setGenderCd(StringUtils.upperCase(partyDO.getGenderCd()));

				partyDO.getPartyQuestionDOList().clear();

				// Add as default question
				IntPartyQuestionDO questionDO = new IntPartyQuestionDO();
				questionDO.setQuestionCd("pedYesNo");
				questionDO.setQuestionSetCd("yesNoExist");
				questionDO.setResponse(yesOrNo("No"));
				partyDO.getPartyQuestionDOList().add(questionDO);

			}

			IntPolicyDO patchedPolicyDO = patchedCreatePolicy.getIntIO().getPolicy();
			if ("FAMILY".equalsIgnoreCase(patchedPolicyDO.getCoverType())) {
				patchedPolicyDO.setCoverType("FAMILYFLOATER");

			}

			for (InputPolicyParams param : inputQuoteParams) {

				String categoryAttributeCode = param.getCategoryAttributeCode();
				if ("AH-MEDICAL_QUESTIONS".equals(categoryAttributeCode)) {

					
					int partyIndex = Arrays.asList(relationships).indexOf(param.getTag().get(0));
					String fullResponse = StringUtils.upperCase(param.getAdditionalValue());
					
					System.out.println(param.getValue() + ": " + fullResponse);
					
					String[] response = fullResponse.split("\\|_\\|");

					if (partyIndex < 0 || response == null || response.length == 0 || "NO".equals(yesOrNo(response[0]))) {
						continue;
					}

					IntPartyDO partyDO = patchedPartyDOList.get(partyIndex);
					IntPartyQuestionDO questionDO;
				

					switch (param.getValue()) {
					
					// SPECIAL CASE. WE NEED THIS TO BE FIRST TO HAVE ATLEAST ONE QUESTION FILLED
					case "MQ005":
						// NOTE THIS !!!!
						partyDO.getPartyQuestionDOList().get(0).setResponse(yesOrNo(response[0]));
						break;

					case "MQ001":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("H001");
						questionDO.setQuestionSetCd("HEDHealthHospitalized");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);
						break;

					case "MQ002":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("H002");
						questionDO.setQuestionSetCd("HEDHealthClaim");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);
						break;

					case "MQ003":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("H003");
						questionDO.setQuestionSetCd("HEDHealthDeclined");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);
						break;

					case "MQ004":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("H004");
						questionDO.setQuestionSetCd("HEDHealthCovered");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);
						break;

					case "MQ006":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("207");
						questionDO.setQuestionSetCd("PEDhyperTensionDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("hyperTensionExistingSince");
							questionDO.setQuestionSetCd("PEDhyperTensionDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ007":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("232");
						questionDO.setQuestionSetCd("PEDliverDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("liverExistingSince");
							questionDO.setQuestionSetCd("PEDliverDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ008":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("114");
						questionDO.setQuestionSetCd("PEDcancerDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("cancerExistingSince");
							questionDO.setQuestionSetCd("PEDcancerDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ009":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("143");
						questionDO.setQuestionSetCd("PEDcardiacDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("cardiacExistingSince");
							questionDO.setQuestionSetCd("PEDcardiacDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ010":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("105");
						questionDO.setQuestionSetCd("PEDjointpainDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {

							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("jointpainExistingSince");
							questionDO.setQuestionSetCd("PEDjointpainDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ011":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("129");
						questionDO.setQuestionSetCd("PEDkidneyDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("kidneyExistingSince");
							questionDO.setQuestionSetCd("PEDkidneyDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ012":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("205");
						questionDO.setQuestionSetCd("PEDdiabetesDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("diabetesExistingSince");
							questionDO.setQuestionSetCd("PEDdiabetesDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ013":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("164");
						questionDO.setQuestionSetCd("PEDparalysisDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("paralysisExistingSince");
							questionDO.setQuestionSetCd("PEDparalysisDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ014":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("122");
						questionDO.setQuestionSetCd("PEDcongenitalDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("congenitalExistingSince");
							questionDO.setQuestionSetCd("PEDcongenitalDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ015":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("147");
						questionDO.setQuestionSetCd("PEDHivaidsDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("hivaidsExistingSince");
							questionDO.setQuestionSetCd("PEDHivaidsDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ016":

						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("250");
						questionDO.setQuestionSetCd("PEDRespiratoryDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 1) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("respiratoryExistingSince");
							questionDO.setQuestionSetCd("PEDRespiratoryDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}
						break;

					case "MQ017":
						questionDO = new IntPartyQuestionDO();
						questionDO.setQuestionCd("210");
						questionDO.setQuestionSetCd("PEDotherDetails");
						questionDO.setResponse(yesOrNo(response[0]));
						partyDO.getPartyQuestionDOList().add(questionDO);

						if (response.length > 2) {
							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("otherExistingSince");
							questionDO.setQuestionSetCd("PEDotherDetails");
							questionDO.setResponse(response[1]);
							partyDO.getPartyQuestionDOList().add(questionDO);

							questionDO = new IntPartyQuestionDO();
							questionDO.setQuestionCd("otherDiseasesDescription");
							questionDO.setQuestionSetCd("PEDotherDetails");
							questionDO.setResponse(response[2]);
							partyDO.getPartyQuestionDOList().add(questionDO);
						}

					}

				}
			}

			// JsonNode patchMedQuestionJsonNode =
			// mapper.valueToTree(patchMedQuestionVOList);
			// JsonPatch medQuestionJsonPatch =
			// JsonPatch.fromJson(patchMedQuestionJsonNode);
			// System.out.println("medQuestionJsonPatch: " +
			// mapper.writeValueAsString(medQuestionJsonPatch));

			// JsonNode orgCreatePolicyJsonNode =
			// mapper.valueToTree(patchedCreatePolicy);
			// final JsonNode patchedCreatePolicyJsonNode =
			// medQuestionJsonPatch.apply(orgCreatePolicyJsonNode);
			JsonNode patchedCreatePolicyJsonNode = mapper.valueToTree(patchedCreatePolicy);

			patchedCreatePolicy = mapper.treeToValue(patchedCreatePolicyJsonNode, CreatePolicy.class);

			System.out.println("Updated Patched Policy: " + mapper.writeValueAsString(patchedCreatePolicy));

			patchedPartyDOList = patchedCreatePolicy.getIntIO().getPolicy().getPartyDOList();
			listIterator = patchedPartyDOList.listIterator();

			// Skip Proposer
			if (listIterator.hasNext()) {
				listIterator.next();
			}
			while (listIterator.hasNext()) {

				IntPartyDO partyDO = listIterator.next();
				if (partyDO.getBirthDt() == null) {
					listIterator.remove();
					continue;
				}

				partyDO.setRoleCd("PRIMARY");

			}

			System.out.println("::::::::::: Final Patched Policy: " + mapper.writeValueAsString(patchedCreatePolicy));

			/// TODO: ************************* ::::: DANGER ::::
			/// *************************///
			/// TODO: ************************* HACK END
			/// *************************///
			/// TODO: ************************* ::::: DANGER ::::
			/// *************************///

			CreatePolicyServiceInt createPolicyServiceInt = new CreatePolicyServiceInt();
			CreatePolicyServiceIntPortType createPolicyServiceIntPortType = createPolicyServiceInt.getCreatePolicyServiceIntHttpSoap12Endpoint();
			BindingProvider bindingProvider = (BindingProvider) createPolicyServiceIntPortType;
			DefaultMessageHandler.printMessage(bindingProvider);
			// ElementNSImpl object = (ElementNSImpl)
			// createPolicyServiceIntPortType.createPolicy(patchedCreatePolicy.getIntIO());
			// String jsonInString = mapper.writeValueAsString(object);
			// JsonNode node = mapper.convertValue(object, JsonNode.class);
			// jsonInString=jsonInString.substring(jsonInString.indexOf("<?xml"));
			// DocumentBuilderFactory factory =
			// DocumentBuilderFactory.newInstance();
			// DocumentBuilder builder = factory.newDocumentBuilder();
			// InputStream inputStream = new
			// ByteArrayInputStream(jsonInString.getBytes());
			// org.w3c.dom.Document document = builder.parse(inputStream);
			// XmlMapper xmlMapper = new XmlMapper();
			// String jsonInString = xmlMapper.writeValueAsString(object);

			final Object content = createPolicyServiceIntPortType.createPolicy(patchedCreatePolicy.getIntIO());
			String proposalNumber = getProposalNumber(content);
			policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
			policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
			policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
			policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
			policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
			policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());	
			policyJson.put(PolicyFields.PAYMENT_ID.getCode(), RandomStringUtils.randomAlphanumeric(10));
			policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
			policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
			policyJson.put(PolicyFields.PROPOSAL_NUMBER.getCode(), proposalNumber);
			policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode()).getValue());
			policyJson.put(PolicyFields.PREMIUM.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_PREMIUM_AMOUNT.getCode()).getValue());
			policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());

			Date date = new Date();
			Date expiryDate = DateUtils.addYears(date, 1);
			SimpleDateFormat formatter = new SimpleDateFormat(DateConstants.DB_DATE_FORMAT.value());
			String dateStr = formatter.format(date);
			String expiryDateStr = formatter.format(expiryDate);
			policyJson.put(PolicyFields.ISSUED_TIME.getCode(), dateStr);
			policyJson.put(PolicyFields.COMMENCEMENT_DATE.getCode(), dateStr);
			policyJson.put(PolicyFields.EXPIRY_DATE.getCode(), expiryDateStr);			
			if(proposalNumber == null){
				policyJson.put(JsonUtils.ERROR,FetchPlanMessage.UNABLE_TO_PROCESS.value());
			}		

		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}

		return policyJson;

	}

	private String getProposalNumber(Object content) {
		if (content != null) {
			org.w3c.dom.Document document = ((Node) content).getOwnerDocument();

			IntPolicyDO policyDO = new IntPolicyDO();

			NodeList nodeList = document.getElementsByTagName("*");
			int len = nodeList.getLength();
			for (int i = 0; i < len; i++) {
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					// do something with the current element

					if ("proposal-num".equalsIgnoreCase(node.getNodeName())) {
						policyDO.setProposalNum(node.getTextContent());
					}

				}
			}

			return policyDO.getProposalNum();
		} else {
			return "";
		}

	}
	
	
	private String yesOrNo(String value) {
		String yesOrNo = "NO";
		
		if(StringUtils.equalsIgnoreCase("YES", value)) {
			yesOrNo = "YES";
		}
	return yesOrNo;
	
	}
	
	
	public VendorResponse getPremiumDetails(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		String strSumAssured = String
				.valueOf(vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode())
						.getValueAsLong() / 100000);
		try {
			int adultCount = vendorResponse.getAdditionalInfoJson().get("AdultCount").asInt();
			int childCount = vendorResponse.getAdditionalInfoJson().get("ChildCount").asInt();
			String insuredCountCode = String.format("%dA%dC", adultCount, childCount);
			int minAgeInBand = vendorResponse.getAdditionalInfoJson().get("MaxAge").asInt();
			System.out.println("InsuredCountCode: " + insuredCountCode + "| minAgeInBand: " + minAgeInBand);

			if (adultCount > 2) {
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.MAX_ADULT_EXCEED.value()));
				return vendorResponse;

			}
			if (childCount > 4) {
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.MAX_CHILDREN_EXCEED.value()));
				return vendorResponse;
			}

			Document searchQuery = new Document();
			searchQuery.put(FIELD_INSURED_COUNT_CODE, insuredCountCode);
			searchQuery.put(FIELD_MIN_AGE_IN_BAND, new BasicDBObject("$lte", minAgeInBand));

			Bson orderBy = Sorts.orderBy(Sorts.descending(FIELD_MIN_AGE_IN_BAND));
			Bson includeFields = Projections.fields(Projections.include(FIELD_INSURED_COUNT_CODE, FIELD_MIN_AGE_IN_BAND,
					FIELD_AGE_BAND, strSumAssured));
			Document document = dataConfigurationRepository.searchFirst(
					VendorCollectionNames.VENDOR_RELIGARE_HEALTH_PREMIUM.value(), searchQuery, orderBy, includeFields);

			if (null != document && document.containsKey(strSumAssured)) {
				double basePremium = ((Number) document.get(strSumAssured)).doubleValue();

				// Additional Covers
				vendorResponse = calculateAdditionalCovers(vendorRequest, basePremium, minAgeInBand, vendorResponse);

				// Discounts
				//vendorResponse = calculateDiscounts(vendorRequest, basePremium, minAgeInBand, vendorResponse);
				
				vendorResponse
				.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode(),
						vendorRequest
								.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode())
								.getValue()));
			} else {
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.SUM_ASSURED_NOT_SUPPORTED.value()));
				return vendorResponse;
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}

		return vendorResponse;

	}

	private VendorResponse calculateAdditionalCovers(VendorRequest vendorRequest, double basePremium, int minAgeInBand,
			VendorResponse vendorResponse) {
		// TODO: Ganesh: Optimize to single db call for getting both NCB and UAR
		try {
			String AH_NO_CLAIM_BONUS_DESCRIPTION = null;
			String strSumAssured = String
					.valueOf(vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode())
							.getValueAsLong() / 100000);
			String optionalCover = "";
			boolean ncbMandatory = isNCBMandatory(Integer.parseInt(strSumAssured));
			if(ncbMandatory) {
				optionalCover ="NCB";
			}
			
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "premiumPlan");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			JsonNode patchedJson = jsonPatch.apply(JsonUtils.newJsonObject());
			if(!Strings.isNullOrEmpty(patchedJson.get("addOn").asText())) {
				optionalCover += patchedJson.get("addOn").asText();
			}
						
			Document document;
			Bson orderBy = Sorts.orderBy(Sorts.descending(FIELD_MIN_AGE_IN_BAND));
			Bson includeFields = Projections
					.fields(Projections.include("optionalCover", FIELD_MIN_AGE_IN_BAND, FIELD_AGE_BAND, strSumAssured));
			Document searchQuery = new Document();

			searchQuery = new Document();
			searchQuery.put("optionalCover", new BasicDBObject("$in",Arrays.asList(optionalCover)));
			searchQuery.put(FIELD_MIN_AGE_IN_BAND, new BasicDBObject("$lte", minAgeInBand));

			document = dataConfigurationRepository.searchFirst(
					VendorCollectionNames.VENDOR_RELIGARE_HEALTH_NCB_UAR.value(), searchQuery, orderBy, includeFields);

			System.out.println(document);
			if (null != document) {
				System.out.println(document.toJson());
			}

			if (null != document && document.containsKey(strSumAssured)) {
				double percentage = ((Number) document.get(strSumAssured)).doubleValue();
				double premium = basePremium * percentage;

				System.out.println("Additional Premium NCB: " + premium);
				// additionalCoverList.add(new
				// AdditionalCover(AdditionalCoverType.NCB, ncbMandatory,
				// premium));
				AH_NO_CLAIM_BONUS_DESCRIPTION = percentage == 0 ? "This plan does not provide no claim bonus"
						: "No claim bonus upto " + percentage + "%";
				vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_NO_CLAIM_BONUS.getCode(),
						String.valueOf(percentage * 100)));
				vendorResponse.addAttributeValue(getAttributeValue(
						HealthAttributeCode.AH_NO_CLAIM_BONUS_DESCRIPTION.getCode(), AH_NO_CLAIM_BONUS_DESCRIPTION));
				// TODO:Remove this and include as part of the original
				// calculation
				// GST
				basePremium = basePremium * 1.18;

				vendorResponse.addAttributeValue(
						getAttributeValue(HealthAttributeCode.AH_PREMIUM_AMOUNT.getCode(), df.format(basePremium)));
				
				System.out.println("Base Premium: " + basePremium);
			}

	
			// Additional Cover: UAR
			boolean uarRequested = true;
			// if (null != reqAdditionalCovers) {
			// // Check if it was requested:
			// uarRequested = reqAdditionalCovers.stream().filter(ac -> ac.getType()
			// == AdditionalCoverType.UAR)
			// .findFirst().isPresent();
			// }
	
			if (uarRequested) {
				searchQuery = new Document();
				searchQuery.put("optionalCover", "UAR");
				searchQuery.put(FIELD_MIN_AGE_IN_BAND, new BasicDBObject("$lte", minAgeInBand));
	
				document = dataConfigurationRepository.searchFirst(
						VendorCollectionNames.VENDOR_RELIGARE_HEALTH_NCB_UAR.value(), searchQuery, orderBy, includeFields);
	
				System.out.println(document);
				if (null != document) {
					System.out.println(document.toJson());
				}
	
				if (null != document && document.containsKey(strSumAssured)) {
					double percentage = ((Number) document.get(strSumAssured)).doubleValue();
					double premium = basePremium * percentage;
					System.out.println("Additional Premium UAR: " + premium);
					// additionalCoverList.add(new
					// AdditionalCover(AdditionalCoverType.UAR, false, premium));
					vendorResponse.addAttributeValue(
							getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode(), "yes"));
					vendorResponse.addAttributeValue(
							getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_PERCENT.getCode(),
									String.valueOf(percentage * 100)));
					vendorResponse.addAttributeValue(getAttributeValue(
							HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_CONDITION.getCode(), ""));
				}
			} else {
				vendorResponse.addAttributeValue(
						getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode(), "no"));
			}
	
			// Additional Cover: Personal Accident
			AdditionalCover paAdditionalCover = null;
			// if (null != reqAdditionalCovers) {
			// // Check if it was requested:
			// Optional<AdditionalCover> additionalCoverOptional =
			// reqAdditionalCovers.stream()
			// .filter(ac -> ac.getType() == AdditionalCoverType.PA).findFirst();
			// if (additionalCoverOptional.isPresent()) {
			// paAdditionalCover = additionalCoverOptional.get();
			// }
			// }
	
			if (null != paAdditionalCover) {
				double paPremium = Double.parseDouble(strSumAssured) * PA_PREMIUM_PER_LAKH;
				System.out.println("Additional Premium PA: " + paPremium);
				// additionalCoverList.add(new
				// AdditionalCover(AdditionalCoverType.PA, false, paPremium));
			}
			// }
		} catch (JsonPatchException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vendorResponse;

	}
	private boolean isNCBMandatory(int sumAssuredInLakhs) {

		return mandatoryNCBSumAssuredList.contains(sumAssuredInLakhs);
	}

}
