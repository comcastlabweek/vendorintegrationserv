package com.arka.integrationserv.models.vendorplan.insurance.health;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.ws.service.WsServiceUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.inject.Inject;

import play.libs.Json;

public class StarHealthInsuranceConnector extends HealthVendorConnector {

    public static final String ARKA_VENDOR_STAR_HEALTH_FETCH_PLAN_URL = "arka.vendor.starHealth.fetchPlanUrl";
    @Inject
    CategoryTransformationUtils categoryTransformationUtils;

    @Inject
    private WsServiceUtils wsUtils;

    @Inject
    private PropUtils propUtils;


    private static final Logger logger = LoggerFactory.getLogger(StarHealthInsuranceConnector.class);

    public VendorResponse fetchPlanFromVendor(VendorRequest vendorsCommonRequest)  {
        VendorResponse vendorResponse = new VendorResponse();
        String url = propUtils.get(ARKA_VENDOR_STAR_HEALTH_FETCH_PLAN_URL);
        try {
            Boolean isFamily = vendorsCommonRequest.getInput_quote_params().stream().anyMatch(inputQuoteParams -> { return (inputQuoteParams.getCategoryAttributeCode().equalsIgnoreCase("AH-HEALTH_INSURANCE_TYPE")
                    && inputQuoteParams.getValue().equalsIgnoreCase("FAMILY"));
            });


            JsonNode vendorReq =  makeRequestObjectUsing(vendorsCommonRequest,"PREMIUMCALCULATE");

            switch (vendorReq.get("policyTypeName").asText()) {
                case "MCINEW":
                    if(!isFamily){
                        validationForIndividualPlan(vendorsCommonRequest,vendorResponse,vendorReq);
                        apiCallToStarAndConvertToVendorResponse(url,vendorReq,vendorResponse,vendorsCommonRequest);
                        appendBasicInfo(vendorsCommonRequest, vendorResponse,vendorReq.get("policyTypeName").asText());
                    }else {
                        vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
                    }
                    break;
                case "COMPREHENSIVEIND":
                    if(!isFamily){
                        validationForIndividualPlan(vendorsCommonRequest,vendorResponse,vendorReq);
                        apiCallToStarAndConvertToVendorResponse(url,vendorReq,vendorResponse,vendorsCommonRequest);
                        appendBasicInfo(vendorsCommonRequest, vendorResponse,vendorReq.get("policyTypeName").asText());
                    }else {
                        vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(), FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(), null));
                    }
                    break;
                case "FHONEW":
                    if(isFamily){
                        ValidationForFamilyPlan( vendorReq,  vendorsCommonRequest, vendorResponse);
                        apiCallToStarAndConvertToVendorResponse(url,vendorReq,vendorResponse,vendorsCommonRequest);
                        appendBasicInfo(vendorsCommonRequest, vendorResponse,vendorReq.get("policyTypeName").asText());
                    }else {
                        vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
                    }
                    break;
                case "COMPREHENSIVE":
                    if(isFamily){
                        ValidationForFamilyPlan( vendorReq,  vendorsCommonRequest, vendorResponse);
                        apiCallToStarAndConvertToVendorResponse(url,vendorReq,vendorResponse,vendorsCommonRequest);
                        appendBasicInfo(vendorsCommonRequest, vendorResponse,vendorReq.get("policyTypeName").asText());
                    }else {
                        vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
                    }
                    break;
                default:

            }

        } catch (IOException | JsonPatchException | InterruptedException | ExecutionException e) {
            System.out.println(e.getMessage());
            vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
			return vendorResponse;
        }
        return vendorResponse;
    }

    private JsonNode makeRequestObjectUsing(VendorRequest vendorsCommonRequest, String vendorObjectName) throws JsonPatchException, IOException {
        List<PatchRequestVO> patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorsCommonRequest, "PREMIUMCALCULATE");
        JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
        JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
        return jsonPatch.apply(Json.parse("{}"));
    }

    private String getSchemeId(VendorRequest vendorsCommonRequest,VendorResponse vendorResponse){
        formInsuredMembers(vendorsCommonRequest,vendorResponse);
            String insuredCountCode = String.format("%dA%dC", vendorResponse.getAdditionalInfoJson().get("AdultCount").asInt(), vendorResponse.getAdditionalInfoJson().get("ChildCount").asInt());
            switch(insuredCountCode){
                case "2A":
                    return "1";
                case "1A1C":
                    return "2";
                case "1A2C":
                    return "3";
                case "1A3C":
                    return "4";
                case "2A1C":
                    return "5";
                case "2A2C":
                    return "6";
                case "2A3C":
                    return "7";
            }
        return "0";
    }

    private VendorResponse appendBasicInfo(VendorRequest vendorRequest,VendorResponse vendorResponse,String quoteKeySuffix){
        vendorResponse.setCategoryId(vendorRequest.getCategoryId());
        vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
        vendorResponse.setProductId(vendorRequest.getProductId());
        vendorResponse.setVendorId(vendorRequest.getVendorId());
        vendorResponse.setVendorName(vendorRequest.getVendorName());
        vendorResponse = appendProductInfoAndBenefits(vendorRequest, vendorResponse);
        vendorResponse.setQuoteKey("STAR_HEALTH_"+ quoteKeySuffix + AttributeUtil.generateRandom());
        return vendorResponse;
    }

    private VendorResponse validationForIndividualPlan(VendorRequest vendorRequest, VendorResponse vendorResponse, JsonNode reqToVendor){

        return vendorResponse;
    }
    private VendorResponse ValidationForFamilyPlan(JsonNode reqToVendor, VendorRequest vendorRequest, VendorResponse vendorResponse){

        ((ObjectNode) reqToVendor).replace("schemeId",Json.parse(getSchemeId( vendorRequest, vendorResponse)));
        List<String> removeList = new ArrayList<>();
        System.out.println("reqToVendor: "+reqToVendor);
        Iterator<String> iterator = ((ObjectNode) reqToVendor).fieldNames();
        while (iterator.hasNext()) {
            String key = iterator.next();
            JsonNode element = reqToVendor.get(key);
            if(element.get("dob") != null && element.get("dob").asText().equalsIgnoreCase("")){
                removeList.add(key);
            }
        }
        ((ObjectNode) reqToVendor).remove(removeList);
        System.out.println("reqToVendor: "+reqToVendor);
//        makeVendorRequestInsuredsSequential(reqToVendor);

        return  vendorResponse;
    }
    private VendorResponse apiCallToStarAndConvertToVendorResponse(String url, JsonNode reqToVendor, VendorResponse vendorResponse, VendorRequest vendorRequest) throws ExecutionException, InterruptedException {
        JsonNode vendorResponsesFromStar = wsUtils.doPost(url, null, null, reqToVendor).toCompletableFuture().get();
        System.out.println("REQUEST"+Json.toJson(reqToVendor));
        System.out.println("RESPONSE FROM STAR"+Json.toJson(vendorResponsesFromStar));
        vendorResponse=categoryTransformationUtils.mapToCatalogCategory(vendorRequest, vendorResponsesFromStar, vendorResponse, "PREMIUMCALCULATERESPONSE");
        vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValue()));
        return vendorResponse;
    }
    private JsonNode makeVendorRequestInsuredsSequential(JsonNode reqToVendor){
        Iterator<String> iterator = ((ObjectNode)reqToVendor).fieldNames();
        List<String> replaceList = new ArrayList<>();
        while(iterator.hasNext()){
            String key = iterator.next();
            if(key.toString().contains("insureds")){
                replaceList.add(key);
            }
        }
        int size = replaceList.size();
        try {
            for (int i = 0; i < size; i++) {

                ((ObjectNode) reqToVendor).set("insureds[" + i + "]", ((ObjectNode) reqToVendor).get(replaceList.get(i)));
                System.out.println(Integer.parseInt(replaceList.get(i).substring(9, 10)));
                if (Integer.parseInt(replaceList.get(i).substring(9, 10)) > size) {
                    ((ObjectNode) reqToVendor).remove(replaceList.get(i));
                }

            }
            System.out.println(Json.prettyPrint(reqToVendor));
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return reqToVendor;
    }


}
