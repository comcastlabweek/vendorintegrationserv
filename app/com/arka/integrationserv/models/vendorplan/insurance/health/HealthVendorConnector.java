package com.arka.integrationserv.models.vendorplan.insurance.health;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.procurement.constants.UIconstants;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.arka.integrationserv.models.vendorplan.VendorConnector;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;

public class HealthVendorConnector implements VendorConnector {

	private static final Logger logger = LoggerFactory.getLogger(HealthVendorConnector.class);
	private static int MIN_ADULT_AGE = 18;
	private static int MAX_CHILD_AGE = 24;
	
	private static String PRODUCT_FEATURE="PRODUCT_FEATURE";
	protected String AH_ROOM_RENT_ELIGIBLITY = "";
	protected String AH_ROOM_RENT_CASH_LIMIT = "";
	protected String AH_ROOM_RENT_TYPE = "";
	protected String AH_ROOM_RENT_DESCRIPTION = "";

	protected String AH_DAY_CARE_TREATMENT = "";
	protected String AH_DAY_CARE_TREATMENT_DESCRIPTION = "";

	protected String AH_AMBULANCE_COVER = "";
	protected String AH_AMBULANCE_COVER_CASH_LIMIT = "";
	protected String AH_AMBULANCE_DESCRIPTION = "";

	protected String AH_NO_CLAIM_BONUS = "";
	protected String AH_NO_CLAIM_BONUS_DESCRIPTION = "";

	protected String AH_DOMICILIARY_HOSPITALISATION = "";
	protected String AH_DOMICILIARY_HOSPITALISATION_CASH_LIMIT = "";
	protected String AH_DOMICILIARY_HOSPITALISATION_DESCRIPTION = "";

	protected String AH_AUTOMATIC_RESTORATION_BENEFITS = "";
	protected String AH_AUTOMATIC_RESTORATION_BENEFITS_PERCENT = "";
	protected String AH_AUTOMATIC_RESTORATION_BENEFITS_CONDITION = "";
	protected String AH_AUTOMATIC_RESTORATION_BENEFITS_DESCRIPTION = "";

	protected String AH_PRE_EXISTING_ILLNESS_COVER = "";
	protected String AH_PRE_EXISTING_ILLNESS_COVER_DESCRIPTION = "";

	protected String AH_CO_PAY = "";
	protected String AH_CO_PAY_AGE_LIMIT = "";
	protected String AH_CO_PAY_NON_NETWORK_HOSPITAL = "";
	protected String AH_CO_PAY_PERCENT = "";
	protected String AH_CO_PAY_DESCRIPTION = "";

	protected String AH_CRITICAL_ILLNESS_COVERAGE = "";
	protected String AH_CRITICAL_ILLNESS_COVERAGE_DESCRIPTION = "";

	protected String AH_OUTPATIENT_TREATMENT_CASH_LIMIT = "";
	protected String AH_OUTPATIENT_TREATMENT_CASH_LIMIT_DESCRIPTION = "";

	protected String AH_MEDICAL_CHECK_UP = "";
	protected String AH_MEDICAL_CHECK_UP_CASH_LIMIT_PERIODICITY = "";
	protected String AH_MEDICAL_CHECK_UP_CASH_LIMIT = "";
	protected String AH_MEDICAL_CHECK_UP_DESCRIPTION = "";

	protected String AH_WORLD_WIDE_EMEREGENCY_COVER = "";
	protected String AH_WORLD_WIDE_EMEREGENCY_COVER_CASH_LIMIT = "";
	protected String AH_WORLD_WIDE_EMEREGENCY_COVER_DESCRIPTION = "";

	protected String AH_ORGAN_DONOR = "";
	protected String AH_ORGAN_DONOR_DESCRIPTION = "";

	protected String AH_POST_HOSPITALIZATION_EXPENSES = "";
	protected String AH_POST_HOSPITALIZATION_EXPENSES_DAY_LIMIT = "";
	protected String AH_POST_HOSPITALIZATION_EXPENSES_DESCRIPTION = "";

	protected String AH_PRE_HOSPITALIZATION_EXPENSES = "";
	protected String AH_PRE_HOSPITALIZATION_EXPENSES_DAY_LIMIT = "";
	protected String AH_PRE_HOSPITALIZATION_EXPENSES_DESCRIPTION = "";

	protected String AH_DAILY_HOSPITALIZATION_EXPENSES = "";
	protected String AH_DAILY_HOSPITALIZATION_EXPENSES_DAY_LIMIT = "";
	protected String AH_DAILY_HOSPITALIZATION_EXPENSES_CASH_PER_DAY = "";
	protected String AH_DAILY_HOSPITALIZATION_EXPENSES_DESCRIPTION = "";

	protected String AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT = "";
	protected String AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_WITH_CASH_LIMIT = "";
	protected String AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_DESCRIPTION = "";

	protected String AH_DENTAL_COVER = "";
	protected String AH_DENTAL_COVER_WAITING_PERIOD = "";
	protected String AH_DENTAL_COVER_CASH_LIMIT = "";
	protected String AH_DENTAL_COVER_DESCRIPTION = "";

	protected String AH_CONVALESCENCE_EXPENSES = "";
	protected String AH_CONVALESCENCE_EXPENSES_WITH_DAY_LIMIT = "";
	protected String AH_CONVALESCENCE_EXPENSES_CASH_LIMIT_PER_DAY = "";
	protected String AH_CONVALESCENCE_EXPENSES_DESCRIPTION = "";

	protected String AH_MATERNITY_BENEFITS = "";
	protected String AH_MATERNITY_BENEFITS_WAITING_PERIOD = "";
	protected String AH_MATERNITY_BENEFITS_NO_OF_CHILD_BIRTHS = "";
	protected String AH_MATERNITY_BENEFITS_CASH_LIMIT = "";
	protected String AH_MATERNITY_BENEFITS_DESCRIPTION = "";

	protected String AH_NEW_BORN_BABY_COVER = "";
	protected String AH_NEW_BORN_BABY_COVER_DESCRIPTION = "";
	protected String AH_NEW_BORN_BABY_COVER_DAY_LIMIT = "";

	protected String AH_CASHLESS_FACILITY = "";
	protected String AH_CASHLESS_FACILITY_DESCRIPTION = "";

	protected String AH_SUB_LIMIT_TYPE = "";
	protected String AH_SUB_LIMIT_AMOUNT = "";
	protected String AH_SUB_LIMIT_PERCENT = "";
	protected String AH_SUB_LIMIT_DESCRIPTION = "";

	protected String AH_DIABETES_CARE = "";
	protected String AH_DIABETES_CARE_CASH_LIMIT = "";
	protected String AH_DIABETES_CARE_DESCRIPTION = "";

	protected String AH_LIFE_LONG_RENEWABILITY = "";
	protected String AH_LIFE_LONG_RENEWABILITY_DESCRIPTION = "";

	protected String AH_DOCTOR_ON_CALL_SERVICE = "";
	protected String AH_DOCTOR_ON_CALL_SERVICE_DESCRIPTION = "";
	Map<String, JsonNode> categoryAttributeIdMap;

	protected List<String> uitags = Arrays.asList(
			new String[] { UIconstants.FILTER.name(), UIconstants.COMPARE.name(), UIconstants.HIGHLIGHT.name() });
	
	public static ObjectMapper mapper;

	static {
		mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
		SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
		mapper.setDateFormat(outputFormat);
	}

	@Inject
	DBUtil dbUtil;

	public ObjectNode getValidationError(String errorCode,String errorKey, String errorText) {
		ObjectNode errorJson = Json.newObject();
		errorJson.put(JsonUtils.ERROR_CODE, errorCode);
		errorJson.put(JsonUtils.ERROR, errorKey);
		errorJson.put(JsonUtils.ERROR_MESSAGE, errorText);
		ObjectNode validationErrorJson = Json.newObject();
		validationErrorJson.set(ResponsePayLoad.ERROR, errorJson);
		return validationErrorJson;
	}
	
	public VendorResponse appendProductInfoAndBenefits(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		ObjectNode vendorOutputJson = Json.newObject();
		String productId = vendorRequest.getProductId();
		JsonNode productJson = dbUtil.getProductJson(productId);
		if (productJson == null || !productJson.hasNonNull(ProductFields.PRODUCT_ID.value())) {
			logger.error(FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value() + " : " + productId);
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value(),
					FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value() + " : " + productId));
			return vendorResponse;
		}
		vendorOutputJson.put(ProductFields.PRODUCT_NAME.value(),
				productJson.get(ProductFields.PRODUCT_NAME.value()).asText());
		vendorOutputJson.put(ProductFields.PRODUCT_CODE.value(),
				productJson.get(ProductFields.PRODUCT_CODE.value()).asText());
		String productImage = "";
		String productDocument = "";

		if (JsonUtils.isValidField(productJson, ProductFields.PRODUCT_IMAGE_URL.value())) {
			productImage = productJson.get(ProductFields.PRODUCT_IMAGE_URL.value()).asText();

		}
		if (JsonUtils.isValidField(productJson, ProductFields.PRODUCT_DOCUMENT_URL.value())) {
			productDocument = productJson.get(ProductFields.PRODUCT_DOCUMENT_URL.value()).asText();
		}

		vendorOutputJson.put(ProductFields.PRODUCT_IMAGE_URL.value(), productImage);
		vendorOutputJson.put(ProductFields.PRODUCT_DOCUMENT_URL.value(), productDocument);
		vendorResponse.setProduct(vendorOutputJson);
		
		categoryAttributeIdMap = dbUtil.getCategoryAttributesIDMap(vendorRequest.getCategoryId(), Boolean.TRUE);
		if (categoryAttributeIdMap == null) {
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.CATEGORY_ATTRIBUTE_ID_DOES_NOT_EXISTS.value(),
					FetchPlanMessage.CATEGORY_ATTRIBUTE_ID_DOES_NOT_EXISTS.value() + " : " + vendorRequest.getCategoryId()));
			return vendorResponse;
		}
		
		Map<String, String> descTagMap = new HashMap<String, String>();
		Map<String, String> productFeatureTagMap = new HashMap<String, String>();
		if (JsonUtils.isValidField(productJson, ProductFields.PRODUCT_ATTRIBUTES.value())) {
			JsonNode productAttributeArr = productJson.get(ProductFields.PRODUCT_ATTRIBUTES.value());
			productAttributeArr.elements().forEachRemaining(productAttribute -> {
				String productAttributeValue = productAttribute.get(ProductFields.PRODUCT_ATTRIBUTE_VALUE.value())
						.asText();
				String productAttributeId = CryptUtils.decryptData(productAttribute.get(ProductFields.PRODUCT_ATTRIBUTE_ID.value()).asText());
				if (categoryAttributeIdMap.containsKey(productAttributeId)) {
					JsonNode categoryAttributeJson = categoryAttributeIdMap.get(productAttributeId);
					JsonNode tagListJson = categoryAttributeJson
							.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAGS.value());
					tagListJson.elements().forEachRemaining(tagJson -> {
						String tagName = tagJson.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAG_NAME.value())
								.asText();
						if (!uitags.contains(tagName.toUpperCase()) &&categoryAttributeJson
								.hasNonNull(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value())) {
							descTagMap.put(categoryAttributeJson
									.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText(), productAttributeValue);
							if(PRODUCT_FEATURE.equalsIgnoreCase(tagName.toUpperCase())){
								productFeatureTagMap.put(categoryAttributeJson
										.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText(), productAttributeValue);
							}
							
						}
					});
				}
			});
		}
		loadRoomRent(vendorResponse, descTagMap);
		loadDayCareTreatment(vendorResponse, descTagMap);
		loadAmbulanceCover(vendorResponse, descTagMap);
		loadNoclaimBonus(vendorResponse, descTagMap);
		loadDomicilaiaryHospitalization(vendorResponse, descTagMap);
		loadAutomaticRestorationBenefits(vendorResponse, descTagMap);
		loadPreExistingIllnessCover(vendorResponse, descTagMap);
		loadCoPay(vendorResponse, descTagMap);
		loadCriticalIllnessCoverage(vendorResponse, descTagMap);
		loadOutPatientTreatment(vendorResponse, descTagMap);
		loadMedicalCheckUp(vendorResponse, descTagMap);
		loadWorldWideEmergencyCover(vendorResponse, descTagMap);
		loadOrganDonor(vendorResponse, descTagMap);
		loadPostHospitalizationExpenses(vendorResponse, descTagMap);
		loadPreHospitalizationExpenses(vendorResponse, descTagMap);
		loadDailyHospitalizationExpenses(vendorResponse, descTagMap);
		loadAyurVedaTreatment(vendorResponse, descTagMap);
		loadDentalCover(vendorResponse, descTagMap);
		loadConvalescenceExpenses(vendorResponse, descTagMap);
		loadMaternityBenefits(vendorResponse, descTagMap);
		loadNewBornBabyCover(vendorResponse, descTagMap);
		loadCashlessfacilty(vendorResponse, descTagMap);
		loadSubLimitType(vendorResponse, descTagMap);
		loadDiabetesCare(vendorResponse, descTagMap);
		loadLifeLongRenewability(vendorResponse, descTagMap);
		loadDoctorOncallService(vendorResponse, descTagMap);

		return vendorResponse;

	}	

	protected VendorResponse formInsuredMembers(VendorRequest vendorRequest,VendorResponse vendorResponse) {
	    int adultCount = 0;
	    int childCount = 0;
	    int maxAge = 0;
		ObjectNode insuredMemebersJson = Json.newObject();
		ObjectNode insuredMemebersObj = insuredMemebersJson.putObject("INSURED_MEMBERS");
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_SELF_AGE.getCode())) {
			 adultCount++;
			int selfAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SELF_AGE.getCode())
					.getValueAsInt();
			maxAge = selfAge;
			ObjectNode selfDetailObj = insuredMemebersObj.putObject("self");
			selfDetailObj.put("age", selfAge);
		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_SPOUSE_AGE.getCode())) {
			 adultCount++;
			int spouseAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SPOUSE_AGE.getCode())
					.getValueAsInt();
			if(spouseAge>maxAge){
				maxAge = spouseAge;
			}
			ObjectNode selfDetailObj = insuredMemebersObj.putObject("spouse");
			selfDetailObj.put("age", spouseAge);

		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_SON_AGE.getCode())) {
			List<InputPolicyParams> sonAgeList = vendorRequest
					.getInputQuoteForAttribute(HealthAttributeCode.AH_SON_AGE.getCode());
			ObjectNode sonDetailObj = insuredMemebersObj.putObject("son");
			for (InputPolicyParams sonAge : sonAgeList) {
				int sonAgeAsInt = sonAge.getValueAsInt();
				if ((adultCount == 0 && sonAgeAsInt > MIN_ADULT_AGE) || sonAgeAsInt > MAX_CHILD_AGE) {
	                adultCount++;
	            } else {
	                childCount++;
	            }
				List<String> sonTag = sonAge.getTag();
				if (sonTag != null && sonTag.size() > 0) {
					sonTag.stream().filter(
							sonTagElement -> sonTagElement.startsWith("son") && !sonTagElement.equalsIgnoreCase("son"))
							.forEach(sonTagElement -> {
								try {
									int sonIndex = Integer.parseInt(sonTagElement.split("son_")[1]);
									ObjectNode sonIndexObj = sonDetailObj.putObject(String.valueOf(sonIndex));
									sonIndexObj.put("age", sonAge.getValue());

								} catch (Exception e) {
									throw new ArkaRunTimeException(e);
								}

							});
				}

			}
		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_DAUGHTER_AGE.getCode())) {
			List<InputPolicyParams> daughterAgeList = vendorRequest
					.getInputQuoteForAttribute(HealthAttributeCode.AH_DAUGHTER_AGE.getCode());
			ObjectNode daughterDetailObj = insuredMemebersObj.putObject("daughter");
			for (InputPolicyParams daughterAge : daughterAgeList) {
				int daughterAgeAsInt = daughterAge.getValueAsInt();
				if ((adultCount == 0 && daughterAgeAsInt > MIN_ADULT_AGE) || daughterAgeAsInt > MAX_CHILD_AGE) {
	                adultCount++;
	            } else {
	                childCount++;
	            }
				List<String> daughterTag = daughterAge.getTag();
				if (daughterTag != null && daughterTag.size() > 0) {
					daughterTag.stream().filter(daughterTagElement -> daughterTagElement.startsWith("daughter")
							&& !daughterTagElement.equalsIgnoreCase("daughter")).forEach(daughterTagElement -> {
								try {
									int daughterIndex = Integer.parseInt(daughterTagElement.split("daughter_")[1]);
									ObjectNode daughterIndexObj = daughterDetailObj
											.putObject(String.valueOf(daughterIndex));
									daughterIndexObj.put("age", daughterAge.getValue());

								} catch (Exception e) {
									throw new ArkaRunTimeException(e);
								}

							});
				}

			}
		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_FATHER_AGE.getCode())) {
			 adultCount++;
			int fatherAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_FATHER_AGE.getCode())
					.getValueAsInt();
			if(fatherAge > maxAge){
				maxAge = fatherAge;
			}
			ObjectNode fatherDetailObj = insuredMemebersObj.putObject("father");
			fatherDetailObj.put("age", fatherAge);

		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_MOTHER_AGE.getCode())) {
			 adultCount++;
			int motherAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_MOTHER_AGE.getCode())
					.getValueAsInt();
			if(motherAge > maxAge){
				maxAge = motherAge;
			}
			ObjectNode motherDetailObj = insuredMemebersObj.putObject("mother");
			motherDetailObj.put("age", motherAge);

		}
		insuredMemebersJson.put("AdultCount", adultCount);
		insuredMemebersJson.put("ChildCount", childCount);
		insuredMemebersJson.put("MaxAge", maxAge);
		vendorResponse.addToAdditionalInfoJson(insuredMemebersJson);
		return vendorResponse;
	}
	protected AttributeValue getAttributeValue(String categoryAttributeCode, String value) {
		AttributeValue paramValueObj = new AttributeValue();
		paramValueObj.setCategoryAttributeCode(categoryAttributeCode);
		paramValueObj.setValue(value);
		paramValueObj.setTagsAsList(getTagNameListForAttribute(categoryAttributeCode));
		getDisplayNameAndWeight(paramValueObj, categoryAttributeCode);
		return paramValueObj;
	}

	protected List<String> getTagNameListForAttribute(String attributeCode) {

		if (attributeCode == null || attributeCode.trim().isEmpty()) {
			return null;
		}

		List<String> tagNameList = new ArrayList<String>();
		new ArrayList<JsonNode>(categoryAttributeIdMap.values()).stream()
				.filter(categoryAttributeJson -> categoryAttributeJson
						.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText().equals(attributeCode))
				.forEach(categoryAttributeJson -> {
					if (JsonUtils.isValidField(categoryAttributeJson,
							CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAGS.value())) {
						JsonNode tagListJson = categoryAttributeJson
								.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAGS.value());
						tagListJson.elements().forEachRemaining(tagJson -> {
							String tagName = tagJson.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAG_NAME.value())
									.asText();
							tagNameList.add(tagName);
						});
					}
				});

		return tagNameList;
	}
	protected void getDisplayNameAndWeight(AttributeValue paramValueObj,String attributeCode) {

		if (attributeCode != null &&  !attributeCode.trim().isEmpty()) {
			
		new ArrayList<JsonNode>(categoryAttributeIdMap.values()).stream()
				.filter(categoryAttributeJson -> categoryAttributeJson
						.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText().equals(attributeCode))
				.forEach(categoryAttributeJson -> {
					if (JsonUtils.isValidField(categoryAttributeJson,"display_label")) {
						paramValueObj.setDisplayName(categoryAttributeJson
									.get("display_label").asText());
						
					}
					if (JsonUtils.isValidField(categoryAttributeJson,"weight")) {
						paramValueObj.setWeight(categoryAttributeJson
									.get("weight").asInt());
						
					}
					
					
				});
		}
	}

	@Override
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest)  {
		// TODO Auto-generated method stub
		return null;
	}

	private VendorResponse loadRoomRent(VendorResponse vendorResponse, Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_ROOM_RENT_ELIGIBLITY.getCode())) {
			AH_ROOM_RENT_ELIGIBLITY = descTagMap.get(HealthAttributeCode.AH_ROOM_RENT_ELIGIBLITY.getCode());
		}
		if (descTagMap.containsKey(HealthAttributeCode.AH_ROOM_RENT_CASH_LIMIT.getCode())) {
			AH_ROOM_RENT_CASH_LIMIT = descTagMap.get(HealthAttributeCode.AH_ROOM_RENT_CASH_LIMIT.getCode());
		}
		if (descTagMap.containsKey(HealthAttributeCode.AH_ROOM_RENT_TYPE.getCode())) {
			AH_ROOM_RENT_TYPE = descTagMap.get(HealthAttributeCode.AH_ROOM_RENT_TYPE.getCode());
		}
		if (descTagMap.containsKey(HealthAttributeCode.AH_ROOM_RENT_DESCRIPTION.getCode())) {
			AH_ROOM_RENT_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_ROOM_RENT_DESCRIPTION.getCode());
		}

		
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_ROOM_RENT_ELIGIBLITY.getCode(), AH_ROOM_RENT_ELIGIBLITY));
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_ROOM_RENT_DESCRIPTION.getCode(), AH_ROOM_RENT_DESCRIPTION));

		if (!AH_ROOM_RENT_ELIGIBLITY.equalsIgnoreCase("no_limit")) {
			if (AH_ROOM_RENT_ELIGIBLITY.equalsIgnoreCase("cash_limit")) {
				vendorResponse.addAttributeValue(getAttributeValue(
						HealthAttributeCode.AH_ROOM_RENT_CASH_LIMIT.getCode(), AH_ROOM_RENT_CASH_LIMIT));
			} else {
				vendorResponse.addAttributeValue(
						getAttributeValue(HealthAttributeCode.AH_ROOM_RENT_TYPE.getCode(), AH_ROOM_RENT_TYPE));
			}
		}

		return vendorResponse;

		
	}
	private VendorResponse loadDayCareTreatment(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {

		if (descTagMap.containsKey(HealthAttributeCode.AH_DAY_CARE_TREATMENT_DESCRIPTION.getCode())
				&& descTagMap.containsKey(HealthAttributeCode.AH_DAY_CARE_TREATMENT.getCode())) {
			AH_DAY_CARE_TREATMENT_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_DAY_CARE_TREATMENT_DESCRIPTION.getCode());
		}
		if (descTagMap.containsKey(HealthAttributeCode.AH_DAY_CARE_TREATMENT.getCode())) {
			AH_DAY_CARE_TREATMENT = descTagMap.get(HealthAttributeCode.AH_DAY_CARE_TREATMENT.getCode());
		}

		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_DAY_CARE_TREATMENT.getCode(), AH_DAY_CARE_TREATMENT));
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_DAY_CARE_TREATMENT_DESCRIPTION.getCode(), AH_DAY_CARE_TREATMENT_DESCRIPTION));
		return vendorResponse;

	}

	private VendorResponse loadAmbulanceCover(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {

		if (descTagMap.containsKey(HealthAttributeCode.AH_AMBULANCE_COVER.getCode())) {
			AH_AMBULANCE_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_AMBULANCE_COVER.getCode());
		}

		/*
		 * AH_AMBULANCE_DESCRIPTION = AH_AMBULANCE_COVER.equals("not_covered") ?
		 * "Ambulnace charges not covered in this plan " : "Rs." +
		 * AH_AMBULANCE_COVER_CASH_LIMIT + " for ambulance charges ";
		 */
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_AMBULANCE_COVER.getCode(), AH_AMBULANCE_COVER));
		if (AH_AMBULANCE_COVER.equalsIgnoreCase("covered_with_cash_limit")) {
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_AMBULANCE_COVER_CASH_LIMIT.getCode(), AH_AMBULANCE_COVER_CASH_LIMIT));
		}
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_AMBULANCE_DESCRIPTION.getCode(), AH_AMBULANCE_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadNoclaimBonus(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {

		if (descTagMap.containsKey(HealthAttributeCode.AH_NO_CLAIM_BONUS.getCode())) {
			AH_NO_CLAIM_BONUS_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_NO_CLAIM_BONUS.getCode());
		}

		/*
		 * AH_NO_CLAIM_BONUS_DESCRIPTION = AH_NO_CLAIM_BONUS.equals("0") ?
		 * "This plan does not provide no claim bonus" : "No claim bonus upto " +
		 * AH_NO_CLAIM_BONUS + "%";
		 */
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_NO_CLAIM_BONUS.getCode(), AH_NO_CLAIM_BONUS));
		vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_NO_CLAIM_BONUS_DESCRIPTION.getCode(),
				AH_NO_CLAIM_BONUS_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadDomicilaiaryHospitalization(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_DOMICILIARY_HOSPITALISATION.getCode())) {
			AH_DOMICILIARY_HOSPITALISATION_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_DOMICILIARY_HOSPITALISATION.getCode());

		}

		// AH_DOMICILIARY_HOSPITALISATION_DESCRIPTION =
		// "DomicialiaryHospitalization expenses"
		// + (AH_DOMICILIARY_HOSPITALISATION.equals("covered")
		// ? " upto Rs." + AH_DOMICILIARY_HOSPITALISATION_CASH_LIMIT + " are
		// covered"
		// : " are not covered in this plan");
		//
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_DOMICILIARY_HOSPITALISATION.getCode(), AH_DOMICILIARY_HOSPITALISATION));
		if (AH_DOMICILIARY_HOSPITALISATION.equalsIgnoreCase("covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_DOMICILIARY_HOSPITALISATION_CASH_LIMIT.getCode(),
							AH_DOMICILIARY_HOSPITALISATION_CASH_LIMIT));
		}
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_DOMICILIARY_HOSPITALISATION_DESCRIPTION.getCode(),
						AH_DOMICILIARY_HOSPITALISATION_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadAutomaticRestorationBenefits(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {

		if (descTagMap.containsKey(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode())) {
			AH_AUTOMATIC_RESTORATION_BENEFITS_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode());

		}

		/*
		 * AH_AUTOMATIC_RESTORATION_BENEFITS_DESCRIPTION =
		 * AH_AUTOMATIC_RESTORATION_BENEFITS.equals("yes") ?
		 * "This plan reinstaes your basic cover " :
		 * "This plan does not reinstaes your basic cover  ";
		 */
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode(), AH_AUTOMATIC_RESTORATION_BENEFITS));
		if (AH_AUTOMATIC_RESTORATION_BENEFITS.equalsIgnoreCase("yes")) {

			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_PERCENT.getCode(),
							AH_AUTOMATIC_RESTORATION_BENEFITS_PERCENT));
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_CONDITION.getCode(),
							AH_AUTOMATIC_RESTORATION_BENEFITS_CONDITION));
		}
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_DESCRIPTION.getCode(),
						AH_AUTOMATIC_RESTORATION_BENEFITS_DESCRIPTION));
		return vendorResponse;

	}

	private VendorResponse loadPreExistingIllnessCover(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {

		if (descTagMap.containsKey(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER.getCode())) {
			AH_PRE_EXISTING_ILLNESS_COVER_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER.getCode());
		}

		// AH_PRE_EXISTING_ILLNESS_COVER_DESCRIPTION = "Covered in " +
		// AH_PRE_EXISTING_ILLNESS_COVER + " years ";
		vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER.getCode(),
				AH_PRE_EXISTING_ILLNESS_COVER));
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_PRE_EXISTING_ILLNESS_COVER_DESCRIPTION.getCode(),
						AH_PRE_EXISTING_ILLNESS_COVER_DESCRIPTION));
		return vendorResponse;

	}

	private VendorResponse loadCoPay(VendorResponse vendorResponse, Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_CO_PAY.getCode())) {
			AH_CO_PAY_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_CO_PAY.getCode());
		}

		/*
		 * AH_CO_PAY_DESCRIPTION = AH_CO_PAY.equals("not_mandatory") ?
		 * "This plan has not co payment " : AH_CO_PAY.equals("co_pay_based_on_age") ?
		 * "This plan has " + AH_CO_PAY_PERCENT + "% after the age of " +
		 * AH_CO_PAY_AGE_LIMIT : AH_CO_PAY.equals("co_pay_in_non_network_hospital") ?
		 * "This plan has" + AH_CO_PAY_PERCENT +
		 * "% if you take treatment in a non network hospital " : "This plan has " +
		 * AH_CO_PAY_PERCENT + "% coPay";
		 */ vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_CO_PAY.getCode(), AH_CO_PAY));

		if (AH_CO_PAY.equalsIgnoreCase("co_pay_based_on_age")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_CO_PAY_AGE_LIMIT.getCode(), AH_CO_PAY_AGE_LIMIT));
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_CO_PAY_PERCENT.getCode(), AH_CO_PAY_PERCENT));
		} else if (AH_CO_PAY.equalsIgnoreCase("co_pay_in_non_network_hospital")) {
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_CO_PAY_NON_NETWORK_HOSPITAL.getCode(), AH_CO_PAY_NON_NETWORK_HOSPITAL));
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_CO_PAY_PERCENT.getCode(), AH_CO_PAY_PERCENT));
		} else if (AH_CO_PAY.equalsIgnoreCase("mandatory")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_CO_PAY_PERCENT.getCode(), AH_CO_PAY_PERCENT));
		}
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_CO_PAY_DESCRIPTION.getCode(), AH_CO_PAY_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadCriticalIllnessCoverage(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_CRITICAL_ILLNESS_COVERAGE.getCode())) {
			AH_CRITICAL_ILLNESS_COVERAGE_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_CRITICAL_ILLNESS_COVERAGE.getCode());

		}

		/*
		 * AH_CRITICAL_ILLNESS_COVERAGE_DESCRIPTION =
		 * AH_CRITICAL_ILLNESS_COVERAGE.equals("covered") ?
		 * "200% coverage for critical illness" :
		 * "This plan does not cover critical Illness";
		 */
		vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_CRITICAL_ILLNESS_COVERAGE.getCode(),
				AH_CRITICAL_ILLNESS_COVERAGE));
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_CRITICAL_ILLNESS_COVERAGE_DESCRIPTION.getCode(),
						AH_CRITICAL_ILLNESS_COVERAGE_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadOutPatientTreatment(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_OUTPATIENT_TREATMENT_CASH_LIMIT.getCode())) {
			AH_OUTPATIENT_TREATMENT_CASH_LIMIT_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_OUTPATIENT_TREATMENT_CASH_LIMIT.getCode());
		}

		/*
		 * AH_OUTPATIENT_TREATMENT_CASH_LIMIT_DESCRIPTION =
		 * !AH_OUTPATIENT_TREATMENT_CASH_LIMIT.equals("0") ? "OPD benefit up to Rs. " +
		 * AH_OUTPATIENT_TREATMENT_CASH_LIMIT :
		 * "This plan offers no outpatient benefits";
		 */
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_OUTPATIENT_TREATMENT_CASH_LIMIT.getCode(), AH_OUTPATIENT_TREATMENT_CASH_LIMIT));
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_OUTPATIENT_TREATMENT_CASH_LIMIT_DESCRIPTION.getCode(),
						AH_OUTPATIENT_TREATMENT_CASH_LIMIT_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadMedicalCheckUp(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_MEDICAL_CHECK_UP.getCode())) {
			AH_MEDICAL_CHECK_UP_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_MEDICAL_CHECK_UP.getCode());
		}

		AH_MEDICAL_CHECK_UP_DESCRIPTION = AH_MEDICAL_CHECK_UP.equals("covered")
				? "Free health checkup upto Rs." + AH_MATERNITY_BENEFITS_CASH_LIMIT
						+ " provided you have not claimed for " + AH_MEDICAL_CHECK_UP_CASH_LIMIT_PERIODICITY + " years "
				: "This plan does not offer free health checkups";
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_MEDICAL_CHECK_UP.getCode(), AH_MEDICAL_CHECK_UP));

		if (AH_MEDICAL_CHECK_UP.equalsIgnoreCase("covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_MEDICAL_CHECK_UP_CASH_LIMIT_PERIODICITY.getCode(),
							AH_MEDICAL_CHECK_UP_CASH_LIMIT_PERIODICITY));
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_MEDICAL_CHECK_UP_CASH_LIMIT.getCode(), AH_MEDICAL_CHECK_UP_CASH_LIMIT));
		}

		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_MEDICAL_CHECK_UP_DESCRIPTION.getCode(), AH_MEDICAL_CHECK_UP_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadWorldWideEmergencyCover(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {

		if (descTagMap.containsKey(HealthAttributeCode.AH_WORLD_WIDE_EMEREGENCY_COVER.getCode())) {
			AH_WORLD_WIDE_EMEREGENCY_COVER_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_WORLD_WIDE_EMEREGENCY_COVER.getCode());
		}

		/*
		 * AH_WORLD_WIDE_EMEREGENCY_COVER_DESCRIPTION =
		 * AH_WORLD_WIDE_EMEREGENCY_COVER.equals("not_covered") ?
		 * "This plan does not offer world wide emergency cover" : "This plan offers " +
		 * AH_WORLD_WIDE_EMEREGENCY_COVER_CASH_LIMIT + "% of sum assured";
		 */
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_WORLD_WIDE_EMEREGENCY_COVER.getCode(), AH_WORLD_WIDE_EMEREGENCY_COVER));
		if (!AH_WORLD_WIDE_EMEREGENCY_COVER.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_WORLD_WIDE_EMEREGENCY_COVER_CASH_LIMIT.getCode(),
							AH_WORLD_WIDE_EMEREGENCY_COVER_CASH_LIMIT));

		}

		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_WORLD_WIDE_EMEREGENCY_COVER_DESCRIPTION.getCode(),
						AH_WORLD_WIDE_EMEREGENCY_COVER_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadOrganDonor(VendorResponse vendorResponse, Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_ORGAN_DONOR.getCode())) {
			AH_ORGAN_DONOR_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_ORGAN_DONOR.getCode());
		}

		/*
		 * AH_ORGAN_DONOR_DESCRIPTION = AH_ORGAN_DONOR.equals("covered") ?
		 * "Hospitalization chrages for organ donor are covered in this plan" :
		 * "This plan does not cover organ donor expenses";
		 */vendorResponse
				.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_ORGAN_DONOR.getCode(), AH_ORGAN_DONOR));
		vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_ORGAN_DONOR_DESCRIPTION.getCode(),
				AH_ORGAN_DONOR_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadPostHospitalizationExpenses(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_POST_HOSPITALIZATION_EXPENSES.getCode())) {
			AH_POST_HOSPITALIZATION_EXPENSES_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_POST_HOSPITALIZATION_EXPENSES.getCode());
		}

		/*
		 * AH_POST_HOSPITALIZATION_EXPENSES_DESCRIPTION =
		 * AH_POST_HOSPITALIZATION_EXPENSES.equals("not_covered") ?
		 * "This plan does not cover post medical expenses" : "Medical Expenses upto " +
		 * AH_POST_HOSPITALIZATION_EXPENSES_DAY_LIMIT +
		 * " days after discharge are covered";
		 */
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_POST_HOSPITALIZATION_EXPENSES.getCode(), AH_POST_HOSPITALIZATION_EXPENSES));
		if (!AH_POST_HOSPITALIZATION_EXPENSES.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_POST_HOSPITALIZATION_EXPENSES_DAY_LIMIT.getCode(),
							AH_POST_HOSPITALIZATION_EXPENSES_DAY_LIMIT));
		}

		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_POST_HOSPITALIZATION_EXPENSES_DESCRIPTION.getCode(),
						AH_POST_HOSPITALIZATION_EXPENSES_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadPreHospitalizationExpenses(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_PRE_HOSPITALIZATION_EXPENSES.getCode())) {
			AH_PRE_HOSPITALIZATION_EXPENSES_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_PRE_HOSPITALIZATION_EXPENSES.getCode());
		}

		/*
		 * AH_PRE_HOSPITALIZATION_EXPENSES_DESCRIPTION =
		 * AH_PRE_HOSPITALIZATION_EXPENSES.equals("not_covered") ?
		 * "This plan does not cover pre medical expenses" : "Medical Expenses upto " +
		 * AH_PRE_HOSPITALIZATION_EXPENSES_DAY_LIMIT +
		 * " days bfore admission are covered";
		 */vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_PRE_HOSPITALIZATION_EXPENSES.getCode(), AH_PRE_HOSPITALIZATION_EXPENSES));
		if (!AH_PRE_HOSPITALIZATION_EXPENSES.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_PRE_HOSPITALIZATION_EXPENSES_DAY_LIMIT.getCode(),
							AH_PRE_HOSPITALIZATION_EXPENSES_DAY_LIMIT));
		}

		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_PRE_HOSPITALIZATION_EXPENSES_DESCRIPTION.getCode(),
						AH_PRE_HOSPITALIZATION_EXPENSES_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadDailyHospitalizationExpenses(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_DAILY_HOSPITALIZATION_EXPENSES.getCode())) {
			AH_DAILY_HOSPITALIZATION_EXPENSES_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_DAILY_HOSPITALIZATION_EXPENSES.getCode());
		}

		/*
		 * AH_DAILY_HOSPITALIZATION_EXPENSES_DESCRIPTION =
		 * AH_DAILY_HOSPITALIZATION_EXPENSES.equals("not_covered") ?
		 * "Daily Hospitalization Expenses not covered in this plan" : "Rs." +
		 * AH_DAILY_HOSPITALIZATION_EXPENSES_CASH_PER_DAY + " for " +
		 * AH_DAILY_HOSPITALIZATION_EXPENSES_DAY_LIMIT + " days";
		 */ vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_DAILY_HOSPITALIZATION_EXPENSES.getCode(), AH_DAILY_HOSPITALIZATION_EXPENSES));
		if (!AH_DAILY_HOSPITALIZATION_EXPENSES.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_DAILY_HOSPITALIZATION_EXPENSES_DAY_LIMIT.getCode(),
							AH_DAILY_HOSPITALIZATION_EXPENSES_DAY_LIMIT));
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_DAILY_HOSPITALIZATION_EXPENSES_CASH_PER_DAY.getCode(),
							AH_DAILY_HOSPITALIZATION_EXPENSES_CASH_PER_DAY));
		}
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_DAILY_HOSPITALIZATION_EXPENSES_DESCRIPTION.getCode(),
						AH_DAILY_HOSPITALIZATION_EXPENSES_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadAyurVedaTreatment(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT.getCode())) {
			AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT.getCode());
		}

		/*
		 * AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_DESCRIPTION =
		 * AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT .equals("not_covered") ?
		 * "This plan does not allow non-allopathic expenses" :
		 * "Ayurvedic are covered upto a sum of Rs." +
		 * AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_WITH_CASH_LIMIT;
		 */
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT.getCode(),
						AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT));
		if (!AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_WITH_CASH_LIMIT.getCode(),
					AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_WITH_CASH_LIMIT));
		}

		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_DESCRIPTION.getCode(),
				AH_AYURVEDA_TREATMENT_OR_ALTERNATIVE_TREATMENT_DESCRIPTION));
		return vendorResponse;

	}

	private VendorResponse loadDentalCover(VendorResponse vendorResponse, Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_DENTAL_COVER.getCode())) {
			AH_DENTAL_COVER_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_DENTAL_COVER.getCode());
		}

		/*
		 * AH_DENTAL_COVER_DESCRIPTION = AH_DENTAL_COVER.equals("not_covered") ?
		 * "Dental expenses are not covered in this plan " : "Dental expenses upto Rs."
		 * + AH_DENTAL_COVER_CASH_LIMIT + " are covered after " +
		 * AH_DENTAL_COVER_WAITING_PERIOD + " months";
		 */
		vendorResponse
				.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_DENTAL_COVER.getCode(), AH_DENTAL_COVER));
		if (!AH_DENTAL_COVER.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_DENTAL_COVER_WAITING_PERIOD.getCode(), AH_DENTAL_COVER_WAITING_PERIOD));
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_DENTAL_COVER_CASH_LIMIT.getCode(), AH_DENTAL_COVER_CASH_LIMIT));
		}

		vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_DENTAL_COVER_DESCRIPTION.getCode(),
				AH_DENTAL_COVER_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadConvalescenceExpenses(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_CONVALESCENCE_EXPENSES.getCode())) {
			AH_CONVALESCENCE_EXPENSES_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_CONVALESCENCE_EXPENSES.getCode());
		}

		/*
		 * AH_CONVALESCENCE_EXPENSES_DESCRIPTION =
		 * AH_CONVALESCENCE_EXPENSES.equals("not_covered") ?
		 * "Convalescenes expenses are not covered in this plan " :
		 * "Convalescenes expenses upto Rs." +
		 * AH_CONVALESCENCE_EXPENSES_CASH_LIMIT_PER_DAY + " are covered for " +
		 * AH_CONVALESCENCE_EXPENSES_WITH_DAY_LIMIT + " days ";
		 */
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_CONVALESCENCE_EXPENSES.getCode(), AH_CONVALESCENCE_EXPENSES));
		if (!AH_CONVALESCENCE_EXPENSES.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_CONVALESCENCE_EXPENSES_WITH_DAY_LIMIT.getCode(),
							AH_CONVALESCENCE_EXPENSES_WITH_DAY_LIMIT));
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_CONVALESCENCE_EXPENSES_CASH_LIMIT_PER_DAY.getCode(),
							AH_CONVALESCENCE_EXPENSES_CASH_LIMIT_PER_DAY));

		}

		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_CONVALESCENCE_EXPENSES_DESCRIPTION.getCode(),
						AH_CONVALESCENCE_EXPENSES_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadMaternityBenefits(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_MATERNITY_BENEFITS.getCode())) {
			AH_MATERNITY_BENEFITS_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_MATERNITY_BENEFITS.getCode());
		}

		/*
		 * AH_MATERNITY_BENEFITS_DESCRIPTION =
		 * "Maternity expenses claimed after 4 year of Rs." + AH_MATERNITY_BENEFITS +
		 * "/- for a cesarean delivery and Rs." + AH_MATERNITY_BENEFITS_CASH_LIMIT +
		 * "/- for normal delivery ";
		 */
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_MATERNITY_BENEFITS.getCode(), AH_MATERNITY_BENEFITS));

		if (!AH_MATERNITY_BENEFITS.equals("not_covered")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_MATERNITY_BENEFITS_WAITING_PERIOD.getCode(),
							AH_MATERNITY_BENEFITS_WAITING_PERIOD));
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_MATERNITY_BENEFITS_NO_OF_CHILD_BIRTHS.getCode(),
							AH_MATERNITY_BENEFITS_NO_OF_CHILD_BIRTHS));
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_MATERNITY_BENEFITS_CASH_LIMIT.getCode(), AH_MATERNITY_BENEFITS_CASH_LIMIT));

		}

		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_MATERNITY_BENEFITS_DESCRIPTION.getCode(), AH_MATERNITY_BENEFITS_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadNewBornBabyCover(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_NEW_BORN_BABY_COVER.getCode())) {
			AH_NEW_BORN_BABY_COVER_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_NEW_BORN_BABY_COVER.getCode());
		}

		AH_NEW_BORN_BABY_COVER_DESCRIPTION = AH_NEW_BORN_BABY_COVER.equals("not_covered")
				? "New Born baby expenses not covered in this plan "
				: "New Born baby expenses covered upto " + AH_NEW_BORN_BABY_COVER_DAY_LIMIT + " days";
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_NEW_BORN_BABY_COVER.getCode(), AH_NEW_BORN_BABY_COVER));
		if (!AH_NEW_BORN_BABY_COVER.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_NEW_BORN_BABY_COVER_DAY_LIMIT.getCode(), AH_NEW_BORN_BABY_COVER_DAY_LIMIT));
		}

		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_NEW_BORN_BABY_COVER_DESCRIPTION.getCode(), AH_NEW_BORN_BABY_COVER_DESCRIPTION));
		return vendorResponse;

	}

	private VendorResponse loadCashlessfacilty(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_CASHLESS_FACILITY.getCode())) {
			AH_CASHLESS_FACILITY_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_CASHLESS_FACILITY.getCode());
		}

		// AH_CASHLESS_FACILITY_DESCRIPTION = "Always covered";
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_CASHLESS_FACILITY.getCode(), AH_CASHLESS_FACILITY));
		vendorResponse.addAttributeValue(getAttributeValue(
				HealthAttributeCode.AH_CASHLESS_FACILITY_DESCRIPTION.getCode(), AH_CASHLESS_FACILITY_DESCRIPTION));
		return vendorResponse;

	}

	private VendorResponse loadSubLimitType(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_SUB_LIMIT_TYPE.getCode())) {
			AH_SUB_LIMIT_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_SUB_LIMIT_TYPE.getCode());
		}

		/*
		 * AH_SUB_LIMIT_DESCRIPTION = AH_SUB_LIMIT_TYPE.equals("percent") ?
		 * AH_SUB_LIMIT_PERCENT + "% of sum insured " : "Rs. " + AH_SUB_LIMIT_AMOUNT +
		 * "/- ";
		 */ vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_SUB_LIMIT_TYPE.getCode(), AH_SUB_LIMIT_TYPE));
		if (AH_SUB_LIMIT_TYPE.equalsIgnoreCase("percent")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_SUB_LIMIT_PERCENT.getCode(), AH_SUB_LIMIT_PERCENT));

		} else if (AH_SUB_LIMIT_TYPE.equalsIgnoreCase("sub_limit_amount")) {
			vendorResponse.addAttributeValue(
					getAttributeValue(HealthAttributeCode.AH_SUB_LIMIT_AMOUNT.getCode(), AH_SUB_LIMIT_AMOUNT));

		}

		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_SUB_LIMIT_DESCRIPTION.getCode(), AH_SUB_LIMIT_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadDiabetesCare(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_DIABETES_CARE.getCode())) {
			AH_DIABETES_CARE_DESCRIPTION = descTagMap.get(HealthAttributeCode.AH_DIABETES_CARE.getCode());
		}

		/*
		 * AH_DIABETES_CARE_DESCRIPTION = AH_DIABETES_CARE.equals("not_covered") ?
		 * "Diabetes not covered in this plan " :
		 * "Diabetes expenses are covered upto Rs." + AH_DIABETES_CARE_CASH_LIMIT;
		 */ vendorResponse
				.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_DIABETES_CARE.getCode(), AH_DIABETES_CARE));
		if (!AH_DIABETES_CARE.equalsIgnoreCase("not_covered")) {
			vendorResponse.addAttributeValue(getAttributeValue(
					HealthAttributeCode.AH_DIABETES_CARE_CASH_LIMIT.getCode(), AH_DIABETES_CARE_CASH_LIMIT));
		}

		vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_DIABETES_CARE_DESCRIPTION.getCode(),
				AH_DIABETES_CARE_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadLifeLongRenewability(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_LIFE_LONG_RENEWABILITY_DESCRIPTION.getCode())) {
			AH_LIFE_LONG_RENEWABILITY_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_LIFE_LONG_RENEWABILITY_DESCRIPTION.getCode());
		}

		/*
		 * AH_LIFE_LONG_RENEWABILITY_DESCRIPTION =
		 * AH_LIFE_LONG_RENEWABILITY.equals("covered") ? "Life long renewablity covered"
		 * : "Life long renewablity not covered";
		 */ vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_LIFE_LONG_RENEWABILITY.getCode(), AH_LIFE_LONG_RENEWABILITY));
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_LIFE_LONG_RENEWABILITY_DESCRIPTION.getCode(),
						AH_LIFE_LONG_RENEWABILITY_DESCRIPTION));
		return vendorResponse;
	}

	private VendorResponse loadDoctorOncallService(VendorResponse vendorResponse,
			Map<String, String> descTagMap) {
		if (descTagMap.containsKey(HealthAttributeCode.AH_DOCTOR_ON_CALL_SERVICE.getCode())) {
			AH_DOCTOR_ON_CALL_SERVICE_DESCRIPTION = descTagMap
					.get(HealthAttributeCode.AH_DOCTOR_ON_CALL_SERVICE.getCode());
		}

		/*
		 * AH_DOCTOR_ON_CALL_SERVICE_DESCRIPTION =
		 * AH_DOCTOR_ON_CALL_SERVICE.equals("covered") ?
		 * "Doctor on call service covered" : "Doctor on call service not covered";
		 */ vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_DOCTOR_ON_CALL_SERVICE.getCode(), AH_DOCTOR_ON_CALL_SERVICE));
		vendorResponse.addAttributeValue(
				getAttributeValue(HealthAttributeCode.AH_DOCTOR_ON_CALL_SERVICE_DESCRIPTION.getCode(),
						AH_DOCTOR_ON_CALL_SERVICE_DESCRIPTION));
		return vendorResponse;
	}

	@Override
	public VendorResponse buyPlanFromVendor(VendorRequest vendorRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectNode policyPdf(String policyNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectNode purchasePlan(JsonNode reqJson) {
		// TODO Auto-generated method stub
		return null;
	}

}
