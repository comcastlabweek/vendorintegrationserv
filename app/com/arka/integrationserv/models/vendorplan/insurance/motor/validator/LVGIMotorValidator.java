package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;

/**
** @author Arul
*/

public class LVGIMotorValidator extends MotorBaseValidator {

	public boolean validateForBreakInPolicy(VendorRequest vendorRequest) {
		boolean flag = false;
		
		if(vendorRequest.getCategoryCode().equals(CategoryCode.BIKE.name()) && 
				vendorRequest.isAttributePresent(CarAttributeCode.AC_COVER_POLICY_EXPIRED_BEFORE_90_DAYS.getCode())) {
			return vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_COVER_POLICY_EXPIRED_BEFORE_90_DAYS.getCode()).getValueAsBoolean();
		}else if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired")){
				flag =true;
			}
		}

		return flag;
	}


}

