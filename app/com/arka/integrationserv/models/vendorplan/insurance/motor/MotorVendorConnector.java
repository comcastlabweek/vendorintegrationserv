package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.controllers.utils.AnyCollectionQueryResponseValidator;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.VendorConnector;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.Json;

public class MotorVendorConnector implements VendorConnector {

	private static final Logger logger = LoggerFactory.getLogger(MotorVendorConnector.class);

	// fetch product related info

	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	DBUtil dbUtil;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	AnyCollectionQueryResponseValidator anyCollectionRespVal;

	Map<String, JsonNode> categoryAttributeIdMap;
	
	public static ObjectMapper mapper;
	
	static {
		mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

	}

	DecimalFormat df = new DecimalFormat("###############");
	
	protected VendorRequest modifyCategoryAttributeToRequest(VendorRequest vendorRequest, String categoryCode,
			String categoryAttributeValue) {

		vendorRequest.getInput_quote_params().stream()
				.filter(param -> param.getCategoryAttributeCode().equals(categoryCode)).findAny().get()
				.setValue(categoryAttributeValue);
		return vendorRequest;

	}
	public ObjectNode getValidationError(String errorCode,String errorKey, String errorText) {
		ObjectNode errorJson = Json.newObject();
		errorJson.put(JsonUtils.ERROR_CODE, errorCode);
		errorJson.put(JsonUtils.ERROR, errorKey);
		errorJson.put(JsonUtils.ERROR_MESSAGE, errorText);
		ObjectNode validationErrorJson = Json.newObject();
		validationErrorJson.set(ResponsePayLoad.ERROR, errorJson);
		return validationErrorJson;
	}
	
	protected VendorRequest addCategoryAttributeToRequest(VendorRequest vendorRequest, String categoryCode, String categoryAttributeValue){
		if(vendorRequest.isAttributePresent(categoryCode)){
			vendorRequest.getInput_quote_params().stream()
			.filter(param -> param.getCategoryAttributeCode().equals(categoryCode)).findAny().get()
			.setValue(categoryAttributeValue);
			return vendorRequest;
		}else{
			InputPolicyParams inputPolicyParams = new InputPolicyParams();
			inputPolicyParams.setCategoryAttributeCode(categoryCode);
			inputPolicyParams.setValue(categoryAttributeValue);
			vendorRequest.addAttributeValue(inputPolicyParams);
			return vendorRequest;
		}				
	}
	protected VendorResponse addCategoryAttributeToResponse(VendorResponse vendorResponse, String categoryCode, String categoryAttributeValue){
		AttributeValue attributeValue = new AttributeValue();
		attributeValue.setCategoryAttributeCode(categoryCode);
		attributeValue.setValue(categoryAttributeValue);
		vendorResponse.addAttributeValue(attributeValue);
		return vendorResponse;
		
	}

	public ObjectNode appendProductInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		ObjectNode vendorOutputJson = Json.newObject();
		String productId = vendorRequest.getProductId();
		JsonNode productJson = dbUtil.getProductJson(productId);
		if (productJson == null || !productJson.hasNonNull(ProductFields.PRODUCT_ID.value())) {
			logger.error(FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value() + " : " + productId);
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value(),
					FetchPlanMessage.PRODUCT_DOES_NOT_EXISTS.value() + " : " + productId));
		}
		vendorOutputJson.put(ProductFields.PRODUCT_NAME.value(),
				productJson.get(ProductFields.PRODUCT_NAME.value()).asText());
		vendorOutputJson.put(ProductFields.PRODUCT_CODE.value(),
				productJson.get(ProductFields.PRODUCT_CODE.value()).asText());
		String productImage = "";
		String productDocument = "";

		if (JsonUtils.isValidField(productJson, ProductFields.PRODUCT_IMAGE_URL.value())) {
			productImage = productJson.get(ProductFields.PRODUCT_IMAGE_URL.value()).asText();

		}
		if (JsonUtils.isValidField(productJson, ProductFields.PRODUCT_DOCUMENT_URL.value())) {
			productDocument = productJson.get(ProductFields.PRODUCT_DOCUMENT_URL.value()).asText();
		}

		vendorOutputJson.put(ProductFields.PRODUCT_IMAGE_URL.value(), productImage);
		vendorOutputJson.put(ProductFields.PRODUCT_DOCUMENT_URL.value(), productDocument);
		return vendorOutputJson;

	}	
	protected AttributeValue getAttributeValue(String categoryAttributeCode, String value) {
		AttributeValue paramValueObj = new AttributeValue();
		paramValueObj.setCategoryAttributeCode(categoryAttributeCode);
		paramValueObj.setValue(value);
		paramValueObj.setTagsAsList(getTagNameListForAttribute(categoryAttributeCode));
		return paramValueObj;

	}

	protected List<String> getTagNameListForAttribute(String attributeCode) {

		if (attributeCode == null || attributeCode.trim().isEmpty()) {
			return null;
		}

		if (MapUtils.isEmpty(categoryAttributeIdMap)) {
			return null;
		}

		List<String> tagNameList = new ArrayList<String>();
		new ArrayList<JsonNode>(categoryAttributeIdMap.values()).stream()

				.filter(categoryAttributeJson -> categoryAttributeJson
						.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CODE.value()).asText().equals(attributeCode))
				.forEach(categoryAttributeJson -> {

					if (JsonUtils.isValidField(categoryAttributeJson,
							CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAGS.value())) {

						JsonNode tagListJson = categoryAttributeJson
								.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAGS.value());
						tagListJson.elements().forEachRemaining(tagJson -> {
							String tagName = tagJson.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_TAG_NAME.value())
									.asText();
							tagNameList.add(tagName);
						});
					}
				});

		return tagNameList;

	}

	@Override
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VendorResponse buyPlanFromVendor(VendorRequest vendorRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectNode policyPdf(String policyNum) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectNode purchasePlan(JsonNode reqJson) {
		// TODO Auto-generated method stub
		return null;
	}

}
