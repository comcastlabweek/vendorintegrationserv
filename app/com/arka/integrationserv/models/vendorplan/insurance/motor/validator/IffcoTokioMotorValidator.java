package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import itgi.motor.paymentwrapper.Request;


public class IffcoTokioMotorValidator extends MotorBaseValidator{

	private static final Logger logger = LoggerFactory.getLogger(IffcoTokioMotorValidator.class);
	private static final double MAX_IDV_TWP_BREAKINPOLICY = 300000.0;
	private static final int NEW_VEHICLE_CRITERIA = 200;
	
	
	public JsonNode validateMandatoryFields(Request getPayRequest,ObjectNode policyJson) {
		if(isValidAttribute(getPayRequest.getPolicy().getNominee()) ||
				isValidAttribute(getPayRequest.getPolicy().getNomineeRelationship())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NOMINEE.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getVehicle().getRegistrationNumber1()) ||
				isValidAttribute(getPayRequest.getVehicle().getRegistrationNumber4())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_REGNO.value());
			return policyJson;
		}
		if(isValidAttribute(String.valueOf(getPayRequest.getVehicle().getManufacturingYear()))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_MANUFACTUREYEAR.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getVehicle().getEngineNumber())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ENGINENO.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getVehicle().getChassisNumber())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_CHASISNO.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getDOB())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_DOB.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getFirstName())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NAME.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getInsuredPAN())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_PAN.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getInsuredAadhar())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_AADHARNO.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getSex())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_GENDER.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getAddressLine1())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ADDRESS.value());
			return policyJson;
		}
		if(isValidAttribute(String.valueOf(getPayRequest.getContact().getPinCode()))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_PINCODE.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getCity())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_CITY.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getCountry())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_COUNTRY.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getOccupation())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_OCCUPATION.value());
			return policyJson;
		}
		if(isValidAttribute(String.valueOf(getPayRequest.getContact().getMobilePhone()))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_MOBILE.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getMailId())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_EMAIL.value());
			return policyJson;
		}
		if(isValidAttribute(getPayRequest.getContact().getState())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_STATE.value());
			return policyJson;
		}
		return policyJson;
		
		
	}
	public boolean validateForBreakInPolicy(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		boolean flag = false;
		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired")){
				flag =true;
			}
		else {
			flag =false;
		}			
		}
		
		if(flag && (vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.CAR.name()) || 
				vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name())  ))
	   {
			vendorResponse.setErrorJson(getValidationError(VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
					FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value())); 
			return true;
		}
		return false;
		
	}
	
	public boolean validateIDVForBreakInPolicy(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		boolean flag = false;
		double idv = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired")){
				flag =true;
			}
		else {
			flag =false;
		}			
		}
		if(flag && vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name()) && idv>MAX_IDV_TWP_BREAKINPOLICY ){
			return true;
		}
		return false;
		
	}
	public boolean isNewVehicle(VendorRequest vendorRequest) {
		 org.joda.time.format.DateTimeFormatter DATE_FORMATTER = 
				    new DateTimeFormatterBuilder()
				        .append(null, new DateTimeParser[]{
				                DateTimeFormat.forPattern("yyyy-M-dd").getParser(),
				                DateTimeFormat.forPattern("yyyy-M-d").getParser(),
				                DateTimeFormat.forPattern("yyyy-MM-d").getParser(),
				                DateTimeFormat.forPattern("yyyy-MM-dd").getParser()})
				        .toFormatter();
		 String dateOfRegistrationStr ="",policyStartDateStr = "";
		 org.joda.time.LocalDate dateOfRegistration = null,policyStartDate = null ;
		 boolean flag = false;
		 if(vendorRequest.isAttributePresent(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode())) {
			dateOfRegistrationStr = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode()).getValue();
			dateOfRegistration = DATE_FORMATTER.parseLocalDate(dateOfRegistrationStr);
		 }
		 if(vendorRequest.isAttributePresent(CarAttributeCode.AC_POLICY_START_DATE.getCode())) {
			 policyStartDateStr = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_POLICY_START_DATE.getCode()).getValue();
			 policyStartDate = DATE_FORMATTER.parseLocalDate(policyStartDateStr);
		 }
		 if(dateOfRegistration!=null && policyStartDate!= null) {
			LocalDate newDate = dateOfRegistration.plusDays(NEW_VEHICLE_CRITERIA);
			if( newDate.isAfter(policyStartDate)){
				flag = true;  // new vehicle
			}
		 }
		return flag;
		
	}

}
