package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.RedisDB;
import com.arka.common.exceptions.ServiceRuntimeException;
import com.arka.common.insurance.constants.InsuranceAttributeCode;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.ws.service.WsServiceUtils;
import com.arka.common.utils.ws.service.WsServiceUtilsImpl;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.insurance.motor.validator.LVGIMotorValidator;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.avaje.ebean.enhance.agent.SysoutMessageOutput;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.google.inject.Inject;

import play.libs.Json;

public class LibertyVideoconMotorInsuranceConnector extends MotorVendorConnector{
	
	@Inject
	WsServiceUtilsImpl wsServiceUtilsImpl;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;
	
	@Inject
	private CacheService cache;
	
	@Inject
	private WsServiceUtils wsUtils;
	
	@Inject
	private PropUtils propUtils;

	@Inject
	LVGIMotorValidator lvgiMotorValidator;

	public static ObjectMapper mapper;
	
	static {
		mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

	}
	
	public LibertyVideoconMotorInsuranceConnector()
	{
		
	}
	
	private static final Logger logger = LoggerFactory.getLogger(LibertyVideoconMotorInsuranceConnector.class);
	
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();
		String fetchPlanUrl=propUtils.get("arka.vendor.lvgi.fetchPlanUrl");
		String errorMsgKey="ErrorText";
		try {
			
			if(lvgiMotorValidator.validateForBreakInPolicy(vendorRequest)){
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.BREAK_IN_POICY_NOT_ALLOWED.value()));
				return vendorResponse;
			}
			
			JsonNode finalJson=getVendorReqJson(vendorRequest,false);
			
			if(finalJson==null){
				ObjectNode errorJson=JsonUtils.newJsonObject();
				errorJson.put(JsonUtils.ERROR, FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
				vendorResponse.setErrorJson(errorJson);
				return vendorResponse;
			}
			
			JsonNode resultFromVendor=wsServiceUtilsImpl.doPost(fetchPlanUrl, new HashMap<String,List<String>>(),
					new HashMap<String,List<String>>(), finalJson).toCompletableFuture().get();
			if(JsonUtils.isValidField(resultFromVendor, errorMsgKey) && 
					StringUtils.isNotEmpty(resultFromVendor.get(errorMsgKey).asText())) {				
				String errorText=resultFromVendor.get(errorMsgKey).asText();	
				logger.error(errorText);
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.NOT_SUPPORTED_MODEL.value()));
				return vendorResponse;
			}
			
			vendorResponse=categoryTransformationUtils.
					mapToCatalogCategory(vendorRequest, resultFromVendor, vendorResponse, "LIBERTYMOTORRESPONSE");
			if(vendorResponse.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())){						
				AttributeValue attributeValue= vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
				InputPolicyParams isCompany = vendorRequest.getUniqueInputQuoteForAttribute(InsuranceAttributeCode.AI_IS_COMPANY.getCode());
				if(isCompany!=null && StringUtils.isNotEmpty(isCompany.getValue()) && Boolean.parseBoolean(isCompany.getValue())) {
					String val=Stream.of(attributeValue.getValueAsStr().split(",")).
							filter(ele->!ele.equals(CarAttributeCode.AC_COVER_DRIVER_COVER.getCode())).collect(Collectors.joining(","));
					attributeValue.setValue(val);
				}
				attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
				vendorResponse.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
				vendorResponse.addAttributeValue(attributeValue);
				
			}
			vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
			vendorResponse.setProduct(appendProductInfo(vendorRequest,vendorResponse));
		}catch (Exception e) {
			if(e instanceof ServiceRuntimeException) {
				if(((ServiceRuntimeException)e).getStatusCode()==504) {
					vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
					return vendorResponse;
				}
			}
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return vendorResponse;
	}
	
	
	public JsonNode getVendorReqJson(VendorRequest vendorRequest, boolean isFullQuoteFetch) {
		JsonNode finalJson=JsonUtils.newJsonObject();
		try {
			
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "LIBERTYMOTOR");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			
			finalJson=jsonPatch.apply(JsonUtils.newJsonObject());
				if(isDontKnwCarFlow(vendorRequest)) {
					String[] rtoCode=vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_RTOCODE.getCode()).getValue().split("-");
					if(rtoCode.length==2) {
						((ObjectNode)finalJson).put("RegNo1", rtoCode[0]);
						((ObjectNode)finalJson).put("RegNo2", rtoCode[1]);
						((ObjectNode)finalJson).put("RegNo4", "2342");
					}
				}
			
			if(isCompany(vendorRequest)) {
				((ObjectNode)finalJson).put("LegalLiabilityToPaidDriver","No");
				if(JsonUtils.isValidField(finalJson, "CustmerObj") && 
						JsonUtils.isValidField(finalJson.get("CustmerObj"), "CustomerType")) {
					((ObjectNode)finalJson.get("CustmerObj")).
									put("FirstName", getValueFrmAttr(vendorRequest, InsuranceAttributeCode.AI_COMPANY_NAME.getCode()));
					((ObjectNode)finalJson.get("CustmerObj")).
									put("LastName", getValueFrmAttr(vendorRequest, InsuranceAttributeCode.AI_SHORT_NAME.getCode()));
					
					
				}
			}
			
			accessoriesValueSetter(finalJson,"lstAccessories","ElectricalAccessories");
			accessoriesValueSetter(finalJson,"lstNonElecAccessories","NonElectricalAccessories");
			
			if(!isFullQuoteFetch) {
				((ObjectNode)finalJson).remove("CustmerObj");
			}			
			
			if(JsonUtils.isValidField(finalJson, "ExternalFuelKit") 
					&& StringUtils.isNotEmpty(finalJson.get("ExternalFuelKit").asText())) {
				((ObjectNode)finalJson).put("FuelType", "CNG");
			}
			
		}catch(Exception e) {
			logger.error(e.getLocalizedMessage(), e);
		}
		return finalJson;
	}
	
	private void accessoriesValueSetter(JsonNode finalJson, String accsessoriesKey, String keyTobeChange){
		if(JsonUtils.isValidField(finalJson, accsessoriesKey)
				&& JsonUtils.isValidIndex(finalJson.get(accsessoriesKey), 0) 
				&& JsonUtils.isValidField(finalJson.get(accsessoriesKey).get(0), "SumInsured")
				&& StringUtils.isNotEmpty(finalJson.get(accsessoriesKey).get(0).get("SumInsured").asText())
				&& StringUtils.isNumeric(finalJson.get(accsessoriesKey).get(0).get("SumInsured").asText())
				&& Integer.parseInt(finalJson.get(accsessoriesKey).get(0).get("SumInsured").asText())>0){
			((ObjectNode)finalJson).put(keyTobeChange, "Yes");
		}
	}
	
	
	private boolean isCompany(VendorRequest vendorReq) {
		return isValidAttr(vendorReq,InsuranceAttributeCode.AI_COMPANY_NAME.getCode());
	}
	
	private boolean isDontKnwCarFlow(VendorRequest vendorReq) {
		if(isValidAttr(vendorReq, CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())
				&& isValidAttr(vendorReq, CarAttributeCode.AC_RTOCODE.getCode())
				&& !isValidAttr(vendorReq, CarAttributeCode.AC_REGNO.getCode())) {
			return true;
		}
		return false;
	}
	
	private boolean isValidAttr(VendorRequest vendorReq, String attr) {
		InputPolicyParams companyInpAttr = vendorReq.isAttributePresent(attr) ? 
				vendorReq.getUniqueInputQuoteForAttribute(attr):null;
		return companyInpAttr!=null && StringUtils.isNotEmpty(companyInpAttr.getValue());
	}
	
	private String getValueFrmAttr(VendorRequest vendorReq, String attr) {
		if(isValidAttr(vendorReq,attr)) {
			InputPolicyParams companyInpAttr = vendorReq.isAttributePresent(attr) ? 
					vendorReq.getUniqueInputQuoteForAttribute(attr):null;
			return companyInpAttr.getValue();
		}
		return null;
	}
	
	
	private String getUniqueQuoteKey() {
		
		String uniqueNo="LVGI_"+AttributeUtil.generateRandom();
		return uniqueNo;
	}
	
	
	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("IVGI_MOTOR_"+AttributeUtil.generateRandom());
		return vendorResponse;

	}
	
	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		ObjectNode policyJson = Json.newObject();
		
		String paymentUrl=propUtils.get("arka.vendor.lvgi.paymentUrl");
		String buyPolicyUrl=propUtils.get("arka.vendor.lvgi.buyPolicyUrl");
		String errorMsgKey="ErrorText";
		
		try {
			JsonNode reqJson=getVendorReqJson(vendorRequest,true);
			if(reqJson==null){
				policyJson.setAll(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.NOT_SUPPORTED_MODEL.value()));
				return policyJson;
			}
			
			JsonNode resultFromVendor=wsServiceUtilsImpl.doPost(buyPolicyUrl, new HashMap<String,List<String>>(),
					new HashMap<String,List<String>>(), reqJson).toCompletableFuture().get();
			if(JsonUtils.isValidField(resultFromVendor, errorMsgKey) && 
					StringUtils.isNotEmpty(resultFromVendor.get(errorMsgKey).asText())) {
					
					String errorText=resultFromVendor.get(errorMsgKey).asText();
					
					ObjectNode errorJson=JsonUtils.newJsonObject();
					errorJson.put(JsonUtils.ERROR, FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
					errorJson.put(JsonUtils.ERROR_MESSAGE, errorText);
					return errorJson;
			}else {
				String transid=getUniqueQuoteKey();
				String proposalId=CryptUtils.encryptData(vendorRequest.getProposalId());
				policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
				policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
				policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
				policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
				policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), RandomStringUtils.randomAlphanumeric(10));
				policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
				policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
				policyJson.put(PolicyFields.PAYMENT_ID.getCode(), RandomStringUtils.randomAlphanumeric(10));
				policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue());
				policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
				policyJson.put(PolicyFields.PREMIUM.getCode(), resultFromVendor.get("TotalPremium").asText());
				policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
				policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
				policyJson.put("key", "ARKA INSURE");
				policyJson.put("productInfo", "Liberty Videocon Payment Info");
				policyJson.put("firstName", reqJson.get("CustmerObj").get("FirstName").asText());
				policyJson.put("quotationNumber", resultFromVendor.get("QuotationNumber").asText());
				policyJson.put("customerID", resultFromVendor.get("CustomerID").asText());
				policyJson.put("txnid", transid);
				policyJson.put("amount", resultFromVendor.get("TotalPremium").asText());
				policyJson.put("BUY_POLICY_URL", paymentUrl);
				policyJson.put("phone", reqJson.get("CustmerObj").get("MobileNumber").asText());
				policyJson.put("Email", reqJson.get("CustmerObj").get("EmailId").asText());
				policyJson.put("FURL","/insurance/cancelled/" + proposalId+"?status=TERMINATED_BY_ERROR");
				policyJson.put("SURL","/lvgi/policy/create");
				
				String redisDB = RedisDB.PURCHASE.getValue();
				cache.getOrElse("PremiumAmount"+"."+proposalId,redisDB, ()->resultFromVendor.get("TotalPremium").asText());
				cache.getOrElse("ProductCode"+"."+proposalId,redisDB, ()->reqJson.get("ProductCode").asText());
				cache.getOrElse("QuotationNumber"+"."+proposalId,redisDB, ()->resultFromVendor.get("QuotationNumber").asText());
				cache.getOrElse("PaymentSource"+"."+proposalId,redisDB, ()->"LVGI-PAYU");
				cache.getOrElse("customerID"+"."+proposalId,redisDB, ()->resultFromVendor.get("CustomerID").asText());
				cache.getOrElse("TransactionID"+"."+proposalId,redisDB, ()->transid);
				cache.getOrElse("TPSourceName"+"."+proposalId,redisDB, ()->reqJson.get("TPSourceName").asText());
				cache.getOrElse("TPEmailID"+"."+proposalId,redisDB, ()->"customerservice@imright.com");
				cache.getOrElse("SendEmailtoCustomer"+"."+proposalId,redisDB, ()->"true");
				cache.getOrElse(ProductFields.PRODUCT_CODE.value()+"."+proposalId,redisDB, ()->vendorRequest.getProductCode());
				cache.getOrElse(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_CODE.value()+"."+proposalId,redisDB, ()->vendorRequest.getCategoryCode());
				
				
				
//				JsonNode result=wsServiceUtilsImpl.doPost(buyPolicyUrll, new HashMap<String,List<String>>(),
//						new HashMap<String,List<String>>(), policyCreateJsonReq).toCompletableFuture().get();
//				
//				System.out.println("result :"+Json.prettyPrint(result));
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		
		return policyJson;
	}
	
	@Override 
	public ObjectNode purchasePlan(JsonNode reqJson){
		if(isValidReqJsonForLvgi(reqJson)) {
			String createPolicUrl=propUtils.get("arka.service.vendor.integration.lvgi.vendor.create.policy");
			if(StringUtils.isNotEmpty(createPolicUrl)) {
				ObjectNode respJson=JsonUtils.newJsonObject();
				try {
					respJson = (ObjectNode)wsUtils.doPost(createPolicUrl, null, null, reqJson).thenApplyAsync(resp->{
						if(JsonUtils.isValidField(resp, "PolicyNumber")) {
							ObjectNode pdfReqJson=JsonUtils.newJsonObject();
							pdfReqJson.put("strPolicyNumber", resp.get("PolicyNumber").asText());
							pdfReqJson.put("strCustomerGcId", reqJson.get("customerID").asText());
							pdfReqJson.put("strProductType", reqJson.get("ProductCode").asText());
						}
						return resp;
					}).toCompletableFuture().get();
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return respJson;
			}
		}
		return (ObjectNode)JsonUtils.errorMsg();
	}
	
	
	private boolean isValidReqJsonForLvgi(JsonNode reqJson) {
		
		if(JsonUtils.isValidField(reqJson, "PremiumAmount") && JsonUtils.isValidField(reqJson, "ProductCode")
				&& JsonUtils.isValidField(reqJson, "QuotationNumber") && JsonUtils.isValidField(reqJson, "customerID")
				&& JsonUtils.isValidField(reqJson, "PaymentSource") && JsonUtils.isValidField(reqJson, "TransactionID")
				&& JsonUtils.isValidField(reqJson, "TPSourceName") && JsonUtils.isValidField(reqJson, "TPEmailID")
				&& JsonUtils.isValidField(reqJson, "SendEmailtoCustomer")) {
			return true;
		}
		
		return false;
	}
	
	
	

}
