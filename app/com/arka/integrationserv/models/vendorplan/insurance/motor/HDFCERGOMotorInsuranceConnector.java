package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PaymentFrequency;
import com.arka.common.policy.constants.PaymentMode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.idvcalculationrequest.IDVREQUEST;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.idvcalculationrequest.ObjectFactoryIDVREQUEST;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.idvcalculationresponse.IDVRESPONSE;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.premiumcalculationrequest.ObjectFactoryPremiumCalculation;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.premiumcalculationrequest.PCVPremiumCalc;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.premiumcalculationresponse.PREMIUMOUTPUT;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.proposalRequest.ObjectFactoryProposalRequest;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.proposalRequest.Xmlmotorpolicy;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.proposalRequest.twoWheeler.ObjectFactoryTwoWheelerProposalInput;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.proposalRequest.twoWheeler.XmlmotorpolicyTwoWheeler;
import com.arka.integrationserv.models.vendorplan.insurance.hdfcergo.proposalresponse.WsResult;
import com.arka.integrationserv.models.vendorplan.insurance.motor.validator.HdfcErgoMotorValidator;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.arka.integrationserv.models.vendorplan.utils.DefaultMessageHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.google.inject.Inject;

import hdfcergo.motor.Service;
import hdfcergo.motor.ServiceSoap;
import hdfcergo.motor.proposal.ServiceProposal;
import hdfcergo.motor.proposal.ServiceSoapProposal;
import hdfcergo.tw.proposal.ServiceSoapTwoProposal;
import hdfcergo.tw.proposal.ServiceTwProposal;
import play.libs.Json;
import play.libs.ws.WSClient;

public class HDFCERGOMotorInsuranceConnector extends MotorVendorConnector {

	@Inject
	DBUtil dbUtil;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;

	@Inject
	HdfcErgoMotorValidator hdfcErgoMotorValidator;
	
	@Inject
	private PropUtils propUtils;

	private static final Logger logger = LoggerFactory.getLogger(HDFCERGOMotorInsuranceConnector.class);

	String errorString = "ERROR. ";

	private static final String XML_PROLOG = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

	private static final String XML_PROSTANDALONE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	@Inject
	WSClient ws;

	@Override
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		System.out.println("this is vendorRequest--------"+JsonUtils.toJson(vendorRequest));
		VendorResponse vendorResponse = new VendorResponse();
		try {
			if (hdfcErgoMotorValidator.validateForBreakInPolicy(vendorRequest)) {
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.BREAK_IN_POICY_NOT_ALLOWED.value()));
				return vendorResponse;
			}
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "getIDV");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			System.out.println("this is the patch json" + patchJsonNode);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));

			ObjectFactoryIDVREQUEST idvobjectFactory = new ObjectFactoryIDVREQUEST();
			IDVREQUEST idvRequest = idvobjectFactory.createIDV();
			JsonNode orgJsonNode = mapper.valueToTree(idvRequest);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);

			idvRequest = mapper.treeToValue(patchedJsonNode, IDVREQUEST.class);

			String xmlString = prepareXMLPartString(IDVREQUEST.class, idvRequest, "IDV");
			System.out.println("getIDV------------" + xmlString);

			String response = bindingAndLoggingForFetch().getIDV(xmlString);
			response = response.replace(XML_PROSTANDALONE, "");

			JAXBContext jaxbResponse = JAXBContext.newInstance(IDVRESPONSE.class);
			Unmarshaller unmarshaller = jaxbResponse.createUnmarshaller();
			StringReader reader = new StringReader(response);
			IDVRESPONSE idvresponse = (IDVRESPONSE) unmarshaller.unmarshal(reader);
			System.out.println("response json for idv--------------" + idvresponse);
			if (idvresponse != null && idvresponse.getOutputmessage().isEmpty() && idvresponse.getIdvAmount() != null) {
				if((!vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) ) || (vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) && 
						!vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV.getCode()).getValueAsBoolean())){
					vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_PRODUCT_IDV.getCode(), idvresponse.getIdvAmount().toString());
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_PRODUCT_IDV.getCode(), idvresponse.getIdvAmount().toString());
					vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(),
							idvresponse.getIdvAmount().toString());
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(), idvresponse.getIdvAmount().toString());		
				}
				vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode(), idvresponse.getIdvAmountMin().toString());
				vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode(), idvresponse.getIdvAmountMax().toString());
				vendorRequest = addCategoryAttributeToRequest(vendorRequest,CarAttributeCode.AC_EXSHOWROOM_PRICE.getCode(),
						idvresponse.getExshowroomPrice().toString());
			} else {
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.NOT_SUPPORTED_MODEL.value()));
				return vendorResponse;
			}
			if(hdfcErgoMotorValidator.validateMaxAllowedIDV(vendorRequest,vendorResponse)){
				modifyCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(),
						vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode()).getValue());
			}else if(hdfcErgoMotorValidator.validateMinAllowedIDV(vendorRequest,vendorResponse)){
				modifyCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(), 
						vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode()).getValue());
			}
			if(vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) && 
					vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV.getCode()).getValueAsBoolean()){
				double modifiedIDV = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
				vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_PRODUCT_IDV.getCode(), df.format(modifiedIDV));
			}

			patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "getPremium");
			JsonNode patchJsonNodeForPremium = mapper.valueToTree(patchRequestVOList);
			System.out.println("this is the patchjsonnode premium--------------------------" + patchJsonNodeForPremium);
			jsonPatch = JsonPatch.fromJson(patchJsonNodeForPremium);
			System.out.println("JsonPatch for getPremium: " + mapper.writeValueAsString(jsonPatch));
			System.out.println("this is the request--------" + vendorRequest.getCategoryCode());
			ObjectFactoryPremiumCalculation objfacpremcal = new ObjectFactoryPremiumCalculation();
			PCVPremiumCalc pcvpremiumcalc = objfacpremcal.createPCVPremiumCalc();
			PCVPremiumCalc.AddOnCovers addons = objfacpremcal.createPCVPremiumCalcAddOnCovers();
			pcvpremiumcalc.getAddOnCovers().add(addons);
			orgJsonNode = mapper.valueToTree(pcvpremiumcalc);
			System.out.println("this is the orgJsonNode-------" + orgJsonNode);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			System.out.println("this is the patched json node premium -----------" + patchedJsonNode);
			pcvpremiumcalc = mapper.treeToValue(patchedJsonNode, PCVPremiumCalc.class);
			if ((StringUtils.isNotEmpty(pcvpremiumcalc.getIsPreviousClaim())
					&& pcvpremiumcalc.getIsPreviousClaim().equals("0"))
					|| (StringUtils.isNotEmpty(pcvpremiumcalc.getTypeofbusiness())
							&& pcvpremiumcalc.getTypeofbusiness().equalsIgnoreCase("New Business"))) {
				pcvpremiumcalc.setPreviousdiscount("0");
			}

			System.out.println("its here into patched json PREMIUM ------------" + pcvpremiumcalc);
			xmlString = prepareXMLPartString(PCVPremiumCalc.class, pcvpremiumcalc);
			System.out.println("getPremium------------" + xmlString);

			int vehicleCode = Integer.parseInt(idvRequest.getVehicleClassCd());
			String premiumResponse = bindingAndLoggingForFetch().getPremium(xmlString, vehicleCode);
			premiumResponse = premiumResponse.replace(XML_PROSTANDALONE, "");
			JAXBContext jaxbResponse1 = JAXBContext.newInstance(PREMIUMOUTPUT.class);
			Unmarshaller unmarshaller1 = jaxbResponse1.createUnmarshaller();
			StringReader reader1 = new StringReader(premiumResponse);
			PREMIUMOUTPUT premiumResp = (PREMIUMOUTPUT) unmarshaller1.unmarshal(reader1);
			System.out.println("resposne ----------"+Json.toJson(premiumResp));
			
			if (premiumResp.getNUMTOTALPREMIUM() != null && premiumResp.getTXTERRMSG() != null) {
				if (StringUtils.isEmpty(premiumResp.getTXTERRMSG())) {
					vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
							Json.toJson(premiumResp), vendorResponse, "getPremiumResponse");
					vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
					vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
				} else {
					if(premiumResp.getTXTERRMSG().trim().equalsIgnoreCase("FOR SELECTED MODEL ZERO DEPRECIATION COVER IS NOT APPLICABLE.")) {
						vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
								Json.toJson(premiumResp), vendorResponse, "getPremiumResponse");
						vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
						vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
					}else {
						vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
								FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),
								FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
					}
				}
				if(vendorResponse.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())){						
					AttributeValue attributeValue= vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
					attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
					vendorResponse.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
					vendorResponse.addAttributeValue(attributeValue);
					
				}
			}else {
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),
						FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
					FetchPlanMessage.NOT_SUPPORTED_MODEL.value()));
			return vendorResponse;
		}
		return vendorResponse;
	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		if (StringUtils.isNotEmpty(vendorRequest.getCategoryCode())
				&& vendorRequest.getCategoryCode().equalsIgnoreCase("BIKE")) {
			return purchaseForTwoWheeler(vendorRequest);
		} else {
			return purchaseForFourWheeler(vendorRequest);
		}
	}

	private ObjectNode purchaseForTwoWheeler(VendorRequest vendorRequest) {
		ObjectNode policyJson = Json.newObject();
		try {
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "GenerateTWTransNo");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));

			ObjectFactoryTwoWheelerProposalInput objectFactoryTwoWheelerProposalInput = new ObjectFactoryTwoWheelerProposalInput();
			XmlmotorpolicyTwoWheeler xmlmotorpolicy = objectFactoryTwoWheelerProposalInput.createXmlmotorpolicy();
			JsonNode orgJsonNode = mapper.valueToTree(xmlmotorpolicy);
			// System.out.println("mapped idv request--------" + orgJsonNode);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);

			// System.out.println("this is the patchedJsonNode------------" +
			// patchedJsonNode);

			xmlmotorpolicy = mapper.treeToValue(patchedJsonNode, XmlmotorpolicyTwoWheeler.class);
			if (StringUtils.isEmpty(xmlmotorpolicy.getIsPreviousClaim())
					|| xmlmotorpolicy.getIsPreviousClaim().equals("0")) {
				xmlmotorpolicy.setNCBRenewalPolicy("0");
			}

			if (StringUtils.isNotEmpty(xmlmotorpolicy.getTypeOfBusiness())
					&& xmlmotorpolicy.getTypeOfBusiness().equalsIgnoreCase("New Business")) {
				xmlmotorpolicy.setVehicleRegno("NEW");
			}

			settingValuesAfterCheckingOwnerTypeForTwoWheeler(vendorRequest, xmlmotorpolicy);
			// System.out.println("its here into patched json proposal ------------" +
			// xmlmotorpolicy);

			String xmlString = prepareXMLPartString(XmlmotorpolicyTwoWheeler.class, xmlmotorpolicy, "xmlmotorpolicy");
			System.out.println("proposal------------" + xmlString);

			String response = bindingAndLoggingForTwoWheelerProposal().generateTWTransNo(xmlString);
			response = response.replace(XML_PROLOG, "");
			 System.out.println("this is the response for get proposal------------" + response);

			JAXBContext jaxbResponse = JAXBContext.newInstance(WsResult.class);
			Unmarshaller unmarshaller = jaxbResponse.createUnmarshaller();
			StringReader reader = new StringReader(response);
			WsResult result = (WsResult) unmarshaller.unmarshal(reader);
			// System.out.println("response json for proposal--------------" + result);

			if (result.getWsResultSet().getWsStatus() == 0
					&& StringUtils.isNotEmpty(result.getWsResultSet().getWsMessage())) {
				policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
				policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
				policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
				policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
				policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
				policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
				policyJson.put(PolicyFields.PAYMENT_FREQUENCY.getCode(), PaymentFrequency.ANNUAL.name());
				policyJson.put(PolicyFields.PAYMENT_MODE.getCode(), PaymentMode.ONLINE.name());
				policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
				policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
				policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
				policyJson.put(PolicyFields.PREMIUM.getCode(),
						vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue());
				policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest
						.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValue());
				policyJson.put("CustomerId", result.getWsResultSet().getWsMessage());
				policyJson.put("TxnAmount", xmlmotorpolicy.getTotalAmoutpayable());
				policyJson.put("hdnPayMode", xmlmotorpolicy.getData1());
				policyJson.put("ProducerCd",
						xmlmotorpolicy.getAgentCode() + "-" + result.getWsResultSet().getWsMessage());
				policyJson.put("UserMailId", xmlmotorpolicy.getEmailId());
				policyJson.put("FORM_URL", propUtils.get("arka.vendor.hdfcergo.tw.purchase"));
				policyJson.put("VendorName", propUtils.get("arka.vendor.hdfcergo.vendor.name"));
				policyJson.put("AdditionalInfo2", "TW");
				policyJson.put("ProductCd", "TW");
			} else {
				policyJson.put(JsonUtils.ERROR, result.getWsResultSet().getWsMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR, FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}
		return policyJson;
	}

	private ObjectNode purchaseForFourWheeler(VendorRequest vendorRequest) {
		System.out.println("this is the vendor request-------------"+JsonUtils.toJson(vendorRequest));
		ObjectNode policyJson = Json.newObject();
		try {
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "xmlstring");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));

			ObjectFactoryProposalRequest objectFactoryProposalRequest = new ObjectFactoryProposalRequest();
			Xmlmotorpolicy xmlmotorpolicy = objectFactoryProposalRequest.createXmlmotorpolicy();
			Xmlmotorpolicy.AddOnCovers addOns = objectFactoryProposalRequest.createXmlmotorpolicyAddOnCovers();
			xmlmotorpolicy.setAddOnCovers(addOns);
			JsonNode orgJsonNode = mapper.valueToTree(xmlmotorpolicy);
			System.out.println("mapped idv request--------" + orgJsonNode);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);

			xmlmotorpolicy = mapper.treeToValue(patchedJsonNode, Xmlmotorpolicy.class);
			if (StringUtils.isEmpty(xmlmotorpolicy.getIsPreviousClaim())
					|| xmlmotorpolicy.getIsPreviousClaim().equals("0")) {
				xmlmotorpolicy.setNCBRenewalPolicy("0");
			}

			if (StringUtils.isNotEmpty(xmlmotorpolicy.getTypeOfBusiness())
					&& xmlmotorpolicy.getTypeOfBusiness().equalsIgnoreCase("New Business")) {
				xmlmotorpolicy.setVehicleRegno("NEW");
			}

			if (StringUtils.isNotEmpty(xmlmotorpolicy.getTypeOfBusiness())
					&& xmlmotorpolicy.getTypeOfBusiness().equalsIgnoreCase("RollOver")) {
				if (xmlmotorpolicy.getIsZeroDeptCover().equals("1")) {
					xmlmotorpolicy.setIsZeroDeptRollOver("1");
				}
			}
			xmlmotorpolicy.setIsCustomerAuthenticationDone(1);

			settingValuesAfterCheckingOwnerTypeForFourWheeler(vendorRequest, xmlmotorpolicy);

			System.out.println("its here into patched json proposal ------------" + xmlmotorpolicy);

			String xmlString = prepareXMLPartString(Xmlmotorpolicy.class, xmlmotorpolicy, "xmlmotorpolicy");
			System.out.println("proposal------------" + xmlString);

			String response = bindingAndLoggingForProposal().xmlstring(xmlString);
			response = response.replace(XML_PROLOG, "");
			System.out.println("this is the response for get proposal------------" + response);

			JAXBContext jaxbResponse = JAXBContext.newInstance(WsResult.class);
			Unmarshaller unmarshaller = jaxbResponse.createUnmarshaller();
			StringReader reader = new StringReader(response);
			WsResult result = (WsResult) unmarshaller.unmarshal(reader);
			System.out.println("response json for proposal--------------" + result);

			if (result.getWsResultSet().getWsStatus() == 0
					&& StringUtils.isNotEmpty(result.getWsResultSet().getWsMessage())) {
				policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
				policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
				policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
				policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
				policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
				policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
				policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
				policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
				policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
				policyJson.put(PolicyFields.PREMIUM.getCode(),
						vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue());
				policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest
						.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValue());
				policyJson.put("CustomerId", result.getWsResultSet().getWsMessage());
				policyJson.put("TxnAmount", xmlmotorpolicy.getTotalAmoutpayable());
				policyJson.put("hdnPayMode", xmlmotorpolicy.getData1());
				policyJson.put("ProducerCd",
						xmlmotorpolicy.getAgentCode() + "-" + result.getWsResultSet().getWsMessage());
				policyJson.put("UserMailId", xmlmotorpolicy.getEmailId());
				policyJson.put("FORM_URL", propUtils.get("arka.vendor.hdfcergo.fw.purchase"));
				policyJson.put("VendorName", propUtils.get("arka.vendor.hdfcergo.vendor.name"));
				policyJson.put("AdditionalInfo2", "MOT");
				policyJson.put("ProductCd", "MOT");
			} else {
				policyJson.put(JsonUtils.ERROR, result.getWsResultSet().getWsMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR, FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}
		return policyJson;
	}

	private static void settingValuesAfterCheckingOwnerTypeForFourWheeler(VendorRequest vendorRequest,
			Xmlmotorpolicy xmlmotorpolicy) {
		if (vendorRequest.isAttributePresent("AI-COMPANY_NAME") && vendorRequest.isAttributePresent("AI-SHORT_NAME")) {
			xmlmotorpolicy.setFirstName(vendorRequest.getInputQuoteForAttribute("AI-COMPANY_NAME").stream().filter(
					input_quote_params -> input_quote_params.getCategoryAttributeCode().equals("AI-COMPANY_NAME"))
					.findAny().get().getValue());
			xmlmotorpolicy.setLastName(vendorRequest.getInputQuoteForAttribute("AI-SHORT_NAME").stream()
					.filter(input_quote_params -> input_quote_params.getCategoryAttributeCode().equals("AI-SHORT_NAME"))
					.findAny().get().getValue());
			xmlmotorpolicy.setDateOfBirth("01-01-1900");
		}
	}

	private static void settingValuesAfterCheckingOwnerTypeForTwoWheeler(VendorRequest vendorRequest,
			XmlmotorpolicyTwoWheeler xmlmotorpolicy) {
		if (vendorRequest.isAttributePresent("AI-COMPANY_NAME") && vendorRequest.isAttributePresent("AI-SHORT_NAME")) {
			xmlmotorpolicy.setFirstName(vendorRequest.getInputQuoteForAttribute("AI-COMPANY_NAME").stream().filter(
					input_quote_params -> input_quote_params.getCategoryAttributeCode().equals("AI-COMPANY_NAME"))
					.findAny().get().getValue());
			xmlmotorpolicy.setLastName(vendorRequest.getInputQuoteForAttribute("AI-SHORT_NAME").stream()
					.filter(input_quote_params -> input_quote_params.getCategoryAttributeCode().equals("AI-SHORT_NAME"))
					.findAny().get().getValue());
			xmlmotorpolicy.setDateOfBirth("01-01-1900");
		}
	}

	private static ServiceSoapProposal bindingAndLoggingForProposal() {
		ServiceProposal serviceproposal = new ServiceProposal();
		ServiceSoapProposal servicesoapproposal = serviceproposal.getServiceSoap();
		BindingProvider bindingProvider = (BindingProvider) servicesoapproposal;
		DefaultMessageHandler.printCDataMessage(bindingProvider);
		System.out.println("this is the service proposal opject--------"+JsonUtils.toJson(servicesoapproposal));
		return servicesoapproposal;
	}

	private static ServiceSoapTwoProposal bindingAndLoggingForTwoWheelerProposal() {
		ServiceTwProposal serviceproposal = new ServiceTwProposal();
		ServiceSoapTwoProposal servicesoapproposal = serviceproposal.getServiceSoap();
		BindingProvider bindingProvider = (BindingProvider) servicesoapproposal;
		DefaultMessageHandler.printCDataMessage(bindingProvider);
		return servicesoapproposal;
	}

	private static ServiceSoap bindingAndLoggingForFetch() {
		Service service = new Service();
		ServiceSoap serviceSoap = service.getServiceSoap();
		BindingProvider bindingProvider = (BindingProvider) serviceSoap;
		DefaultMessageHandler.printCDataMessage(bindingProvider);
		System.out.println("this is the service soap for -----------"+JsonUtils.toJson(serviceSoap));
		return serviceSoap;
	}

	private static String prepareXMLPartString(Class clazz, Object obj) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(clazz);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		JAXBElement jx = new JAXBElement(new QName(obj.getClass().getSimpleName()), clazz, obj);
		StringWriter sw = new StringWriter();
		marshaller.marshal(jx, sw);
		return sw.toString().replace(XML_PROLOG, "").replaceFirst("\n|\r", "");
	}

	private static String prepareXMLPartString(Class clazz, Object obj, String xmlRootName) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(clazz);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		JAXBElement jx = new JAXBElement(new QName(xmlRootName), clazz, obj);
		StringWriter sw = new StringWriter();
		marshaller.marshal(jx, sw);
		return sw.toString().replace(XML_PROLOG, "").replaceFirst("\n|\r", "");
	}

	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("HDFCERGO_MOTOR_" + AttributeUtil.generateRandom());
		return vendorResponse;

	}
}
