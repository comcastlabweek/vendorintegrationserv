package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;

public class HdfcErgoMotorValidator extends MotorBaseValidator {
	private static final Logger logger = LoggerFactory.getLogger(HdfcErgoMotorValidator.class);

	public boolean validateForBreakInPolicy(VendorRequest vendorRequest) {
		boolean flag = false;

		if (vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if (vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue()
					.equalsIgnoreCase("expired")) {
				flag = true;
			}
		}
		return flag;
	}
}
