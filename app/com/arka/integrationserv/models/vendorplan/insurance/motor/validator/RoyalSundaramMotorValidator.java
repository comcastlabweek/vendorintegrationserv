package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;

/**
 * @author Jakpren
 *
 */
public class RoyalSundaramMotorValidator extends MotorBaseValidator{

	private static final Logger logger = LoggerFactory.getLogger(RoyalSundaramMotorValidator.class);
	private static final double MAX_IDV_TWP_BREAKINPOLICY = 300000.0;
	private static final int NEW_VEHICLE_CRITERIA = 200;
	
	 private static final Map<String, String> myMap2 = new HashMap<String, String>() {
		private static final long serialVersionUID = 3153302126245804192L;
			{
	            put("E-0006", FetchPlanMessage.INVALID_MANUFACTUREYEAR.value());
	        }
	 };
	
	public static Map<String, String> getMymap2() {
		return myMap2;
	}

	public boolean validateForBreakInPolicy(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		boolean flag = false;
		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired")){
				flag =true;
			}
		else {
			flag =false;
		}			
		}
		
		if(flag && (vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.CAR.name()) || 
				vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name())  ))
	   {
			vendorResponse.setErrorJson(getValidationError(VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
					FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value())); 
			return true;
		}
		return false;
		
	}
	
	public boolean validateIDVForBreakInPolicy(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		boolean flag = false;
		double idv = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValueAsDouble();
		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired")){
				flag =true;
			}
		else {
			flag =false;
		}			
		}
		if(flag && vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name()) && idv>MAX_IDV_TWP_BREAKINPOLICY ){
			return true;
		}
		return false;
		
	}
	public boolean isNewVehicle(VendorRequest vendorRequest) {
		 org.joda.time.format.DateTimeFormatter DATE_FORMATTER = 
				    new DateTimeFormatterBuilder()
				        .append(null, new DateTimeParser[]{
				                DateTimeFormat.forPattern("yyyy-M-dd").getParser(),
				                DateTimeFormat.forPattern("yyyy-M-d").getParser(),
				                DateTimeFormat.forPattern("yyyy-MM-d").getParser(),
				                DateTimeFormat.forPattern("yyyy-MM-dd").getParser()})
				        .toFormatter();
		 String dateOfRegistrationStr ="",policyStartDateStr = "";
		 org.joda.time.LocalDate dateOfRegistration = null,policyStartDate = null ;
		 boolean flag = false;
		 if(vendorRequest.isAttributePresent(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode())) {
			dateOfRegistrationStr = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode()).getValue();
			dateOfRegistration = DATE_FORMATTER.parseLocalDate(dateOfRegistrationStr);
		 }
		 if(vendorRequest.isAttributePresent(CarAttributeCode.AC_POLICY_START_DATE.getCode())) {
			 policyStartDateStr = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_POLICY_START_DATE.getCode()).getValue();
			 policyStartDate = DATE_FORMATTER.parseLocalDate(policyStartDateStr);
		 }
		 if(dateOfRegistration!=null && policyStartDate!= null) {
			LocalDate newDate = dateOfRegistration.plusDays(NEW_VEHICLE_CRITERIA);
			if( newDate.isAfter(policyStartDate)){
				flag = true;  // new vehicle
			}
		 }
		return flag;
		
	}
	
	public boolean validateMaxAllowedIDV(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		double maxIDV = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode()).getValueAsDouble();
		double idv = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValueAsDouble();
		double electricalAccValue =0 ,nonElectricalAccValue =0 ;
		if(vendorRequest.isAttributePresent("AC-COVER-ELECTRICAL_ACCESSORIES_VALUE") && 
				vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble()>0){
			electricalAccValue =  vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble();			
		}
		if(vendorRequest.isAttributePresent("AC-COVER-NON_ELECTRICAL_ACCESSORIES_VALUE") && 
				vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-NON_ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble()>0){
			nonElectricalAccValue =  vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-NON_ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble();			
		}
		boolean flag =false;
		double totalIdv = idv + electricalAccValue + nonElectricalAccValue ;
		if(totalIdv>maxIDV) {
			flag = true;
		}
		return flag;
		
	}

	public boolean validateModifyIDV(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		if(vendorRequest.isAttributePresent("AC-MODIFIED_IDV_VAL") &&
				vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV_VAL").getValueAsDouble()>0) {
			return true;
		}
		return false;
	}

}
