package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoB2CCustDetailsUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotPlanDetailsUser;

public class BajajMotorValidator extends MotorBaseValidator {

	private static final Logger logger = LoggerFactory.getLogger(BajajMotorValidator.class);

	public boolean validateForBreakInPolicy(VendorRequest vendorRequest) {
		boolean flag = false;

		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired")){
				flag =true;
			}
		}

		return flag;
	}

	public boolean validateHP01RTO(VendorRequest vendorRequest) {
		boolean flag = false;

		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_RTOCODE.getCode())) {
			if(vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_RTOCODE.getCode()).getValue().equalsIgnoreCase("HP-01")){
				flag =true;
			}
		}

		return flag;
	}

	public JsonNode validateCustomerDetails(WeoB2CCustDetailsUser patchedpcustdetailsinout,
			WeoMotPlanDetailsUser patchedpweomotpolicyininout, ObjectNode policyJson) {
		
		

		if(patchedpweomotpolicyininout.getPartnerType().equalsIgnoreCase("P")){
		if(isValidAttribute(patchedpcustdetailsinout.getFirstName())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NAME.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getSurname())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NAME.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getDateOfBirth())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_DOB.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getAvailableTime())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_AADHARNO.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getMobileAlerts())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_PAN.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getTitle())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_TITLE.value());
			return policyJson;
		}
		}
		if(patchedpweomotpolicyininout.getPartnerType().equalsIgnoreCase("I")){
		if(isValidAttribute(patchedpcustdetailsinout.getInstitutionName())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NAME.value());
			return policyJson;
		}
		}
		
		if(isValidAttribute(patchedpcustdetailsinout.getAddLine1())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ADDRESS.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getPincode())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_PINCODE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpcustdetailsinout.getEmail())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_EMAIL.value());
			return policyJson;
		}

		if(isValidAttribute(patchedpweomotpolicyininout.getTermStartDate())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_STARTDATE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getTermEndDate())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ENDDATE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getVehicleType())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_VEHICLETYPE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getVehicleMake())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_VEHICLEMAKE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getVehicleModel())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_VEHICLEMODEL.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getVehicleSubtype())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_VEHICLEVARIANT.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getFuel())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_FUEL.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getZone())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ZONE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getEngineNo())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ENGINENO.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getChassisNo())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_CHASISNO.value());
			return policyJson;
		}
		
		if(!patchedpweomotpolicyininout.getPolType().equalsIgnoreCase("1")){
		if(isValidAttribute(patchedpweomotpolicyininout.getRegistrationNo())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_REGNO.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getRegistrationDate())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_REGDATE.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getRegistrationLocation())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_REGLOCATION.value());
			return policyJson;
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getRegiLocOther())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_REGLOCATION.value());
			return policyJson;
		}	
		}
		if(isValidAttribute(patchedpweomotpolicyininout.getYearManf())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_MANUFACTUREYEAR.value());
			return policyJson;
		}

		return policyJson;

	}
	

}
