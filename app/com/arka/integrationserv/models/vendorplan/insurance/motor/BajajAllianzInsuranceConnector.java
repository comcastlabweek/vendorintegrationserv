package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.jws.WebService;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.RedisDB;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.insurance.motor.validator.BajajMotorValidator;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.arka.integrationserv.models.vendorplan.utils.DefaultMessageHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.google.inject.Inject;

import bajajallianz.motwebpolicy.webServiceImplService_wsdl.ClientInfo;
import bajajallianz.motwebpolicy.webServiceImplService_wsdl.ObjectFactory_pdf;
import bajajallianz.motwebpolicy.webServiceImplService_wsdl.WebServiceImplService;
import bajajallianz.motwebpolicy.webServiceImplService_wsdl.WebServiceInterface;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.WebServicePolicy;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.WebServicePolicy_Service;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.ObjectFactory;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoB2CCustDetailsUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoBjazMotQuestionaryUserArray;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotAccessoriesUserArray;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotDetariffObjUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotGenParamUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotGenParamUserArray;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotPlanDetailsUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotPremiumDetailsUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoMotPremiumSummaryUserArray;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoSigMotExtraCoversUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoSigOtherDetailsUser;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoTyacPayRowWsUserArray;
import bajajallianz.motwebpolicy.webservicepolicy_wsdl.types.WeoTygeErrorMessageUserArray;
import play.Configuration;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;

@WebService(targetNamespace = "http://com/bajajallianz/motWebPolicy/WebServicePolicy.wsdl", serviceName = "WebServicePolicy")

public class BajajAllianzInsuranceConnector extends MotorVendorConnector {

	@Inject
	DBUtil dbUtil;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;

	@Inject
	BajajMotorValidator bajajvalidator;

	@Inject
	WSClient wsClient;

	@Inject
	Configuration configure;

	@Inject
	CacheService cache;

	public static final Pattern replacePlaceholder = Pattern.compile("\\[\\{(.*?)\\}\\]");

	public BajajAllianzInsuranceConnector() {

	}

	private static final Logger logger = LoggerFactory.getLogger(BajajAllianzInsuranceConnector.class);

	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {

		VendorResponse vendorResponse = new VendorResponse();

		if (bajajvalidator.validateForBreakInPolicy(vendorRequest)) {
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
					FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
					FetchPlanMessage.BREAK_IN_POICY_NOT_ALLOWED.value()));
			return vendorResponse;
		}
		if (bajajvalidator.validateHP01RTO(vendorRequest)) {
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
					FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
					FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value()));
			return vendorResponse;
		}

		if (vendorRequest.isAttributePresent("AC-MODIFIED_IDV")
				&& Objects.nonNull(vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV"))
				&& vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV").getValueAsBoolean()
				&& vendorRequest.isAttributePresent("AC-MODIFIED_IDV_VAL")
				&& Objects.nonNull(vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV_VAL"))) {

			try {
				int originalIdv = 0;
				InputPolicyParams attributeValue = new InputPolicyParams();
				int modifiedIdv = vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV_VAL")
						.getValue() != null
								? Integer.parseInt(
										vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV_VAL").getValue())
								: 0;
				JsonNode originalKeys = cache
						.getValueWithKeyPrefix("BAGIC" + "." + vendorRequest.getEnquiryId(), RedisDB.DEFAULT.getValue())
						.toCompletableFuture().get();

				JsonNode originalJson = JsonUtils.toJson(
						CacheService.getCacheKeyValue(originalKeys, "BAGIC" + "." + vendorRequest.getEnquiryId()));

				if (JsonUtils.isValidField(originalJson, "attributes")
						&& JsonUtils.isValidField(originalJson.get("attributes"), "AC-PRODUCT_IDV")
						&& JsonUtils.isValidField(originalJson.get("attributes").get("AC-PRODUCT_IDV"), "value")) {
					originalIdv = originalJson.get("attributes").get("AC-PRODUCT_IDV").get("value").asInt();

					if (modifiedIdv != 0 && originalIdv != 0) {
						if (modifiedIdv <= originalIdv) {
							vendorRequest.removeInputQuoteParam("AC-MODIFIED_IDV_VAL");
							attributeValue.setCategoryAttributeCode("AC-MODIFIED_IDV_VAL");
							attributeValue.setValue(String.valueOf(originalIdv));
							vendorRequest.addAttributeValue(attributeValue);
							vendorResponse = getQuotes(vendorRequest, vendorResponse);
						} else {
							vendorResponse = getQuotes(vendorRequest, vendorResponse);
							if (JsonUtils.isValidField(vendorResponse.getErrorJson(), "error")) {
								String errorText = vendorResponse.getErrorJson().get("error")
										.get(JsonUtils.ERROR_MESSAGE).asText();
								if (errorText.contains("Idv entered")) {
									String higherIdv = errorText.substring(errorText.lastIndexOf("Rs.") + 3,
											errorText.lastIndexOf("/"));
									if (!higherIdv.equals(null)) {
										vendorRequest.removeInputQuoteParam("AC-MODIFIED_IDV_VAL");
										attributeValue.setCategoryAttributeCode("AC-MODIFIED_IDV_VAL");
										attributeValue.setValue(higherIdv);
										vendorRequest.addAttributeValue(attributeValue);
										vendorResponse = getQuotes(vendorRequest, new VendorResponse());
									}
								}
							}

						}
					}
				}
			} catch (Exception e) {
				logger.error("Unable to get Bajaj Original IDV from cache : " + e.getMessage());
				vendorResponse = getQuotes(vendorRequest, vendorResponse);
			}
		} else {
			vendorResponse = getQuotes(vendorRequest, vendorResponse);

			if (!(JsonUtils.isValidField(vendorResponse.getErrorJson(), "error"))) {
				JsonNode vendorResponseJson = vendorResponse.formatResponse();
				cache.getOrElse("BAGIC" + "." + vendorRequest.getEnquiryId(), RedisDB.DEFAULT.getValue(),
						() -> JsonUtils.stringify(vendorResponseJson));
			}
		}
		if (vendorResponse.equals(null)) {
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
					FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
					FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
		}
		return vendorResponse;

	}

	private VendorResponse getQuotes(VendorRequest vendorRequest, VendorResponse vendorResponse) {

		try {

			ObjectFactory objectFactory = new ObjectFactory();

			String pUserId = configure.getString("arka.vendor.bajaj.username");
			String pPassword = configure.getString("arka.vendor.bajaj.password");
			String endPointUrl = configure.getString("arka.vendor.bajaj.endpoint.url");

			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"calculateMotorPremiumSigRootMap");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Map<String, Object> orgRootMap = new HashMap<String, Object>();
			orgRootMap.put("pVehicleCode", "");
			orgRootMap.put("pContactNo", "");
			orgRootMap.put("pCity", "");
			orgRootMap.put("pTransactionId_inout", "");
			orgRootMap.put("pTransactionType", "");
			orgRootMap.put("pErrorCode_out", "");

			JsonNode orgJsonNode = mapper.valueToTree(orgRootMap);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
			Map<?, ?> patchedRootMap = mapper.treeToValue(patchedJsonNode, HashMap.class);
			// System.out.println("patchedRootMap"+patchedRootMap);

			String pContactNo = patchedRootMap.get("pContactNo").toString();
			String pCity = patchedRootMap.get("pCity").toString();
			String pTransactionType = patchedRootMap.get("pTransactionType").toString();

			BigDecimal pVehicleCode = null;
			if (patchedRootMap.get("pVehicleCode").toString().equals(null)) {

				logger.error("BAGIC Vehicle Code not found");
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
			} else {
				pVehicleCode = new BigDecimal(patchedRootMap.get("pVehicleCode").toString());
			}

			Holder<BigDecimal> pErrorCodeOut = new Holder<BigDecimal>();
			BigDecimal perrorcodeout = new BigDecimal(patchedRootMap.get("pErrorCode_out").toString());
			pErrorCodeOut.value = perrorcodeout;

			Holder<BigDecimal> pTransactionIdInout = new Holder<BigDecimal>();
			BigDecimal ptransactionidinout = new BigDecimal(patchedRootMap.get("pTransactionId_inout").toString());
			pTransactionIdInout.value = ptransactionidinout;

			patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotPlanDetailsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoMotPlanDetailsUser"+mapper.writeValueAsString(jsonPatch));

			Holder<WeoMotPlanDetailsUser> pWeoMotPolicyInInout = new Holder<WeoMotPlanDetailsUser>();
			WeoMotPlanDetailsUser weomotpolicyininput = objectFactory.createWeoMotPlanDetailsUser();
			orgJsonNode = mapper.valueToTree(weomotpolicyininput);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotPlanDetailsUser patchedweomotpolicyininput = mapper.treeToValue(patchedJsonNode,
					WeoMotPlanDetailsUser.class);
			pWeoMotPolicyInInout.value = patchedweomotpolicyininput;
			// System.out.println("weoMotPlanDetailsUser"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotAccessoriesUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoMotAccessoriesUserArray> accessoriesListInout = new Holder<WeoMotAccessoriesUserArray>();
			WeoMotAccessoriesUserArray accessorieslistinout = objectFactory.createWeoMotAccessoriesUserArray();
			accessorieslistinout.getWeoMotAccessoriesUser().add(objectFactory.createWeoMotAccessoriesUser());
			orgJsonNode = mapper.valueToTree(accessorieslistinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotAccessoriesUserArray patchedaccessorieslistinout = mapper.treeToValue(patchedJsonNode,
					WeoMotAccessoriesUserArray.class);
			accessoriesListInout.value = patchedaccessorieslistinout;
			// System.out.println("weoMotAccessoriesUser"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotGenParamUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoMotGenParamUserArray> paddoncoverListInout = new Holder<WeoMotGenParamUserArray>();
			WeoMotGenParamUserArray paddoncoverlistinout = objectFactory.createWeoMotGenParamUserArray();
			paddoncoverlistinout.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverlistinout.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverlistinout.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverlistinout.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverlistinout.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			orgJsonNode = mapper.valueToTree(paddoncoverlistinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotGenParamUserArray patchedpaddoncoverlistinout = mapper.treeToValue(patchedJsonNode,
					WeoMotGenParamUserArray.class);

			ListIterator<WeoMotGenParamUser> item = patchedpaddoncoverlistinout.getWeoMotGenParamUser().listIterator();
			while (item.hasNext()) {
				WeoMotGenParamUser vehicleItem = item.next();

				if (vehicleItem.getParamRef().equalsIgnoreCase("CNG")) {
					if (!(vendorRequest.isAttributePresent("AC-IS_CNG_OR_LPG_EXTERNALLY_FITTED") && Objects.nonNull(
							vendorRequest.getUniqueInputQuoteForAttribute("AC-IS_CNG_OR_LPG_EXTERNALLY_FITTED")))) {
						item.remove();
					}
				}
			}

			paddoncoverListInout.value = patchedpaddoncoverlistinout;
			// System.out.println("weoMotGenParamUsermapper"+mapper.writeValueAsString(patchedpaddoncoverlistinout));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoSigMotExtraCoversUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			WeoSigMotExtraCoversUser motExtraCover = objectFactory.createWeoSigMotExtraCoversUser();
			orgJsonNode = mapper.valueToTree(motExtraCover);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			motExtraCover = mapper.treeToValue(patchedJsonNode, WeoSigMotExtraCoversUser.class);
			// System.out.println("weoSigMotExtraCoversUsermapper"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoBjazMotQuestionaryUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoBjazMotQuestionaryUserArray> pQuestListInout = new Holder<WeoBjazMotQuestionaryUserArray>();
			WeoBjazMotQuestionaryUserArray pquestlistinout = objectFactory.createWeoBjazMotQuestionaryUserArray();
			pquestlistinout.getWeoBjazMotQuestionaryUser().add(objectFactory.createWeoBjazMotQuestionaryUser());
			orgJsonNode = mapper.valueToTree(pquestlistinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoBjazMotQuestionaryUserArray patchedpquestlistinout = mapper.treeToValue(patchedJsonNode,
					WeoBjazMotQuestionaryUserArray.class);
			pQuestListInout.value = patchedpquestlistinout;
			// System.out.println("weoBjazMotQuestionaryUsermapper"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotDetariffObjUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoMotDetariffObjUser> pDetariffObjInout = new Holder<WeoMotDetariffObjUser>();
			WeoMotDetariffObjUser pdetariffobjinout = objectFactory.createWeoMotDetariffObjUser();
			orgJsonNode = mapper.valueToTree(pdetariffobjinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotDetariffObjUser patchedpdetariffobjinout = mapper.treeToValue(patchedJsonNode,
					WeoMotDetariffObjUser.class);
			pDetariffObjInout.value = patchedpdetariffobjinout;
			// System.out.println("weoMotDetariffObjUsermapper"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoMotPremiumDetailsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoMotPremiumDetailsUser> premiumDetailsOutOut = new Holder<WeoMotPremiumDetailsUser>();
			WeoMotPremiumDetailsUser premiumdetailsoutout = objectFactory.createWeoMotPremiumDetailsUser();
			orgJsonNode = mapper.valueToTree(premiumdetailsoutout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotPremiumDetailsUser patchedpremiumdetailsoutout = mapper.treeToValue(patchedJsonNode,
					WeoMotPremiumDetailsUser.class);
			premiumDetailsOutOut.value = patchedpremiumdetailsoutout;
			// System.out.println("weoMotPremiumDetailsUsermapper"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoMotPremiumSummaryUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoMotPremiumSummaryUserArray> premiumSummaryListOut = new Holder<WeoMotPremiumSummaryUserArray>();
			WeoMotPremiumSummaryUserArray premiumsummarylistout = objectFactory.createWeoMotPremiumSummaryUserArray();
			premiumsummarylistout.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumsummarylistout.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumsummarylistout.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumsummarylistout.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumsummarylistout.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumsummarylistout.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			orgJsonNode = mapper.valueToTree(premiumsummarylistout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotPremiumSummaryUserArray patchedpremiumsummarylistout = mapper.treeToValue(patchedJsonNode,
					WeoMotPremiumSummaryUserArray.class);
			premiumSummaryListOut.value = patchedpremiumsummarylistout;
			// System.out.println("weoMotPremiumSummaryUsermapper"+mapper.writeValueAsString(patchedJsonNode));

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoTygeErrorMessageUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Holder<WeoTygeErrorMessageUserArray> pErrorOut = new Holder<WeoTygeErrorMessageUserArray>();
			WeoTygeErrorMessageUserArray perrorout = objectFactory.createWeoTygeErrorMessageUserArray();
			perrorout.getWeoTygeErrorMessageUser().add(objectFactory.createWeoTygeErrorMessageUser());
			orgJsonNode = mapper.valueToTree(perrorout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoTygeErrorMessageUserArray patchedperrorout = mapper.treeToValue(patchedJsonNode,
					WeoTygeErrorMessageUserArray.class);
			pErrorOut.value = patchedperrorout;
			// System.out.println("weoTygeErrorMessageUsermapper"+mapper.writeValueAsString(patchedJsonNode));

			WebServicePolicy_Service webServicePolicyServ = new WebServicePolicy_Service();
			WebServicePolicy webServicePolicyPort = webServicePolicyServ.getWebServicePolicyPort();

			System.out.println("BAGIC REQUEST STARTS");

			BindingProvider bindingProvider = (BindingProvider) webServicePolicyPort;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrl);
			DefaultMessageHandler.printMessage(bindingProvider);

			webServicePolicyPort.calculateMotorPremiumSig(pUserId, pPassword, pVehicleCode, pCity, pWeoMotPolicyInInout,
					accessoriesListInout, paddoncoverListInout, motExtraCover, pQuestListInout, pDetariffObjInout,
					premiumDetailsOutOut, premiumSummaryListOut, pErrorOut, pErrorCodeOut, pTransactionIdInout,
					pTransactionType, pContactNo);

			System.out.println("BAGIC REQUEST ENDS");

			if (pErrorCodeOut.value.intValueExact() == 0) {

				vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
						Json.toJson(pWeoMotPolicyInInout.value), vendorResponse, "weoMotPlanDetailsUser_response");

				vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
						Json.toJson(premiumSummaryListOut.value), vendorResponse, "weoMotPremiumSummaryUser_response");

				vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
						Json.toJson(premiumDetailsOutOut.value), vendorResponse, "weoMotPremiumDetailsUser_response");

				AttributeValue attributeValue = new AttributeValue();
				attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_QUOTE_ID.getCode());
				attributeValue.setValue(pTransactionIdInout.value.toString());
				vendorResponse.addAttributeValue(attributeValue);
				if (vendorResponse.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())) {
					attributeValue = vendorResponse
							.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
					attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
					vendorResponse.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
					vendorResponse.addAttributeValue(attributeValue);

				}
				vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
				vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
			} else {
				logger.error(pErrorOut.value.getWeoTygeErrorMessageUser().get(0).getErrText());
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						pErrorOut.value.getWeoTygeErrorMessageUser().get(0).getErrText()));
			}

		} catch (Exception e) {

			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),
					FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(), null));
			return vendorResponse;
		}
		return vendorResponse;

	}

	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("BAGIC_MOTOR_" + AttributeUtil.generateRandom());
		return vendorResponse;

	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		ObjectNode policyJson = Json.newObject();

		try {

			ObjectFactory objectFactory = new ObjectFactory();

			String pUserId = configure.getString("arka.vendor.bajaj.username");
			String pPassword = configure.getString("arka.vendor.bajaj.password");
			String returnUrl = configure.getString("arka.vendor.default.return.url") + "?";
			String endPointUrl = configure.getString("arka.vendor.bajaj.endpoint.url");
			String paymentUrl = configure.getString("arka.vendor.bajaj.payment.url");

			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "issuePolicyRootMap");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);

			Map<String, Object> orgRootMap = new HashMap<String, Object>();
			orgRootMap.put("pTransactionId_inout", "");
			orgRootMap.put("ppolicyref_out", "");
			orgRootMap.put("ppolicyissuedate_out", "");
			orgRootMap.put("ppartId_out", "");
			orgRootMap.put("pErrorCode_out", "");
			orgRootMap.put("ppremiumpayerid", "");
			orgRootMap.put("paymentmode", "");

			JsonNode orgJsonNode = mapper.valueToTree(orgRootMap);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
			Map<?, ?> patchedRootMap = mapper.treeToValue(patchedJsonNode, HashMap.class);
			// System.out.println("patchedJsonNode"+mapper.writeValueAsString(patchedJsonNode));

			BigDecimal pTransactionId = new BigDecimal(patchedRootMap.get("pTransactionId_inout").toString());
			// System.out.println("pTransactionId_inout"+pTransactionId);

			Holder<String> ppolicyrefOut = new Holder<String>();
			String ppolicyref_out = patchedRootMap.get("ppolicyref_out").toString();
			ppolicyrefOut.value = ppolicyref_out;
			// System.out.println("ppolicyrefOut"+ppolicyrefOut.value);

			Holder<String> ppolicyissuedateOut = new Holder<String>();
			String ppolicyissuedate_out = patchedRootMap.get("ppolicyissuedate_out").toString();
			ppolicyissuedateOut.value = ppolicyissuedate_out;
			// System.out.println("ppolicyissuedateOut"+ppolicyissuedateOut.value);

			Holder<String> ppartIdOut = new Holder<String>();
			String ppartId_out = patchedRootMap.get("ppartId_out").toString();
			ppartIdOut.value = ppartId_out;
			// System.out.println("ppartIdOut"+ppartIdOut.value);

			Holder<BigDecimal> pErrorCodeOut = new Holder<BigDecimal>();
			BigDecimal perrorcodeout = new BigDecimal(patchedRootMap.get("pErrorCode_out").toString());
			pErrorCodeOut.value = perrorcodeout;
			// System.out.println("pErrorCodeOut"+pErrorCodeOut.value);

			BigDecimal ppremiumpayerid = new BigDecimal(patchedRootMap.get("ppremiumpayerid").toString());
			// System.out.println("ppremiumpayerid"+ppremiumpayerid);

			String paymentmode = patchedRootMap.get("paymentmode").toString();
			// System.out.println("paymentmode"+paymentmode);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoTyacPayRowWsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoTyacPayRowWsUser: " +
			// mapper.writeValueAsString(jsonPatch));

			Holder<WeoTyacPayRowWsUserArray> pRcptListInout = new Holder<WeoTyacPayRowWsUserArray>();
			WeoTyacPayRowWsUserArray prcptlistinout = objectFactory.createWeoTyacPayRowWsUserArray();
			prcptlistinout.getWeoTyacPayRowWsUser().add(objectFactory.createWeoTyacPayRowWsUser());
			orgJsonNode = mapper.valueToTree(prcptlistinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoTyacPayRowWsUserArray patchedprcptlistinout = mapper.treeToValue(patchedJsonNode,
					WeoTyacPayRowWsUserArray.class);
			pRcptListInout.value = patchedprcptlistinout;

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoB2CCustDetailsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoB2CCustDetailsUser: " +
			// mapper.writeValueAsString(jsonPatch));

			Holder<WeoB2CCustDetailsUser> pCustDetailsInout = new Holder<WeoB2CCustDetailsUser>();
			WeoB2CCustDetailsUser pcustdetailsinout = objectFactory.createWeoB2CCustDetailsUser();
			prcptlistinout.getWeoTyacPayRowWsUser().add(objectFactory.createWeoTyacPayRowWsUser());
			orgJsonNode = mapper.valueToTree(pcustdetailsinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoB2CCustDetailsUser patchedpcustdetailsinout = mapper.treeToValue(patchedJsonNode,
					WeoB2CCustDetailsUser.class);
			pCustDetailsInout.value = patchedpcustdetailsinout;

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotPlanDetailsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoMotPlanDetailsUser: " +
			// mapper.writeValueAsString(jsonPatch));

			Holder<WeoMotPlanDetailsUser> pWeoMotPolicyInInout = new Holder<WeoMotPlanDetailsUser>();
			WeoMotPlanDetailsUser pweomotpolicyininout = objectFactory.createWeoMotPlanDetailsUser();
			orgJsonNode = mapper.valueToTree(pweomotpolicyininout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoMotPlanDetailsUser patchedpweomotpolicyininout = mapper.treeToValue(patchedJsonNode,
					WeoMotPlanDetailsUser.class);
			pWeoMotPolicyInInout.value = patchedpweomotpolicyininout;

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotAccessoriesUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("WeoMotAccessoriesUserArray: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoMotAccessoriesUserArray accessoriesList = objectFactory.createWeoMotAccessoriesUserArray();
			accessoriesList.getWeoMotAccessoriesUser().add(objectFactory.createWeoMotAccessoriesUser());
			orgJsonNode = mapper.valueToTree(accessoriesList);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			accessoriesList = mapper.treeToValue(patchedJsonNode, WeoMotAccessoriesUserArray.class);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotGenParamUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("WeoMotGenParamUserArray: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoMotGenParamUserArray paddoncoverList = objectFactory.createWeoMotGenParamUserArray();
			paddoncoverList.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverList.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverList.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverList.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			paddoncoverList.getWeoMotGenParamUser().add(objectFactory.createWeoMotGenParamUser());
			orgJsonNode = mapper.valueToTree(paddoncoverList);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			paddoncoverList = mapper.treeToValue(patchedJsonNode, WeoMotGenParamUserArray.class);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoSigMotExtraCoversUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoSigMotExtraCoversUser: " +
			// mapper.writeValueAsString(jsonPatch));

			Holder<WeoSigMotExtraCoversUser> motExtraCoverInout = new Holder<WeoSigMotExtraCoversUser>();
			WeoSigMotExtraCoversUser motextracoverinout = objectFactory.createWeoSigMotExtraCoversUser();
			orgJsonNode = mapper.valueToTree(motextracoverinout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoSigMotExtraCoversUser patchedmotextracoverinout = mapper.treeToValue(patchedJsonNode,
					WeoSigMotExtraCoversUser.class);
			motExtraCoverInout.value = patchedmotextracoverinout;

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoMotPremiumDetailsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoMotPremiumDetailsUser: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoMotPremiumDetailsUser premiumDetails = objectFactory.createWeoMotPremiumDetailsUser();
			orgJsonNode = mapper.valueToTree(premiumDetails);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			premiumDetails = mapper.treeToValue(patchedJsonNode, WeoMotPremiumDetailsUser.class);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoMotPremiumSummaryUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoMotPremiumSummaryUserArray: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoMotPremiumSummaryUserArray premiumSummeryList = objectFactory.createWeoMotPremiumSummaryUserArray();
			premiumSummeryList.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumSummeryList.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumSummeryList.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumSummeryList.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumSummeryList.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			premiumSummeryList.getWeoMotPremiumSummaryUser().add(objectFactory.createWeoMotPremiumSummaryUser());
			orgJsonNode = mapper.valueToTree(premiumSummeryList);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			premiumSummeryList = mapper.treeToValue(patchedJsonNode, WeoMotPremiumSummaryUserArray.class);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoBjazMotQuestionaryUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoBjazMotQuestionaryUserArray: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoBjazMotQuestionaryUserArray pQuestList = objectFactory.createWeoBjazMotQuestionaryUserArray();
			pQuestList.getWeoBjazMotQuestionaryUser().add(objectFactory.createWeoBjazMotQuestionaryUser());
			orgJsonNode = mapper.valueToTree(pQuestList);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			pQuestList = mapper.treeToValue(patchedJsonNode, WeoBjazMotQuestionaryUserArray.class);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoTygeErrorMessageUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoTygeErrorMessageUserArray: " +
			// mapper.writeValueAsString(jsonPatch));

			Holder<WeoTygeErrorMessageUserArray> pErrorOut = new Holder<WeoTygeErrorMessageUserArray>();
			WeoTygeErrorMessageUserArray perrorout = objectFactory.createWeoTygeErrorMessageUserArray();
			perrorout.getWeoTygeErrorMessageUser().add(objectFactory.createWeoTygeErrorMessageUser());
			orgJsonNode = mapper.valueToTree(perrorout);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			WeoTygeErrorMessageUserArray patchedperrorout = mapper.treeToValue(patchedJsonNode,
					WeoTygeErrorMessageUserArray.class);
			pErrorOut.value = patchedperrorout;

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"weoSigOtherDetailsUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoSigOtherDetailsUser: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoSigOtherDetailsUser potherdetails = objectFactory.createWeoSigOtherDetailsUser();
			orgJsonNode = mapper.valueToTree(potherdetails);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			potherdetails = mapper.treeToValue(patchedJsonNode, WeoSigOtherDetailsUser.class);

			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "weoMotDetariffObjUser");
			patchJsonNode = mapper.valueToTree(patchRequestVOList);
			jsonPatch = JsonPatch.fromJson(patchJsonNode);
			// System.out.println("weoMotDetariffObjUser: " +
			// mapper.writeValueAsString(jsonPatch));

			WeoMotDetariffObjUser pMotDetariff = objectFactory.createWeoMotDetariffObjUser();
			orgJsonNode = mapper.valueToTree(pMotDetariff);
			patchedJsonNode = jsonPatch.apply(orgJsonNode);
			pMotDetariff = mapper.treeToValue(patchedJsonNode, WeoMotDetariffObjUser.class);

			pMotDetariff.setExtCol20(returnUrl);

			if (pWeoMotPolicyInInout.value.getPrvInsCompany().intValueExact() == 6) {
				pWeoMotPolicyInInout.value.setPolType("2");
			}

			bajajvalidator.validateCustomerDetails(patchedpcustdetailsinout, patchedpweomotpolicyininout, policyJson);
			if (JsonUtils.isValidField(policyJson, JsonUtils.ERROR)) {
				return policyJson;
			}

			WebServicePolicy_Service webServicePolicyServ = new WebServicePolicy_Service();
			WebServicePolicy webServicePolicyPort = webServicePolicyServ.getWebServicePolicyPort();

			BindingProvider bindingProvider = (BindingProvider) webServicePolicyPort;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrl);
			DefaultMessageHandler.printMessage(bindingProvider);

			webServicePolicyPort.issuePolicy(pUserId, pPassword, pTransactionId, pRcptListInout, pCustDetailsInout,
					pWeoMotPolicyInInout, accessoriesList, paddoncoverList, motExtraCoverInout, premiumDetails,
					premiumSummeryList, pQuestList, ppolicyrefOut, ppolicyissuedateOut, ppartIdOut, pErrorOut,
					pErrorCodeOut, ppremiumpayerid, paymentmode, potherdetails, pMotDetariff);

			if (pErrorCodeOut.value.intValueExact() == 0) {

				String formUrl = null;

				if (paymentmode.equalsIgnoreCase("CC")) {
					formUrl = paymentUrl + motExtraCoverInout.value.getExtraField3() + "&Username=" + pUserId
							+ "&sourceName=WS_MOTOR";
				} else {
					formUrl = returnUrl + "vg_payStatus=Y&p_policy_ref=" + ppolicyrefOut.value
							+ "&p_pay_status=Y&policyref=" + ppolicyrefOut.value + "&requestId="
							+ motExtraCoverInout.value.getExtraField3() + "&custName=" + pUserId;

				}

				policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
				policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
				policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
				policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
				policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
				policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
				policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
				policyJson.put(PolicyFields.PREMIUM.getCode(),
						vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue());
				policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest
						.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValue());
				policyJson.put("FORM_URL", formUrl);
				policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
				policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
			} else {
				logger.error(pErrorOut.value.getWeoTygeErrorMessageUser().get(0).getErrText());
				policyJson.put(JsonUtils.ERROR, FetchPlanMessage.UNABLE_TO_PROCESS.value());
				return policyJson;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR, FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
		}

		return policyJson;

	}

	public ObjectNode policyPdf(String policyNum) {

		ObjectNode policyJson = Json.newObject();
		String userId = configure.getString("arka.vendor.bajaj.username");
		String password = configure.getString("arka.vendor.bajaj.password");
		String endPointUrl = configure.getString("arka.vendor.bajaj.pdf.url");
		String fileUrl = configure.getString("arka.content.support.upload.file");
		String associateUrl = configure.getString("arka.content.support.associate.file");

		WSRequest wsRequest = wsClient.url(fileUrl);
		WSResponse wsResponse = null;
		JsonNode cmsJson;

		try {

			ObjectFactory_pdf objectFactory = new ObjectFactory_pdf();
			ClientInfo arg = objectFactory.createClientInfo();
			arg.setUserId(userId);
			arg.setPassword(password);
			arg.setPolicyNum(policyNum);
			arg.setPdfMode("WS_POLICY_PDF");

			WebServiceImplService webServiceImplServicec = new WebServiceImplService();
			WebServiceInterface webServiceInterfacePort = webServiceImplServicec.getWebServiceImplPort();

			BindingProvider bindingProvider = (BindingProvider) webServiceInterfacePort;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointUrl);
			DefaultMessageHandler.printMessage(bindingProvider);

			Object downloadPdf = webServiceInterfacePort.downloadFile(arg);

			if (downloadPdf instanceof ClientInfo) {
				ClientInfo cliInfo = (ClientInfo) downloadPdf;
				logger.error("Bajaj policy pdf error" + cliInfo.getErrorMsg());
			}

			if (downloadPdf instanceof byte[]) {
				try {

					byte[] byteArray = (byte[]) downloadPdf;
					InputStream inputStream = new ByteArrayInputStream(byteArray);
					wsResponse = wsRequest.setContentType("application/pdf").post(inputStream).toCompletableFuture()
							.join();
					cmsJson = mapper.readTree(wsResponse.getBody());

					if (JsonUtils.isValidField(cmsJson, "fileId")) {
						wsRequest = wsClient.url(associateUrl);
						String requestBody = "{\"items\" : [{\"ownerId\": \"" + policyNum
								+ "\",\"ownerType\": \"BUSINESS\",\"fileActions\": {\"" + cmsJson.get("fileId").asText()
								+ "\" : {\"associate\": \"true\",\"urlType\": \"PERMANENT\" } } }] }";
						JsonNode request = mapper.readTree(requestBody);
						wsResponse = wsRequest
								.setHeader("X-DOE-SECURITY-CONTEXT",
										"{\"internalPrivileges\": [ { \"name\": \"asc_file\" } ]}")
								.setContentType("application/json").post(request).toCompletableFuture().join();
						if (wsResponse.getStatus() == 200) {
							policyJson.put(PolicyFields.CMS_ID.getCode(), cmsJson.get("fileId").asText());
						}
					}

				} catch (JsonProcessingException e) {
					logger.error(e.getMessage());
				} catch (IOException e) {
					logger.error(e.getMessage());
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return policyJson;
	}
}
