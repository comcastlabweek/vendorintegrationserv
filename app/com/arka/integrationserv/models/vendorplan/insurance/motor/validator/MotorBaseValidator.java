package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import org.elasticsearch.common.Strings;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class MotorBaseValidator {
	
	protected boolean isValidAttribute(String value){
		if(Strings.isNullOrEmpty(value)){
			return true;
		}
		return false;
		
	}
	
	public ObjectNode getValidationError(String errorKey, String errorValue) {
		ObjectNode errorJson = Json.newObject();
		errorJson.put(errorKey, errorValue);
		ObjectNode validationErrorJson = Json.newObject();
		validationErrorJson.set(ResponsePayLoad.ERROR, errorJson);
		return validationErrorJson;
	}
	
	public boolean validateAllowedIDV(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		double maxIDV = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode()).getValueAsDouble();
		double minIDV = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode()).getValueAsDouble();
		double modifiedIDV = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
		boolean flag =false;
		if((modifiedIDV>0)&&(modifiedIDV>=minIDV && modifiedIDV<=maxIDV)){
			flag = true;
		}
		return flag;
		
	}
	
	public boolean validateMaxAllowedIDV(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		double maxIDV = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode()).getValueAsDouble();
		double modifiedIDV = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
		boolean flag =false;
		if((modifiedIDV>0 && modifiedIDV>=maxIDV)){
			flag = true;
		}
		return flag;
		
	}
	public boolean validateMinAllowedIDV(VendorRequest vendorRequest,VendorResponse vendorResponse) {
		double minIDV = vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode()).getValueAsDouble();
		double modifiedIDV = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
		boolean flag =false;
		if((modifiedIDV>0 && modifiedIDV<=minIDV )){
			flag = true;
		}
		return flag;
		
	}
}
