package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.insurance.motor.validator.RoyalSundaramMotorValidator;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.inject.Inject;

import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import royalsundaram.motor.calculatepremiumrequest.CALCULATEPREMIUMREQUEST;
import royalsundaram.motor.calculatepremiumrequest.ObjectFactory;
import royalsundaram.motor.gproposalrequest.GPROPOSALREQUEST;
import royalsundaram.motor.gproposalrequest.GProposalObjectFactory;

/**
 * @author Jakpren
 *
 */
public class RoyalSundaramMotorInsuranceConnector extends MotorVendorConnector {

	@Inject
	DBUtil dbUtil;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;

	@Inject
	RoyalSundaramMotorValidator royalSundaramMotorValidator;

	@Inject
	private PropUtils propUtils;

	@Inject
	WSClient ws;

	private static final Logger logger = LoggerFactory.getLogger(RoyalSundaramMotorInsuranceConnector.class);
    private static int NUMBER_THIRTEEN = 30;
    private static int NUMBER_HUNDRED = 100;
    private static int NUMBER_FIFTEEN = 15;
    private static int NUMBER_ZERO = 0;
    private static int NUMBER_FIVE = 5;
    private static int NUMBER_TEN = 10;
    private static String SUCCESS_CODE = "S-0002";
    private static String SUCCESS_CODE_5 = "S-0005";

//    private String RS_MOTOR_SERVICE_ENDPOINT = propUtils.get("arka.vendor.rs.motor.service.request");
//	private String RS_MOTOR_PAYMENT_URL = propUtils.get("arka.vendor.rs.motor.payment.url");

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();

		// breakin policy
		if (royalSundaramMotorValidator.validateForBreakInPolicy(vendorRequest, vendorResponse)) {
			return quotesNotSupportedError(vendorResponse);
		}

		// modified idv
		if (royalSundaramMotorValidator.validateModifyIDV(vendorRequest, vendorResponse)) {
			long modifiedIdv = vendorRequest.getUniqueInputQuoteForAttribute("AC-MODIFIED_IDV").getValueAsLong();
			vendorResponse = getQuotes(vendorRequest, vendorResponse);
			long actualIdv = vendorResponse.getAttributeValueList().stream()
					.filter(outputParams -> outputParams.getCategoryAttributeCode().equals("AC-PRODUCT_IDV")).findAny()
					.get().getValueAsLong();
			double diffAmount = 0.0;
			long amtPercentage = NUMBER_ZERO;
            vendorRequest = setIDVCategoryCodeValue(vendorRequest, actualIdv,
                    CarAttributeCode.AC_IDV.getCode());

            // increase
			if (modifiedIdv > actualIdv) {
				diffAmount = modifiedIdv - actualIdv;
				amtPercentage = Math.round((diffAmount / actualIdv * NUMBER_HUNDRED));

				if (amtPercentage <= NUMBER_THIRTEEN) {
					vendorRequest = setIDVCategoryCodeValue(vendorRequest,
							(actualIdv + actualIdv * amtPercentage / NUMBER_HUNDRED),
							CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode());
					vendorRequest = setCategoryCodeValue(vendorRequest, (byte) (amtPercentage),
							CarAttributeCode.AC_MODIFIED_IDV_PERCENTAGE.getCode());
				} else {
				    //maximum percentage
                    vendorRequest = setIDVCategoryCodeValue(vendorRequest,
                            (actualIdv + actualIdv * NUMBER_THIRTEEN / NUMBER_HUNDRED),
                            CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode());
                    vendorRequest = setCategoryCodeValue(vendorRequest, (byte) (NUMBER_THIRTEEN),
                            CarAttributeCode.AC_MODIFIED_IDV_PERCENTAGE.getCode());
					return getQuotes(vendorRequest, vendorResponse);
				}
			} else if (modifiedIdv < actualIdv) {
				// decrease
				diffAmount = actualIdv - modifiedIdv;
				amtPercentage = Math.round((diffAmount / actualIdv * NUMBER_HUNDRED));

				if (amtPercentage <= NUMBER_FIFTEEN) {
					vendorRequest = setIDVCategoryCodeValue(vendorRequest,
							(actualIdv - actualIdv * amtPercentage / NUMBER_HUNDRED),
							CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode());
					vendorRequest = setCategoryCodeValue(vendorRequest, (byte) (-amtPercentage),
							CarAttributeCode.AC_MODIFIED_IDV_PERCENTAGE.getCode());
				} else {
				    //lowest percentage
                    vendorRequest = setIDVCategoryCodeValue(vendorRequest,
                            (actualIdv - actualIdv * NUMBER_FIFTEEN / NUMBER_HUNDRED),
                            CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode());
                    vendorRequest = setCategoryCodeValue(vendorRequest, (byte) (-NUMBER_FIFTEEN),
                            CarAttributeCode.AC_MODIFIED_IDV_PERCENTAGE.getCode());
					return getQuotes(vendorRequest, vendorResponse);
				}
			}
			return getQuotes(vendorRequest, vendorResponse);
		}
		return getQuotes(vendorRequest, vendorResponse);
	}

	private VendorRequest setCategoryCodeValue(VendorRequest vendorRequest, byte value, String catCode) {
		vendorRequest = modifyCategoryAttributeToRequest(vendorRequest, catCode, String.valueOf(value));
		return vendorRequest;
	}

	protected VendorRequest modifyCategoryAttributeToRequest(VendorRequest vendorRequest, String categoryCode,
			String categoryAttributeValue) {

		vendorRequest.getInput_quote_params().stream()
				.filter(param -> param.getCategoryAttributeCode().equals(categoryCode)).findAny().get()
				.setValue(categoryAttributeValue);
		return vendorRequest;

	}

	private VendorRequest setIDVCategoryCodeValue(VendorRequest vendorRequest, long value, String catCode) {
		if (vendorRequest.isAttributePresent("AC-IDV")) {
			vendorRequest = modifyCategoryAttributeToRequest(vendorRequest, catCode, String.valueOf(value));
		} else {
			vendorRequest = addCategoryAttributeToRequest(vendorRequest, catCode, String.valueOf(value));
		}
		return vendorRequest;
	}

	private VendorRequest setProductIDVCategoryCodeValue(VendorRequest vendorRequest, long value, String catCode) {
		if (vendorRequest.isAttributePresent("AC-PRODUCT_IDV")) {
			vendorRequest = modifyCategoryAttributeToRequest(vendorRequest, catCode, String.valueOf(value));
		} else {
			vendorRequest = addCategoryAttributeToRequest(vendorRequest, catCode, String.valueOf(value));
		}
		return vendorRequest;
	}

	private VendorResponse quotesNotSupportedError(VendorResponse vendorResponse) {
		vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
				FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
				FetchPlanMessage.BREAK_IN_POICY_NOT_ALLOWED.value()));
		return vendorResponse;
	}

	private Boolean checkforReValidate(VendorRequest request) {
		if (   request.getUniqueInputQuoteForAttribute("AI-RE_VALIDATE") != null && request.getUniqueInputQuoteForAttribute("AI-RE_VALIDATE").getValue() != null
				&& request.getUniqueInputQuoteForAttribute("AI-RE_VALIDATE").getValueAsBoolean()) {
			return true;
		}else {
			return false;
		}
	}

	private VendorResponse getQuotes(VendorRequest vendorRequest, VendorResponse vendorResponse) {

		System.out.println("VENDOR REQUEST ON QUOTE CALL FOR ROYAL SUNDARAM-------------------------" + JsonUtils.toJson(vendorRequest));
		try {
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			ObjectFactory objectFactory = new ObjectFactory();
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest,
					"CALCULATEPREMIUMREQUEST");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);

			CALCULATEPREMIUMREQUEST calculatePremiumReq = calculatePremium(jsonPatch, objectFactory);
			JAXBContext context = JAXBContext.newInstance(CALCULATEPREMIUMREQUEST.class);
			Marshaller jaxbMarshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(calculatePremiumReq, sw);
			String xmlString = sw.toString();
			System.out.println("\n ROYAL SUNDARAM REQUEST FOR CalculatePremium ================> \n" + xmlString);
			String motorType = setMotorType(vendorRequest);

			WSResponse wsResponse = null;

			try {
				if (checkforReValidate(vendorRequest)) {
					patchJsonNode = mapper.valueToTree(patchRequestVOList);
					jsonPatch = JsonPatch.fromJson(patchJsonNode);
					setCompanyDetails(vendorRequest, calculatePremiumReq);
					JsonNode orgJsonNode = mapper.valueToTree(calculatePremiumReq);
					JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);

					// empty the stringwritter
					sw.getBuffer().setLength(NUMBER_ZERO);
					jaxbMarshaller.marshal(calculatePremiumReq, sw);
					String updateXmlString = sw.toString();

					if ("TwoWheeler".equals(motorType)) {
						updateXmlString = updateXmlString.replaceAll("strEmail", "emailId");
					}

					System.out.println("\n ROYAL SUNDARAM REQUEST FOR UpdateVehicleDetails ================> \n" + updateXmlString);

					// Update calculatePremiumRequest
					WSRequest request = ws.url(propUtils.get("arka.vendor.rs.motor.service.request") + motorType
							+ "/UpdateVehicleDetails");
					wsResponse = request.setHeader("Content-Type", "application/xml").post(updateXmlString)
							.toCompletableFuture().get();
					JSONObject outputJson = XML.toJSONObject(wsResponse.getBody());
					ObjectNode outputObjNode = mapper.readValue(outputJson.toString(), ObjectNode.class);
					System.out.println("this is the object response after update calll----------"+outputObjNode);
					if ((SUCCESS_CODE)
							.equalsIgnoreCase(outputObjNode.get("PREMIUMDETAILS").get("Status").get("StatusCode").asText())
							|| (SUCCESS_CODE_5).equalsIgnoreCase(
									outputObjNode.get("PREMIUMDETAILS").get("Status").get("StatusCode").asText())) {

						System.out.println("\n ROYAL SUNDARAM RESPONSE FOR UpdateVehicleDetails ================> \n"
								+ mapper.writeValueAsString(outputObjNode));
						vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
								Json.toJson(outputObjNode.get("PREMIUMDETAILS")), vendorResponse, "PREMIUMDETAILS");
						vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
						vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
						if(vendorResponse.isAttributeValuePresentinResponse("AI-SGST") && vendorResponse.isAttributeValuePresentinResponse("AI-CGST")) {
							vendorResponse.setAttributeValueForCode("AC-SERVICE_TAX", ""+(vendorResponse.getAttributeValueForCode("AI-SGST").getValueAsLong()+vendorResponse.getAttributeValueForCode("AI-CGST").getValueAsLong()));
						}
						return vendorResponse;
					}
				} else {
					WSRequest request = ws.url(
							propUtils.get("arka.vendor.rs.motor.service.request") + motorType + "/CalculatePremium");
					wsResponse = request.setHeader("Content-Type", "application/xml").post(xmlString)
							.toCompletableFuture().get();
					JSONObject outputQuoteParamsObj = XML.toJSONObject(wsResponse.getBody());
					ObjectNode outputQuoteParam = mapper.readValue(outputQuoteParamsObj.toString(), ObjectNode.class);
					System.out.println("\n ROYAL SUNDARAM RESPONSE FOR CalculatePremium ================> \n" + outputQuoteParam);
					if ((SUCCESS_CODE).equalsIgnoreCase(
							outputQuoteParam.get("PREMIUMDETAILS").get("Status").get("StatusCode").asText())) {
						vendorResponse = getVendorResponse(vendorRequest, vendorResponse, patchRequestVOList,
								outputQuoteParam);

						if (("Yes").equalsIgnoreCase(
								calculatePremiumReq.getVehicleDetails().getClaimsMadeInPreviousPolicy())
								|| ("new").equals(getVehicleState(vendorRequest))) {
							if (vendorResponse
									.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())) {
								AttributeValue attributeValue = vendorResponse
										.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
								attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
								vendorResponse
										.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
								attributeValue
										.setValue(attributeValue.getValue().replace("AC-COVER-NCB_PROTECTION", ""));
								vendorResponse.addAttributeValue(attributeValue);
							}
						} else {
							if (vendorResponse
									.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())) {
								AttributeValue attributeValue = vendorResponse
										.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
								attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
								vendorResponse
										.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
								vendorResponse.addAttributeValue(attributeValue);
							}
						}
					} else {
						logger.error(outputQuoteParam.get("PREMIUMDETAILS").get("Status").get("Message").toString());
						vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
								FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
								FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
						return vendorResponse;
					}
				}
				System.out.println("\n ROYAL SUNDARAM RESPONSE ============> \n" + vendorResponse);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),
						FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(), null));
				return vendorResponse;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),
					FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(), null));
			return vendorResponse;
		}
		if(vendorResponse.isAttributeValuePresentinResponse("AI-SGST") && vendorResponse.isAttributeValuePresentinResponse("AI-CGST")) {
			vendorResponse.setAttributeValueForCode("AC-SERVICE_TAX", "" + (vendorResponse.getAttributeValueForCode("AI-SGST").getValueAsLong() + vendorResponse.getAttributeValueForCode("AI-CGST").getValueAsLong()));
		}
		return vendorResponse;
	}

	private VendorResponse getVendorResponse(VendorRequest vendorRequest, VendorResponse vendorResponse,
			List<PatchRequestVO> patchRequestVOList, ObjectNode outputQuoteParam) {
		patchRequestVOList.clear();
		vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
				Json.toJson(outputQuoteParam.get("PREMIUMDETAILS")), vendorResponse, "PREMIUMDETAILS");
		vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
		vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
		return vendorResponse;
	}

	private CALCULATEPREMIUMREQUEST calculatePremium(JsonPatch jsonPatch, ObjectFactory objectFactory)
			throws JsonPatchException, JsonProcessingException {
		CALCULATEPREMIUMREQUEST calcPremiumRequest = objectFactory.createCALCULATEPREMIUMREQUEST();
		calcPremiumRequest.setAuthenticationDetails(objectFactory.createCALCULATEPREMIUMREQUESTAuthenticationDetails());
		calcPremiumRequest.setProposerDetails(objectFactory.createCALCULATEPREMIUMREQUESTProposerDetails());
		calcPremiumRequest.setVehicleDetails(objectFactory.createCALCULATEPREMIUMREQUESTVehicleDetails());
		calcPremiumRequest.getVehicleDetails().setElectricalAccessories(
				objectFactory.createCALCULATEPREMIUMREQUESTVehicleDetailsElectricalAccessories());
		calcPremiumRequest.getVehicleDetails().setNonElectricalAccesories(
				objectFactory.createCALCULATEPREMIUMREQUESTVehicleDetailsNonElectricalAccesories());
		for (int i = NUMBER_ZERO; i < NUMBER_FIVE; i++) {
			calcPremiumRequest.getVehicleDetails().getElectricalAccessories().getElectronicAccessoriesDetails()
					.add(objectFactory
							.createCALCULATEPREMIUMREQUESTVehicleDetailsElectricalAccessoriesElectronicAccessoriesDetails());
		}
		for (int i = NUMBER_ZERO; i < NUMBER_FIVE; i++) {
			calcPremiumRequest.getVehicleDetails().getNonElectricalAccesories().getNonelectronicAccessoriesDetails()
					.add(objectFactory
							.createCALCULATEPREMIUMREQUESTVehicleDetailsNonElectricalAccesoriesNonelectronicAccessoriesDetails());
		}
		JsonNode orgJsonNode = mapper.valueToTree(calcPremiumRequest);
		JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
		CALCULATEPREMIUMREQUEST calculatePremiumReq = mapper.treeToValue(patchedJsonNode,
				CALCULATEPREMIUMREQUEST.class);
		return calculatePremiumReq;
	}

	private String setMotorType(VendorRequest vendorRequest) {
		List<InputPolicyParams> input_quote_params = vendorRequest.getInput_quote_params();

		for (InputPolicyParams input : input_quote_params) {
			if (input.getCategoryAttributeCode().equals(CarAttributeCode.AC_VEHSTATE.getCode())) {
				if (("rolloverbike").equals(input.getValue()) || ("newbike").equals(input.getValue())) {
					System.out.println("Motor Type = TwoWheeler");
					return "TwoWheeler";
				} else if (("rollovercar").equals(input.getValue()) || ("newcar").equals(input.getValue())) {
					System.out.println("Motor Type = PrivateCar");
					return "PrivateCar";
				}
			}
		}
		return null;
	}

	private String getVehicleState(VendorRequest vendorRequest) {
		List<InputPolicyParams> input_quote_params = vendorRequest.getInput_quote_params();

		for (InputPolicyParams input : input_quote_params) {
			if (input.getCategoryAttributeCode().equals(CarAttributeCode.AC_VEHSTATE.getCode())) {
				if (("newbike").equals(input.getValue())) {
					System.out.println("VehicleState = new");
					return "rollover";
				} else if (("newcar").equals(input.getValue())) {
					System.out.println("VehicleState = new");
					return "new";
				}
			}
		}
		return null;
	}

	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("ROYAL_SUNDARAM_MOTOR_" + AttributeUtil.generateRandom());
		return vendorResponse;
	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();
		ObjectFactory objectFactory = new ObjectFactory();
		WSResponse wsResponse;
		JSONObject outputJson;
		ObjectNode outputObjNode = Json.newObject();
		WSRequest request;
		ObjectNode policyJson = Json.newObject();
		List<PatchRequestVO> patchVendorRequestList = categoryTransformationUtils.createRequestPatch(vendorRequest,
				"CALCULATEPREMIUMREQUEST");
		JsonNode patchJsonNode = mapper.valueToTree(patchVendorRequestList);

		try {
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			CALCULATEPREMIUMREQUEST calculatePremiumReq = calculatePremium(jsonPatch, objectFactory);
			setCompanyDetails(vendorRequest, calculatePremiumReq);
			JAXBContext context = JAXBContext.newInstance(CALCULATEPREMIUMREQUEST.class);
			Marshaller jaxbMarshaller = context.createMarshaller();
			StringWriter calculateStringWritter = new StringWriter();
			jaxbMarshaller.marshal(calculatePremiumReq, calculateStringWritter);
			String motorType = setMotorType(vendorRequest);
			JsonNode orgJsonNode = mapper.valueToTree(calculatePremiumReq);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
			String updateXmlString = calculateStringWritter.toString();

			if ("TwoWheeler".equals(motorType)) {
				updateXmlString = updateXmlString.replaceAll("strEmail", "emailId");
			}

			System.out.println("\n ROYAL SUNDARAM REQUEST ================> \n" + updateXmlString);

			// Update calculatePremiumRequest
			request = ws.url(
					propUtils.get("arka.vendor.rs.motor.service.request") + motorType + "/UpdateVehicleDetails");
			wsResponse = request.setHeader("Content-Type", "application/xml").post(updateXmlString)
					.toCompletableFuture().get();
			outputJson = XML.toJSONObject(wsResponse.getBody());
			outputObjNode = mapper.readValue(outputJson.toString(), ObjectNode.class);

			if ((SUCCESS_CODE)
					.equalsIgnoreCase(outputObjNode.get("PREMIUMDETAILS").get("Status").get("StatusCode").asText())
					|| (SUCCESS_CODE_5).equalsIgnoreCase(
							outputObjNode.get("PREMIUMDETAILS").get("Status").get("StatusCode").asText())) {

				System.out.println("\n ROYAL SUNDARAM RESPONSE ================> \n"
						+ mapper.writeValueAsString(outputObjNode));
				vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
						Json.toJson(outputObjNode.get("PREMIUMDETAILS")), vendorResponse, "PREMIUMDETAILS");
				vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
				vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
				InputPolicyParams param = new InputPolicyParams();
				param.setCategoryAttributeCode("AC-QUOTE_ID");
				param.setValue(outputObjNode.get("PREMIUMDETAILS").get("DATA").get("QUOTE_ID").asText());
				vendorRequest.getInput_quote_params().add(param);

				patchVendorRequestList = categoryTransformationUtils.createRequestPatch(vendorRequest,
						"GPROPOSALREQUEST");
				patchJsonNode = mapper.valueToTree(patchVendorRequestList);
				jsonPatch = null;
				jsonPatch = JsonPatch.fromJson(patchJsonNode);
				System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));

				policyJson.put("agentId", patchedJsonNode.get("authenticationDetails").get("agentId").asText());
				policyJson.put("apikey", patchedJsonNode.get("authenticationDetails").get("apikey").asText());
				policyJson.put("strFirstName", vendorRequest.getPolicyHolderName());
				policyJson.put("paymentUrl", propUtils.get("arka.vendor.rs.motor.payment.url"));
				policyJson.put("EMAIL", patchedJsonNode.get("proposerDetails").get("strEmail").asText());
				policyJson.put("elc_value", outputObjNode.get("PREMIUMDETAILS").get("DATA").get("OD_PREMIUM")
						.get("ELECTRICAL_ACCESSORIES").asText());
				policyJson.put("VERSION_NO",
						outputObjNode.get("PREMIUMDETAILS").get("DATA").get("VERSION_NO").asText());


				// Gproposal Request
				GProposalObjectFactory gPropObjectFactory = new GProposalObjectFactory();
				GPROPOSALREQUEST gProposalReq = gPropObjectFactory.createGPROPOSALREQUEST();
				gProposalReq
						.setAuthenticationDetails(gPropObjectFactory.createGPROPOSALREQUESTAuthenticationDetails());
				orgJsonNode = mapper.valueToTree(gProposalReq);
				patchedJsonNode = null;
				patchedJsonNode = jsonPatch.apply(orgJsonNode);
				GPROPOSALREQUEST createPolicyRequest = mapper.treeToValue(patchedJsonNode, GPROPOSALREQUEST.class);
				createPolicyRequest
						.setPremium(outputObjNode.get("PREMIUMDETAILS").get("DATA").get("PREMIUM").intValue());
				context = JAXBContext.newInstance(GPROPOSALREQUEST.class);
				jaxbMarshaller = context.createMarshaller();

				// empty the stringwritter
				calculateStringWritter.getBuffer().setLength(NUMBER_ZERO);
				jaxbMarshaller.marshal(createPolicyRequest, calculateStringWritter);
				String gProposalXmlString = calculateStringWritter.toString();
				gProposalXmlString = ("TwoWheeler").equals(motorType)
						? gProposalXmlString.replaceAll("strEmail", "emailId")
						: gProposalXmlString;
				System.out.println("\n ROYAL SUNDARAM REQUEST FOR GPROPOSALSERVICE \n" + gProposalXmlString);

				// Output of GProposal
				request = ws.url(
						propUtils.get("arka.vendor.rs.motor.service.request") + motorType + "/GProposalService");
				wsResponse = request.setHeader("Content-Type", "application/xml").post(gProposalXmlString)
						.toCompletableFuture().get();
				outputJson = XML.toJSONObject(wsResponse.getBody());
				outputObjNode = mapper.readValue(outputJson.toString(), ObjectNode.class);

				if (outputObjNode.get("PREMIUMDETAILS").get("Status") != null) {
					patchVendorRequestList.clear();
					patchVendorRequestList = categoryTransformationUtils.createRequestPatch(vendorRequest,
							"GPROPOSALREQUEST");
					patchJsonNode = mapper.valueToTree(patchVendorRequestList);
					jsonPatch = JsonPatch.fromJson(patchJsonNode);
					orgJsonNode = mapper.valueToTree(outputObjNode);
					System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
					vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
							Json.toJson(outputObjNode.get("PREMIUMDETAILS")), vendorResponse, "PREMIUMDETAILS");
					vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
					vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));

					// Set PolicyJson Object to be saved as policy
					policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
					policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
					policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
					policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
					policyJson.put(PolicyFields.POLICY_NUMBER.getCode(), RandomStringUtils.randomAlphanumeric(NUMBER_TEN));
					policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
					policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
					policyJson.put(PolicyFields.PAYMENT_ID.getCode(), RandomStringUtils.randomAlphanumeric(NUMBER_TEN));
					policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
					policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
					policyJson.put(PolicyFields.PREMIUM.getCode(),
							vendorResponse.getAttributeValueForCode("AC-PRODUCT_PREMIUM").getValue());
					policyJson.put(PolicyFields.QUOTE_ID.getCode(),
							vendorRequest.getInputQuoteForAttribute("AC-QUOTE_ID").get(NUMBER_ZERO).getValue().toString());
					policyJson.put(PolicyFields.SUM_ASSURED.getCode(),
							vendorResponse.getAttributeValueForCode("AC-PRODUCT_IDV").getValue());
					policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
					if (vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.CAR.name())) {
						policyJson.put("vehicleType", "privatePassengerCar");
					} else if (vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name())) {
						policyJson.put("vehicleType", "motorCycle");
					}
				} else {
					logger.error(outputObjNode.get("PREMIUMDETAILS").get("Status").get("Message").toString());
					policyJson.put(JsonUtils.ERROR, FetchPlanMessage.UNABLE_TO_PROCESS.value());
					return policyJson;
				}
			} else {
				System.out.println("\n <----ROYAL SUNDARAM ERROR------> \n"
						+ outputObjNode.get("PREMIUMDETAILS").get("Status").get("Message").toString());
				logger.error(outputObjNode.get("PREMIUMDETAILS").get("Status").get("Message").toString());
				policyJson.put(JsonUtils.ERROR, FetchPlanMessage.UNABLE_TO_PROCESS.value());
				return policyJson;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(
					"\n <----ROYAL SUNDARAM ERROR------> \n" + outputObjNode.get("PREMIUMDETAILS").get("Status").get("Message").toString());
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR, FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}
		return policyJson;
	}

	private static void setCompanyDetails(VendorRequest vendorRequest, CALCULATEPREMIUMREQUEST calculatePremiumReq) {
		if (vendorRequest.isAttributePresent("AI-COMPANY_NAME") && vendorRequest.isAttributePresent("AI-SHORT_NAME")) {
			calculatePremiumReq.getProposerDetails().setFirstName(vendorRequest
					.getInputQuoteForAttribute("AI-SHORT_NAME").stream()
					.filter(input_quote_params -> input_quote_params.getCategoryAttributeCode().equals("AI-SHORT_NAME"))
					.findAny().get().getValue());
			calculatePremiumReq.getProposerDetails().setStrFirstName(vendorRequest
					.getInputQuoteForAttribute("AI-SHORT_NAME").stream()
					.filter(input_quote_params -> input_quote_params.getCategoryAttributeCode().equals("AI-SHORT_NAME"))
					.findAny().get().getValue());
		}
	}
}