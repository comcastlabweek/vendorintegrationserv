package com.arka.integrationserv.models.vendorplan.insurance.motor.validator;

import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import usgic.motor.Root.Authentication;
import usgic.motor.Root.Customer;
import usgic.motor.Root.Product.Risks;

public class USGICMotorValidator extends MotorBaseValidator {

	//private static final Logger logger = LoggerFactory.getLogger(USGICMotorValidator.class);
	
	
	public JsonNode validateAuthenticationMandatoryFields(Authentication authentication,ObjectNode policyJson) {
		if(isValidAttribute(String.valueOf(authentication.getWAAppCode()))||
			isValidAttribute(String.valueOf(authentication.getWACode()))||isValidAttribute(String.valueOf(authentication.getWAType()))||
			isValidAttribute(String.valueOf(authentication.getWAUserID()))|| isValidAttribute(String.valueOf(authentication.getWAUserPwd()))) {
				policyJson.put(JsonUtils.ERROR,FetchPlanMessage.DEFAULT_VALUE_MISSING.value());
				return policyJson;
		}
		return policyJson;
		
	}
	
	public JsonNode validateVehicleMandatoryFields(Risks risks,ObjectNode policyJson) {
		if(isValidAttribute(String.valueOf(risks.getYearOfManufacture().get(1)))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_MANUFACTUREYEAR.value());
			return policyJson;
		}
		
		if(isValidAttribute(String.valueOf(risks.getRegistrationNumber().get(1)))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_REGNO.value());
			return policyJson;
		}

		if(isValidAttribute(String.valueOf(risks.getEngineNumber().get(1)))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ENGINENO.value());
			return policyJson;
		}
		if(isValidAttribute(String.valueOf(risks.getChassisNumber().get(1)))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_CHASISNO.value());
			return policyJson;
		}
		return policyJson;
		
	}
	public JsonNode validateCustomerMandatoryFields(Customer customer,ObjectNode policyJson) {
		if(isValidAttribute(customer.getCustomerType()) || isValidAttribute(customer.getProductCode()) || 
				isValidAttribute(customer.getProductName())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.DEFAULT_VALUE_MISSING.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getDOB())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_DOB.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getCustomerName())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NAME.value());
			return policyJson;
		}
//		if(isValidAttribute(getPayRequest.getContact().getInsuredPAN())) {
//			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_PAN.value());
//			return policyJson;
//		}
//		if(isValidAttribute(getPayRequest.getContact().getInsuredAadhar())) {
//			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_AADHARNO.value());
//			return policyJson;
//		}
		if(isValidAttribute(customer.getGender())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_GENDER.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getPermanentAddressLine1())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_ADDRESS.value());
			return policyJson;
		}
		if(isValidAttribute(String.valueOf(customer.getPermanentPinCode()))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_PINCODE.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getPermanentCityDistCode())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_CITY.value());
			return policyJson;
		}
		if(isValidAttribute(String.valueOf(customer.getMobileNo()))) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_MOBILE.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getEmailid())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_EMAIL.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getPermanentStateCode())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_STATE.value());
			return policyJson;
		}
		if(isValidAttribute(customer.getNomineeName()) ||
				isValidAttribute(customer.getNomineeRelation())) {
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.INVALID_NOMINEE.value());
			return policyJson;
		}
		return policyJson;
		
		
	}
	
	
	}
