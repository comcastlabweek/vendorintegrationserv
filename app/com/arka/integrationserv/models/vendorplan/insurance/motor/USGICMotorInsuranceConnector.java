 package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.insurance.constants.InsuranceAttributeCode;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PaymentFrequency;
import com.arka.common.policy.constants.PaymentMode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.insurance.motor.validator.USGICMotorValidator;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.arka.integrationserv.models.vendorplan.utils.DefaultMessageHandler;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.inject.Inject;

import play.libs.Json;
import usgic.motor.ObjectFactory;
import usgic.motor.Root;
import usgic.motor.Root.Authentication;
import usgic.motor.Root.Customer;
import usgic.motor.Root.Errors;
import usgic.motor.Root.PaymentDetails;
import usgic.motor.Root.Product;
import usgic.motor.Root.Product.GeneralProposal;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp.FinancierDtlGrpData;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp.FinancierDtlGrpData.AgreementType;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp.FinancierDtlGrpData.BranchName;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp.FinancierDtlGrpData.FinancierCode;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp.FinancierDtlGrpData.FinancierName;
import usgic.motor.Root.Product.GeneralProposal.FinancierDetails.FinancierDtlGrp.FinancierDtlGrpData.SrNo;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.DistributionChannel;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.DistributionChannel.BranchDetails;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.DistributionChannel.SPDetails;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.BusinessType;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.DealId;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.PolicyEffectiveDate;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.PolicyNumberChar;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.ProposalDate;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.Sector;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.ServiceTaxExemptionCategory;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.TypeOfBusiness;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.VehicleLaidUpFrom;
import usgic.motor.Root.Product.GeneralProposal.GeneralProposalGroup.GeneralProposalInformation.VehicleLaidUpTo;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.OfficeAddress;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.ClaimAmount;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.ClaimNo;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.ClaimPremium;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.ClaimSettled;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.CorporateCustomerId;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.DateOfInspection;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.DateofLoss;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.InsurerAddress;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.InsurerName;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.NatureofLoss;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.OfficeCode;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.PolicyEffectiveFrom;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.PolicyEffectiveTo;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.PolicyNo;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.PolicyPremium;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.PolicyStatus;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.PolicyYear;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolDtlGroup.PreviousPolDtlGroupData.ProductCode;
import usgic.motor.Root.Product.GeneralProposal.PreviousPolicyDetails.PreviousPolicyType;
import usgic.motor.Root.Product.PremiumCalculation;
import usgic.motor.Root.Product.PremiumCalculation.NetPremium;
import usgic.motor.Root.Product.PremiumCalculation.ServiceTax;
import usgic.motor.Root.Product.PremiumCalculation.StampDuty2;
import usgic.motor.Root.Product.PremiumCalculation.TotalPremium;
import usgic.motor.Root.Product.Risks;
import usgic.motor.Root.Product.Risks.Risk;
import usgic.motor.Root.Product.Risks.Risk.RisksData;
import usgic.motor.Root.Product.Risks.Risk.RisksData.AddonCoverDetails;
import usgic.motor.Root.Product.Risks.Risk.RisksData.AddonCoverDetails.AddonCovers;
import usgic.motor.Root.Product.Risks.Risk.RisksData.AddonCoverDetails.AddonCovers.AddonCoversData;
import usgic.motor.Root.Product.Risks.Risk.RisksData.CoverDetails;
import usgic.motor.Root.Product.Risks.Risk.RisksData.CoverDetails.Covers;
import usgic.motor.Root.Product.Risks.Risk.RisksData.CoverDetails.Covers.CoversData;
import usgic.motor.Root.Product.Risks.Risk.RisksData.DeTariffDiscounts;
import usgic.motor.Root.Product.Risks.Risk.RisksData.DeTariffDiscounts.DeTariffDiscountGroup;
import usgic.motor.Root.Product.Risks.Risk.RisksData.DeTariffDiscounts.DeTariffDiscountGroup.DeTariffDiscountGroupData;
import usgic.motor.Root.Product.Risks.Risk.RisksData.DeTariffLoadings;
import usgic.motor.Root.Product.Risks.Risk.RisksData.DeTariffLoadings.DeTariffLoadingGroup;
import usgic.motor.Root.Product.Risks.Risk.RisksData.DeTariffLoadings.DeTariffLoadingGroup.DeTariffLoadingGroupData;
import usgic.motor.Root.Product.Risks.Risk.RisksData.OtherDiscounts;
import usgic.motor.Root.Product.Risks.Risk.RisksData.OtherDiscounts.OtherDiscountGroup;
import usgic.motor.Root.Product.Risks.Risk.RisksData.OtherDiscounts.OtherDiscountGroup.OtherDiscountGroupData;
import usgic.motor.Root.Product.Risks.Risk.RisksData.OtherLoadings;
import usgic.motor.Root.Product.Risks.Risk.RisksData.OtherLoadings.OtherLoadingGroup;
import usgic.org.IService1;
import usgic.org.Service1;

/*****************************************************************************************************
 * 
 * After Converting  XSD to JAXB Classes be note to change xmltype "discountAmountOrDiscountRateOrSumInsured"
 * to "root" in Root Class 
 *
 *
 ******************************************************************************************************/
public class USGICMotorInsuranceConnector extends MotorVendorConnector {

	@Inject
	DBUtil dbUtil;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;
	
	@Inject
	ObjectFactory  motorObjectFactory;
	
	@Inject
	USGICMotorValidator usgicCMotorValidator;
	
	@Inject
	private PropUtils propUtils;

	 public static final Pattern replacePlaceholder = Pattern.compile("\\[\\{(.*?)\\}\\]");
	 private static final Logger logger = LoggerFactory.getLogger(USGICMotorInsuranceConnector.class);
	 private static final String XML_PROLOG = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	 private static final int MAX_COVER_DATA = 9;
	 private static final int MAX_ADDON_COVER_DATA = 7;
	 private static final double MAX_IDV_PERCENTAGE = 1.10; //10%IDV
	 private static final double MIN_IDV_PERCENTAGE = 0.95; //-5%IDV

	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();
		try {
			Root rootResp = null;
			Root.Errors error = null;
			if(vendorRequest.isAttributePresent(InsuranceAttributeCode.AI_PLAN_REQUEST_TYPE.getCode()) && 
					!vendorRequest.getUniqueInputQuoteForAttribute(InsuranceAttributeCode.AI_PLAN_REQUEST_TYPE.getCode()).getValue().equalsIgnoreCase("PURCHASE")) {
			    rootResp = callMotorRequest(vendorRequest);
			    error = (Errors) rootResp.getRoot().get(4);
				JsonNode responseJson = Json.toJson(rootResp);
				String vehicleIDV =  responseJson.at("/root/2/risks/0/vehicleIDV/0/value").asText();
				String exShoowroomPrice = responseJson.at("/root/2/risks/0/vehicleExShowroomPrice/0/value").asText();
				  
				if((error.getErrorCode().isEmpty() || error== null)&&!(vehicleIDV.isEmpty()|| vehicleIDV.equals("0"))) {
					if((!vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) ) || (vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) && 
							!vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV.getCode()).getValueAsBoolean())){
						vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_PRODUCT_IDV.getCode(),vehicleIDV);
						vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_PRODUCT_IDV.getCode(), vehicleIDV);
						vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(),vehicleIDV);
						vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(), vehicleIDV);
						
					}
					double vehicleIDVValue = Double.parseDouble(vehicleIDV);
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode(), df.format(vehicleIDVValue*MIN_IDV_PERCENTAGE));
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode(), df.format(vehicleIDVValue*MAX_IDV_PERCENTAGE));

				}else {
					vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
							FetchPlanMessage.NOT_SUPPORTED_MODEL.value()));
					return vendorResponse;
				}
				if((error.getErrorCode().isEmpty() || error== null)&&!(exShoowroomPrice.isEmpty()|| exShoowroomPrice.equals("0"))) {
				    addCategoryAttributeToRequest(vendorRequest,CarAttributeCode.AC_EXSHOWROOM_PRICE.getCode(),exShoowroomPrice);
				}	
				if(usgicCMotorValidator.validateMaxAllowedIDV(vendorRequest,vendorResponse)){
					modifyCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(),
							vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode()).getValue());
				}else if(usgicCMotorValidator.validateMinAllowedIDV(vendorRequest,vendorResponse)){
					modifyCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(), 
							vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode()).getValue());
				}
				if(vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) && 
						vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV.getCode()).getValueAsBoolean()){
					double modifiedIDV = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
					vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_PRODUCT_IDV.getCode(),df.format(modifiedIDV));
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_PRODUCT_IDV.getCode(), df.format(modifiedIDV));

				}
				rootResp = callMotorRequest(vendorRequest);
			    error = (Errors) rootResp.getRoot().get(4);
				if(error.getErrorCode().isEmpty() || error==null) {
				    vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest, Json.toJson(rootResp),vendorResponse, "commBRIDGEFusionMOTORRes");			
				    if(vendorResponse.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())){						
						AttributeValue attributeValue= vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
						attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
						vendorResponse.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
						vendorResponse.addAttributeValue(attributeValue);					
					}
				    vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
					vendorResponse.setProduct(appendProductInfo(vendorRequest,vendorResponse));
				}else {
					logger.error(error.getErrorCode() + "........." +error.getErrDescription());
					vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
							FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
					return vendorResponse;
				}

			}else {
				rootResp = callMotorRequest(vendorRequest);
			    error = (Errors) rootResp.getRoot().get(4);
				if(error.getErrorCode().isEmpty() || error==null) {
				    vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest, Json.toJson(rootResp),vendorResponse, "commBRIDGEFusionMOTORRes");			
				    if(vendorResponse.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())){						
						AttributeValue attributeValue= vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
						attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
						vendorResponse.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
						vendorResponse.addAttributeValue(attributeValue);					
					}
				    vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
					vendorResponse.setProduct(appendProductInfo(vendorRequest,vendorResponse));
				}else {
					logger.error(error.getErrorCode() + "........." +error.getErrDescription());
					vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
							FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
					return vendorResponse;
				}

			}		   
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
			return vendorResponse;
		}

		return vendorResponse;

	}
	
	private Root callMotorRequest(VendorRequest vendorRequest) throws IOException, JAXBException, JsonPatchException{
		List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
		patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "commBRIDGEFusionMOTORREQ");
		JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
		JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);

		Root root = motorObjectFactory.createRoot();
		Authentication authentication = motorObjectFactory.createRootAuthentication();
		Customer customer = motorObjectFactory.createRootCustomer();
		Product product = motorObjectFactory.createRootProduct();
		PaymentDetails paymentDetails = motorObjectFactory.createRootPaymentDetails();
		paymentDetails.getPaymentEntry().add(motorObjectFactory.createRootPaymentDetailsPaymentEntry());
		Errors errors = motorObjectFactory.createRootErrors();
		
		product.getGeneralProposal().add(createRootProductGeneralProposal());
		product.getPremiumCalculation().add(createRootProductPremiumCalculation());
		product.getRisks().add(createRootProductRisks());
 
		if(vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.CAR.name())){
			authentication.setWACode(propUtils.get("arka.vendor.usgi.pcpp.wacode"));
			authentication.setWAAppCode(propUtils.get("arka.vendor.usgi.pcpp.waappcode"));
			authentication.setWAType(propUtils.get("arka.vendor.usgi.pcpp.watype"));
			authentication.setWAUserID(propUtils.get("arka.vendor.usgi.pcpp.wauserid"));
			authentication.setWAUserPwd(propUtils.get("arka.vendor.usgi.pcpp.wauserpwd"));			
		}else if(vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name())){
			authentication.setWACode(propUtils.get("arka.vendor.usgi.twpp.wacode"));
			authentication.setWAAppCode(propUtils.get("arka.vendor.usgi.twpp.waappcode"));
			authentication.setWAType(propUtils.get("arka.vendor.usgi.twpp.watype"));
			authentication.setWAUserID(propUtils.get("arka.vendor.usgi.twpp.wauserid"));
			authentication.setWAUserPwd(propUtils.get("arka.vendor.usgi.twpp.wauserpwd"));			
		}

		root.getRoot().add(authentication);  //0
		root.getRoot().add(customer);		//1
		root.getRoot().add(product);        //2
		root.getRoot().add(paymentDetails); //3
		root.getRoot().add(errors);         //4
		JsonNode orgJsonNode = mapper.valueToTree(root);		
		JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);		
		String xmlStringPart;
		StringBuffer RootXMLBuffer = new StringBuffer("<Root>").append(System.lineSeparator());	
		
		Root.Authentication motorAuthenticationRequest = mapper.treeToValue(patchedJsonNode.at("/root/0"), Root.Authentication.class);
		xmlStringPart = prepareXMLPartString(Root.Authentication.class, motorAuthenticationRequest);
		RootXMLBuffer.append(xmlStringPart);
		
		Root.Customer motorCustomerRequest = mapper.treeToValue(patchedJsonNode.at("/root/1"), Root.Customer.class);
		//hack incase of company
		if (vendorRequest.isAttributePresent(InsuranceAttributeCode.AI_IS_COMPANY.getCode()) && vendorRequest.getUniqueInputQuoteForAttribute(InsuranceAttributeCode.AI_IS_COMPANY.getCode()).getValueAsBoolean()) {
			motorCustomerRequest.setCustomerName(vendorRequest.getUniqueInputQuoteForAttribute(InsuranceAttributeCode.AI_COMPANY_NAME.getCode()).getValue());
		}
		xmlStringPart = prepareXMLPartString(Root.Customer.class, motorCustomerRequest);
		RootXMLBuffer.append(xmlStringPart);
		
		Root.Product motorProductRequest = mapper.treeToValue(patchedJsonNode.at("/root/2"), Root.Product.class);
		xmlStringPart = prepareXMLPartString(Root.Product.class, motorProductRequest);
		RootXMLBuffer.append(xmlStringPart);
		
		Root.PaymentDetails motorPaymentDetailsRequest = mapper.treeToValue(patchedJsonNode.at("/root/3"), Root.PaymentDetails.class);
		xmlStringPart = prepareXMLPartString(Root.PaymentDetails.class, motorPaymentDetailsRequest);
		RootXMLBuffer.append(xmlStringPart);
		
		Root.Errors motorErrorsRequest = mapper.treeToValue(patchedJsonNode.at("/root/4"), Root.Errors.class);
		xmlStringPart = prepareXMLPartString(Root.Errors.class, motorErrorsRequest);
		RootXMLBuffer.append(xmlStringPart);		
		RootXMLBuffer.append("</Root>");
		//System.out.println("RootXML: \n" + RootXMLBuffer.toString());
				
		Service1 service1 = new Service1();
		IService1 basicHttpBindingIService1 = service1.getBasicHttpBindingIService1();
		BindingProvider bindingProvider = (BindingProvider)basicHttpBindingIService1;
		bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, propUtils.get("arka.vendor.usgic.motorservice"));
		DefaultMessageHandler.printCDataMessage(bindingProvider);
	    String response = basicHttpBindingIService1.commBRIDGEFusionMOTOR(RootXMLBuffer.toString());
	    //System.out.println("response: \n" + response);	
	    response=response.replace(XML_PROLOG, "");		
	    JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
	    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
	    StringReader reader = new StringReader(response);
	    Root rootResp = (Root) unmarshaller.unmarshal(reader);
		return rootResp;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	private static String prepareXMLPartString(Class clazz, Object obj) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(clazz);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        JAXBElement jx = new JAXBElement(new QName(obj.getClass().getSimpleName()), clazz, obj);
        StringWriter sw = new StringWriter();
        marshaller.marshal(jx, sw);
        return sw.toString().replace(XML_PROLOG, "").replaceFirst("\n|\r", "");
		
	}
	
	private PremiumCalculation createRootProductPremiumCalculation() {
		PremiumCalculation premiumCalculation = motorObjectFactory.createRootProductPremiumCalculation();
		NetPremium netPremium = motorObjectFactory.createRootProductPremiumCalculationNetPremium();
		ServiceTax serviceTax = motorObjectFactory.createRootProductPremiumCalculationServiceTax();
		StampDuty2 stampDuty2 = motorObjectFactory.createRootProductPremiumCalculationStampDuty2();
		TotalPremium totalPremium = motorObjectFactory.createRootProductPremiumCalculationTotalPremium();
		premiumCalculation.getNetPremium().add(netPremium);
		premiumCalculation.getServiceTax().add(serviceTax);
		premiumCalculation.getStampDuty2().add(stampDuty2);
		premiumCalculation.getTotalPremium().add(totalPremium);
		return premiumCalculation;
		
	}
	private Risks createRootProductRisks() {
		Risks risks = motorObjectFactory.createRootProductRisks();
		Risk risk = motorObjectFactory.createRootProductRisksRisk();
		RisksData risksData = motorObjectFactory.createRootProductRisksRiskRisksData();
		DeTariffLoadings deTariffLoadings = motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffLoadings();
		DeTariffDiscounts deTariffDiscounts = motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffDiscounts();
		CoverDetails coverDetails = motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetails();
		AddonCoverDetails addonCoverDetails = motorObjectFactory.createRootProductRisksRiskRisksDataAddonCoverDetails();
		OtherLoadings otherLoadings = motorObjectFactory.createRootProductRisksRiskRisksDataOtherLoadings();
		OtherDiscounts otherDiscounts = motorObjectFactory.createRootProductRisksRiskRisksDataOtherDiscounts();
		
		DeTariffLoadingGroup deTariffLoadingGroup = motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffLoadingsDeTariffLoadingGroup();
		DeTariffLoadingGroupData deTariffLoadingGroupData = motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffLoadingsDeTariffLoadingGroupDeTariffLoadingGroupData();
		deTariffLoadingGroupData.getLoadingAmount().add(motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffLoadingsDeTariffLoadingGroupDeTariffLoadingGroupDataLoadingAmount());
		deTariffLoadingGroupData.getLoadingRate().add(motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffLoadingsDeTariffLoadingGroupDeTariffLoadingGroupDataLoadingRate());
		deTariffLoadingGroupData.getSumInsured().add(motorObjectFactory.createSumInsured());
		deTariffLoadingGroupData.getPremium().add(motorObjectFactory.createPremium());
		deTariffLoadingGroupData.getRate().add(motorObjectFactory.createRate());
		deTariffLoadingGroupData.getApplicable().add(motorObjectFactory.createApplicable());
		deTariffLoadingGroupData.getDescription().add(motorObjectFactory.createDescription());	
		deTariffLoadingGroup.getDeTariffLoadingGroupData().add(deTariffLoadingGroupData);
		deTariffLoadings.getDeTariffLoadingGroup().add(deTariffLoadingGroup);
		
		DeTariffDiscountGroup deTariffDiscountGroup = motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffDiscountsDeTariffDiscountGroup();
		DeTariffDiscountGroupData deTariffDiscountGroupData =motorObjectFactory.createRootProductRisksRiskRisksDataDeTariffDiscountsDeTariffDiscountGroupDeTariffDiscountGroupData();
		deTariffDiscountGroupData.getDiscountAmount().add(motorObjectFactory.createDiscountAmount());
		deTariffDiscountGroupData.getDiscountRate().add(motorObjectFactory.createDiscountRate());
		deTariffDiscountGroupData.getSumInsured().add(motorObjectFactory.createSumInsured());
		deTariffDiscountGroupData.getRate().add(motorObjectFactory.createRate());
		deTariffDiscountGroupData.getPremium().add(motorObjectFactory.createPremium());
		deTariffDiscountGroupData.getApplicable().add(motorObjectFactory.createApplicable());
		deTariffDiscountGroupData.getDescription().add(motorObjectFactory.createDescription());
		deTariffDiscountGroup.getDeTariffDiscountGroupData().add(deTariffDiscountGroupData);
		deTariffDiscounts.getDeTariffDiscountGroup().add(deTariffDiscountGroup);
		
		Covers covers = motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetailsCovers();		
		for(int i=0;i<MAX_COVER_DATA;i++){
			covers.getCoversData().add(createCoverData());
		}
		coverDetails.getCovers().add(covers);
		
		AddonCovers addonCovers = motorObjectFactory.createRootProductRisksRiskRisksDataAddonCoverDetailsAddonCovers();		
		for(int i=0;i<MAX_ADDON_COVER_DATA;i++){
			addonCovers.getAddonCoversData().add(createAddonCoverData());
		}
		addonCoverDetails.getAddonCovers().add(addonCovers);
		
		OtherLoadingGroup otherLoadingGroup = motorObjectFactory.createRootProductRisksRiskRisksDataOtherLoadingsOtherLoadingGroup();
		otherLoadings.getOtherLoadingGroup().add(otherLoadingGroup);
		
		OtherDiscountGroup otherDiscountGroup = motorObjectFactory.createRootProductRisksRiskRisksDataOtherDiscountsOtherDiscountGroup();
		OtherDiscountGroupData  otherDiscountGroupData = motorObjectFactory.createRootProductRisksRiskRisksDataOtherDiscountsOtherDiscountGroupOtherDiscountGroupData();
        otherDiscountGroupData.getDiscountAmount().add(motorObjectFactory.createDiscountAmount());
	    otherDiscountGroupData.getDiscountRate().add(motorObjectFactory.createDiscountRate());
	    otherDiscountGroupData.getSumInsured().add(motorObjectFactory.createSumInsured());
	    otherDiscountGroupData.getRate().add(motorObjectFactory.createRate());
	    otherDiscountGroupData.getPremium().add(motorObjectFactory.createPremium());
	    otherDiscountGroupData.getApplicable().add(motorObjectFactory.createApplicable());
	    otherDiscountGroupData.getDescription().add(motorObjectFactory.createDescription());
	    otherDiscountGroup.getOtherDiscountGroupData().add(otherDiscountGroupData);
	    otherDiscounts.getOtherDiscountGroup().add(otherDiscountGroup);
		
	    risksData.getDeTariffLoadings().add(deTariffLoadings);
		risksData.getDeTariffDiscounts().add(deTariffDiscounts);
		risksData.getCoverDetails().add(coverDetails);
		risksData.getAddonCoverDetails().add(addonCoverDetails);
		risksData.getOtherLoadings().add(otherLoadings);
		risksData.getOtherDiscounts().add(otherDiscounts);
		risk.getRisksData().add(risksData);
		risks.getRisk().add(risk);
		
		risks.getVehicleClassCode().add(motorObjectFactory.createRootProductRisksVehicleClassCode());
		risks.getVehicleMakeCode().add(motorObjectFactory.createRootProductRisksVehicleMakeCode());
		risks.getVehicleModelCode().add(motorObjectFactory.createRootProductRisksVehicleModelCode());
		risks.getRTOLocationCode().add(motorObjectFactory.createRootProductRisksRTOLocationCode());
		risks.getNoOfClaimsOnPreviousPolicy().add(motorObjectFactory.createRootProductRisksNoOfClaimsOnPreviousPolicy());
		risks.getRegistrationNumber().add(motorObjectFactory.createRootProductRisksRegistrationNumber());
		risks.getBodyTypeCode().add(motorObjectFactory.createRootProductRisksBodyTypeCode());
		risks.getModelStatus().add(motorObjectFactory.createRootProductRisksModelStatus());
		risks.getGrossVehicleWeight().add(motorObjectFactory.createRootProductRisksGrossVehicleWeight());
		risks.getCarryingCapacity().add(motorObjectFactory.createRootProductRisksCarryingCapacity());
		risks.getVehicleType().add(motorObjectFactory.createRootProductRisksVehicleType());
		risks.getPlaceOfRegistration().add(motorObjectFactory.createRootProductRisksPlaceOfRegistration());
		risks.getVehicleModel().add(motorObjectFactory.createRootProductRisksVehicleModel());
		risks.getVehicleExShowroomPrice().add(motorObjectFactory.createRootProductRisksVehicleExShowroomPrice());
		risks.getDateOfDeliveryOrRegistration().add(motorObjectFactory.createRootProductRisksDateOfDeliveryOrRegistration());
		risks.getYearOfManufacture().add(motorObjectFactory.createRootProductRisksYearOfManufacture());
		risks.getDateOfFirstRegistration().add(motorObjectFactory.createRootProductRisksDateOfFirstRegistration());
		risks.getRegistrationNumberSection1().add(motorObjectFactory.createRootProductRisksRegistrationNumberSection1());
		risks.getRegistrationNumberSection2().add(motorObjectFactory.createRootProductRisksRegistrationNumberSection2());
		risks.getRegistrationNumberSection3().add(motorObjectFactory.createRootProductRisksRegistrationNumberSection3());
		risks.getRegistrationNumberSection4().add(motorObjectFactory.createRootProductRisksRegistrationNumberSection4());
		risks.getEngineNumber().add(motorObjectFactory.createRootProductRisksEngineNumber());
		risks.getChassisNumber().add(motorObjectFactory.createRootProductRisksChassisNumber());
		risks.getBodyColour().add(motorObjectFactory.createRootProductRisksBodyColour());
		risks.getFuelType().add(motorObjectFactory.createRootProductRisksFuelType());
		risks.getExtensionCountryName().add(motorObjectFactory.createRootProductRisksExtensionCountryName());
		risks.getRegistrationAuthorityName().add(motorObjectFactory.createRootProductRisksRegistrationAuthorityName());
		risks.getAutomobileAssocnFlag().add(motorObjectFactory.createRootProductRisksAutomobileAssocnFlag());
		risks.getAutomobileAssociationNumber().add(motorObjectFactory.createRootProductRisksAutomobileAssociationNumber());
		risks.getVoluntaryExcess().add(motorObjectFactory.createRootProductRisksVoluntaryExcess());
		risks.getTPPDLimit().add(motorObjectFactory.createRootProductRisksTPPDLimit());
		risks.getAntiTheftDiscFlag().add(motorObjectFactory.createRootProductRisksAntiTheftDiscFlag());
		risks.getHandicapDiscFlag().add(motorObjectFactory.createRootProductRisksHandicapDiscFlag());
		risks.getNumberOfDrivers().add(motorObjectFactory.createRootProductRisksNumberOfDrivers());
		risks.getNumberOfEmployees().add(motorObjectFactory.createRootProductRisksNumberOfEmployees());
		risks.getTransferOfNCB().add(motorObjectFactory.createRootProductRisksTransferOfNCB());
		risks.getTransferOfNCBPercent().add(motorObjectFactory.createRootProductRisksTransferOfNCBPercent());
		risks.getNCBDeclaration().add(motorObjectFactory.createRootProductRisksNCBDeclaration());
		risks.getPreviousVehicleSaleDate().add(motorObjectFactory.createRootProductRisksPreviousVehicleSaleDate());
		risks.getBonusOnPreviousPolicy().add(motorObjectFactory.createRootProductRisksBonusOnPreviousPolicy());
		risks.getVehicleClass().add(motorObjectFactory.createRootProductRisksVehicleClass());
		risks.getVehicleMake().add(motorObjectFactory.createRootProductRisksVehicleMake());
		risks.getBodyTypeDescription().add(motorObjectFactory.createRootProductRisksBodyTypeDescription());
		risks.getNumberOfWheels().add(motorObjectFactory.createRootProductRisksNumberOfWheels());
		risks.getCubicCapacity().add(motorObjectFactory.createRootProductRisksCubicCapacity());
		risks.getSeatingCapacity().add(motorObjectFactory.createRootProductRisksSeatingCapacity());
		risks.getRegistrationZone().add(motorObjectFactory.createRootProductRisksRegistrationZone());
		risks.getVehiclesDrivenBy().add(motorObjectFactory.createRootProductRisksVehiclesDrivenBy());
		risks.getDriversAge().add(motorObjectFactory.createRootProductRisksDriversAge());
		risks.getDriversExperience().add(motorObjectFactory.createRootProductRisksDriversExperience());
		risks.getDriversQualification().add(motorObjectFactory.createRootProductRisksDriversQualification());
		risks.getVehicleModelCluster().add(motorObjectFactory.createRootProductRisksVehicleModelCluster());
		risks.getOpenCoverNoteFlag().add(motorObjectFactory.createRootProductRisksOpenCoverNoteFlag());
		risks.getLegalLiability().add(motorObjectFactory.createRootProductRisksLegalLiability());
		risks.getPaidDriver().add(motorObjectFactory.createRootProductRisksPaidDriver());
		risks.getNCBConfirmation().add(motorObjectFactory.createRootProductRisksNCBConfirmation());
		risks.getRegistrationDate().add(motorObjectFactory.createRootProductRisksRegistrationDate());
		risks.getTPLoadingRate().add(motorObjectFactory.createRootProductRisksTPLoadingRate());
		risks.getExtensionCountry().add(motorObjectFactory.createRootProductRisksExtensionCountry());
		risks.getVehicleAge().add(motorObjectFactory.createRootProductRisksVehicleAge());
		risks.getLocationCode().add(motorObjectFactory.createRootProductRisksLocationCode());
		risks.getRegistrationZoneDescription().add(motorObjectFactory.createRootProductRisksRegistrationZoneDescription());
		risks.getNumberOfWorkmen().add(motorObjectFactory.createRootProductRisksNumberOfWorkmen());
		risks.getVehicCd().add(motorObjectFactory.createRootProductRisksVehicCd());
		risks.getSalesTax().add(motorObjectFactory.createRootProductRisksSalesTax());
		risks.getModelOfVehicle().add(motorObjectFactory.createRootProductRisksModelOfVehicle());
		risks.getPopulateDetails().add(motorObjectFactory.createRootProductRisksPopulateDetails());
		risks.getVehicleIDV().add(motorObjectFactory.createRootProductRisksVehicleIDV());
		risks.getShowroomPriceDeviation().add(motorObjectFactory.createRootProductRisksShowroomPriceDeviation());
		risks.getNewVehicle().add(motorObjectFactory.createRootProductRisksNewVehicle());
		
		return risks;
		
	}
	private CoversData createCoverData(){
		CoversData coversData = motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetailsCoversCoversData();
		coversData.getPremium().add(motorObjectFactory.createPremium());
		coversData.getRate().add(motorObjectFactory.createRate());
		coversData.getSumInsured().add(motorObjectFactory.createSumInsured());
		coversData.getNomineeName().add(motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetailsCoversCoversDataNomineeName());
		coversData.getNomineeRelation().add(motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetailsCoversCoversDataNomineeRelation());
		coversData.getNomineeAge().add(motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetailsCoversCoversDataNomineeAge());
		coversData.getApplicable().add(motorObjectFactory.createApplicable());
		coversData.getCoverGroups().add(motorObjectFactory.createRootProductRisksRiskRisksDataCoverDetailsCoversCoversDataCoverGroups());
		return coversData;
	}
	private AddonCoversData createAddonCoverData(){
		AddonCoversData addonCoversData = motorObjectFactory.createRootProductRisksRiskRisksDataAddonCoverDetailsAddonCoversAddonCoversData();
		addonCoversData.getPremium().add(motorObjectFactory.createPremium());
		addonCoversData.getRate().add(motorObjectFactory.createRate());
		addonCoversData.getSumInsured().add(motorObjectFactory.createSumInsured());
		addonCoversData.getApplicable().add(motorObjectFactory.createApplicable());
		addonCoversData.getAddonCoverGroups().add(motorObjectFactory.createRootProductRisksRiskRisksDataAddonCoverDetailsAddonCoversAddonCoversDataAddonCoverGroups());
		return addonCoversData;
	}
	
	private GeneralProposal createRootProductGeneralProposal() {
		GeneralProposal generalProposal = motorObjectFactory.createRootProductGeneralProposal();
		//Product - GeneralProposal			
		GeneralProposalGroup generalProposalGroup = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroup();
		FinancierDetails financierDetails = motorObjectFactory.createRootProductGeneralProposalFinancierDetails();
		PreviousPolicyDetails previousPolicyDetails = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetails();
		
		//Product - GeneralProposal ---- GeneralProposalGroup
		DistributionChannel distributionChannel = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannel();
		GeneralProposalInformation generalProposalInformation = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformation();
		
		//Product - GeneralProposal ---- GeneralProposalGroup ------ DistributionChannel
		BranchDetails branchDetails = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannelBranchDetails();
		branchDetails.getIMDBranchCode().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannelBranchDetailsIMDBranchCode());
		branchDetails.getIMDBranchName().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannelBranchDetailsIMDBranchName());
		SPDetails spDetails = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannelSPDetails();
		spDetails.getSPCode().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannelSPDetailsSPCode());
		spDetails.getSPName().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupDistributionChannelSPDetailsSPName());
		distributionChannel.getBranchDetails().add(branchDetails);
		distributionChannel.getSPDetails().add(spDetails);
		generalProposalGroup.getDistributionChannel().add(distributionChannel);
		
		
		//Product - GeneralProposal ---- GeneralProposalGroup ------ GeneralProposalInformation
		TypeOfBusiness typeOfBusiness = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationTypeOfBusiness();
		ServiceTaxExemptionCategory serviceTaxExemptionCategory = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationServiceTaxExemptionCategory();
		BusinessType businessType = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationBusinessType();
		Sector sector = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationSector();
		ProposalDate proposalDate = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationProposalDate();	
		DealId dealId = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationDealId();
		PolicyNumberChar policyNumberChar = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationPolicyNumberChar();
		VehicleLaidUpFrom vehicleLaidUpFrom = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationVehicleLaidUpFrom();
		VehicleLaidUpTo vehicleLaidUpTo = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationVehicleLaidUpTo();
		PolicyEffectiveDate policyEffectiveDate = motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationPolicyEffectiveDate();
		policyEffectiveDate.getFromdate().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationPolicyEffectiveDateFromdate());
		policyEffectiveDate.getFromhour().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationPolicyEffectiveDateFromhour());
		policyEffectiveDate.getTodate().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationPolicyEffectiveDateTodate());
		policyEffectiveDate.getTohour().add(motorObjectFactory.createRootProductGeneralProposalGeneralProposalGroupGeneralProposalInformationPolicyEffectiveDateTohour());
		generalProposalInformation.getTypeOfBusiness().add(typeOfBusiness);
		generalProposalInformation.getServiceTaxExemptionCategory().add(serviceTaxExemptionCategory);
		generalProposalInformation.getBusinessType().add(businessType);
		generalProposalInformation.getSector().add(sector);
		generalProposalInformation.getProposalDate().add(proposalDate);
		generalProposalInformation.getDealId().add(dealId);
		generalProposalInformation.getPolicyNumberChar().add(policyNumberChar);
		generalProposalInformation.getVehicleLaidUpFrom().add(vehicleLaidUpFrom);
		generalProposalInformation.getVehicleLaidUpTo().add(vehicleLaidUpTo);
		generalProposalInformation.getPolicyEffectiveDate().add(policyEffectiveDate);
		generalProposalGroup.getGeneralProposalInformation().add(generalProposalInformation);
					
		//Product - GeneralProposal ---- FinancierDetails
		FinancierDtlGrp financierDtlGrp = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrp();
				
		//Product - GeneralProposal ---- FinancierDetails -----  FinancierDtlGrpData
		FinancierDtlGrpData financierDtlGrpData = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrpFinancierDtlGrpData();
		FinancierCode financierCode = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrpFinancierDtlGrpDataFinancierCode();
		AgreementType agreementType = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrpFinancierDtlGrpDataAgreementType();
		BranchName branchName = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrpFinancierDtlGrpDataBranchName();
		FinancierName financierName = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrpFinancierDtlGrpDataFinancierName();
		SrNo srNo = motorObjectFactory.createRootProductGeneralProposalFinancierDetailsFinancierDtlGrpFinancierDtlGrpDataSrNo();
		financierDtlGrpData.getAgreementType().add(agreementType);
		financierDtlGrpData.getFinancierCode().add(financierCode);
		financierDtlGrpData.getBranchName().add(branchName);
		financierDtlGrpData.getFinancierName().add(financierName);
		financierDtlGrpData.getSrNo().add(srNo);
		financierDtlGrp.getFinancierDtlGrpData().add(financierDtlGrpData);
		financierDetails.getFinancierDtlGrp().add(financierDtlGrp);
		
		
		//Product - GeneralProposal ---- PreviousPolicyDetails
		PreviousPolDtlGroup previousPolDtlGroup = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroup();
		PreviousPolicyType previousPolicyType = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolicyType();
		OfficeAddress officeAddress = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsOfficeAddress();
		
		//Product - GeneralProposal ---- PreviousPolicyDetails--PreviousPolDtlGroup
		PreviousPolDtlGroupData previousPolDtlGroupData = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupData();
		ProductCode productCode = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataProductCode();
		ClaimSettled claimSettled = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataClaimSettled();
		ClaimPremium claimPremium = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataClaimPremium();
		ClaimAmount claimAmount = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataClaimAmount();
		DateofLoss dateofLoss = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataDateofLoss();
		NatureofLoss natureofLoss = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataNatureofLoss();
		ClaimNo claimNo = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataClaimNo();
		PolicyEffectiveTo policyEffectiveTo = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataPolicyEffectiveTo();
		PolicyEffectiveFrom policyEffectiveFrom = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataPolicyEffectiveFrom();
		DateOfInspection dateOfInspection = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataDateOfInspection();
		PolicyPremium policyPremium = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataPolicyPremium();
		PolicyNo policyNo = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataPolicyNo();
		PolicyYear policyYear = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataPolicyYear();
		OfficeCode officeCode = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataOfficeCode();
		PolicyStatus policyStatus = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataPolicyStatus();
		CorporateCustomerId corporateCustomerId = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataCorporateCustomerId();
		InsurerName insurerName = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataInsurerName();
		InsurerAddress insurerAddress = motorObjectFactory.createRootProductGeneralProposalPreviousPolicyDetailsPreviousPolDtlGroupPreviousPolDtlGroupDataInsurerAddress();
		previousPolDtlGroupData.getProductCode().add(productCode);
		previousPolDtlGroupData.getClaimSettled().add(claimSettled);
		previousPolDtlGroupData.getClaimPremium().add(claimPremium);
		previousPolDtlGroupData.getClaimAmount().add(claimAmount);
		previousPolDtlGroupData.getDateofLoss().add(dateofLoss);
		previousPolDtlGroupData.getNatureofLoss().add(natureofLoss);
		previousPolDtlGroupData.getClaimNo().add(claimNo);
		previousPolDtlGroupData.getPolicyEffectiveTo().add(policyEffectiveTo);
		previousPolDtlGroupData.getPolicyEffectiveFrom().add(policyEffectiveFrom);
		previousPolDtlGroupData.getDateOfInspection().add(dateOfInspection);
		previousPolDtlGroupData.getPolicyPremium().add(policyPremium);
		previousPolDtlGroupData.getPolicyNo().add(policyNo);
		previousPolDtlGroupData.getPolicyYear().add(policyYear);
		previousPolDtlGroupData.getOfficeCode().add(officeCode);
		previousPolDtlGroupData.getPolicyStatus().add(policyStatus);
		previousPolDtlGroupData.getCorporateCustomerId().add(corporateCustomerId);
		previousPolDtlGroupData.getInsurerName().add(insurerName);
		previousPolDtlGroupData.getInsurerAddress().add(insurerAddress);
		previousPolDtlGroup.getPreviousPolDtlGroupData().add(previousPolDtlGroupData);
		previousPolicyDetails.getPreviousPolDtlGroup().add(previousPolDtlGroup);
		previousPolicyDetails.getOfficeAddress().add(officeAddress);
		previousPolicyDetails.getPreviousPolicyType().add(previousPolicyType);
	
		generalProposal.getGeneralProposalGroup().add(generalProposalGroup);
		generalProposal.getFinancierDetails().add(financierDetails);
		generalProposal.getPreviousPolicyDetails().add(previousPolicyDetails);
		return generalProposal;		
	}
	
	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("USGI_MOTOR_" + AttributeUtil.generateRandom());
		return vendorResponse;

	}

	
	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		ObjectNode policyJson = Json.newObject();
		String waCode ="";
		try {			
			VendorResponse vendorResponse =fetchPlanFromVendor(vendorRequest);
			policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
			policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
			policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
			policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
			policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
			policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
			policyJson.put(PolicyFields.PAYMENT_FREQUENCY.getCode(), PaymentFrequency.ANNUAL.name());
			policyJson.put(PolicyFields.PAYMENT_MODE.getCode(), PaymentMode.ONLINE.name());
			policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
			policyJson.put(PolicyFields.PREMIUM.getCode(), vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue());
			policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValue());
			policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
			policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
			if(vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.CAR.name())){
				waCode = propUtils.get("arka.vendor.usgi.pcpp.wacode");
			}else{
				waCode = propUtils.get("arka.vendor.usgi.twpp.wacode");
			}
			String FORM_URL = propUtils.get("arka.vendor.usgic.payment")+
					vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_QUOTE_ID.getCode()).getValue()+"&FinalPremium="+ 
			vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue() + "&Src=WA&SubSrc="+waCode;
			policyJson.put("FORM_URL", FORM_URL);
			policyJson.put("UNIQUE_QUOTEID",vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_QUOTE_ID.getCode()).getValue());						
		} catch (Exception e) {
			e.printStackTrace();
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}

		return policyJson;

	}

	
}
