package com.arka.integrationserv.models.vendorplan.insurance.motor;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.ws.BindingProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.insurance.constants.InsuranceAttributeCode;
import com.arka.common.motor.constants.CarAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.CategoryCode;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.PatchRequestVO;
import com.arka.integrationserv.models.vendorplan.constants.VendorErrorCode;
import com.arka.integrationserv.models.vendorplan.insurance.motor.validator.IffcoTokioMotorValidator;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.arka.integrationserv.models.vendorplan.utils.AttributeUtil;
import com.arka.integrationserv.models.vendorplan.utils.CategoryTransformationUtils;
import com.arka.integrationserv.models.vendorplan.utils.DBUtil;
import com.arka.integrationserv.models.vendorplan.utils.DefaultMessageHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.inject.Inject;

import itgi.motor.paymentwrapper.ObjectFactory;
import itgi.motor.paymentwrapper.Request;
import itgi.motor.paymentwrapper.Request.Coverage;
import itgi.motor.premiumwrapper.ArrayOfVehicleCoverage;
import itgi.motor.premiumwrapper.GetMotorPremium;
import itgi.motor.premiumwrapper.IDVWebService;
import itgi.motor.premiumwrapper.IDVWebServiceRequest;
import itgi.motor.premiumwrapper.IDVWebServiceResponse;
import itgi.motor.premiumwrapper.IDVWebServiceService;
import itgi.motor.premiumwrapper.MotorPremiumWebserviceVA;
import itgi.motor.premiumwrapper.MotorPremiumWebserviceVAService;
import itgi.motor.premiumwrapper.Partner;
import itgi.motor.premiumwrapper.Policy;
import itgi.motor.premiumwrapper.PolicyHeader;
import itgi.motor.premiumwrapper.PremiumDetailsVA;
import itgi.motor.premiumwrapper.Vehicle;
import itgi.motor.premiumwrapper.VehicleCoverage;
import play.libs.Json;

public class IffcoTokioInsuranceConnector extends MotorVendorConnector {

	@Inject
	DBUtil dbUtil;

	@Inject
	CategoryTransformationUtils categoryTransformationUtils;
	
	@Inject
	IffcoTokioMotorValidator iffcoValidator;
	
	@Inject
	private PropUtils propUtils;
	
	private static final Logger logger = LoggerFactory.getLogger(IffcoTokioInsuranceConnector.class);

	public VendorResponse fetchPlanFromVendor(VendorRequest vendorRequest) {
		VendorResponse vendorResponse = new VendorResponse();
		try {
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			if(iffcoValidator.validateForBreakInPolicy(vendorRequest,vendorResponse)){
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.BREAK_IN_POICY_NOT_ALLOWED.value()));
				return vendorResponse;
			}
			expiredTWPInceptionDate(vendorRequest);
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "getVehicleIdv");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			IDVWebServiceRequest idvWebServiceRequest = new IDVWebServiceRequest();
			JsonNode orgJsonNode = mapper.valueToTree(idvWebServiceRequest);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
			IDVWebServiceRequest patchedIDVWebServiceRequest = mapper.treeToValue(patchedJsonNode,
					IDVWebServiceRequest.class);
			setMakecodeFormat(vendorRequest,patchedIDVWebServiceRequest);
			IDVWebServiceService service = new IDVWebServiceService();
			IDVWebService server = service.getIDVWebService();
			BindingProvider bindingProvider = (BindingProvider)server;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, propUtils.get("arka.vendor.itgi.idv.endpoint"));
			DefaultMessageHandler.printMessage(bindingProvider);
			IDVWebServiceResponse idvWebServiceResponse = server.getVehicleIdv(patchedIDVWebServiceRequest);
			if(idvWebServiceResponse.getIdv()!=null) {
				if((!vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) ) || (vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) && 
						!vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV.getCode()).getValueAsBoolean())){
					vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_PRODUCT_IDV.getCode(), idvWebServiceResponse.getIdv().toString());
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_PRODUCT_IDV.getCode(), idvWebServiceResponse.getIdv().toString());
					vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(),
							idvWebServiceResponse.getIdv().toString());
					vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(), idvWebServiceResponse.getIdv().toString());		
				}
				vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode(), idvWebServiceResponse.getMinimumIdvAllowed().toString());
				vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode(), idvWebServiceResponse.getMaximumIdvAllowed().toString());
			}else{
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.NOT_SUPPORTED_MODEL.value()));
				return vendorResponse;
			}
			if(iffcoValidator.validateIDVForBreakInPolicy(vendorRequest,vendorResponse)){
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.IDV_LIMIT_EXCEED_FOR_BREAKINPOLICY.value()));
				return vendorResponse;
			}
			if(iffcoValidator.validateMaxAllowedIDV(vendorRequest,vendorResponse)){
				modifyCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(),
						vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MAX_ALLOWED_IDV.getCode()).getValue());		
			}else if(iffcoValidator.validateMinAllowedIDV(vendorRequest,vendorResponse)){
				modifyCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode(), 
						vendorResponse.getAttributeValueForCode(CarAttributeCode.AC_MIN_ALLOWED_IDV.getCode()).getValue());
			}
			if(vendorRequest.isAttributePresent(CarAttributeCode.AC_MODIFIED_IDV.getCode()) && 
					vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV.getCode()).getValueAsBoolean()){
				double modifiedIDV = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_MODIFIED_IDV_VALUE.getCode()).getValueAsDouble();
				vendorRequest = addCategoryAttributeToRequest(vendorRequest, CarAttributeCode.AC_PRODUCT_IDV.getCode(),df.format(modifiedIDV));
				vendorResponse = addCategoryAttributeToResponse(vendorResponse, CarAttributeCode.AC_PRODUCT_IDV.getCode(), df.format(modifiedIDV));
			
			}
			return getMotorPremium(vendorRequest, vendorResponse);

		} catch (Exception e) {
			logger.error(e.getMessage());
			vendorResponse.setErrorJson(getValidationError(VendorErrorCode.TIMEOUT.name(),FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value(),null));
			return vendorResponse;

		}
	}

	private VendorResponse getMotorPremium(VendorRequest vendorRequest, VendorResponse vendorResponse)
			throws IOException, JsonProcessingException, JsonPatchException {
		List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
		patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "getMotorPremium");
		JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
		JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
		System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
		GetMotorPremium getMotorPremium = createMotorPremium();
		JsonNode orgJsonNode = mapper.valueToTree(getMotorPremium);
		JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
		GetMotorPremium getMotorPremiumReq = mapper.treeToValue(patchedJsonNode, GetMotorPremium.class);
		ListIterator<VehicleCoverage> item = getMotorPremiumReq.getPolicy().getVehicle().getVehicleCoverage().getItem()
				.listIterator();
		while (item.hasNext()) {
			VehicleCoverage vehicleItem = item.next();
			if (vehicleItem.getCoverageId().equalsIgnoreCase("No Claim Bonus")) {
				if (iffcoValidator.isNewVehicle(vendorRequest)) {
					item.remove();
				}
				if (vendorRequest.isAttributePresent(CarAttributeCode.AC_CLAIMED.getCode())
						&& vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_CLAIMED.getCode())
								.getValue().equals("true")) {
					item.remove();
				}

			} else if (vehicleItem.getCoverageId().equalsIgnoreCase("Legal Liability to Driver")
					&& getMotorPremiumReq.getPolicy().getContractType().equalsIgnoreCase("TWP")) {
				item.remove();
			}
		}

		MotorPremiumWebserviceVAService motorPremiumService = new MotorPremiumWebserviceVAService();
		MotorPremiumWebserviceVA motorPremiumServer = motorPremiumService.getMotorPremiumWebserviceVA();

		BindingProvider bindingProvider = (BindingProvider) motorPremiumServer;
		bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				propUtils.get("arka.vendor.itgi.motorpremiumva.endpoint"));
		DefaultMessageHandler.printMessage(bindingProvider);
		List<PremiumDetailsVA> premiumDetailsVAList = motorPremiumServer.getMotorPremium(
				getMotorPremiumReq.getPolicyHeader(), getMotorPremiumReq.getPolicy(), getMotorPremiumReq.getPartner());
		System.out.println("premiumDetails..........." + mapper.writeValueAsString(premiumDetailsVAList));

		boolean isSuccess = false;
		String errorString = "ERROR. ";

		PremiumDetailsVA premiumDetailsVAHolder = null;
		PremiumDetailsVA premiumDetailsVAWithCover = null;

		if (premiumDetailsVAList != null) {

			for (PremiumDetailsVA premiumDetailsVA : premiumDetailsVAList) {

				if (premiumDetailsVA.getError().isEmpty()) {
					if (premiumDetailsVA.isAutocoverage()) {
						premiumDetailsVAWithCover = premiumDetailsVA;
					} else {
						premiumDetailsVAHolder = premiumDetailsVA;

					}
					isSuccess = true;
				} else {
					errorString += premiumDetailsVA.getError().toString();
					isSuccess = false;
				}
			}

			if (isSuccess && null != premiumDetailsVAHolder) {
				if (null != premiumDetailsVAWithCover && null != premiumDetailsVAWithCover.getCoveragePremiumDetail()) {

					premiumDetailsVAHolder.getCoveragePremiumDetail().clear();
					premiumDetailsVAHolder.getCoveragePremiumDetail()
							.addAll(premiumDetailsVAWithCover.getCoveragePremiumDetail());
				}
				vendorResponse = categoryTransformationUtils.mapToCatalogCategory(vendorRequest,
						Json.toJson(premiumDetailsVAHolder), vendorResponse, "getMotorPremiumResponse");
				if (vendorResponse.isAttributeValuePresentForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode())) {
					AttributeValue attributeValue = vendorResponse
							.getAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
					attributeValue.setCategoryAttributeCode(CarAttributeCode.AC_ADDON_APPLICABLE.getCode());
					vendorResponse.removeAttributeValueForCode(CarAttributeCode.AC_ADDON_SUPPORTED.getCode());
					vendorResponse.addAttributeValue(attributeValue);

				}
				vendorResponse = appendBasicInfo(vendorRequest, vendorResponse);
				vendorResponse.setProduct(appendProductInfo(vendorRequest, vendorResponse));
				// setCngLpgPremium(vendorRequest,vendorResponse);
				calculateBasePremium(vendorRequest, vendorResponse);
				calculatePremiumWithoutLLDriverCover(vendorRequest, vendorResponse);
				calculatePremiumWithAddon(vendorRequest, vendorResponse);
			} else {
				logger.error(errorString);
				vendorResponse.setErrorJson(getValidationError(VendorErrorCode.GENERAL_ERROR.name(),
						FetchPlanMessage.QUOTES_NOT_SUPPORTED_FROM_VENDOR.value(),
						FetchPlanMessage.NO_PREMIUM_RETURNED.value()));
			}
		}
		return vendorResponse;
	}


	private IDVWebServiceRequest setMakecodeFormat(VendorRequest vendorRequest,IDVWebServiceRequest patchedIDVWebServiceRequest) {
		String makeCode = patchedIDVWebServiceRequest.getMakeCode();
		String registrationDate = vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_DATE_OF_REGISTRATION.getCode()).getValue();
		if(vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name())){
			makeCode = "TWP-"+makeCode+"-"+categoryTransformationUtils.vendorDateFormatConversion(registrationDate, "yyyy");
		}else if(vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.CAR.name())){
			makeCode = "PCP-"+makeCode+"-"+categoryTransformationUtils.vendorDateFormatConversion(registrationDate, "yyyy");
		}
		patchedIDVWebServiceRequest.setMakeCode(makeCode);
		return patchedIDVWebServiceRequest;
	}

	private GetMotorPremium createMotorPremium() {
		GetMotorPremium getMotorPremium = new GetMotorPremium();
		Policy policy = new Policy();
		PolicyHeader policyHeader = new PolicyHeader();
		Partner partner = new Partner();
		Vehicle vehicle = new Vehicle();
		partner.setPartnerCode(propUtils.get("arka.vendor.itgi.partner_code"));
		partner.setPartnerBranch(propUtils.get("arka.vendor.itgi.partner_branch"));
		partner.setPartnerSubBranch(propUtils.get("arka.vendor.itgi.partner_sub_branch"));
		ArrayOfVehicleCoverage item = new ArrayOfVehicleCoverage();
		for(int i=0; i<8;i++){
			item.getItem().add(new VehicleCoverage());
		}
		vehicle.setVehicleCoverage(item);
		policy.setVehicle(vehicle);
		getMotorPremium.setPartner(partner);
		getMotorPremium.setPolicy(policy);
		getMotorPremium.setPolicyHeader(policyHeader);
		return getMotorPremium;

	}
	//incase of breakin ITGI TWP
	private VendorRequest expiredTWPInceptionDate(VendorRequest vendorRequest) {
		if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode())){
			if(vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name()) &&
			  ( vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PREVPOLICYSTATUS.getCode()).getValue().equalsIgnoreCase("expired"))
			) {
				String inceptionDate = LocalDate.now().plusDays(3).toString();
				String expiryDate =  LocalDate.now().plusDays(2).plusYears(1).toString();
				if(vendorRequest.isAttributePresent(CarAttributeCode.AC_INCEPTION_DATE.getCode())) {
					vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_INCEPTION_DATE.getCode()).setValue(inceptionDate);	
				}
				if(vendorRequest.isAttributePresent(CarAttributeCode.AC_PRESENT_POLICY_EXPIRY_DATE.getCode())) {
					vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PRESENT_POLICY_EXPIRY_DATE.getCode()).setValue(expiryDate);		
				}
			}
		}	
		return vendorRequest;
		
	}
	private VendorResponse calculateBasePremium(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		double idvODPremium = vendorResponse.getAttributeValueForCode("AC-IDV_BASIC_ODPREMIUM").getValueAsDouble();
		double idvTPPremium = vendorResponse.getAttributeValueForCode("AC-IDV_BASIC_TPPREMIUM").getValueAsDouble();
		double paOwnerDriverTPPRemium = vendorResponse.getAttributeValueForCode("AC-PRODUCT_PA_OWNER_DRIVER_TPPREMIUM").getValueAsDouble();
		double oddiscount = vendorResponse.getAttributeValueForCode("AC-PRODUCT_DISCOUNTS").getValueAsDouble();
		double basePremium = idvODPremium + idvTPPremium + paOwnerDriverTPPRemium + oddiscount;
		addAttribute(vendorResponse, CarAttributeCode.AC_PRODUCT_BASE_PRICE.getCode(), basePremium);
		addAttribute(vendorResponse, "AI-SERVICE_TAX_PERCENT", 0.18);		
		return vendorResponse;
	}
	
	private VendorResponse addAttribute(VendorResponse vendorResponse,String attributeCode,double basePremium){
		AttributeValue attributeValue = new AttributeValue();
		attributeValue.setCategoryAttributeCode(attributeCode);
		attributeValue.setValue(String.valueOf(basePremium));
		vendorResponse.addAttributeValue(attributeValue);
		return vendorResponse;
	}
	
	private VendorResponse calculatePremiumWithoutLLDriverCover(VendorRequest vendorRequest, VendorResponse vendorResponse){
		DecimalFormat df = new DecimalFormat("###.##");
		double driverCover = 0;
		String planRequestType = vendorRequest.getUniqueInputQuoteForAttribute(InsuranceAttributeCode.AI_PLAN_REQUEST_TYPE.getCode()).getValue();
		if(vendorRequest.isAttributePresent(InsuranceAttributeCode.AI_PLAN_REQUEST_TYPE.getCode())) {
			if((planRequestType.equalsIgnoreCase("FETCH") && (!vendorRequest.isAttributePresent("AC-COVER-DRIVER_COVER") || vendorRequest.isAttributePresent("AC-COVER-DRIVER_COVER")) 
				||  
			   (!vendorRequest.isAttributePresent("AC-COVER-DRIVER_COVER") && planRequestType.equalsIgnoreCase("BUY")))) {
					driverCover =  vendorResponse.getAttributeValueForCode("AC-PRODUCT_LL_DRIVER_TPPREMIUM").getValueAsDouble();	
					double premiumPayable = vendorResponse.getAttributeValueForCode("AC-PRODUCT_PREMIUM").getValueAsDouble();
					double serviceTax =  vendorResponse.getAttributeValueForCode("AC-SERVICE_TAX").getValueAsDouble();
					premiumPayable -= driverCover*1.18;
					serviceTax -= driverCover * 0.18;
					vendorResponse.getAttributeValueForCode("AC-PRODUCT_PREMIUM").setValue(df.format(premiumPayable));		
					vendorResponse.getAttributeValueForCode("AC-SERVICE_TAX").setValue(df.format(serviceTax));
			}
			
		}		
		return vendorResponse;	
		
	}
	/*private VendorResponse setCngLpgPremium(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		double cngODPremium = 0,cngTPPremium =0;
				
		if(vendorResponse.isAttributeValuePresentForCode("AC-PRODUCT_LPG_CNG_KIT_ODPREMIUM")) {
			cngODPremium = vendorResponse.getAttributeValueForCode("AC-PRODUCT_LPG_CNG_KIT_ODPREMIUM").getValueAsDouble();
		}
		if(vendorResponse.isAttributeValuePresentForCode("AC-PRODUCT_LPG_CNG_KIT_TPPREMIUM")) {
			cngTPPremium = vendorResponse.getAttributeValueForCode("AC-PRODUCT_LPG_CNG_KIT_TPPREMIUM").getValueAsDouble();
		}
		double totalPremium = cngODPremium + cngTPPremium ;
		addAttribute(vendorResponse, "AC-PRODUCT_LPG_CNG_KIT_PREMIUM", totalPremium);	
		return vendorResponse;
	}
	*/
	private VendorResponse calculatePremiumWithAddon(VendorRequest vendorRequest, VendorResponse vendorResponse){	
		DecimalFormat df = new DecimalFormat("###.##");
		double depreciationPremium=0 ,towingPremium = 0;
		if(vendorRequest.isAttributePresent("AC-COVER-ZERO_DEPRICIATION") && 
				vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-ZERO_DEPRICIATION").getValueAsBoolean()){
			 depreciationPremium =  vendorResponse.getAttributeValueForCode("AC-PRODUCT_ZERODEP_PREMIUM").getValueAsDouble();		
			 towingPremium =  vendorResponse.getAttributeValueForCode("AC-PRODUCT_TOWING_RELATED_PREMIUM").getValueAsDouble();
		}
		double premiumPayable = vendorResponse.getAttributeValueForCode("AC-PRODUCT_PREMIUM").getValueAsDouble();
		double serviceTax =  vendorResponse.getAttributeValueForCode("AC-SERVICE_TAX").getValueAsDouble();
		double totalPremimAfterDiscLoad = vendorResponse.getAttributeValueForCode("AC-GROSS_PREMIUM").getValueAsDouble();
		double totalAddon = depreciationPremium + towingPremium ;
		double taxOnAddon = 0;
		if(totalAddon >0){
			taxOnAddon = totalAddon*18/100;
		}
		serviceTax += taxOnAddon;
		totalPremimAfterDiscLoad += totalAddon;
		premiumPayable += totalAddon + taxOnAddon;
		if(totalAddon >0) {
			vendorResponse.getAttributeValueForCode("AC-PRODUCT_PREMIUM").setValue(df.format(premiumPayable));		
			vendorResponse.getAttributeValueForCode("AC-SERVICE_TAX").setValue(df.format(serviceTax));
			vendorResponse.getAttributeValueForCode("AC-GROSS_PREMIUM").setValue(df.format(totalPremimAfterDiscLoad));
		}
		return vendorResponse;
	}
	private Request calculateTotalSumInsured(VendorRequest vendorRequest,Request paymentRequest){	
		double electricalAccValue =0 ,nonElectricalAccValue =0,cngLpgExternallyFitValue = 0 ;
		if(vendorRequest.isAttributePresent("AC-COVER-ELECTRICAL_ACCESSORIES_VALUE") && 
				vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble()>0){
			electricalAccValue =  vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble();			
		}
		if(vendorRequest.isAttributePresent("AC-COVER-NON_ELECTRICAL_ACCESSORIES_VALUE") && 
				vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-NON_ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble()>0){
			nonElectricalAccValue =  vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-NON_ELECTRICAL_ACCESSORIES_VALUE").getValueAsDouble();			
		}
		if(vendorRequest.isAttributePresent("AC-CNG_OR_LPG_EXTERNALLY_FITTED_VALUE") && 
				vendorRequest.getUniqueInputQuoteForAttribute("AC-CNG_OR_LPG_EXTERNALLY_FITTED_VALUE").getValueAsDouble()>0){
			cngLpgExternallyFitValue =  vendorRequest.getUniqueInputQuoteForAttribute("AC-CNG_OR_LPG_EXTERNALLY_FITTED_VALUE").getValueAsDouble();			
		}
		double totalsumInsured = paymentRequest.getPolicy().getTotalSumInsured();
		totalsumInsured += electricalAccValue + nonElectricalAccValue+cngLpgExternallyFitValue ;
		paymentRequest.getPolicy().setTotalSumInsured(totalsumInsured);
		return paymentRequest;
	}
	
	
	public VendorResponse appendBasicInfo(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		vendorResponse.setEnquiryId(vendorRequest.getEnquiryId());
		vendorResponse.setProductId(vendorRequest.getProductId());
		vendorResponse.setCategoryId(vendorRequest.getCategoryId());
		vendorResponse.setVendorId(vendorRequest.getVendorId());
		vendorResponse.setVendorName(vendorRequest.getVendorName());
		vendorResponse.setQuoteKey("ITGI_MOTOR_" + AttributeUtil.generateRandom());
		return vendorResponse;

	}

	@Override
	public ObjectNode purchasePlan(VendorRequest vendorRequest) {
		ObjectNode policyJson = Json.newObject();
		try {	
			String partnerCode = propUtils.get("arka.vendor.itgi.partner_code");
			String partnerBranch = propUtils.get("arka.vendor.itgi.partner_branch");
			String partnerSubBranch = propUtils.get("arka.vendor.itgi.partner_sub_branch");
			List<PatchRequestVO> patchRequestVOList = new ArrayList<>();
			expiredTWPInceptionDate(vendorRequest);
			patchRequestVOList = categoryTransformationUtils.createRequestPatch(vendorRequest, "Request");
			JsonNode patchJsonNode = mapper.valueToTree(patchRequestVOList);
			JsonPatch jsonPatch = JsonPatch.fromJson(patchJsonNode);
			System.out.println("JsonPatch: " + mapper.writeValueAsString(jsonPatch));
			ObjectFactory objectFactory = new ObjectFactory();
			Request payRequest = objectFactory.createRequest();
			payRequest.setAccount(objectFactory.createRequestAccount());
			payRequest.setContact(objectFactory.createRequestContact());
			payRequest.setPolicy(objectFactory.createRequestPolicy());
			payRequest.setVehicle(objectFactory.createRequestVehicle());
			payRequest.setVehicleThirdParty(objectFactory.createRequestVehicleThirdParty());
			for(int i=0; i<10;i++){
				payRequest.getCoverage().add(objectFactory.createRequestCoverage());
			}
			payRequest.getPolicy().setExternalBranch(partnerBranch);
			payRequest.getPolicy().setExternalServiceConsumer(partnerCode);
			payRequest.getPolicy().setExternalSubBranch(partnerSubBranch);
			JsonNode orgJsonNode = mapper.valueToTree(payRequest);
			JsonNode patchedJsonNode = jsonPatch.apply(orgJsonNode);
			Request getPayRequest = mapper.treeToValue(patchedJsonNode, Request.class);
			ListIterator<Coverage> item = getPayRequest.getCoverage().listIterator();
			calculateTotalSumInsured(vendorRequest,getPayRequest);
			while (item.hasNext()) {
				Coverage vehicleItem = item.next();
				if (vehicleItem.getCode().equalsIgnoreCase("No Claim Bonus")) { 
					if(iffcoValidator.isNewVehicle(vendorRequest)) {
						item.remove();
					}
					if(vendorRequest.isAttributePresent(CarAttributeCode.AC_CLAIMED.getCode()) && 
							vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_CLAIMED.getCode()).getValue().equals("true")){
						item.remove();
					}
				}else if (vehicleItem.getCode().equalsIgnoreCase("Legal Liability to Driver") && 
						vendorRequest.getCategoryCode().equalsIgnoreCase(CategoryCode.BIKE.name())) {
					item.remove();
				}
			
				if (vehicleItem.getCode().equalsIgnoreCase("Depreciation Waiver") || vehicleItem.getCode().equalsIgnoreCase("Towing & Related")){
					if(!(vendorRequest.isAttributePresent("AC-COVER-ZERO_DEPRICIATION") && 
							vendorRequest.getUniqueInputQuoteForAttribute("AC-COVER-ZERO_DEPRICIATION").getValueAsBoolean())){
						item.remove();		
					}
				}			
			}
			iffcoValidator.validateMandatoryFields(getPayRequest,policyJson);
			if(JsonUtils.isValidField(policyJson, JsonUtils.ERROR)){
				return policyJson;
			}
			JAXBContext context = JAXBContext.newInstance(Request.class);
	        Marshaller jaxbMarshaller = context.createMarshaller();
			StringWriter sw = new StringWriter();
			jaxbMarshaller.marshal(getPayRequest, sw);
			String xmlString = sw.toString();
			System.out.println("getPayRequest..................." + xmlString);
			xmlString=xmlString.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");			
			
			policyJson.put(PolicyFields.CATEGORY_ID.getCode(), vendorRequest.getCategoryId());
			policyJson.put(PolicyFields.CATEGORY_CODE.getCode(), vendorRequest.getCategoryCode());
			policyJson.put(PolicyFields.VENDOR_ID.getCode(), vendorRequest.getVendorId());
			policyJson.put(PolicyFields.USER_ID.getCode(), vendorRequest.getUserId());
			policyJson.put(PolicyFields.PRODUCT_ID.getCode(), vendorRequest.getProductId());
			policyJson.put(PolicyFields.PROPOSAL_ID.getCode(), vendorRequest.getProposalId());
			policyJson.put(PolicyFields.POLICY_HOLDER_NAME.getCode(), vendorRequest.getPolicyHolderName());
			policyJson.put(PolicyFields.PREMIUM.getCode(), vendorRequest.getUniqueInputQuoteForAttribute("AC-PRODUCT_PREMIUM").getValue());
			policyJson.put(PolicyFields.SUM_ASSURED.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(CarAttributeCode.AC_PRODUCT_IDV.getCode()).getValue());
			policyJson.put(PolicyFields.VENDOR_CODE.getCode(), vendorRequest.getVendorCode());
			policyJson.put(PolicyFields.PRODUCT_CODE.getCode(), vendorRequest.getProductCode());
			policyJson.put("PARTNER_CODE", getPayRequest.getPolicy().getExternalServiceConsumer());
			policyJson.put(PolicyFields.RETURN_URL.getCode(),propUtils.get("arka.vendor.itgi.return.url"));
			policyJson.put("FORM_URL", propUtils.get("arka.vendor.itgi.payment"));
			policyJson.put("UNIQUE_QUOTEID", getPayRequest.getPolicy().getUniqueQuoteId());
			policyJson.put("XML_DATA", xmlString);
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			policyJson.put(JsonUtils.ERROR,FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
			return policyJson;
		}

		return policyJson;

	}
}
