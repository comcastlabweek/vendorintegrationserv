package com.arka.integrationserv.models.vendorplan.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.fasterxml.jackson.annotation.JsonIgnore;

import play.data.validation.ValidationError;

public class InputPolicyParams {

	private String categoryAttributeCode;

	private String value;

	private String additionalValue;

	private List<String> tag;

	public String getCategoryAttributeCode() {
		return categoryAttributeCode;
	}

	public void setCategoryAttributeCode(String categoryAttributeCode) {
		this.categoryAttributeCode = categoryAttributeCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getAdditionalValue() {
		return additionalValue;
	}

	public void setAdditionalValue(String additionalValue) {
		this.additionalValue = additionalValue;
	}

	public List<String> getTag() {
		return tag;
	}

	public void setTag(List<String> tag) {
		this.tag = tag;
	}

	@JsonIgnore
	public Integer getValueAsInt() {
		return Integer.parseInt(value);
	}

	@JsonIgnore
	public Long getValueAsLong() {
		return Long.parseLong(value);
	}

	@JsonIgnore
	public Double getValueAsDouble() {
		return Double.parseDouble(value);
	}

	@JsonIgnore
	public Boolean getValueAsBoolean() {
		return Boolean.parseBoolean(value);
	}

	@JsonIgnore
	public boolean isTagPresent(String tagName) {
		if (tag == null || tag.size() == 0) {
			return false;
		}

		return tag.stream().filter(tag -> tag.equalsIgnoreCase(tagName)).findAny().isPresent();

	}

	@JsonIgnore
	public List<ValidationError> validate() {
		List<ValidationError> errorList = new ArrayList<ValidationError>();
		if (StringUtils.isEmpty(categoryAttributeCode)) {
			errorList.add(new ValidationError(VendorQuoteConstants.ENQUIRY_CATEGORY_ATTRIBUTE_CODE.value(),
					FetchPlanMessage.ENQUIRY_CATEGORY_ATTRIBUTE_CODE_IS_EMPTY.value()));
		}
		if (StringUtils.isEmpty(value)) {
			errorList.add(new ValidationError(VendorQuoteConstants.ENQUIRY_VALUE.value(),
					FetchPlanMessage.ENQUIRY_VALUE_IS_EMPTY.value()));
		}

		if (errorList.size() > 0) {
			return errorList;
		}

		return null;
	}

	@Override
	public String toString() {
		return "InputQuoteParams [categoryAttributeCode=" + categoryAttributeCode + ", value=" + value
				+ ", additionalValue=" + additionalValue + ", tag=" + tag + "]";
	}

}
