package com.arka.integrationserv.models.vendorplan.request;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.fasterxml.jackson.annotation.JsonIgnore;

import play.data.validation.ValidationError;

public class VendorRequest {
	private String enquiryId;
	private String productId;
	private String productCode;
	private String categoryId;
	private String categoryCode;
	private String vendorId;
	private String vendorName;
	private String quoteKey;
	private String userId;
	private String proposalId;
	private String policyHolderName;
	private String vendorCode;

	private List<InputPolicyParams> input_quote_params;

	public String getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getQuoteKey() {
		return quoteKey;
	}

	public void setQuoteKey(String quoteKey) {
		this.quoteKey = quoteKey;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProposalId() {
		return proposalId;
	}

	public void setProposalId(String proposalId) {
		this.proposalId = proposalId;
	}

	

	public String getPolicyHolderName() {
		return policyHolderName;
	}

	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public List<InputPolicyParams> getInput_quote_params() {
		return input_quote_params;
	}

	public void setInput_quote_params(List<InputPolicyParams> input_quote_params) {
		this.input_quote_params = input_quote_params;
	}
	
	private static final Logger logger = LoggerFactory.getLogger(VendorRequest.class);

	public boolean isAttributePresent(String attributeCode) {
		return input_quote_params.stream()
				.filter(input_quote_params -> input_quote_params.getCategoryAttributeCode().equals(attributeCode))
				.findAny().isPresent();
	}
	
	@JsonIgnore
	public void addAttributeValue(InputPolicyParams attributeValue) {
		this.input_quote_params.add(attributeValue);
	}

	public List<InputPolicyParams> getInputQuoteForAttribute(String attributeCode) {

		return input_quote_params.stream().filter(
				input_quote_params -> input_quote_params.getCategoryAttributeCode().equalsIgnoreCase(attributeCode))
				.collect(Collectors.toList());

	}

	public List<InputPolicyParams> getInputQuoteForTag(String attributeTag) {

		return input_quote_params.stream().filter(input_quote_params -> input_quote_params.isTagPresent(attributeTag))
				.collect(Collectors.toList());

	}

	public InputPolicyParams getUniqueInputQuoteForAttribute(String attributeCode) {
		if(isAttributePresent(attributeCode)) {
			return input_quote_params.stream()
					.filter(input_quote_params -> input_quote_params.getCategoryAttributeCode().equalsIgnoreCase(attributeCode))
					.findAny().get();		
			}else {
				logger.error("Attribute Code" + attributeCode + "not Present");
				return null;
			}
		
	}

	public List<InputPolicyParams> removeInputQuoteParam(String attributeCode) {
		if (isAttributePresent(attributeCode)) {
			List<InputPolicyParams> filteredInputQuoteParams = input_quote_params.stream()
					.filter(input_quote_params -> !input_quote_params.getCategoryAttributeCode()
							.equalsIgnoreCase(attributeCode))
					.collect(Collectors.toList());
			this.input_quote_params = filteredInputQuoteParams;
		}
		return input_quote_params;
	}

	public boolean isQuoteKeyEmpty() {
		return quoteKey == null || quoteKey.trim().isEmpty();
	}

	public List<ValidationError> validate() {
		List<ValidationError> errorList = new ArrayList<ValidationError>();
		if (enquiryId == null || enquiryId.toString().trim().isEmpty()) {
			errorList.add(new ValidationError(VendorQuoteConstants.ENQUIRY_RESPONSE_ID.value(),
					FetchPlanMessage.ENQUIRY_ID_EMPTY.value()));
		}
		if (StringUtils.isEmpty(productId)) {
			errorList.add(new ValidationError(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(),
					FetchPlanMessage.PRODUCT_ID_EMPTY.value()));
		}
		if (StringUtils.isEmpty(productCode)) {
			errorList.add(new ValidationError(VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
					FetchPlanMessage.PRODUCT_CODE_EMPTY.value()));
		}
		if (StringUtils.isEmpty(categoryId)) {
			errorList.add(new ValidationError(VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(),
					FetchPlanMessage.CATEGORY_ID_EMPTY.value()));
		}
		if (StringUtils.isEmpty(categoryCode)) {
			errorList.add(new ValidationError(VendorQuoteConstants.CATEGORY_RESPONSE_CODE.value(),
					FetchPlanMessage.CATEGORY_CODE_EMPTY.value()));
		}

		if (CollectionUtils.isEmpty(input_quote_params)) {
			errorList.add(new ValidationError(VendorQuoteConstants.INPUT_QUOTE.value(),
					FetchPlanMessage.QUOTE_INFO_EMPTY.value()));
		} else {
			input_quote_params.stream().map(input_quote_param -> input_quote_param.validate()).filter(
					input_quote_param_error -> input_quote_param_error != null && !input_quote_param_error.isEmpty())
					.forEach(input_quote_param_error -> {
						errorList.addAll(input_quote_param_error);
					});
		}

		if (errorList.size() > 0) {
			return errorList;
		}

		return null;
	}

}
