package com.arka.integrationserv.models.vendorplan.request;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class VendorResponse {

	private Long id;

	private String productId;

	private String categoryId;

	private String enquiryId;

	private String vendorId;

	private String vendorName;

	private String quoteKey;

	private List<AttributeValue> attributeValueList = new ArrayList<AttributeValue>();

	private ObjectNode additionalInfoJson = Json.newObject();

	private ObjectNode product;

	private ObjectNode errorJson = Json.newObject();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public ObjectNode getProduct() {
		return product;
	}

	public void setProduct(ObjectNode product) {
		this.product = product;
	}

	public String getQuoteKey() {
		return quoteKey;
	}

	public void setQuoteKey(String quoteKey) {
		this.quoteKey = quoteKey;
	}

	public List<AttributeValue> getAttributeValueList() {
		return attributeValueList;
	}

	public void setAttributeValueList(List<AttributeValue> attributeValueList) {
		this.attributeValueList = attributeValueList;
	}

	@JsonIgnore
	public void addAttributeValue(AttributeValue attributeValue) {
		this.attributeValueList.add(attributeValue);
	}

	public ObjectNode getAdditionalInfoJson() {
		return additionalInfoJson;
	}

	public void setAdditionalInfoJson(ObjectNode additionalInfoJson) {
		this.additionalInfoJson = additionalInfoJson;
	}

	public void addToAdditionalInfoJson(ObjectNode additionalInfo) {
		additionalInfo.fields().forEachRemaining(additionalInfoObj -> {
			String additionalInfoKey = additionalInfoObj.getKey();
			JsonNode additionalInfoJson = additionalInfoObj.getValue();
			this.additionalInfoJson.set(additionalInfoKey, additionalInfoJson);

		});
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public ObjectNode getErrorJson() {
		return errorJson;
	}

	public void setErrorJson(ObjectNode errorJson) {
		this.errorJson = errorJson;
	}

	@JsonIgnore
	public boolean isErrorPresent() {
		return errorJson.size() > 0;
	}

	@JsonIgnore
	public boolean isAdditionalInfoPresent() {
		return additionalInfoJson.size() > 0;
	}

	@JsonIgnore
	public boolean isAttributeListPresent() {
		return attributeValueList != null && attributeValueList.size() > 0;
	}

	@JsonIgnore
	public boolean isAttributeValuePresentForCode(String categoryAttributeCode) {
		return isAttributeValuePresentinResponse(categoryAttributeCode);
	}

	@JsonIgnore
	public boolean isAttributeValuePresentinResponse(String categoryAttributeCode) {
		if (CollectionUtils.isNotEmpty(attributeValueList)) {
			return attributeValueList.stream()
					.filter(attributeValue -> attributeValue.getCategoryAttributeCode().equals(categoryAttributeCode))
					.findAny().isPresent();
		}
		return false;
	}

	@JsonIgnore
	public AttributeValue getAttributeValueForCode(String categoryAttributeCode) {
		if (isAttributeValuePresentinResponse(categoryAttributeCode)) {
			return attributeValueList.stream()
					.filter(attributeValue -> attributeValue.getCategoryAttributeCode().equals(categoryAttributeCode))
					.findAny().get();
		}
		return null;
	}

	@JsonIgnore
	public void removeAttributeValueForCode(String categoryAttributeCode) {
		if (isAttributeValuePresentinResponse(categoryAttributeCode)) {
			attributeValueList = attributeValueList.stream()
					.filter(attributeValue -> !attributeValue.getCategoryAttributeCode().equals(categoryAttributeCode))
					.collect(Collectors.toList());
		}
	}

	@JsonIgnore
	public void setAttributeValueForCode(String categoryAttributeCode, String value) {
		AttributeValue attributeValue = getAttributeValueForCode(categoryAttributeCode);
		if (attributeValue != null) {
			attributeValue.setValue(value);
		} else {
		}

	}

	public JsonNode formatResponse() {

		ObjectNode vendorResponseJson = Json.newObject();
		try {

			if (isErrorPresent()) {
				return getErrorJson();
			}

			vendorResponseJson.put(VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), getProductId());
			vendorResponseJson.put(VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(), getCategoryId());
			vendorResponseJson.put(VendorQuoteConstants.ENQUIRY_RESPONSE_ID.value(), getEnquiryId());
			vendorResponseJson.put(VendorQuoteConstants.VENDOR_RESPONSE_ID.value(), getVendorId());
			vendorResponseJson.put(VendorQuoteConstants.VENDOR_RESPONSE_NAME.value(), getVendorName());
			vendorResponseJson.set(VendorQuoteConstants.PRODUCT.value(), getProduct());
			vendorResponseJson.put("quoteKey", getQuoteKey());
			ObjectNode attributeJson = vendorResponseJson.putObject("attributes");
			if (isAttributeListPresent()) {
				attributeValueList.stream().forEach(attributeValue -> {
					attributeJson.set(attributeValue.getCategoryAttributeCode(), attributeValue.formatResponse());
					;
				});
			}

			if (isAdditionalInfoPresent()) {
				additionalInfoJson.fields().forEachRemaining(additionalInfo -> {
					String additionalInfoKey = additionalInfo.getKey();
					JsonNode additionalInfoJson = additionalInfo.getValue();
					vendorResponseJson.set(additionalInfoKey, additionalInfoJson);

				});
			}

			return vendorResponseJson;

		} catch (Exception e) {
			return Json.toJson(this);
		}

	}

}
