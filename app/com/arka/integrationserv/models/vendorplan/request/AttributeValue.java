package com.arka.integrationserv.models.vendorplan.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.arka.common.utils.JsonUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;

public class AttributeValue {

	private String categoryAttributeCode;

	private String value;

	private String tags;
	
	private String displayName;
	
	private Integer weight;
	
	private String glossaryDescription;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTags() {
		return tags;
	}
	
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public void setTags(String tags) {

		this.tags = tags.toLowerCase(Locale.ENGLISH);

	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getGlossaryDescription() {
		return glossaryDescription;
	}

	public void setGlossaryDescription(String glossaryDescription) {
		this.glossaryDescription = glossaryDescription;
	}

	@JsonIgnore
	public List<String> getTagsAsList() {

		if (tags != null && !tags.trim().isEmpty()) {
			return Arrays.asList(tags.split(","));
		}
		return new ArrayList<String>();

	}

	@JsonIgnore
	public void setTagsAsList(List<String> tagNameList) {
		if (tagNameList != null && tagNameList.size() > 0) {
			this.tags = String.join(",", tagNameList);
		}
	}

	public String getCategoryAttributeCode() {
		return categoryAttributeCode;
	}

	public void setCategoryAttributeCode(String categoryAttributeCode) {
		this.categoryAttributeCode = categoryAttributeCode;
	}

	@JsonIgnore
	public Integer getValueAsInt() {
		return Integer.parseInt(value);
	}

	@JsonIgnore
	public String getValueAsStr() {
		return value;
	}

	@JsonIgnore
	public Long getValueAsLong() {
		return Long.parseLong(value);
	}

	@JsonIgnore
	public Double getValueAsDouble() {
		return Double.parseDouble(value);
	}

	@JsonIgnore
	public boolean getValueAsBoolean() {
		return Boolean.parseBoolean(value);
	}

	@JsonIgnore
	public ObjectNode formatResponse() {

		ObjectNode attributeValueJson = (ObjectNode) Json.toJson(this);
		attributeValueJson.remove("categoryAttributeCode");
		attributeValueJson.remove("groupattributeId");
		attributeValueJson.remove("enquiryResponseId");
		attributeValueJson.remove("id");

		String tags = this.tags;
		ArrayNode tagJsonArr = Json.newArray();
		if (tags != null && !tags.trim().isEmpty()) {
			Arrays.asList(tags.split(",")).forEach(tagElement -> {
				tagJsonArr.add(tagElement);
			});
		}
		attributeValueJson.set("tags", tagJsonArr);

		attributeValueJson = removeIfFieldIsEmpty(attributeValueJson, "deductible");
		attributeValueJson = removeIfFieldIsEmpty(attributeValueJson, "description");
		return attributeValueJson;
	}

	private ObjectNode removeIfFieldIsEmpty(ObjectNode jsonObj, String key) {
		if (JsonUtils.isValidField(jsonObj, key)) {
			JsonNode value = jsonObj.get(key);

			if (value == null || value.asText().equalsIgnoreCase("null") || value.asText().trim().isEmpty()) {
				jsonObj.remove(key);
			}
		} else {
			jsonObj.remove(key);
		}
		return jsonObj;
	}
}
