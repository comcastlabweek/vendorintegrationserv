package com.arka.integrationserv.models.vendorplan;

import com.arka.integrationserv.models.vendorplan.insurance.health.StarHealthInsuranceConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.integrationserv.models.vendorplan.insurance.health.IffcoTokioHealthInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.health.ReligareNoClaimBonusSuper;
import com.arka.integrationserv.models.vendorplan.insurance.motor.BajajAllianzInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.HDFCERGOMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.IffcoTokioInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.LibertyVideoconMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.RoyalSundaramMotorInsuranceConnector;
import com.arka.integrationserv.models.vendorplan.insurance.motor.USGICMotorInsuranceConnector;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import play.inject.Injector;

@Singleton
public class VendorConnectorFactory {

	@Inject
	Injector injector;

	public static final Logger logger = LoggerFactory.getLogger(VendorConnectorFactory.class);

	public VendorConnector createVendorConnector(String productCode, String categoryCode) {

		logger.debug("fetching vendor connector for : " + productCode + ", " + categoryCode);

		// check for validity of productCode

		switch (categoryCode) {
		case "CAR":
			switch (productCode) {
				case "RS_CAR_1":
					return injector.instanceOf(RoyalSundaramMotorInsuranceConnector.class);
				case "ITGI_CAR_1":
					return injector.instanceOf(IffcoTokioInsuranceConnector.class);
				case "USGI_CAR_1":
					return injector.instanceOf(USGICMotorInsuranceConnector.class);
				case "BAGIC_CAR_1":
					return injector.instanceOf(BajajAllianzInsuranceConnector.class);
				case "HDFCERGO_CAR_1":
					return injector.instanceOf(HDFCERGOMotorInsuranceConnector.class);
//				case "UIIC_CAR_1":
//					return injector.instanceOf(UIICMotorInsuranceConnector.class);	
				case "LVGI_CAR_1":
					return injector.instanceOf(LibertyVideoconMotorInsuranceConnector.class);
				default:
					return null;
			}
		case "BIKE":
			switch (productCode) {
				case "ITGI_BIKE_1":
					return injector.instanceOf(IffcoTokioInsuranceConnector.class);
				case "RS_BIKE_1":
					return injector.instanceOf(RoyalSundaramMotorInsuranceConnector.class);
				case "USGI_BIKE_1":
					return injector.instanceOf(USGICMotorInsuranceConnector.class);
				case "BAGIC_BIKE_1":
					return injector.instanceOf(BajajAllianzInsuranceConnector.class);
				case "HDFCERGO_BIKE_1":
					return injector.instanceOf(HDFCERGOMotorInsuranceConnector.class);
				case "LVGI_BIKE_1":
					return injector.instanceOf(LibertyVideoconMotorInsuranceConnector.class);
				default:
					return null;
			}
		
		case "HEALTH":
			switch (productCode) {
				case "REL_HEALTH_1":
					return injector.instanceOf(ReligareNoClaimBonusSuper.class);			
				case "ITGI_HEALTH_1":
					return injector.instanceOf(IffcoTokioHealthInsuranceConnector.class);
				case "STAR_HEALTH_4":
					return injector.instanceOf(StarHealthInsuranceConnector.class);
				case "STAR_HEALTH_2":
					return injector.instanceOf(StarHealthInsuranceConnector.class);
				case "STAR_HEALTH_3":
					return injector.instanceOf(StarHealthInsuranceConnector.class);
				case "STAR_HEALTH_1":
					return injector.instanceOf(StarHealthInsuranceConnector.class);
				default:
					return null;
			}

		default:
			return null;

		}

	}

}
