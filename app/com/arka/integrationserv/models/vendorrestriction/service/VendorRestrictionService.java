package com.arka.integrationserv.models.vendorrestriction.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.integrationserv.models.vendorrestriction.VendorRestriction;
import com.arka.integrationserv.models.vendorrestriction.service.Impl.VendorRestrictionServiceImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(VendorRestrictionServiceImpl.class)
public interface VendorRestrictionService<ID> {
	String ALL_VENDOR_RESTRICTION_CACHE_KEY_SUFFIX = "arka.catalog.vendor.restriction.all";
	
    
	public void saveVendorRestriction(VendorRestriction vendorRestriction) throws ArkaValidationException;

	public boolean updateVendorRestriction(VendorRestriction vendorRestriction) throws ArkaValidationException;
	
	public CompletionStage<VendorRestriction> findVendorRestrictionById(ID id);
    
	public CompletionStage<Collection<VendorRestriction>> findVendorRestrictions(Map<String, List<String>> criteriaMap);
	
	public boolean deleteVendorRestrictionById(ID id) throws ArkaValidationException;
	
	public boolean deleteVendorRestrictions(Map<String, List<String>> deleteQuery) throws ArkaValidationException;
	
	public long getVendorRestrictionCount();
	
	public long getVendorRestrictionCount(Map<String, List<String>> criteriaMap) throws ArkaValidationException;
}
