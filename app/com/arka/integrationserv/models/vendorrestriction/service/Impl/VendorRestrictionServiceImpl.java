package com.arka.integrationserv.models.vendorrestriction.service.Impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import org.apache.commons.collections4.MapUtils;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorrestriction.VendorRestriction;
import com.arka.integrationserv.models.vendorrestriction.constant.VendorRestrictionErrorMsg;
import com.arka.integrationserv.models.vendorrestriction.constant.VendorRestrictionFieldNames;
import com.arka.integrationserv.models.vendorrestriction.repository.VendorRestrictionRepository;
import com.arka.integrationserv.models.vendorrestriction.service.VendorRestrictionService;
import com.google.inject.Inject;

public class VendorRestrictionServiceImpl implements VendorRestrictionService<String> {

	@Inject
	private VendorRestrictionRepository vendorRestRepo;

	@Inject
	private CacheService cache;

	@Override
	public void saveVendorRestriction(VendorRestriction vendorRestriction) throws ArkaValidationException {

		Map<String, List<String>> errors = validate(vendorRestriction);
		if (MapUtils.isNotEmpty(errors)) {

			throw new ArkaValidationException(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_VALIDATION_FAILED.value(),
					errors);
		}
		vendorRestRepo.insert(vendorRestriction);
		cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_RESTRICTION.value());
		cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_RESTRICTION.value() + "."
				+ VendorRestrictionService.ALL_VENDOR_RESTRICTION_CACHE_KEY_SUFFIX);
	}

	@Override
	public boolean updateVendorRestriction(VendorRestriction restriction) throws ArkaValidationException {

		Map<String, List<String>> errors = validate(restriction);
		if (MapUtils.isNotEmpty(errors)) {
			throw new ArkaValidationException(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_VALIDATION_FAILED.value(),
					errors);
		}
		boolean updated = vendorRestRepo.updateOne(restriction);
		if (updated) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_RESTRICTION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_RESTRICTION.value() + "."
					+ VendorRestrictionService.ALL_VENDOR_RESTRICTION_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_RESTRICTION.value() + "." + restriction.getId());
		}
		return updated;
	}

	@Override
	public CompletionStage<VendorRestriction> findVendorRestrictionById(String id) {
		return cache
				.getOrElse(CacheService.getCacheKey(VendorCollectionNames.VENDOR_RESTRICTION.value(), null, id), () -> {
						VendorRestriction vendorRestriction = vendorRestRepo.findOneById(id);
						if (vendorRestriction != null) {
							return JsonUtils.stringify(vendorRestriction);
						}
						return JsonUtils.stringify(JsonUtils.newJsonObject());
				}).thenApplyAsync(json -> {
					return JsonUtils.fromJson(json, VendorRestriction.class);
				});
	}

	@Override
	public CompletionStage<Collection<VendorRestriction>> findVendorRestrictions(
			Map<String, List<String>> criteriaMap) {

		String cacheKeySuffix = "";
		if (criteriaMap == null || criteriaMap.isEmpty())
			cacheKeySuffix = ALL_VENDOR_RESTRICTION_CACHE_KEY_SUFFIX;

		return cache.getOrElse(
				CacheService.getCacheKey(VendorCollectionNames.VENDOR_RESTRICTION.value(), criteriaMap, cacheKeySuffix),
				() -> {
						return JsonUtils.stringify(vendorRestRepo.findAll(criteriaMap));
				}).thenApplyAsync(vendorRestrictionJson -> {
					return JsonUtils.jsonToList(vendorRestrictionJson, VendorRestriction.class);
				});
	}

	@Override

	public boolean deleteVendorRestrictionById(String id) throws ArkaValidationException {
		boolean deleted = vendorRestRepo.deleteOneById(id);
		if (deleted) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_RESTRICTION.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_RESTRICTION.value() + "."
					+ VendorRestrictionService.ALL_VENDOR_RESTRICTION_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_RESTRICTION.value() + "." + id);
		}
		return deleted;
	}

	@Override
	public boolean deleteVendorRestrictions(Map<String, List<String>> deleteQuery) throws ArkaValidationException {
		boolean deleted = vendorRestRepo.deleteMany(deleteQuery);
		if (deleted) {
			cache.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_RESTRICTION.value());
		}
		return deleted;
	}

	@Override
	public long getVendorRestrictionCount() {

		return vendorRestRepo.count();
	}

	@Override
	public long getVendorRestrictionCount(Map<String, List<String>> criteriaMap) throws ArkaValidationException {

		return vendorRestRepo.count(criteriaMap);
	}

	public Map<String, List<String>> validate(VendorRestriction vendorRestriction) throws ArkaValidationException {
		Map<String, List<String>> errorMap = new LinkedHashMap<>();

		if (vendorRestriction == null) {
			errorMap.put(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_ERROR.value(),
					Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NULL.value()));
			return errorMap;
		}
		fillVendorRestrictionCodeError(errorMap, vendorRestriction);

		fillVendorRestrictionNameError(errorMap, vendorRestriction);

		fillVendorRestrictionStatusError(errorMap, vendorRestriction);

		return errorMap;
	}

	private void fillVendorRestrictionStatusError(Map<String, List<String>> errorMap,
			VendorRestriction vendorRestriction) {

		if (vendorRestriction.getStatus() == null) {
			errorMap.put(VendorRestrictionFieldNames.STATUS.value(),
					Arrays.asList(VendorRestrictionErrorMsg.STATUS_NULL.value()));
		} else if (!Status.ACTIVE.equals(vendorRestriction.getStatus())
				&& !Status.INACTIVE.equals(vendorRestriction.getStatus())) {
			errorMap.put(VendorRestrictionFieldNames.STATUS.value(),
					Arrays.asList(VendorRestrictionErrorMsg.STATUS_INVALID.value()));
		}
	}

	private void fillVendorRestrictionCodeError(Map<String, List<String>> errorMap, VendorRestriction vendorRestriction)
			throws ArkaValidationException {
		if (vendorRestriction.getCode() == null || vendorRestriction.getCode().trim().isEmpty()) {
			errorMap.put(VendorRestrictionFieldNames.CODE.value(),
					Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_CODE_NULL.value()));
		} else {
			Map<String, List<String>> query = new HashMap<>();
			query.put(VendorRestrictionFieldNames.CODE.value(), Arrays.asList(vendorRestriction.getCode()));
			try {
				long count = getVendorRestrictionCount(query);
				if (vendorRestriction.getId() != null) {
					if (count > 1)
						errorMap.put(VendorRestrictionFieldNames.CODE.value(),
								Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_CODE_ALREADY_EXIST.value()));
				} else {
					if (count > 0)
						errorMap.put(VendorRestrictionFieldNames.CODE.value(),
								Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_CODE_ALREADY_EXIST.value()));
				}
			} catch (ArkaValidationException e) {
				errorMap.putAll(e.getErrors());
			}
		}
	}

	private void fillVendorRestrictionNameError(Map<String, List<String>> errorMap, VendorRestriction vendorRestriction)
			throws ArkaValidationException {
		if (vendorRestriction.getName() == null || vendorRestriction.getName().trim().isEmpty()) {
			errorMap.put(VendorRestrictionFieldNames.NAME.value(),
					Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NAME_NULL.value()));
		} else {
			Map<String, List<String>> query = new HashMap<>();
			query.put(VendorRestrictionFieldNames.NAME.value(), Arrays.asList(vendorRestriction.getName()));
			try {
				long count = getVendorRestrictionCount(query);
				if (vendorRestriction.getId() != null) {
					if (count > 1)
						errorMap.put(VendorRestrictionFieldNames.NAME.value(),
								Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NAME_ALREADY_EXIST.value()));
				} else {
					if (count > 0)
						errorMap.put(VendorRestrictionFieldNames.NAME.value(),
								Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NAME_ALREADY_EXIST.value()));
				}
			} catch (ArkaValidationException e) {
				errorMap.putAll(e.getErrors());
			}
		}
	}

}