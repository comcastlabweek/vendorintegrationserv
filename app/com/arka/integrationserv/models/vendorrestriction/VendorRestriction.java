package com.arka.integrationserv.models.vendorrestriction;

import java.io.Serializable;
import java.util.List;

import com.arka.common.models.base.BaseModel;
import com.arka.common.models.constant.Status;
import com.arka.common.models.tag.Tag;
import com.arka.integrationserv.models.vendorrestriction.constant.ConstraintType;
import com.arka.integrationserv.models.vendorrestriction.constant.DataType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class VendorRestriction extends BaseModel<String> implements Serializable {

	private static final long serialVersionUID = -7747924811464811936L;

	@JsonProperty(value = "vendor_id")
	private String vendorId;

	@JsonProperty(value = "vendor_product")
	private List<String> vendorProduct;

	@JsonProperty(value = "code")
	private String code;

	@JsonProperty(value = "name")
	private String name;

	@JsonProperty(value = "tags")
	private List<Tag> tags;

	@JsonProperty(value = "description")
	private String description;

	@JsonProperty(value = "data_type")
	private DataType dataType;

	@JsonProperty(value = "constraint_type")
	private ConstraintType constraintType;

	@JsonProperty(value = "constraint_value")
	private ArrayNode constraintValue;

	@JsonProperty(value = "status")
	private Status status;

	@JsonProperty(value = "flags")
	private Integer flags;

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public List<String> getVendorProduct() {
		return vendorProduct;
	}

	public void setVendorProduct(List<String> vendorProduct) {
		this.vendorProduct = vendorProduct;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public ConstraintType getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(ConstraintType constraintType) {
		this.constraintType = constraintType;
	}

	public ArrayNode getConstraintValue() {
		return constraintValue;
	}

	public void setConstraintValue(ArrayNode constraintValue) {
		this.constraintValue = constraintValue;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getFlags() {
		return flags;
	}

	public void setFlags(Integer flags) {
		this.flags = flags;
	}

	@Override
	public String toString() {
		return "VendorRestriction [vendorId=" + vendorId + ", vendorProduct=" + vendorProduct + ", code=" + code
				+ ", name=" + name + ", tags=" + tags + ", description=" + description + ", dataType=" + dataType
				+ ", constraintType=" + constraintType + ", constraintValue=" + constraintValue + ", status=" + status
				+ ", flags=" + flags + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((constraintType == null) ? 0 : constraintType.hashCode());
		result = prime * result + ((constraintValue == null) ? 0 : constraintValue.hashCode());
		result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((flags == null) ? 0 : flags.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((vendorId == null) ? 0 : vendorId.hashCode());
		result = prime * result + ((vendorProduct == null) ? 0 : vendorProduct.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendorRestriction other = (VendorRestriction) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (constraintType != other.constraintType)
			return false;
		if (constraintValue == null) {
			if (other.constraintValue != null)
				return false;
		} else if (!constraintValue.equals(other.constraintValue))
			return false;
		if (dataType != other.dataType)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (flags == null) {
			if (other.flags != null)
				return false;
		} else if (!flags.equals(other.flags))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (status != other.status)
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (vendorId == null) {
			if (other.vendorId != null)
				return false;
		} else if (!vendorId.equals(other.vendorId))
			return false;
		if (vendorProduct == null) {
			if (other.vendorProduct != null)
				return false;
		} else if (!vendorProduct.equals(other.vendorProduct))
			return false;
		return true;
	}


}
