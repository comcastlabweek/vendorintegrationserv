package com.arka.integrationserv.models.vendorrestriction.constant;

public enum DataType {
	STRING,INT,NUMBER,RANGE,ENUM,DATE
}
