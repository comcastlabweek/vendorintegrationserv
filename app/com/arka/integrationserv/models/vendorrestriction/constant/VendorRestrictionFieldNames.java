package com.arka.integrationserv.models.vendorrestriction.constant;

public enum VendorRestrictionFieldNames {
   
    CODE("code"),
	
	NAME("name"),
	
	VENDOR_ID("vendor_id"),
	
	VENDOR_PRODUCT("vendor_product"),
	
	DATATYPE("data_type"),
	
	CONSTRAINTTYPE("constraint_Type"),
	
	CONSTRAINTVALUE("constraint_value"),
	
	DESCRIPTION("description"),
	
	STATUS("status"),
	
	TAGS("tags"),
	
	WEIGHT( "weight"),
	;
	private String value;

	private VendorRestrictionFieldNames (String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

}
