package com.arka.integrationserv.models.vendorrestriction.constant;

import java.time.LocalDate;

public enum ConstraintType {
    OTHER("other"),
    RANGE("range"),
    FIELD("field"),
    AUTOCOMPLETE("autocomplete"),
    SINGLESELECT("single_select"),
    RANGE_ENUM(String.valueOf(LocalDate.now().getYear())),
    MAP("map");
    
    
private String value;
	
	ConstraintType(String value){
		this.value=value;
	}
	
	public String value() {
		return this.value;
	}
}
