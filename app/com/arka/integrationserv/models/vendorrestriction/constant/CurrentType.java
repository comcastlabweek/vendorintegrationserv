package com.arka.integrationserv.models.vendorrestriction.constant;

public enum CurrentType {

	CURRENT_YEAR("current_year"), 
	CURRENT_MONTH("current_month"),
	CURRENT_DATE("current_date"),
	;
   private String value;
	
	private CurrentType(String value){
		
		this.value=value;
	}
	public String value(){
		return value;
	}
}
