package com.arka.integrationserv.models.vendorrestriction.constant;

public enum VendorRestrictionErrorMsg {
   
    VENDOR_RESTRICTION_ERROR("errors"),
	
	VENDOR_RESTRICTION_NOT_FOUND("vendor.restriction.not.found"),
	
	VENDOR_RESTRICTION_NOT_UPDATED("vendor.restriction.not.updated"),
	
	PATCH_OPERATION_NOT_SUPPORTED("patch.operation.not.supported"),
	
	VENDOR_RESTRICTION_NOT_UPDATED_OP_OR_PATH_MISSING_IN_PATCH_DATA(
			"vendor.restriction.not.updated.OP.or.PATH.missing.in.PATCH.data"),
	
	VENDOR_RESTRICTION_VALIDATION_FAILED("vendor.restriction.validation.failed"),
	
	VENDOR_RESTRICTION_NULL("vendor.restriction.null"),
	
	STATUS_NULL("vendor.restriction.status.null"),

	STATUS_INVALID("vendor.restriction.status.invalid"),
	
	VENDOR_RESTRICTION_CODE_ALREADY_EXIST("vendor.restriction.code.already.exist"),
	
	VENDOR_RESTRICTION_CODE_NULL("vendor.restriction.code.null"),
	
	VENDOR_RESTRICTION_NAME_NULL("vendor.restriction.name.null"),
	
	VENDOR_RESTRICTION_NAME_ALREADY_EXIST("vendor.restriction.name.already.exist"),
	
    VENDOR_RESTRICTION_CONSTRAINT_TYPE_INVALID("vendor.restriction.constraint.type.invalid"),
	
	VENDOR_RESTRICTION_DATA_TYPE_INVALID("vendor.restriction.data.type.invalid"),
	
	VENDOR_RESTRICTION_VENDOR_ID("vendor.restriction.vendor.id.invalid"),
	
	VENDOR_RESTRICTION_WEIGHT("vendro.restriction.weight.invalid"),
	
	INVALID_PATH("invalid.path"),
	
	MESSAGE("message"),
	;
	private String value;

	private VendorRestrictionErrorMsg(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
