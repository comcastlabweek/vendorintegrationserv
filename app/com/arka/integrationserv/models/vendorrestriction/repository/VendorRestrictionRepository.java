package com.arka.integrationserv.models.vendorrestriction.repository;

import com.arka.common.models.base.repository.BaseRepository;
import com.arka.integrationserv.models.vendorrestriction.VendorRestriction;
import com.arka.integrationserv.models.vendorrestriction.repository.Impl.VendorRestrictionRepositoryImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(VendorRestrictionRepositoryImpl.class)
public interface VendorRestrictionRepository extends BaseRepository<VendorRestriction> {

}
