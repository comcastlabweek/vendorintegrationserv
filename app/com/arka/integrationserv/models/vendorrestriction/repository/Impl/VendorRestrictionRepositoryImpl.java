package com.arka.integrationserv.models.vendorrestriction.repository.Impl;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.base.repository.impl.MongoBaseRepositoryImpl;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorrestriction.VendorRestriction;
import com.arka.integrationserv.models.vendorrestriction.constant.ConstraintType;
import com.arka.integrationserv.models.vendorrestriction.constraint.ConstraintValue;
import com.arka.integrationserv.models.vendorrestriction.constraint.ConstraintValueForMap;
import com.arka.integrationserv.models.vendorrestriction.constraint.ConstraintValueForRangeYear;
import com.arka.integrationserv.models.vendorrestriction.constraint.ConstraintValueForSingleSelect;
import com.arka.integrationserv.models.vendorrestriction.repository.VendorRestrictionRepository;
import com.fasterxml.jackson.databind.node.ArrayNode;

import play.libs.Json;

public class VendorRestrictionRepositoryImpl extends MongoBaseRepositoryImpl<VendorRestriction>
		implements VendorRestrictionRepository {

	public VendorRestrictionRepositoryImpl() {
		super(VendorCollectionNames.VENDOR_RESTRICTION.value());
	}

	@Override
	public void insert(VendorRestriction vendorRestriction) throws ArkaValidationException {
		vendorRestriction.setTags(getTagsWithId(vendorRestriction.getTags()));
		setConstraintValueWithId(vendorRestriction);
		super.insert(vendorRestriction);
	}

	@Override
	public boolean updateOne(VendorRestriction vendorRestriction) throws ArkaValidationException {
		vendorRestriction.setTags(getTagsWithId(vendorRestriction.getTags()));
		setConstraintValueWithId(vendorRestriction);
		return super.updateOne(vendorRestriction);
	}

	public ArrayNode setConstraintValueWithId(VendorRestriction vendorRestriction) {
		ArrayNode constraintsNode = Json.newArray();

		if (vendorRestriction.getConstraintValue() != null) {

			if (ConstraintType.RANGE.equals(vendorRestriction.getConstraintType())) {
				vendorRestriction.getConstraintValue().forEach(val -> {
					ConstraintValue constraintValue = JsonUtils.fromJson(val, ConstraintValue.class);

					if (constraintValue.getRange_id()=="" || constraintValue.getRange_id() == null) {
						
						constraintValue.setRange_id(generateNewId());

					}
					constraintsNode.add(JsonUtils.toJson(constraintValue));
				});
				vendorRestriction.setConstraintValue(constraintsNode);

			}else if(ConstraintType.SINGLESELECT.equals(vendorRestriction.getConstraintType())){
				  
				  vendorRestriction.getConstraintValue().forEach(val->{
						
						ConstraintValueForSingleSelect constraintValue=JsonUtils.fromJson(val,ConstraintValueForSingleSelect.class);
						if (constraintValue.getSingleSelectId()=="" || constraintValue.getSingleSelectId() == null) {
							
							constraintValue.setSingleSelectId(generateNewId());
						}
						constraintsNode.add(JsonUtils.toJson(constraintValue));
					});
					vendorRestriction.setConstraintValue(constraintsNode);
				}else if(ConstraintType.RANGE_ENUM.equals(vendorRestriction.getConstraintType())){
					
					vendorRestriction.getConstraintValue().forEach(val->{
					    ConstraintValueForRangeYear constraintRangeYear=JsonUtils.fromJson(val,ConstraintValueForRangeYear.class);
					    
					    if (constraintRangeYear.getRangeYearId()=="" || constraintRangeYear.getRangeYearId() == null) {
					    	constraintRangeYear.setRangeYearId(generateNewId());
						  }
					    constraintsNode.add(JsonUtils.toJson(constraintRangeYear));
					    
						});
					vendorRestriction.setConstraintValue(constraintsNode);
				}else if(ConstraintType.MAP.equals(vendorRestriction.getConstraintType())){
					
					vendorRestriction.getConstraintValue().forEach(val->{
					    ConstraintValueForMap constraintValueForMap=JsonUtils.fromJson(val,ConstraintValueForMap.class);
					    
					    if (constraintValueForMap.getMapId()=="" || constraintValueForMap.getMapId() == null) {
					    	constraintValueForMap.setMapId(generateNewId());
						  }
					    constraintsNode.add(JsonUtils.toJson(constraintValueForMap));
					    
						});
					vendorRestriction.setConstraintValue(constraintsNode);
				}
			

			} 
          return constraintsNode;
	}
}
