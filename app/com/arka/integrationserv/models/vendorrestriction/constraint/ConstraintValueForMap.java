package com.arka.integrationserv.models.vendorrestriction.constraint;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ArrayNode;

public class ConstraintValueForMap implements Serializable{

	private static final long serialVersionUID = 1110976694701094976L;
    
	@JsonProperty(value = "map_id")
	private String mapId;
	
	@JsonProperty(value = "key")
	private String key;
	
	@JsonProperty(value = "value")
	private ArrayNode value;

	public String getMapId() {
		return mapId;
	}

	public void setMapId(String mapId) {
		this.mapId = mapId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public ArrayNode getValue() {
		return value;
	}

	public void setValue(ArrayNode value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ConstraintValueForMap [mapId=" + mapId + ", key=" + key + ", value=" + value + "]";
	}

	
  }
