package com.arka.integrationserv.models.vendorrestriction.constraint;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConstraintValue implements Serializable {

	private static final long serialVersionUID = -2707728931114655064L;

	@JsonProperty(value = "range_id")
	private String range_id;

	@JsonProperty(value = "min")
	private int min;

	@JsonProperty(value = "max")
	private int max;

	@JsonProperty(value = "interval")
	private int interval;

	@JsonProperty(value = "prefix")
	private String prefix;

	@JsonProperty(value = "suffix")
	private String suffix;

	public String getRange_id() {
		return range_id;
	}

	public void setRange_id(String range_id) {
		this.range_id = range_id;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@Override
	public String toString() {
		return "ConstraintValue [range_id=" + range_id + ", min=" + min + ", max=" + max + ", interval=" + interval
				+ ", prefix=" + prefix + ", suffix=" + suffix + "]";
	}

}
