package com.arka.integrationserv.models.vendorrestriction.constraint;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConstraintValueForSingleSelect implements Serializable {

	private static final long serialVersionUID = 5820738506042047074L;

	@JsonProperty(value = "single_select_id")
	private String singleSelectId;

	@JsonProperty(value = "vendor_value")
	private String vendorValue;

	@JsonProperty(value = "catalog_value")
	private String catalogValue;

	public String getSingleSelectId() {
		return singleSelectId;
	}

	public void setSingleSelectId(String singleSelectId) {
		this.singleSelectId = singleSelectId;
	}

	public String getVendorValue() {
		return vendorValue;
	}

	public void setVendorValue(String vendorValue) {
		this.vendorValue = vendorValue;
	}

	public String getCatalogValue() {
		return catalogValue;
	}

	public void setCatalogValue(String catalogValue) {
		this.catalogValue = catalogValue;
	}

	@Override
	public String toString() {
		return "ConstraintValueForSingleSelect [singleSelectId=" + singleSelectId + ", vendorValue=" + vendorValue
				+ ", catalogValue=" + catalogValue + "]";
	}

}
