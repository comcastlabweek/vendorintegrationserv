package com.arka.integrationserv.models.vendorrestriction.constraint;

import java.io.Serializable;

import com.arka.integrationserv.models.vendorrestriction.constant.CurrentType;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ConstraintValueForRangeYear implements Serializable {

	private static final long serialVersionUID = 5347412052553306049L;

	@JsonProperty(value = "range_year_id")
	private String rangeYearId;

	@JsonProperty(value = "min")
	private int min;
	
    @JsonProperty(value = "current")
    private CurrentType current;
   
    @JsonProperty(value="max")
    private int max;

	public String getRangeYearId() {
		return rangeYearId;
	}

	public void setRangeYearId(String rangeYearId) {
		this.rangeYearId = rangeYearId;
	}

	
	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public CurrentType getCurrent() {
		return current;
	}

	public void setCurrent(CurrentType current) {
		this.current = current;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public String toString() {
		return "ConstraintValueForRangeYear [rangeYearId=" + rangeYearId + ", min=" + min + ", current=" + current
				+ ", max=" + max + "]";
	}

	
}
