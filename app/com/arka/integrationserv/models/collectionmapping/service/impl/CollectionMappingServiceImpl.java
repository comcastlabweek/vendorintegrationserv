/**
 * 
 */
package com.arka.integrationserv.models.collectionmapping.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.arka.integrationserv.models.collectionmapping.repository.CollectionMappingRepository;
import com.arka.integrationserv.models.collectionmapping.service.CollectionMappingService;
import com.arka.integrationserv.services.VendorProdMgmtServ;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author arul.p
 *
 */
public class CollectionMappingServiceImpl implements CollectionMappingService {

	@Inject
	VendorProdMgmtServ vendorProdMgmtServ;

	@Inject
	CollectionMappingRepository collectionMappingRepository;

	@Override
	public CompletionStage<JsonNode> getCollection(String collection_name, Map<String, List<String>> queryString,
			Map<String, List<String>> headerMap) {
		return vendorProdMgmtServ.getCollection(collection_name, queryString, headerMap);
	}

	@Override
	public CompletionStage<JsonNode> getCollectionWithFilter(String collection_name, Map<String, List<String>> dataMap,
			Map<String, List<String>> headerMap) {
		return vendorProdMgmtServ.getCollectionWithFilter(collection_name, dataMap, headerMap);
	}

	@Override
	public CompletionStage<JsonNode> getRestrictionById(String arkaCollectionName,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap) {
		return vendorProdMgmtServ.getRestrictionById(arkaCollectionName, queryString, headerMap);
	}

	@Override
	public CompletionStage<JsonNode> putProductIdInArkaCollection(JsonNode reqJson,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap) {
		return vendorProdMgmtServ.putProductIdInArkaCollection(reqJson, queryString, headerMap);
	}

	@Override
	public Long recordCount(String collectionName) {
		return collectionMappingRepository.recordCount(collectionName);
	}

	@Override
	public void dropCollection(String collectionName) {
		collectionMappingRepository.dropCollection(collectionName);
	}

}
