/**
 * 
 */
package com.arka.integrationserv.models.collectionmapping.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.integrationserv.models.collectionmapping.service.impl.CollectionMappingServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

/**
 * @author arul.p
 *
 */
@ImplementedBy(CollectionMappingServiceImpl.class)
public interface CollectionMappingService {

	public CompletionStage<JsonNode> getCollection(String collection_name, Map<String, List<String>> queryString,
			Map<String, List<String>> headerMap);

	public CompletionStage<JsonNode> getCollectionWithFilter(String collection_name, Map<String, List<String>> dataMap,
			Map<String, List<String>> headerMap);

	/**
	 * @param arkaCollectionName
	 */
	public CompletionStage<JsonNode> getRestrictionById(String arkaCollectionName,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap);

	/**
	 * @param reqJson
	 */
	public CompletionStage<JsonNode> putProductIdInArkaCollection(JsonNode reqJson,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap);
	
	public Long recordCount(String collectionName);
	
	public void dropCollection(String collectionName);

}
