package com.arka.integrationserv.models.collectionmapping.repository.impl;

import javax.inject.Inject;

import com.arka.common.provider.db.MongoDBService;
import com.arka.integrationserv.models.collectionmapping.repository.CollectionMappingRepository;

public class CollectionMappingRepositoryImpl implements CollectionMappingRepository{
	
	@Inject
	MongoDBService dbService;

	@Override
	public Long recordCount(String collectionName) {
		return dbService.getCollection(collectionName).count();
	}

	@Override
	public void dropCollection(String collectionName) {
		dbService.getCollection(collectionName).drop();
	}

}
