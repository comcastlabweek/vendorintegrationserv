package com.arka.integrationserv.models.collectionmapping.repository;

import com.arka.integrationserv.models.collectionmapping.repository.impl.CollectionMappingRepositoryImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(CollectionMappingRepositoryImpl.class)
public interface CollectionMappingRepository {
	
	public Long recordCount(String collectionName);
	
	public void dropCollection(String collectionName);

}
