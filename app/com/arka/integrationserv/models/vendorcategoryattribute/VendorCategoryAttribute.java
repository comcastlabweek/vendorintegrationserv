package com.arka.integrationserv.models.vendorcategoryattribute;

import java.io.Serializable;
import java.util.List;

import com.arka.common.models.base.BaseModel;
import com.arka.common.models.constant.Status;
import com.arka.common.models.tag.Tag;
import com.arka.common.models.tag.VendorObjectPatch;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.DataType;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.TransformToVendor;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.TransformationToCatalog;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.UsageType;
import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * @author gokulan.r
 *
 */
public class VendorCategoryAttribute extends BaseModel<String> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7233225551707761964L;

	@JsonProperty(value = "category_id")
	private String categoryId;
	
	@JsonProperty(value = "vendor_id")
	private String vendor_id;
	
	@JsonProperty(value ="product_id")
	private List<String> productId;
	
	@JsonProperty(value = "category_attribute_id")
	private String categoryAttributeId;
	
	@JsonProperty(value = "code")
	private String code;

	@JsonProperty(value = "name")
	private String name;

	
	@JsonProperty(value = "path")
	private String path;

	@JsonProperty(value = "description")
	private String description;

	@JsonProperty(value = "mandatory")
	private Boolean mandatory;
	
	@JsonProperty(value = "data_type")
	private DataType dataType;

	@JsonProperty(value = "data_type_template")
	private String dataTypeTemplate;

	@JsonProperty(value = "default_value")
	private String defaultValue;

	@JsonProperty(value = "usage_type")
	private UsageType usageType;
	
	@JsonProperty(value = "transformation_to_vendor")
	private TransformToVendor transformToVendor;

	@JsonProperty(value ="transformation_to_catalog")
	private TransformationToCatalog  transformationToCatalog;

	@JsonProperty(value = "restriction_id")
	private String restrictionId;

	@JsonProperty(value = "tags")
	private List<Tag> tags;
	
	@JsonProperty(value = "weight")
	private Integer weight;
	
	@JsonProperty(value = "status")
	private Status status;

	@JsonProperty(value = "flags")
	private Integer flags;

	@JsonProperty(value = "vendor_object_patches")
	private List<VendorObjectPatch> vendorObjectPatches;
	
	public List<String> getProductId() {
		return productId;
	}

	public void setProductId(List<String> productId) {
		this.productId = productId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getVendor_id() {
		return vendor_id;
	}
	
	public void setVendor_id(String vendor_id) {
		this.vendor_id = vendor_id;
	}

	public String getCategoryAttributeId() {
		return categoryAttributeId;
	}

	public void setCategoryAttributeId(String categoryAttributeId) {
		this.categoryAttributeId = categoryAttributeId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getMandatory() {
		return mandatory;
	}

	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public String getDataTypeTemplate() {
		return dataTypeTemplate;
	}

	public void setDataTypeTemplate(String dataTypeTemplate) {
		this.dataTypeTemplate = dataTypeTemplate;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public UsageType getUsageType() {
		return usageType;
	}

	public void setUsageType(UsageType usageType) {
		this.usageType = usageType;
	}

	public TransformToVendor getTransformToVendor() {
		return transformToVendor;
	}

	public void setTransformToVendor(TransformToVendor transformToVendor) {
		this.transformToVendor = transformToVendor;
	}

	public TransformationToCatalog getTransformationToCatalog() {
		return transformationToCatalog;
	}

	public void setTransformationToCatalog(TransformationToCatalog transformationToCatalog) {
		this.transformationToCatalog = transformationToCatalog;
	}

	public String getRestrictionId() {
		return restrictionId;
	}

	public void setRestrictionId(String restrictionId) {
		this.restrictionId = restrictionId;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getFlags() {
		return flags;
	}

	public void setFlags(Integer flags) {
		this.flags = flags;
	}

	public List<VendorObjectPatch> getVendorObjectPatches() {
		return vendorObjectPatches;
	}

	public void setVendorObjectPatches(List<VendorObjectPatch> vendorObjectPatches) {
		this.vendorObjectPatches = vendorObjectPatches;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((categoryAttributeId == null) ? 0 : categoryAttributeId.hashCode());
		result = prime * result + ((categoryId == null) ? 0 : categoryId.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((dataType == null) ? 0 : dataType.hashCode());
		result = prime * result + ((dataTypeTemplate == null) ? 0 : dataTypeTemplate.hashCode());
		result = prime * result + ((defaultValue == null) ? 0 : defaultValue.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((flags == null) ? 0 : flags.hashCode());
		result = prime * result + ((mandatory == null) ? 0 : mandatory.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((restrictionId == null) ? 0 : restrictionId.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((transformToVendor == null) ? 0 : transformToVendor.hashCode());
		result = prime * result + ((transformationToCatalog == null) ? 0 : transformationToCatalog.hashCode());
		result = prime * result + ((usageType == null) ? 0 : usageType.hashCode());
		result = prime * result + ((vendorObjectPatches == null) ? 0 : vendorObjectPatches.hashCode());
		result = prime * result + ((vendor_id == null) ? 0 : vendor_id.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendorCategoryAttribute other = (VendorCategoryAttribute) obj;
		if (categoryAttributeId == null) {
			if (other.categoryAttributeId != null)
				return false;
		} else if (!categoryAttributeId.equals(other.categoryAttributeId))
			return false;
		if (categoryId == null) {
			if (other.categoryId != null)
				return false;
		} else if (!categoryId.equals(other.categoryId))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (dataType != other.dataType)
			return false;
		if (dataTypeTemplate == null) {
			if (other.dataTypeTemplate != null)
				return false;
		} else if (!dataTypeTemplate.equals(other.dataTypeTemplate))
			return false;
		if (defaultValue == null) {
			if (other.defaultValue != null)
				return false;
		} else if (!defaultValue.equals(other.defaultValue))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (flags == null) {
			if (other.flags != null)
				return false;
		} else if (!flags.equals(other.flags))
			return false;
		if (mandatory == null) {
			if (other.mandatory != null)
				return false;
		} else if (!mandatory.equals(other.mandatory))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (restrictionId == null) {
			if (other.restrictionId != null)
				return false;
		} else if (!restrictionId.equals(other.restrictionId))
			return false;
		if (status != other.status)
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (transformToVendor != other.transformToVendor)
			return false;
		if (transformationToCatalog != other.transformationToCatalog)
			return false;
		if (usageType != other.usageType)
			return false;
		if (vendorObjectPatches == null) {
			if (other.vendorObjectPatches != null)
				return false;
		} else if (!vendorObjectPatches.equals(other.vendorObjectPatches))
			return false;
		if (vendor_id == null) {
			if (other.vendor_id != null)
				return false;
		} else if (!vendor_id.equals(other.vendor_id))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VendorCategoryAttribute [categoryId=" + categoryId + ", vendor_id=" + vendor_id + ", productId="
				+ productId + ", categoryAttributeId=" + categoryAttributeId + ", code=" + code + ", name=" + name
				+ ", path=" + path + ", description=" + description + ", mandatory=" + mandatory + ", dataType="
				+ dataType + ", dataTypeTemplate=" + dataTypeTemplate + ", defaultValue=" + defaultValue
				+ ", usageType=" + usageType + ", transformToVendor=" + transformToVendor + ", transformationToCatalog="
				+ transformationToCatalog + ", restrictionId=" + restrictionId + ", tags=" + tags + ", weight=" + weight
				+ ", status=" + status + ", flags=" + flags + ", vendorObjectPatches=" + vendorObjectPatches + "]";
	}
	
}