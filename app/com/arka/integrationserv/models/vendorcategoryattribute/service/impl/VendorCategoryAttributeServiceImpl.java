package com.arka.integrationserv.models.vendorcategoryattribute.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import org.apache.commons.collections4.MapUtils;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.VendorCategoryAttributeErrorMsg;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.VendorCategoryAttributeFieldNames;
import com.arka.integrationserv.models.vendorcategoryattribute.repository.VendorCategoryAttributeRepository;
import com.arka.integrationserv.models.vendorcategoryattribute.service.VendorCategoryAttributeService;
import com.google.inject.Inject;

public class VendorCategoryAttributeServiceImpl implements VendorCategoryAttributeService<String> {

	@Inject
	private VendorCategoryAttributeRepository vendorcategoryAttrRepo;

	@Inject
	private CacheService cache;

	@Override
	public CompletionStage<VendorCategoryAttribute> findVendorCategoryAttributeById(String id) {
		return cache.getOrElse(
				CacheService.getCacheKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value(), null, id), () -> {
					VendorCategoryAttribute vendorcateattr = vendorcategoryAttrRepo.findOneById(id);
					if (vendorcateattr != null) {
						return JsonUtils.stringify(vendorcateattr);
					}
					return JsonUtils.stringify(JsonUtils.newJsonObject());
				}).thenApplyAsync(json -> {
					return JsonUtils.fromJson(json, VendorCategoryAttribute.class);
				});
	}

	@Override
	public CompletionStage<Collection<VendorCategoryAttribute>> findVendorCategoryAttributes(
			Map<String, List<String>> criteriaMap) {
		String cacheKeySuffix = "";
		if (criteriaMap == null || criteriaMap.isEmpty())
			cacheKeySuffix = ALL_VENDORCATEGORY_ATTRIBUTE_CACHE_KEY_SUFFIX;
		System.out.println("criteriaMap :"+criteriaMap);
		return cache.getOrElse(CacheService.getCacheKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value(),
				criteriaMap, cacheKeySuffix), () -> {
					return JsonUtils.stringify(vendorcategoryAttrRepo.findAll(criteriaMap));
				}).thenApplyAsync(cachedVendorCategoriesattributeJson -> {
					return JsonUtils.jsonToList(cachedVendorCategoriesattributeJson, VendorCategoryAttribute.class);
				});

	}

	@Override
	public void saveVendorCategoryAttribute(VendorCategoryAttribute vendorcategoryAttr) throws ArkaValidationException {
		Map<String, List<String>> errors = validate(vendorcategoryAttr);
		if (MapUtils.isNotEmpty(errors)) {
			throw new ArkaValidationException(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_VALIDATION_FAILED.value(), errors);
		}
		vendorcategoryAttrRepo.insert(vendorcategoryAttr);
		cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value());
		cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "."
				+ VendorCategoryAttributeService.ALL_VENDORCATEGORY_ATTRIBUTE_CACHE_KEY_SUFFIX);

	}

	private Map<String, List<String>> validate(VendorCategoryAttribute vendorcategoryAttr)
			throws ArkaValidationException {
		Map<String, List<String>> errorMap = new LinkedHashMap<>();

		if (vendorcategoryAttr == null) {
			errorMap.put(VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR.value(),
					Arrays.asList(VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NULL.value()));
			return errorMap;
		}

		//fillVendorCategoryAttributeCodeError(errorMap, vendorcategoryAttr);

		fillVendorCategoryAttributeNameError(errorMap, vendorcategoryAttr);

		fillVendorCategoryAttributeStatusError(errorMap, vendorcategoryAttr);

		return errorMap;
	}

	private void fillVendorCategoryAttributeStatusError(Map<String, List<String>> errorMap,
			VendorCategoryAttribute vendorcategoryAttr) {

		if (vendorcategoryAttr.getStatus() == null) {
			errorMap.put(VendorCategoryAttributeFieldNames.STATUS.value(),
					Arrays.asList(VendorCategoryAttributeErrorMsg.STATUS_NULL.value()));
		} else if (!Status.ACTIVE.equals(vendorcategoryAttr.getStatus())
				&& !Status.INACTIVE.equals(vendorcategoryAttr.getStatus())) {
			errorMap.put(VendorCategoryAttributeFieldNames.STATUS.value(),
					Arrays.asList(VendorCategoryAttributeErrorMsg.STATUS_INVALID.value()));
		}
	}

	private void fillVendorCategoryAttributeCodeError(Map<String, List<String>> errorMap,
			VendorCategoryAttribute vendorcategoryAttr) throws ArkaValidationException {
		if (vendorcategoryAttr.getCode() == null || vendorcategoryAttr.getCode().trim().isEmpty()) {
			errorMap.put(VendorCategoryAttributeFieldNames.CODE.value(),
					Arrays.asList(VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_CODE_NULL.value()));
		} else {
			Map<String, List<String>> query = new HashMap<>();
			query.put(VendorCategoryAttributeFieldNames.CODE.value(), Arrays.asList(vendorcategoryAttr.getCode()));
			query.put(VendorCategoryAttributeFieldNames.PATH.value(), Arrays.asList(vendorcategoryAttr.getPath()));
			long count = vendorcategoryAttrRepo.count(query);
			if (vendorcategoryAttr.getId() != null) {
				if (count > 1)
					errorMap.put(VendorCategoryAttributeFieldNames.CODE.value(), Arrays.asList(
							VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_CODE_ALREADY_EXIST.value()));
			} else {
				if (count > 0)
					errorMap.put(VendorCategoryAttributeFieldNames.CODE.value(), Arrays.asList(
							VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_CODE_ALREADY_EXIST.value()));
			}

		}
	}

	private void fillVendorCategoryAttributeNameError(Map<String, List<String>> errorMap,
			VendorCategoryAttribute vendorcategoryAttr) throws ArkaValidationException {
		if (vendorcategoryAttr.getName() == null || vendorcategoryAttr.getName().trim().isEmpty()) {
			errorMap.put(VendorCategoryAttributeFieldNames.NAME.value(),
					Arrays.asList(VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NAME_NULL.value()));
		} else {
			Map<String, List<String>> query = new HashMap<>();
			query.put(VendorCategoryAttributeFieldNames.NAME.value(), Arrays.asList(vendorcategoryAttr.getName()));
			long count = vendorcategoryAttrRepo.count(query);
			if (vendorcategoryAttr.getId() != null) {
				if (count > 1)
					errorMap.put(VendorCategoryAttributeFieldNames.NAME.value(), Arrays.asList(
							VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NAME_ALREADY_EXIST.value()));
			} else {
				if (count > 0)
					errorMap.put(VendorCategoryAttributeFieldNames.NAME.value(), Arrays.asList(
							VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NAME_ALREADY_EXIST.value()));
			}

		}
	}

	@Override
	public boolean deleteVendorCategoryAttributeById(String id) throws ArkaValidationException {
		boolean deleted = vendorcategoryAttrRepo.deleteOneById(id);
		if (deleted) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "."
					+ VendorCategoryAttributeService.ALL_VENDORCATEGORY_ATTRIBUTE_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "." + id);
		}
		return deleted;
	}

	@Override
	public boolean deleteVendorCategoryAttributes(Map<String, List<String>> deleteQuery)
			throws ArkaValidationException {
		boolean deleted = vendorcategoryAttrRepo.deleteMany(deleteQuery);
		if (deleted) {
			cache.inValidateCacheWithPrefix(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value());
		}
		return deleted;
	}

	@Override
	public boolean deleteTagFromVendorCategoryAttribute(String vendorCategoryAttributeId, String tagId)
			throws ArkaValidationException {
		boolean deleted = vendorcategoryAttrRepo.deleteFromArray(vendorCategoryAttributeId, tagId,
				VendorCategoryAttributeFieldNames.TAGS.value());
		if (deleted) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "."
					+ VendorCategoryAttributeService.ALL_VENDORCATEGORY_ATTRIBUTE_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "." + vendorCategoryAttributeId);
		}
		return deleted;
	}

	@Override
	public long getVendorCategoryAttributeCount() {
		return vendorcategoryAttrRepo.count();
	}

	@Override
	public long getVendorCategoryAttributeCount(Map<String, List<String>> criteriaMap) throws ArkaValidationException {
		return vendorcategoryAttrRepo.count(criteriaMap);
	}

	@Override
	public boolean updateVendorCategoryAttribute(VendorCategoryAttribute vendorcategoryAttribute)
			throws ArkaValidationException {
		Map<String, List<String>> errors = validate(vendorcategoryAttribute);
		if (MapUtils.isNotEmpty(errors)) {
			throw new ArkaValidationException(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_VALIDATION_FAILED.value(), errors);
		}
		boolean updated = vendorcategoryAttrRepo.updateOne(vendorcategoryAttribute);
		if (updated) {
			cache.inValidateCacheWithKeyHasQueryParam(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value());
			cache.inValidateCacheWithExactKey(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "."
					+ VendorCategoryAttributeService.ALL_VENDORCATEGORY_ATTRIBUTE_CACHE_KEY_SUFFIX);
			cache.inValidateCacheWithExactKey(
					VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value() + "." + vendorcategoryAttribute.getId());
		}
		return updated;
	}

}