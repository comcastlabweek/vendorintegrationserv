package com.arka.integrationserv.models.vendorcategoryattribute.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.common.exceptions.ArkaValidationException;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorcategoryattribute.service.impl.VendorCategoryAttributeServiceImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(VendorCategoryAttributeServiceImpl.class)
public interface VendorCategoryAttributeService<ID> {

	public String ALL_VENDORCATEGORY_ATTRIBUTE_CACHE_KEY_SUFFIX = "arka.catalog.vendor.category.attributes.all";

	public void saveVendorCategoryAttribute(VendorCategoryAttribute vendorcategoryAttr) throws ArkaValidationException;

	public boolean updateVendorCategoryAttribute(VendorCategoryAttribute vendorcategoryAttribute)
			throws ArkaValidationException;

	public CompletionStage<VendorCategoryAttribute> findVendorCategoryAttributeById(ID id);

	public CompletionStage<Collection<VendorCategoryAttribute>> findVendorCategoryAttributes(
			Map<String, List<String>> criteriaMap);

	public boolean deleteVendorCategoryAttributeById(ID id) throws ArkaValidationException;

	public boolean deleteVendorCategoryAttributes(Map<String, List<String>> deleteQuery) throws ArkaValidationException;

	public boolean deleteTagFromVendorCategoryAttribute(ID vendorCategoryAttributeId, ID tagId)
			throws ArkaValidationException;

	public long getVendorCategoryAttributeCount();

	public long getVendorCategoryAttributeCount(Map<String, List<String>> criteriaMap) throws ArkaValidationException;

}
