package com.arka.integrationserv.models.vendorcategoryattribute.repository.impl;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.base.repository.impl.MongoBaseRepositoryImpl;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorcategoryattribute.repository.VendorCategoryAttributeRepository;

public class VendorCategoryAttributeRepositoryImpl extends MongoBaseRepositoryImpl<VendorCategoryAttribute>
		implements VendorCategoryAttributeRepository {

	public VendorCategoryAttributeRepositoryImpl() {
		super(VendorCollectionNames.VENDOR_CATEGORY_ATTRIBUTES.value());
	}

	@Override
	public void insert(VendorCategoryAttribute vendcatattr) throws ArkaValidationException {
		vendcatattr.setTags(getTagsWithId(vendcatattr.getTags()));
		vendcatattr.setVendorObjectPatches(getVendorObjectPatchWithId(vendcatattr.getVendorObjectPatches()));
		super.insert(vendcatattr);
	}

	@Override
	public boolean updateOne(VendorCategoryAttribute vendcatattr) throws ArkaValidationException {
		vendcatattr.setTags(getTagsWithId(vendcatattr.getTags()));
		vendcatattr.setVendorObjectPatches(getVendorObjectPatchWithId(vendcatattr.getVendorObjectPatches()));
		return super.updateOne(vendcatattr);
	}

}
