package com.arka.integrationserv.models.vendorcategoryattribute.repository;

import com.arka.common.models.base.repository.BaseRepository;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorcategoryattribute.repository.impl.VendorCategoryAttributeRepositoryImpl;
import com.google.inject.ImplementedBy;

@ImplementedBy(VendorCategoryAttributeRepositoryImpl.class)
public interface VendorCategoryAttributeRepository extends BaseRepository<VendorCategoryAttribute> {

}
