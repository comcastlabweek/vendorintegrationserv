package com.arka.integrationserv.models.vendorcategoryattribute.constants;

public enum VendorCategoryAttributeErrorMsg {

	VENDOR_CATEGORY_ATTRIBUTE_ERROR("errors"),

	VENDOR_CATEGORY_ATTRIBUTE_NOT_FOUND("vendor.category.attribute.not.found"),

	VENDOR_CATEGORY_ATTRIBUTE_ID_NOT_FOUND("vendor.category.attribute.id.not.found"),

	VENDOR_CATEGORY_ATTRIBUTE_ID_INVALID("vendor.category.attribute.id.invalid"),

	VENDOR_CATEGORY_ATTRIBUTE_NOT_UPDATED("vendor.category.attribute.not.updated"),

	PATCH_REMOVE_OPERATION_NOT_SUPPORTED("patch.remove.operation.not.supported"),

	VENDOR_CATEGORY_ATTRIBUTE_NOT_UPDATED_OP_OR_PATH_MISSING_IN_PATCH_DATA(
			"vendor.category.attribute.not.updated.OP.or.PATH.missing.in.PATCH.data"),

	VENDOR_CATEGORY_ATTRIBUTE_VALIDATION_FAILED("vendor.category.attribute.validation.failed"),

	VENDOR_CATEGORY_ATTRIBUTE_NULL("vendor.category.attribute.null"),

	STATUS_NULL("vendor.category.attribute.status.null"),

	STATUS_INVALID("vendor.category.attribute.status.invalid"),

	VENDOR_CATEGORY_ATTRIBUTE_CODE_ALREADY_EXIST("vendor.category.attribute.code.already.exist"),

	VENDOR_CATEGORY_ATTRIBUTE_CODE_NULL("vendor.category.attribute.code.null"),

	VENDOR_CATEGORY_ATTRIBUTE_NAME_NULL("vendor.category.attribute.name.null"),

	VENDOR_CATEGORY_ATTRIBUTE_NAME_ALREADY_EXIST("vendor.category.attribute.name.already.exist"),

	VENDOR_CATEGORY_ATTRIBUTE_NOT_REMOVED("vendor.category.attribute.not.removed"),

	INVALID_MANDATORY_INPUT_VALUE("Invalid.mandatory.input.Value.should.be.TRUE.or.FALSE"),

	INVALID_CONTENT_ID("Invalid.content.id"),

	INVAID_WEIGHT("invalid.weight"),

	INVALID_FLAGS("invalid.flags"),

	INVALID_PATH("invalid.path"),

	MESSAGE("Message"),;
	private String value;

	private VendorCategoryAttributeErrorMsg(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
