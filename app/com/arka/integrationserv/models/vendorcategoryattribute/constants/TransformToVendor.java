package com.arka.integrationserv.models.vendorcategoryattribute.constants;

public enum TransformToVendor {
	MAP, LOOKUP, ARRAY_JOIN_NO_DELIMITER,VEHICLE_NUMBER_HYPHEN_DELIMITER,FEET_TO_INCHES,PERCENTAGE_TO_DECIMAL;
}
