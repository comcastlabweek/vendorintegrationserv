package com.arka.integrationserv.models.vendorcategoryattribute.constants;

public enum VendorCategoryAttributeFieldNames {

	CODE("code"),

	NAME("name"),

	TAGS("tags"),

	WEIGHT("weight"),
	
	PATH("path"),

	MANDATORY("mandatory"),

	STATUS("status"),

	FLAGS("flags");
	
	private String value;

	private VendorCategoryAttributeFieldNames(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}
