package com.arka.integrationserv.models.vendorcategoryattribute.constants;

public enum TransformationToCatalog {
	MAP, LOOKUP, ARRAY_JOIN_NO_DELIMITER,FEET_TO_INCHES,PERCENTAGE_TO_DECIMAL,DISCOUNT;
}
