package com.arka.integrationserv.models.vendorcategoryattribute.constants;

public enum DataType {
	INT, NUMBER, BOOLEAN, DATE, STRING, ENUM, GUID, RANDOM , ARRAY_STRING, ARRAY_INT, ARRAY_NUMBER, 
	ARRAY_OBJECT, ARRAY_ENUM, ARRAY_MAP, DERIVED_OBJECT, DERIVED_CSV, ARRAY_TO_ATTRIBUTES;
}
