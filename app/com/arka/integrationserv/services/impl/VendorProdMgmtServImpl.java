/**
 * 
 */
package com.arka.integrationserv.services.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.ws.service.WsServiceUtils;
import com.arka.integrationserv.services.VendorProdMgmtServ;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author arul.p
 *
 */
public class VendorProdMgmtServImpl implements VendorProdMgmtServ {

	@Inject
	private WsServiceUtils wsServiceUtils;

	@Inject
	private PropUtils propUtils;

	@Override
	public CompletionStage<JsonNode> getCollection(String collection_name, Map<String, List<String>> queryString,
			Map<String, List<String>> headerMap) {
		return wsServiceUtils.doGet(propUtils.get("arka.service.collection.mapping.data"), queryString, headerMap);
	}

	@Override
	public CompletionStage<JsonNode> getCollectionWithFilter(String collection_name,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap) {
		return wsServiceUtils.doGet(propUtils.get("arka.service.collection.mapping.data"), queryString, headerMap);
	}

	@Override
	public CompletionStage<JsonNode> getRestrictionById(String arkaCollectionName,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap) {
		return wsServiceUtils.doGet(
				propUtils.get("arka.service.collection.restriction.query").replace(":id", arkaCollectionName),
				queryString, headerMap);
	}

	@Override
	public CompletionStage<JsonNode> putProductIdInArkaCollection(JsonNode reqJson,
			Map<String, List<String>> queryString, Map<String, List<String>> headerMap) {
		return wsServiceUtils.doPost(propUtils.get("arka.collection.data.mapping"), queryString, headerMap,
				JsonUtils.toJson(reqJson));
	}

}
