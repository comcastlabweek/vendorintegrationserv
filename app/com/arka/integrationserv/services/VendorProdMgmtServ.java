/**
 * 
 */
package com.arka.integrationserv.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import com.arka.integrationserv.services.impl.VendorProdMgmtServImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.ImplementedBy;

/**
 * @author arul.p
 *
 */
@ImplementedBy(VendorProdMgmtServImpl.class)
public interface VendorProdMgmtServ {

	/**
	 * @param collection_name
	 * @return
	 */
	CompletionStage<JsonNode> getCollection(String collection_name,Map<String, List<String>> queryString,Map<String, List<String>> headerMap);

	/**
	 * @param collection_name
	 * @param queryString
	 * @return
	 */
	CompletionStage<JsonNode> getCollectionWithFilter(String collection_name, Map<String, List<String>> queryString,Map<String, List<String>> headerMap);

	/**
	 * @param arkaCollectionName
	 * @return
	 */
	CompletionStage<JsonNode> getRestrictionById(String arkaCollectionName, Map<String, List<String>> queryString,
			Map<String, List<String>> headerMap);

	/**
	 * @param reqJson
	 * @return
	 */
	CompletionStage<JsonNode> putProductIdInArkaCollection(JsonNode reqJson,Map<String, List<String>> queryString,Map<String, List<String>> headerMap);

}
