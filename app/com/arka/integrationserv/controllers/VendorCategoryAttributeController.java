package com.arka.integrationserv.controllers;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.JsonPatchUtils;
import com.arka.common.controllers.utils.ResultWrapperUtils;
import com.arka.common.encrypt.decrypt.constant.VendorIntegrationEncryptDecryptFields;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.productmgmt.constants.CategoryAttributeConstants;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.PatchValidator;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorcategoryattribute.VendorCategoryAttribute;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.VendorCategoryAttributeErrorMsg;
import com.arka.integrationserv.models.vendorcategoryattribute.constants.VendorCategoryAttributeFieldNames;
import com.arka.integrationserv.models.vendorcategoryattribute.service.VendorCategoryAttributeService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.inject.Inject;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

public class VendorCategoryAttributeController extends Controller {

	@Inject
	VendorCategoryAttributeService<String> vendorCategoryAttributeService;

	@Inject
	HttpExecutionContext executor;

	@Inject
	PatchValidator patchValidator;

	@Inject
	JsonPatchUtils jsonPatchUtils;

	private final List<String> encDecFields = VendorIntegrationEncryptDecryptFields.getEncAndDecFields();

	public CompletionStage<Result> add() {
		return CompletableFuture.supplyAsync(() -> {
			JsonNode vendorcateattr = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
			VendorCategoryAttribute vendorcategoryattribute = Json.fromJson(vendorcateattr,
					VendorCategoryAttribute.class);
			try {
				vendorCategoryAttributeService.saveVendorCategoryAttribute(vendorcategoryattribute);
			} catch (ArkaValidationException e) {
				return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
			}
			return ok(JsonUtils.toEncryptedIdJson(vendorcategoryattribute, encDecFields));
		}, executor.current());

	}

	public CompletionStage<Result> getAll() {
		Map<String, String[]> queryString = request().queryString();

		if (MapUtils.isNotEmpty(queryString)) {
			String[] projectionFields = queryString.get(CriteriaUtils.PFIELD);
			ObjectNode recordCountDetails = Json.newObject();
			Map<String, List<String>> query = CriteriaUtils.queryStringArrayToList(encDecFields, queryString);
			if (queryString.containsKey(CriteriaUtils.RCOUNT) && queryString.get(CriteriaUtils.RCOUNT) != null) {
				recordCountDetails.put(CriteriaUtils.TOTAL_RECORD_COUNT,
						vendorCategoryAttributeService.getVendorCategoryAttributeCount());
				recordCountDetails.put(CriteriaUtils.FILTERED_RECORD_COUNT,
						vendorCategoryAttributeService.getVendorCategoryAttributeCount(query));
			}
			return vendorCategoryAttributeService.findVendorCategoryAttributes(query).thenApplyAsync(modelList -> {
				return ok(JsonUtils.toEncryptedIdJson(
						ResultWrapperUtils.wrapListInJson(modelList, projectionFields, recordCountDetails),
						encDecFields));
			});

		} else {
			return vendorCategoryAttributeService.findVendorCategoryAttributes(null).thenApplyAsync(coll -> {
				return ok(
						JsonUtils.toEncryptedIdJson(ResultWrapperUtils.wrapListInJson(coll, null, null), encDecFields));
			});
		}

	}

	public CompletionStage<Result> getById(String _id) throws ArkaValidationException {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {
			return vendorCategoryAttributeService.findVendorCategoryAttributeById(_id)
					.thenApplyAsync(Vendorcategoryattribute -> {
						if (Vendorcategoryattribute != null) {
							return ok(JsonUtils.toEncryptedIdJson(Vendorcategoryattribute, encDecFields));
						} else {
							return notFound(JsonUtils.errorDataNotFoundJson(
									VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NOT_FOUND.value()));
						}
					});
		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ID_INVALID.value())));
		}
	}

	public CompletionStage<Result> delete(String _id) {
		_id = CryptUtils.decryptData(_id);
		// do not delete just mark as inactive
		if (StringUtils.isNotEmpty(_id)) {
			return vendorCategoryAttributeService.findVendorCategoryAttributeById(_id)
					.thenApplyAsync(Vendorcategoryattribute -> {
						try {

							if (Vendorcategoryattribute != null) {
								Vendorcategoryattribute.setStatus(Status.INACTIVE);
								vendorCategoryAttributeService.updateVendorCategoryAttribute(Vendorcategoryattribute);
								return ok(JsonUtils.toEncryptedIdJson(Vendorcategoryattribute, encDecFields));
							} else {
								return notFound(JsonUtils.errorDataNotFoundJson(
										VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ID_NOT_FOUND
												.value()));
							}

						} catch (ArkaValidationException e) {
							return internalServerError(
									JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
						}

					});
		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ID_INVALID.value())));
		}
	}

	public CompletionStage<Result> put(String encId) {
		String _id = CryptUtils.decryptData(encId);
		if (StringUtils.isNotEmpty(_id)) {
			return CompletableFuture.supplyAsync(() -> {
				JsonNode vendorcateattrjson = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
				VendorCategoryAttribute Vendorcategoryattribute = Json.fromJson(vendorcateattrjson,
						VendorCategoryAttribute.class);
				try {
					Vendorcategoryattribute.setId(_id);
					vendorCategoryAttributeService.updateVendorCategoryAttribute(Vendorcategoryattribute);
				} catch (ArkaValidationException e) {
					return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
				}
				return ok(JsonUtils.toEncryptedIdJson(Vendorcategoryattribute, encDecFields));
			}, executor.current());

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ID_INVALID.value())));
		}

	}

	public CompletionStage<Result> update(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {
			// TODO : set updated by
			JsonNode vendorcateattrjson = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);

			ObjectNode errorJson = JsonUtils.newJsonObject();

			if (JsonUtils.isValidField(vendorcateattrjson, UpdateNames.OP.value())
					&& JsonUtils.isValidField(vendorcateattrjson, UpdateNames.PATH.value())
					&& JsonUtils.isValidField(vendorcateattrjson, UpdateNames.VALUE.value())) {

				if (JsonUpdateOperations.REPLACE.value()
						.equalsIgnoreCase(vendorcateattrjson.get(UpdateNames.OP.value()).asText())) {
					return vendorCategoryAttributeService.findVendorCategoryAttributeById(_id)
							.thenApplyAsync(Vendorcategoryattribute -> {
								if (Vendorcategoryattribute != null) {
									try {
										Vendorcategoryattribute = updateFieldInCategory(Vendorcategoryattribute,
												vendorcateattrjson);

										boolean updated = vendorCategoryAttributeService
												.updateVendorCategoryAttribute(Vendorcategoryattribute);

										if (updated) {
											return ok(
													JsonUtils.toEncryptedIdJson(Vendorcategoryattribute, encDecFields));

										} else {
											errorJson.put(
													VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR
															.value(),
													VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NOT_UPDATED
															.value());
										}

									} catch (ArkaValidationException e) {
										errorJson.set(
												VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR.value(),
												JsonUtils.toJson(e.getErrors()));
										return internalServerError(JsonUtils.toJson(errorJson));
									}
								} else {
									errorJson.put(
											VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR.value(),
											VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NOT_FOUND
													.value());
								}
								return ok(errorJson);
							});
				} else if (JsonUpdateOperations.REMOVE.value()
						.equalsIgnoreCase(vendorcateattrjson.get(UpdateNames.OP.value()).asText())) {
					return vendorCategoryAttributeService.findVendorCategoryAttributeById(_id)
							.thenApplyAsync(Vendorcategoryattribute -> {
								if (Vendorcategoryattribute != null) {
									try {
										removeFieldInCategoryAttribute(Vendorcategoryattribute, vendorcateattrjson);

										boolean removed = vendorCategoryAttributeService
												.updateVendorCategoryAttribute(Vendorcategoryattribute);

										if (removed) {
											return ok(
													JsonUtils.toEncryptedIdJson(Vendorcategoryattribute, encDecFields));

										} else {
											errorJson.put(
													VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR
															.value(),
													VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NOT_REMOVED
															.value());
										}

									} catch (ArkaValidationException e) {
										return internalServerError(
												JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
									}
								} else {
									errorJson.put(
											VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR.value(),
											VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ID_NOT_FOUND
													.value());
								}
								return ok(errorJson);
							});
				} else {
					errorJson.put(VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR.value(),
							VendorCategoryAttributeErrorMsg.PATCH_REMOVE_OPERATION_NOT_SUPPORTED.value());
				}
			} else {
				errorJson.put(VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ERROR.value(),
						VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_NOT_UPDATED_OP_OR_PATH_MISSING_IN_PATCH_DATA
								.value());
			}
			return CompletableFuture.supplyAsync(() -> ok(errorJson));

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_ID_INVALID.value())));
		}

	}

	private void removeFieldInCategoryAttribute(VendorCategoryAttribute Vendorcategoryattribute,
			JsonNode vendorcateattrjson) throws ArkaValidationException {
		String path = vendorcateattrjson.get(UpdateNames.PATH.value()).asText();
		JsonNode value = vendorcateattrjson.get(UpdateNames.VALUE.value());
//		Map<String, List<String>> errorMap = new HashMap<>();

		if (path == null || value == null)
			return;
		path = path.toLowerCase(Locale.ENGLISH);

		switch (path) {
		case "category_id":
			Vendorcategoryattribute.setCategoryId(null);
			break;
		case "vendor_id":
			Vendorcategoryattribute.setVendor_id(null);
			break;
		case "category_attribute_id":
			Vendorcategoryattribute.setCategoryAttributeId(null);
			break;
		case "version":
			// Vendorcategoryattribute.setVersion(null);
			break;
		case "path":
			Vendorcategoryattribute.setPath(null);
			break;
		case "description":
			Vendorcategoryattribute.setDescription(null);
			break;
		case "mandatory":
			Vendorcategoryattribute.setMandatory(null);
			break;
		case "data_type":
			Vendorcategoryattribute.setDataType(null);
			break;
		case "data_type_template":
			Vendorcategoryattribute.setDataTypeTemplate(null);
			break;
		case "default_value":
			Vendorcategoryattribute.setDefaultValue(null);
			break;
		case "restriction_id":
			Vendorcategoryattribute.setRestrictionId(null);
			break;
		case "weight":
			Vendorcategoryattribute.setWeight(null);
			break;
		case "status":
			Vendorcategoryattribute.setStatus(Status.valueOf(null));
			break;
		case "flags":
			Vendorcategoryattribute.setFlags(null);
			break;
		default:
			if (path.startsWith(VendorCategoryAttributeFieldNames.TAGS.value())) {
				String[] pathArr = path.split("/");
				if (pathArr.length == CategoryAttributeConstants.VENDOR_CATEGORY_ATTRIBUTE_NAME_WEIGHT_PATH_IDENTIFIER.value) {
					Vendorcategoryattribute.getTags().forEach(tag -> {
						if (tag.getId().equals(pathArr[1])) {
							if (pathArr[2].equals(VendorCategoryAttributeFieldNames.NAME.value())) {
								tag.setName(null);
							} else if (pathArr[2].equals(VendorCategoryAttributeFieldNames.WEIGHT.value())) {
								tag.setWeight(null);
							} else {
								tag.setUpdatedTime(null);
							}
						}
					});
				} else if (pathArr.length == CategoryAttributeConstants.VENDOR_CATEGORY_ATTRIBUTE_ID_PATH_IDENTIFIER.value) {
					Vendorcategoryattribute.getTags().forEach(tag -> {
						if (tag.getId().equals(pathArr[1])) {
								vendorCategoryAttributeService.deleteTagFromVendorCategoryAttribute(
										Vendorcategoryattribute.getId().toString(), tag.getId().toString());
						}
					});
				} else {
					Vendorcategoryattribute.setTags(null);
				}
			}
		}
	}

	private VendorCategoryAttribute updateFieldInCategory(VendorCategoryAttribute vendorcategoryattribute,
			JsonNode vendorcateattrjson) throws ArkaValidationException {

		Map<String, List<String>> errorMap = new HashMap<>();

		String path = vendorcateattrjson.get(UpdateNames.PATH.value()).asText();
		JsonNode value = vendorcateattrjson.get(UpdateNames.VALUE.value());

		path = path.substring(1);

		switch (path) {
		case "mandatory":
			if (value.asText().equalsIgnoreCase("TRUE") || value.asText().equalsIgnoreCase("FALSE")) {
				vendorcategoryattribute.setMandatory(Boolean.parseBoolean(value.asText()));
			} else {
				errorMap.put(VendorCategoryAttributeFieldNames.MANDATORY.value(),
						Arrays.asList(VendorCategoryAttributeErrorMsg.INVALID_MANDATORY_INPUT_VALUE.value()));
			}
			break;
		case "weight":
			try {
				vendorcategoryattribute.setWeight(Integer.parseInt(value.asText()));
			} catch (NumberFormatException nef) {
				errorMap.put(VendorCategoryAttributeFieldNames.WEIGHT.value(),
						Arrays.asList(VendorCategoryAttributeErrorMsg.INVAID_WEIGHT.value()));
			}
			break;
		case "flags":
			try {
				vendorcategoryattribute.setFlags(Integer.parseInt(value.asText()));
			} catch (NumberFormatException nef) {
				errorMap.put(VendorCategoryAttributeFieldNames.FLAGS.value(),
						Arrays.asList(VendorCategoryAttributeErrorMsg.INVALID_FLAGS.value()));
			}
			break;
		default:
			try {
				if (path.startsWith(VendorCategoryAttributeFieldNames.TAGS.value())) {
					String[] pathArr = path.split("/");
					if (pathArr.length == CategoryAttributeConstants.VENDOR_CATEGORY_ATTRIBUTE_NAME_WEIGHT_PATH_IDENTIFIER.value) {
						vendorcategoryattribute.getTags().forEach(tag -> {
							if (tag.getId().equals(pathArr[1])) {
								tag.setUpdatedTime(LocalDateTime.now());
								if (pathArr[2].equals(VendorCategoryAttributeFieldNames.NAME.value())) {
									tag.setName(value.asText());
								} else {
									try {
										tag.setWeight(Integer.parseInt(value.asText()));
									} catch (NumberFormatException nef) {
										errorMap.put(VendorCategoryAttributeFieldNames.WEIGHT.value(),
												Arrays.asList(VendorCategoryAttributeErrorMsg.INVAID_WEIGHT.value()));
									}
								}
							}
						});
					}
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				errorMap.put(VendorCategoryAttributeErrorMsg.MESSAGE.value(),
						Arrays.asList(VendorCategoryAttributeErrorMsg.INVALID_PATH.value()));
			}
			break;
		}

		if (MapUtils.isNotEmpty(errorMap)) {
			throw new ArkaValidationException(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_VALIDATION_FAILED.value(), errorMap);
		}

		try {
			vendorcategoryattribute = jsonPatchUtils.performSingleJsonPatch(vendorcateattrjson, vendorcategoryattribute,
					VendorCategoryAttribute.class);
		} catch (IOException | JsonPatchException e) {
			errorMap.put(VendorCategoryAttributeErrorMsg.MESSAGE.value(), Arrays.asList(e.getMessage()));
		}

		if (MapUtils.isNotEmpty(errorMap)) {
			throw new ArkaValidationException(
					VendorCategoryAttributeErrorMsg.VENDOR_CATEGORY_ATTRIBUTE_VALIDATION_FAILED.value(), errorMap);
		}

		return vendorcategoryattribute;
	}

}
