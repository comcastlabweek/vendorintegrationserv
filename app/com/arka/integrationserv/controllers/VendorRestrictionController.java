package com.arka.integrationserv.controllers;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.JsonPatchUtils;
import com.arka.common.controllers.utils.ResultWrapperUtils;
import com.arka.common.encrypt.decrypt.constant.VendorIntegrationEncryptDecryptFields;
import com.arka.common.encrypt.decrypt.constant.VendorProductEncryptDecryptFields;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.productmgmt.constants.CategoryAttributeConstants;
import com.arka.common.productmgmt.constants.RestrictionFields;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.PatchValidator;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.vendorrestriction.VendorRestriction;
import com.arka.integrationserv.models.vendorrestriction.constant.ConstraintType;
import com.arka.integrationserv.models.vendorrestriction.constant.DataType;
import com.arka.integrationserv.models.vendorrestriction.constant.VendorRestrictionErrorMsg;
import com.arka.integrationserv.models.vendorrestriction.constant.VendorRestrictionFieldNames;
import com.arka.integrationserv.models.vendorrestriction.service.VendorRestrictionService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatchException;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

public class VendorRestrictionController extends Controller {

	@Inject
	VendorRestrictionService<String> vendorRestrictionService;

	@Inject
	HttpExecutionContext executor;

	@Inject
	JsonPatchUtils jsonPatchUtils;

	@Inject
	PatchValidator patchValidator;

	private final List<String> encDecFields = VendorIntegrationEncryptDecryptFields.getEncAndDecFields();

	public CompletionStage<Result> getAll() {

		Map<String, String[]> queryString = request().queryString();

		if (MapUtils.isNotEmpty(queryString)) {
			String[] projectionFields = queryString.get(CriteriaUtils.PFIELD);
			Map<String, List<String>> query = CriteriaUtils.queryStringArrayToList(encDecFields, queryString);
			ObjectNode recordCountDetails = Json.newObject();
			if (queryString.containsKey(CriteriaUtils.RCOUNT) && queryString.get(CriteriaUtils.RCOUNT) != null) {

				recordCountDetails.put(CriteriaUtils.TOTAL_RECORD_COUNT,
						vendorRestrictionService.getVendorRestrictionCount());

				recordCountDetails.put(CriteriaUtils.FILTERED_RECORD_COUNT,
						vendorRestrictionService.getVendorRestrictionCount(query));
			}

			return vendorRestrictionService.findVendorRestrictions(query).thenApplyAsync(vendorRestrictionList -> {

				return ok(JsonUtils.toEncryptedIdJson(
						ResultWrapperUtils.wrapListInJson(vendorRestrictionList, projectionFields, recordCountDetails),
						encDecFields));
			});
		}

		return vendorRestrictionService.findVendorRestrictions(null).thenApplyAsync(col -> {
			return ok(JsonUtils.toEncryptedIdJson(ResultWrapperUtils.wrapListInJson(col, null), encDecFields));
		});

	}

	public CompletionStage<Result> add() {

		return CompletableFuture.supplyAsync(() -> {
			try {
				JsonNode reqJson = request().body().asJson();
				putEncryptedObjIdForEmptyMapId(reqJson);
				ObjectNode vendorRestrictionJson = (ObjectNode) JsonUtils.toDecryptedIdJson(reqJson, encDecFields);
				System.out.println("vendorRestrictionJson :"+vendorRestrictionJson);
				VendorRestriction vendorRestriction = Json.fromJson(vendorRestrictionJson, VendorRestriction.class);
				vendorRestrictionService.saveVendorRestriction(vendorRestriction);
				return ok(JsonUtils.toEncryptedIdJson(vendorRestriction, encDecFields));
			} catch (ArkaValidationException e) {
				return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
			}
		}, executor.current());

	}

	public CompletionStage<Result> getById(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {

			return vendorRestrictionService.findVendorRestrictionById(_id).thenApplyAsync(vendorRestriction -> {
				if (vendorRestriction != null) {
					return ok(JsonUtils.toEncryptedIdJson(vendorRestriction, encDecFields));
				} else {
					return notFound(JsonUtils
							.errorDataNotFoundJson(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NOT_FOUND.value()));
				}
			});

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}
	}

	public CompletionStage<Result> update(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {

			JsonNode reqJson = request().body().asJson();
			putEncryptedObjIdForEmptyMapId(reqJson);
			JsonNode restrictionUpdateJson = JsonUtils.toDecryptedIdJson(reqJson, encDecFields);

			ObjectNode errorJson = JsonUtils.newJsonObject();

			if (JsonUtils.isValidField(restrictionUpdateJson, UpdateNames.OP.value())
					&& JsonUtils.isValidField(restrictionUpdateJson, UpdateNames.PATH.value())
					&& JsonUtils.isValidField(restrictionUpdateJson, UpdateNames.VALUE.value())) {

				if (JsonUpdateOperations.REPLACE.value()
						.equalsIgnoreCase(restrictionUpdateJson.get(UpdateNames.OP.value()).asText())) {
					return vendorRestrictionService.findVendorRestrictionById(_id).thenApplyAsync(restriction -> {

						if (restriction != null) {
							try {
								JsonNode patchErrorJson = patchValidator.validatePatchJson(restrictionUpdateJson);
								if (JsonUtils.isValidField(patchErrorJson, JsonUtils.ERROR)) {
									return ok(JsonUtils.toJson(patchErrorJson));
								}
								restriction = updateFieldInVendorRestriction(restriction, restrictionUpdateJson);
								boolean updated = vendorRestrictionService.updateVendorRestriction(restriction);

								if (updated) {
									return ok(JsonUtils.toEncryptedIdJson(restriction, encDecFields));
								} else {
									errorJson.put(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_ERROR.value(),
											VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NOT_UPDATED.value());
								}
							} catch (ArkaValidationException e) {
								errorJson.set(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_ERROR.value(),
										JsonUtils.toJson(e.getErrors()));
							}

						} else {
							errorJson.put(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_ERROR.value(),
									VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NOT_FOUND.value());
						}
						return badRequest(errorJson);
					});
				} else {
					errorJson.put(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_ERROR.value(),
							VendorRestrictionErrorMsg.PATCH_OPERATION_NOT_SUPPORTED.value());
				}

			} else {
				errorJson.put(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_ERROR.value(),
						VendorRestrictionErrorMsg.VENDOR_RESTRICTION_NOT_UPDATED_OP_OR_PATH_MISSING_IN_PATCH_DATA
								.value());
			}

			return CompletableFuture.supplyAsync(() -> badRequest(errorJson));

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}
	}

	public CompletionStage<Result> put(String encId) {
		String _id = CryptUtils.decryptData(encId);
		if (StringUtils.isNotEmpty(_id)) {
			return CompletableFuture.supplyAsync(() -> {
				JsonNode reqJson = request().body().asJson();
				putEncryptedObjIdForEmptyMapId(reqJson);
				JsonNode vendorRestrictionJson = JsonUtils.toDecryptedIdJson(reqJson, encDecFields);
				VendorRestriction vendorRestriction = Json.fromJson(vendorRestrictionJson, VendorRestriction.class);
				try {
					vendorRestriction.setId(_id);
					vendorRestrictionService.updateVendorRestriction(vendorRestriction);
				} catch (ArkaValidationException e) {
					return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
				}
				return ok(JsonUtils.toEncryptedIdJson(vendorRestriction, encDecFields));
			}, executor.current());

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}
	}

	public CompletionStage<Result> delete(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {
			// do not delete just mark as inactive
			return vendorRestrictionService.findVendorRestrictionById(_id).thenApplyAsync(vendorRestriction -> {
				try {
					vendorRestriction.setStatus(Status.INACTIVE);
					vendorRestrictionService.updateVendorRestriction(vendorRestriction);

				} catch (ArkaValidationException e) {
					return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
				}
				return ok(JsonUtils.toEncryptedIdJson(vendorRestriction, encDecFields));
			});

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}
	}

	private VendorRestriction updateFieldInVendorRestriction(VendorRestriction vendorRestriction,
			JsonNode vendorServiceUpdateJson) {

		Map<String, List<String>> errorMap = new HashMap<>();

		String path = vendorServiceUpdateJson.get(UpdateNames.PATH.value()).asText();
		String value = vendorServiceUpdateJson.get(UpdateNames.VALUE.value()).asText();
		if (StringUtils.isEmpty(path) || StringUtils.isEmpty(value)) {
			errorMap.put(UpdateNames.PATH.value(), Arrays.asList("Path Cannot be Null"));
		} else {
			path = path.toLowerCase(Locale.ENGLISH);
			switch (path) {

			case "vendor_id":
				try {
					Long.parseLong(value);
				} catch (NumberFormatException nef) {
					errorMap.put(VendorRestrictionFieldNames.VENDOR_ID.value(),
							Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_VENDOR_ID.value()));
				}
				break;
			case "data_type":
				try {
					DataType.valueOf(value);
				} catch (IllegalArgumentException ex) {
					errorMap.put(VendorRestrictionFieldNames.DATATYPE.value(),
							Arrays.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_DATA_TYPE_INVALID.value()));
				}
				break;
			case "constraint_type":
				try {
					ConstraintType.valueOf(value);
				} catch (IllegalArgumentException ex) {
					errorMap.put(VendorRestrictionFieldNames.CONSTRAINTTYPE.value(), Arrays
							.asList(VendorRestrictionErrorMsg.VENDOR_RESTRICTION_CONSTRAINT_TYPE_INVALID.value()));
				}
				break;
			case "status":
				try {
					Status.valueOf(value);
				} catch (IllegalArgumentException ex) {
					errorMap.put(VendorRestrictionFieldNames.STATUS.value(),
							Arrays.asList(VendorRestrictionErrorMsg.STATUS_INVALID.value()));
				}
				break;
			default:
				try {
					if (path.startsWith(VendorRestrictionFieldNames.TAGS.value())) {
						String[] pathArr = path.split("/");
						if (pathArr.length == CategoryAttributeConstants.VENDOR_CATEGORY_ATTRIBUTE_NAME_WEIGHT_PATH_IDENTIFIER.value) {
							vendorRestriction.getTags().forEach(tag -> {
								if (tag.getId().equals(pathArr[1])) {
									tag.setUpdatedTime(LocalDateTime.now());
									if (pathArr[2].equals(VendorRestrictionFieldNames.NAME.value())) {
										tag.setName(value);
									} else {
										try {
											tag.setWeight(Integer.parseInt(value));
										} catch (NumberFormatException nef) {
											errorMap.put(VendorRestrictionFieldNames.WEIGHT.value(), Arrays.asList(
													VendorRestrictionErrorMsg.VENDOR_RESTRICTION_WEIGHT.value()));
										}
									}
								}
							});
						}
					}
				} catch (ArrayIndexOutOfBoundsException a) {
					errorMap.put(VendorRestrictionErrorMsg.MESSAGE.value(),
							Arrays.asList(VendorRestrictionErrorMsg.INVALID_PATH.value()));
				}
				break;
			}

			if (path.startsWith(VendorRestrictionFieldNames.TAGS.value())) {
				if (MapUtils.isNotEmpty(errorMap)) {
					throw new ArkaValidationException(
							VendorRestrictionErrorMsg.VENDOR_RESTRICTION_VALIDATION_FAILED.value(), errorMap);
				} else {
					return vendorRestriction;
				}
			}
			try {
				vendorRestriction = jsonPatchUtils.performSingleJsonPatch(vendorServiceUpdateJson, vendorRestriction,
						VendorRestriction.class);
			} catch (IOException | JsonPatchException e) {
				errorMap.put(VendorRestrictionErrorMsg.MESSAGE.value(),
						Arrays.asList(VendorRestrictionErrorMsg.PATCH_OPERATION_NOT_SUPPORTED.value()));
			}
			if (MapUtils.isNotEmpty(errorMap)) {
				throw new ArkaValidationException(
						VendorRestrictionErrorMsg.VENDOR_RESTRICTION_VALIDATION_FAILED.value(), errorMap);
			}
		}
		return vendorRestriction;
	}
	
	private void putEncryptedObjIdForEmptyMapId(JsonNode obj) {
		RestrictionFields.RESTRICTION_CONSTRAINT_TYPE.value();
		
		if(JsonUtils.isValidField(obj, RestrictionFields.RESTRICTION_CONSTRAINT_TYPE.value())
				&& (obj.get(RestrictionFields.RESTRICTION_CONSTRAINT_TYPE.value()).asText().equals(ConstraintType.MAP.name())
				|| obj.get(RestrictionFields.RESTRICTION_CONSTRAINT_TYPE.value()).asText().equals(ConstraintType.RANGE.name()))
				&& JsonUtils.isValidField(obj, RestrictionFields.RESTRICTION_CONSTRAINT_VALUE.value())
				&& JsonUtils.isValidIndex(obj.get(RestrictionFields.RESTRICTION_CONSTRAINT_VALUE.value()), 0)) {
			
			obj.get(RestrictionFields.RESTRICTION_CONSTRAINT_VALUE.value()).forEach(ele->{
				
				if(ele.has(VendorProductEncryptDecryptFields.MAP_ID.getValue())
						&& ele.hasNonNull(VendorProductEncryptDecryptFields.MAP_ID.getValue())) {
					if(ele.get(VendorProductEncryptDecryptFields.MAP_ID.getValue()).asText().trim().length()==0) {
						((ObjectNode)ele).put(VendorProductEncryptDecryptFields.MAP_ID.getValue(), 
								CryptUtils.encryptData(new ObjectId().toHexString()));
					}
				}
				
				if(ele.has(VendorProductEncryptDecryptFields.RANGE_ID.getValue())
						&& ele.hasNonNull(VendorProductEncryptDecryptFields.RANGE_ID.getValue())) {
					if(ele.get(VendorProductEncryptDecryptFields.RANGE_ID.getValue()).asText().trim().length()==0) {
						((ObjectNode)ele).put(VendorProductEncryptDecryptFields.RANGE_ID.getValue(), 
								CryptUtils.encryptData(new ObjectId().toHexString()));
					}
				}
				
				if(ele.has(VendorProductEncryptDecryptFields.RANGE_YEAR_ID.getValue())
						&& ele.hasNonNull(VendorProductEncryptDecryptFields.RANGE_YEAR_ID.getValue())) {
					if(ele.get(VendorProductEncryptDecryptFields.RANGE_YEAR_ID.getValue()).asText().trim().length()==0) {
						((ObjectNode)ele).put(VendorProductEncryptDecryptFields.RANGE_YEAR_ID.getValue(), 
								CryptUtils.encryptData(new ObjectId().toHexString()));
					}
				}
				
			});
			
		}
	}
}