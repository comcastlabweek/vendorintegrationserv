package com.arka.integrationserv.controllers;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arka.common.constants.RedisDB;
import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.controllers.utils.RequestWrapperUtils;
import com.arka.common.encrypt.decrypt.constant.VendorIntegrationEncryptDecryptFields;
import com.arka.common.insurance.constants.InsuranceAttributeCode;
import com.arka.common.policy.constants.PolicyFields;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.productmgmt.constants.CategoryAttributeFields;
import com.arka.common.productmgmt.constants.ProductFields;
import com.arka.common.provider.cache.CacheService;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.Validation;
import com.arka.integrationserv.models.vendorplan.VendorConnector;
import com.arka.integrationserv.models.vendorplan.VendorConnectorFactory;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.avaje.ebean.enhance.agent.SysoutMessageOutput;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.data.Form;
import play.data.FormFactory;
import play.data.validation.ValidationError;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

// controller for processing user enquiry

public class VendorQuoteController extends Controller {

	public static final Logger logger = LoggerFactory.getLogger(VendorQuoteController.class);

	@Inject
	FormFactory formFactory;

	@Inject
	VendorConnectorFactory vendorConnectorFactory;

	@Inject
	CacheService cache;

	@Inject
	RequestWrapperUtils requestWrapperUtils;
	
	@Inject
	MandatoryParams mandatoryParams;


	// get vendor plans for given product

	private final List<String> encDecFields = VendorIntegrationEncryptDecryptFields.getEncAndDecFields();

	public CompletionStage<Result> getVendorQuote() {

		logger.debug("Request for fetch plan :: " + Json.prettyPrint(request().body().asJson()));

		Form<VendorRequest> enquiryQuote = null;

		enquiryQuote = requestWrapperUtils.buildDecryptedIdWithFormObj(request().body().asJson(), encDecFields,
				VendorRequest.class);

		// check for errors
		if (enquiryQuote.hasErrors()) {
			logger.debug("Cannot process the request : " + enquiryQuote.errors());
			return CompletableFuture
					.completedFuture(badRequest(ResponsePayLoad.getJsonresponseForBadRequest(enquiryQuote.errors())));
		}
		VendorRequest enquiryQuoteReq = enquiryQuote.get();
		VendorConnector vendorConnector = vendorConnectorFactory.createVendorConnector(enquiryQuoteReq.getProductCode(),
				enquiryQuoteReq.getCategoryCode());
		// if no vendor connector found return as error
		if (vendorConnector == null) {
			ValidationError vendorConnectorError = new ValidationError(
					VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
					FetchPlanMessage.CANNOT_SEEK_VENDOR_CONNECTOR_FOR_PRODUCT.value() + ":"
							+ enquiryQuoteReq.getProductCode() + "," + enquiryQuoteReq.getCategoryCode());
			return CompletableFuture
					.completedFuture(badRequest(JsonUtils.getJsonresponseForBadRequest(vendorConnectorError)));
		}

		logger.debug("connector for fetching vendor quote : " + vendorConnector.getClass().getName());

		VendorResponse enquiryQuoteRes = vendorConnector.fetchPlanFromVendor(enquiryQuoteReq);
		JsonNode vendorJsonResponse = enquiryQuoteRes.formatResponse();
		logger.debug("Respnse from vendor : " + Json.prettyPrint(vendorJsonResponse));
		return CompletableFuture.completedFuture(ok(JsonUtils.toEncryptedIdJson(vendorJsonResponse, encDecFields)));
	}

	public Result buyVendorPlan() {
		logger.debug("Request for buy plan :: " + Json.prettyPrint(request().body().asJson()));

		Form<VendorRequest> enquiryQuote = null;
		enquiryQuote = requestWrapperUtils.buildDecryptedIdWithFormObj(request().body().asJson(), encDecFields,
				VendorRequest.class);

		// check for errors
		if (enquiryQuote.hasErrors()) {
			logger.debug("Cannot process the request : " + enquiryQuote.errors());
			return badRequest(ResponsePayLoad.getJsonresponseForBadRequest(enquiryQuote.errors()));
		}
		VendorRequest enquiryQuoteReq = enquiryQuote.get();
		if (enquiryQuoteReq.isQuoteKeyEmpty()) {
			return badRequest(ResponsePayLoad.getJsonresponseForBadRequest(new ValidationError(
					VendorQuoteConstants.QUOTE_KEY.value(), FetchPlanMessage.QUOTE_KEY_EMPTY.value())));
		}

		VendorConnector vendorConnector = vendorConnectorFactory.createVendorConnector(enquiryQuoteReq.getProductCode(),
				enquiryQuoteReq.getCategoryCode());
		if (vendorConnector == null) {
			ValidationError vendorConnectorError = new ValidationError(
					VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
					FetchPlanMessage.CANNOT_SEEK_VENDOR_CONNECTOR_FOR_PRODUCT.value() + ":"
							+ enquiryQuoteReq.getProductCode() + "," + enquiryQuoteReq.getCategoryCode());
			return badRequest(JsonUtils.getJsonresponseForBadRequest(vendorConnectorError));
		}

		logger.debug("connector for buying vendor quote : " + vendorConnector.getClass().getName());
		VendorResponse enquiryQuoteRes = vendorConnector.buyPlanFromVendor(enquiryQuoteReq);
		JsonNode vendorJsonResponse = enquiryQuoteRes.formatResponse();
		logger.debug("Respnse from vendor : " + Json.prettyPrint(vendorJsonResponse));
		return ok(JsonUtils.toEncryptedIdJson(vendorJsonResponse, encDecFields));
	}

	public Result purchaseVendorPlan() {
		Form<VendorRequest> purchasePolicyRequestform = null;
		// JsonNode proposalJson =
		// JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
		Map<String, List<String>> mandatQP = CriteriaUtils.queryStringArrayToList(request().queryString());
		if(Validation.queryValidator("isLvgi").test(mandatQP) && 
				Validation.queryValueGetter(mandatQP, "isLvgi").equals("true")){
			 return lvgiCreatePolicy();
		}
		
		purchasePolicyRequestform = requestWrapperUtils.buildDecryptedIdWithFormObj(request().body().asJson(),
				encDecFields, VendorRequest.class);
		VendorRequest purchasePolicyRequest = purchasePolicyRequestform.get();
		
		//logic for aadharvalue to be set inside request
		JsonNode aadharValue = cache.getValueWithKeyPrefix(VendorQuoteConstants.REQUEST_PROPOSAL.value()+"."+String.valueOf(CryptUtils.encryptData(purchasePolicyRequest.getProposalId()))+"."+InsuranceAttributeCode.AI_AADHARCARD.getCode(),RedisDB.PURCHASE.getValue()).toCompletableFuture().join();
		if (JsonUtils.isValidField(aadharValue, "keys")) {
			InputPolicyParams inputPolicyParams = new InputPolicyParams();
			aadharValue.get("keys").forEach(ele -> {
				if (JsonUtils.isValidField(ele,VendorQuoteConstants.REQUEST_PROPOSAL.value()+"."+String.valueOf(CryptUtils.encryptData(purchasePolicyRequest.getProposalId()))+"."+InsuranceAttributeCode.AI_AADHARCARD.getCode())) {
					JsonNode aadharJson = JsonUtils.toJson(ele.get(VendorQuoteConstants.REQUEST_PROPOSAL.value()+"."+String.valueOf(CryptUtils.encryptData(purchasePolicyRequest.getProposalId()))+"."+InsuranceAttributeCode.AI_AADHARCARD.getCode()).asText());
					if (JsonUtils.isValidField(aadharJson, InsuranceAttributeCode.AI_AADHARCARD.getCode())){
						inputPolicyParams.setCategoryAttributeCode(InsuranceAttributeCode.AI_AADHARCARD.getCode());
						inputPolicyParams.setValue(CryptUtils.decryptData(aadharJson.get(InsuranceAttributeCode.AI_AADHARCARD.getCode()).asText()));
					}
				}
			});
			if(StringUtils.isNotEmpty(inputPolicyParams.getCategoryAttributeCode())) {
				List<InputPolicyParams> obj = purchasePolicyRequest.getInput_quote_params();
				obj.add(inputPolicyParams);
				purchasePolicyRequest.setInput_quote_params(obj);
			}
		}
		String productCode = purchasePolicyRequest.getProductCode();
		String categoryCode = purchasePolicyRequest.getCategoryCode();

		if (purchasePolicyRequestform.hasErrors()) {
			logger.debug("Cannot process the request : " + purchasePolicyRequestform.errors());
			return badRequest(ResponsePayLoad.getJsonresponseForBadRequest(new ValidationError(
					VendorQuoteConstants.REQUEST_PROPOSAL.value(), FetchPlanMessage.PROPOSAL_INFO_EMPTY.value())));
		}
		if (purchasePolicyRequest.getProductId().isEmpty()) {
			return badRequest(ResponsePayLoad.getJsonresponseForBadRequest(new ValidationError(
					VendorQuoteConstants.PRODUCT_RESPONSE_ID.value(), FetchPlanMessage.PRODUCT_ID_EMPTY.value())));
		}

		VendorConnector vendorConnector = vendorConnectorFactory.createVendorConnector(productCode, categoryCode);
		if (vendorConnector == null) {
			ValidationError vendorConnectorError = new ValidationError(
					VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
					FetchPlanMessage.CANNOT_SEEK_VENDOR_CONNECTOR_FOR_PRODUCT.value() + ":" + productCode + ","
							+ categoryCode);
			return badRequest(JsonUtils.getJsonresponseForBadRequest(vendorConnectorError));
		}
		System.out.println("these are the elements-----------"+purchasePolicyRequestform);
		logger.debug("connector for purchasing vendor quote : " + vendorConnector.getClass().getName());
		// ObjectNode purchasePlanJson = vendorConnector.purchasePlan(proposalJson);
		ObjectNode purchasePlanJson = vendorConnector.purchasePlan(purchasePolicyRequest);
		logger.debug("Respnse from vendor : " + Json.prettyPrint(purchasePlanJson));
		return ok(JsonUtils.toEncryptedIdJson(purchasePlanJson, encDecFields));
	}
	
	public Result lvgiCreatePolicy() {
		JsonNode reqJson=request().body().asJson();
		System.out.println("VendorQuoteController.lvgiCreatePolicy()===>"+reqJson);
		
		if(JsonUtils.isValidField(reqJson, ProductFields.PRODUCT_CODE.value())
				&& JsonUtils.isValidField(reqJson, CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_CODE.value())){
			
			String productCode= reqJson.get(ProductFields.PRODUCT_CODE.value()).asText();
			String categoryCode=  reqJson.get(CategoryAttributeFields.CATEGORY_ATTRIBUTE_CATEGORY_CODE.value()).asText();
			
			VendorConnector vendorConnector = vendorConnectorFactory.createVendorConnector(productCode, categoryCode);
			if (vendorConnector == null) {
				ValidationError vendorConnectorError = new ValidationError(
						VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
						FetchPlanMessage.CANNOT_SEEK_VENDOR_CONNECTOR_FOR_PRODUCT.value() + ":" + productCode + ","
								+ categoryCode);
					return badRequest(JsonUtils.getJsonresponseForBadRequest(vendorConnectorError));
			}
			
			ObjectNode purchasePlan = vendorConnector.purchasePlan(reqJson);
			if(purchasePlan!=null){
					return ok(purchasePlan);
			}
		}
		
			return badRequest(JsonUtils.errorMsg());
	}

	public CompletionStage<Result> policyPdf(){
		
		JsonNode reqJson=request().body().asJson();
		System.out.println("VendorQuoteController.policyPdf()===>"+reqJson);
		
		if(JsonUtils.isValidField(reqJson, "VendorCode")
				&& JsonUtils.isValidField(reqJson, "categoryCode")){
			
			String productCode= reqJson.get("VendorCode").asText();
			String categoryCode=  reqJson.get("categoryCode").asText();
			
			VendorConnector vendorConnector = vendorConnectorFactory.createVendorConnector(productCode, categoryCode);
			if (vendorConnector == null) {
				ValidationError vendorConnectorError = new ValidationError(
						VendorQuoteConstants.PRODUCT_RESPONSE_CODE.value(),
						FetchPlanMessage.CANNOT_SEEK_VENDOR_CONNECTOR_FOR_PRODUCT.value() + ":" + productCode + ","
								+ categoryCode);
					return CompletableFuture.completedFuture(badRequest(JsonUtils.getJsonresponseForBadRequest(vendorConnectorError)));
			}
			
			ObjectNode policyPdf = vendorConnector.policyPdf(reqJson.get(PolicyFields.POLICY_NUMBER.getCode()).asText());
			System.out.println("////////// policyPdf"+Json.prettyPrint(policyPdf));
			
			if(policyPdf!=null){
					return CompletableFuture.completedFuture(ok(policyPdf));
			}
		}
		
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorMsg()));
		
		
	}

}
