package com.arka.integrationserv.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.services.UserServ;
import com.arka.common.services.VendorProdMgmtServ;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.DataConfigurationField;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationDynamicCollectionField;
import com.arka.integrationserv.models.dataconfiguration.constants.VendorFileUploadConstants;
import com.arka.integrationserv.models.dataconfiguration.service.DataConfigurationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;

import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;

public class VendorFileUploadController extends Controller {

	@Inject
	DataConfigurationService<String> dataConfigurationService;

	@Inject
	HttpExecutionContext executor;

	@Inject
	PropUtils propUtils;

	@Inject
	VendorProdMgmtServ vendorProductService;

	@Inject
	UserServ vendorService;

	@Inject
	MandatoryParams mandatoryParams;

	public CompletionStage<Result> addVendorUploadedFile() {

		MultipartFormData<Object> asMultipartFormData = request().body().asMultipartFormData();
		Map<String, String[]> asFormUrlEncoded = asMultipartFormData.asFormUrlEncoded();
		FilePart<Object> filePart = asMultipartFormData.getFile(VendorFileUploadConstants.VENDOR_UPLOADED_FILE.value());
		ObjectNode messageJson = JsonUtils.newJsonObject();

		if (filePart != null && asFormUrlEncoded != null && !asFormUrlEncoded.isEmpty()) {
			String dataConfigId = asFormUrlEncoded.get(VendorFileUploadConstants.COLLECTION_ID.value())[0];
			dataConfigId = CryptUtils.decryptData(dataConfigId);
			Map<String, List<String>> fileValidationMap = isDataConfigurationIdValid(filePart, dataConfigId);
			if (MapUtils.isEmpty(fileValidationMap)) {
				messageJson.put(VendorFileUploadConstants.SUCCESS.value(), VendorFileUploadConstants.SUCCESS.value());
				return CompletableFuture.completedFuture(ok(messageJson));
			} else {
				messageJson.set(VendorFileUploadConstants.ERROR.value(), JsonUtils.toJson(fileValidationMap));
				return CompletableFuture.completedFuture(badRequest(messageJson));
			}
		} else {
			messageJson.put(VendorFileUploadConstants.ERROR.value(),
					VendorFileUploadConstants.FILE_NOT_FOUND_OR_COLLECTIONID_NOT_FOUND.value());
			return CompletableFuture.completedFuture(notFound(messageJson));
		}
	}

	private Map<String, List<String>> isDataConfigurationIdValid(FilePart<Object> file, String dataConfigId) {
		List<String> errorList = new ArrayList<>();
		Map<String, List<String>> validationErrorMap = new HashMap<>();

		if (StringUtils.isNotEmpty(dataConfigId)) {
			DataConfiguration dataConfiguration = dataConfigurationService.findDataConfigurationById(dataConfigId)
					.toCompletableFuture().join();
			Map<String, List<String>> errors = isFileDataValid(file, dataConfiguration);
			validationErrorMap.putAll(errors);
		} else {
			errorList.add(VendorFileUploadConstants.DATA_CONFIGURATION_ID_NULL.value());
		}
		if (CollectionUtils.isNotEmpty(errorList)) {
			validationErrorMap.put(VendorFileUploadConstants.VENDOR_FILE_UPLOAD_ERROR.value(), errorList);
		}
		return validationErrorMap;
	}

	private Map<String, List<String>> isFileDataValid(FilePart<Object> file, DataConfiguration dataConfiguration) {
		Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());
		Map<String, List<String>> validationErrorMap = new HashMap<>();
		List<String> errorList = new ArrayList<>();
		Map<String, DataConfigurationField> fieldMap = new HashMap<>();

		if (dataConfiguration.getDataConfigurationField() != null
				&& !dataConfiguration.getDataConfigurationField().isEmpty()) {
			dataConfiguration.getDataConfigurationField().forEach(field -> {
				fieldMap.put(field.getFieldName(), field);
			});
		}

		if (file == null || file.getFilename() == null || file.getFilename().isEmpty() || file.getFile() == null) {
			errorList.add(VendorFileUploadConstants.FILE_OBJECT_NULL.value());
		} else {
			String filename = file.getFilename();

			File uploadedFile = (File) file.getFile();
			StringBuilder fileDir = new StringBuilder(
					propUtils.get(VendorFileUploadConstants.VENDOR_FILE_UPLOAD_PATH.value()));

			if (dataConfiguration.getVendorId() != null && dataConfiguration.getProductId() != null) {

				JsonNode vendorJson = vendorService.getVendor(dataConfiguration.getVendorId(), mandatQP, mandatHP)
						.toCompletableFuture().join();
				JsonNode productJson = vendorProductService
						.getProduct(CryptUtils.encryptData(dataConfiguration.getProductId()), mandatQP, mandatHP).toCompletableFuture().join();

				fileDir.delete(0, fileDir.length());
				fileDir.append(
						propUtils.get(VendorFileUploadConstants.VENDOR_FILE_UPLOAD_PATH.value()).trim() + File.separator
								+ productJson.get(VendorFileUploadConstants.PRODUCT_NAME.value()).asText().trim()
								+ File.separator
								+ vendorJson.get(VendorFileUploadConstants.VENDOR_NAME.value()).asText().trim());
			}

			File uploadedFileDir = new File(fileDir.toString());

			if (!uploadedFileDir.exists()) {
				try {
					uploadedFileDir.mkdirs();
				} catch (SecurityException se) {
					errorList.add(se.getMessage());
				}
			}

			try (FileInputStream fIP = new FileInputStream(uploadedFile)) {
				Workbook workbook = WorkbookFactory.create(uploadedFile);
				Sheet spreadsheet = workbook.getSheetAt(0);
				Row row = spreadsheet.getRow(0);
				Map<String, List<String>> headerFieldValidationError = headerFieldValidation(row, dataConfiguration);
				validationErrorMap.putAll(headerFieldValidationError);

				if (validationErrorMap.isEmpty()) {

					Map<String, List<String>> validationForFieldserror = dataTypeValidationForFields(spreadsheet,
							fieldMap);
					validationErrorMap.putAll(validationForFieldserror);
					fIP.close();
					workbook.close();

					if (validationErrorMap.isEmpty()) {
						try {
							boolean createTempCollection = dataConfigurationService
									.createTempCollection(dataConfiguration, dataConfiguration.isHasMapping());
							if (createTempCollection) {
								Files.move(uploadedFile.toPath(),
										new File(fileDir.toString() + File.separator + filename).toPath(),
										StandardCopyOption.REPLACE_EXISTING);

								String tempCollectionName = dataConfiguration.getTempMappingCollections()
										.get(dataConfiguration.getTempMappingCollections().size() - 1);
								List<Map<String, Object>> listDoc = convertExcelToMap(spreadsheet, dataConfiguration);
								boolean hasError = listDoc.stream().filter(list -> {
									if (list.containsKey(JsonUtils.ERROR))
										return true;
									return false;
								}).map(map -> {
									errorList.add(map.get(JsonUtils.ERROR).toString());
									return map;
								}).count() > 0 ? true : false;

								if (!hasError) {
									dataConfigurationService.addVendorUploadedFilesIntoCollection(listDoc,
											tempCollectionName, dataConfiguration);

									if (StringUtils.isNotEmpty(dataConfiguration.getCurrentVersion())) {
										if (StringUtils.isNotEmpty(dataConfiguration.getCollectionName())) {

											boolean createTempFromOriginalCollection = dataConfigurationService
													.createTempFromOriginalCollection(dataConfiguration);

											if (createTempFromOriginalCollection && !dataConfiguration.isHasMapping()
													&& dataConfiguration.getTempMappingCollections().size() > 1) {

												dataConfigurationService
														.updateCurrentVersionandHistory(dataConfiguration);
											}
										}
									}
									dataConfigurationService.createTempCollectionEntryInDataConfig(dataConfiguration);
								}
							} else {
								errorList.add(VendorFileUploadConstants.COLLECTION_NOT_ABLE_TO_CREATED.value());
							}
						} catch (IOException | ArkaValidationException e) {
							errorList.add(e.getMessage());
						}
					}

				}
			} catch (IOException | EncryptedDocumentException | InvalidFormatException e) {
				errorList.add(e.getMessage());
			}
		}
		if (!errorList.isEmpty()) {
			validationErrorMap.put(VendorFileUploadConstants.VENDOR_FILE_UPLOAD_ERROR.value(), errorList);
		}
		return validationErrorMap;
	}

	private List<Map<String, Object>> convertExcelToMap(Sheet spreadsheet, DataConfiguration dataConfiguration) {
		int lastRowNum = spreadsheet != null ? spreadsheet.getLastRowNum() : 0;
		Row headerRow = null;
		short lastCellNum = 0;
		if (lastRowNum > 0) {
			headerRow = spreadsheet.getRow(0);
			lastCellNum = spreadsheet.getRow(0).getLastCellNum();
		}

		List<Map<String, Object>> dataDocs = new ArrayList<>();
		if (headerRow != null && lastCellNum > 0) {
			for (int row = 1; row <= lastRowNum; row++) {

				StringBuilder appendStringToBeEncoded = new StringBuilder();
				Map<String, Object> docmap = new HashMap<>();
				for (int cellIndex = 0; cellIndex < lastCellNum; cellIndex++) {
					String headerField = headerRow.getCell(cellIndex).getStringCellValue();
					StringBuilder dataType = new StringBuilder();
					dataConfiguration.getDataConfigurationField().forEach(name -> {
						if (headerField.equalsIgnoreCase(name.getFieldName())) {
							dataType.append(name.getDataType());
						}
					});
					Cell eachCell = spreadsheet.getRow(row).getCell(cellIndex);
					if (eachCell != null) {
						switch (eachCell.getCellType()) {

						case Cell.CELL_TYPE_STRING:
							docmap.put(headerField, eachCell.getStringCellValue());
							break;
						case Cell.CELL_TYPE_NUMERIC:
							if ("STRING".equalsIgnoreCase(dataType.toString())) {
								double CellValue = eachCell.getNumericCellValue();
								double fractionCellValue = CellValue - Math.round(eachCell.getNumericCellValue());
								if (!(fractionCellValue > 0)) {
									docmap.put(headerField, String.valueOf(Math.round(eachCell.getNumericCellValue())));
								} else {
									docmap.put(headerField, String.valueOf(eachCell.getNumericCellValue()));
								}
							} else {
								docmap.put(headerField, eachCell.getNumericCellValue());
							}
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							docmap.put(headerField, eachCell.getBooleanCellValue());
							break;
						case Cell.CELL_TYPE_FORMULA:
							docmap.put(headerField, eachCell.getCellFormula());
							break;
						case Cell.CELL_TYPE_BLANK:
							docmap.put(headerField, "");
							break;
						case Cell.CELL_TYPE_ERROR:
							docmap.put(headerField, eachCell.getErrorCellValue());
							break;
						default:
							docmap.put(headerField, null);
						}

					} else {
						docmap.put(headerField, "");
					}

				}
				if (MapUtils.isNotEmpty(docmap)) {
					docmap.entrySet().forEach(entry -> {
						appendStringToBeEncoded.append(entry.getValue());
					});
				}
				try {
					String encodeToString = Base64.getEncoder()
							.encodeToString(appendStringToBeEncoded.toString().getBytes("utf-8"));
					docmap.put(DataConfigurationDynamicCollectionField._HASH.value(), encodeToString);
				} catch (UnsupportedEncodingException e) {
					Map<String, Object> error = new HashMap<>();
					error.put(JsonUtils.ERROR, e.getMessage());
					dataDocs.add(error);
					return dataDocs;
				}
				dataDocs.add(docmap);
			}
		}

		return dataDocs;
	}

	private Map<String, List<String>> headerFieldValidation(Row row, DataConfiguration dataConfiguration) {
		Map<String, List<String>> errormap = new HashMap<>();
		AtomicBoolean flag = new AtomicBoolean(false);

		if (dataConfiguration.getDataConfigurationField() != null
				&& !dataConfiguration.getDataConfigurationField().isEmpty()) {
			row.cellIterator().forEachRemaining(cell -> {
				String cellFieldName = cell.getStringCellValue();
				List<String> errorList = new ArrayList<>();
				flag.set(false);
				dataConfiguration.getDataConfigurationField().forEach(field -> {
					if (cellFieldName.equals(field.getFieldName())) {
						flag.set(true);
						return;
					}
				});
				if (StringUtils.isNotEmpty(cellFieldName) && !flag.get()) {
					errorList.add(cellFieldName + VendorFileUploadConstants.TAB_SEPERATOR.value()
							+ VendorFileUploadConstants.FIELDNAME_NOT_MATCHED_WITH_ACTUAL_FIELDNAME.value());
					errormap.put(cellFieldName, errorList);
				}
			});
		}
		return errormap;
	}

	private Map<String, List<String>> dataTypeValidationForFields(Sheet spreadsheet,
			Map<String, DataConfigurationField> fieldMap) {
		Map<String, List<String>> errormap = new HashMap<>();
		int fieldMapSize = fieldMap.size();
		int lastRowNum = spreadsheet.getLastRowNum();
		if (lastRowNum <= 0) {
			errormap.put(VendorFileUploadConstants.ROW.value(),
					Arrays.asList(VendorFileUploadConstants.FILE_IS_EMPTY.value()));
		} else {
			for (int i = 0; i < fieldMapSize; i++) {
				Cell headerCell = spreadsheet.getRow(0).getCell(i);
				String headerString = null;
				if (headerCell != null) {
					headerString = headerCell.getStringCellValue();
				}

				for (int j = 1; j <= lastRowNum; j++) {

					if (fieldMap.containsKey(headerString)) {
						DataConfigurationField dataConfigField = fieldMap.get(headerString);

						String dataType = dataConfigField.getDataType().toString();
						Cell cell = spreadsheet.getRow(j).getCell(i);
						boolean flagForCheckingDataTypeForField = checkEachCellDataType(dataType, cell);
						if (!flagForCheckingDataTypeForField) {
							int rownum = j + 1;
							int columnnum = i + 1;
							if (!errormap.containsKey("Row No" + rownum)) {
								List<String> errorList = new ArrayList<>();
								errorList.add("Row no:\t" + rownum + "\tColumn no:\t" + columnnum
										+ "\t value doesn't match with the " + dataType + " data type");
								errormap.put("Row No" + rownum, errorList);
							} else {
								List<String> errorList = errormap.get("Row No" + rownum);
								errorList.add("Row no:\t" + rownum + "\tColumn no:\t" + columnnum
										+ "\t value doesn't match with the " + dataType + " data type");
							}
						}
					}
				}
			}
		}
		return errormap;

	}

	private boolean checkEachCellDataType(String dataType, Cell cell) {
		boolean flag = false;
		if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return true;
		}
		// try {
		switch (dataType) {

		case "STRING":
		case "CHAR":
			flag = checkForStringCell(cell);
			break;

		case "BOOLEAN":
			cell.getBooleanCellValue();
			flag = true;
			break;

		case "INTEGER":
		case "DOUBLE":
		case "FLOAT":
		case "LONG":
		case "SHORT":
		case "BYTE":
			cell.getNumericCellValue();
			flag = true;
			break;

		case "DATE":
			cell.getDateCellValue();
			flag = true;
			break;

		default:
			cell.getStringCellValue();
			flag = true;
			break;
		}
		// } catch (Exception e) {
		// if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
		// String nullCheck = cell.getStringCellValue();
		// if ("null".equalsIgnoreCase(nullCheck)) {
		// flag = true;
		// }
		// }
		// }

		return flag;
	}

	private boolean checkForStringCell(Cell cell) {
		boolean flag = false;
		if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
			cell.getStringCellValue();
			flag = true;
		} else if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
			cell.getNumericCellValue();
			flag = true;
		} else if (Cell.CELL_TYPE_BOOLEAN == cell.getCellType()) {
			cell.getBooleanCellValue();
			flag = true;
		} else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			cell.getCellFormula();
			flag = true;
		} else {
			flag = false;
		}
		return flag;
	}

}