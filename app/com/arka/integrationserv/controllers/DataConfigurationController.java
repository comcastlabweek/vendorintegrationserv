package com.arka.integrationserv.controllers;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.JsonPatchUtils;
import com.arka.common.controllers.utils.ResultWrapperUtils;
import com.arka.common.encrypt.decrypt.constant.VendorIntegrationEncryptDecryptFields;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.models.constant.Status;
import com.arka.common.update.JsonUpdateOperations;
import com.arka.common.update.PatchValidator;
import com.arka.common.update.UpdateNames;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationConstant;
import com.arka.integrationserv.models.dataconfiguration.constants.DataConfigurationErrorMsg;
import com.arka.integrationserv.models.dataconfiguration.service.DataConfigurationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonpatch.JsonPatchException;
import com.google.inject.Inject;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

public class DataConfigurationController extends Controller {

	@Inject
	DataConfigurationService<String> dataConfigurationService;

	@Inject
	HttpExecutionContext executor;

	@Inject
	PatchValidator patchValidator;

	@Inject
	JsonPatchUtils jsonPatchUtils;

	private final List<String> encDecFields = VendorIntegrationEncryptDecryptFields.getEncAndDecFields();

	public CompletionStage<Result> getAll() {

		Map<String, String[]> queryString = request().queryString();

		if (MapUtils.isNotEmpty(queryString)) {

			String[] projectionFields = queryString.get(CriteriaUtils.PFIELD);
			Map<String, List<String>> query = CriteriaUtils.queryStringArrayToList(encDecFields, queryString);
			ObjectNode recordCountDetails = Json.newObject();
			if (queryString.containsKey(CriteriaUtils.RCOUNT) && queryString.get(CriteriaUtils.RCOUNT) != null) {
				recordCountDetails.put(CriteriaUtils.TOTAL_RECORD_COUNT,
						dataConfigurationService.getDataConfigurationCount());
				recordCountDetails.put(CriteriaUtils.FILTERED_RECORD_COUNT,
						dataConfigurationService.getDataConfigurationCount(query));
			}

			return dataConfigurationService.findDataConfigurations(query).thenApplyAsync(col -> {
				return ok(JsonUtils.toEncryptedIdJson(
						ResultWrapperUtils.wrapListInJson(col, projectionFields, recordCountDetails), encDecFields));
			});
		}

		return dataConfigurationService.findDataConfigurations(null).thenApplyAsync(col -> {
			return ok(JsonUtils.toEncryptedIdJson(ResultWrapperUtils.wrapListInJson(col, null), encDecFields));
		});
	}

	public CompletionStage<Result> get(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {

			return dataConfigurationService.findDataConfigurationById(_id).thenApplyAsync(dataConfig -> {
				if (dataConfig != null) {
					return ok(JsonUtils.toEncryptedIdJson(dataConfig, encDecFields));
				} else {
					return notFound(JsonUtils
							.errorDataNotFoundJson(DataConfigurationErrorMsg.DATA_CONFIGURATION_NOT_FOUND.value()));
				}
			});
		} else {
			return CompletableFuture.completedFuture(badRequest("RESOURCE NOT FOUND"));
		}
	}

	public CompletionStage<Result> add() {

		return CompletableFuture.supplyAsync(() -> {
			try {
				JsonNode dataConfigJson = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
				DataConfiguration dataConfig = Json.fromJson(dataConfigJson, DataConfiguration.class);
				dataConfigurationService.saveDataConfiguration(dataConfig);
				return ok(JsonUtils.toEncryptedIdJson(dataConfig, encDecFields));
			} catch (ArkaValidationException e) {
				return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
			}
		}, executor.current());
	}

	public CompletionStage<Result> delete(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {
			return dataConfigurationService.findDataConfigurationById(_id).thenApplyAsync(dataConfig -> {
				if(dataConfig!=null){
					if ("ACTIVE".equalsIgnoreCase(dataConfig.getStatus().toString())) {
						dataConfig.setStatus(Status.INACTIVE);
					} else {
						dataConfig.setStatus(Status.ACTIVE);
					}
					// dataConfigurationService.activeandInactiveDataConfiguration(dataConfig);
					dataConfigurationService.updateDataConfiguration(dataConfig);
				}else{
					return badRequest("RESOURCE NOT FOUND");
				}
				return ok(JsonUtils.toEncryptedIdJson(dataConfig, encDecFields));
			});
		} else {
			return CompletableFuture.completedFuture(badRequest("RESOURCE NOT FOUND"));
		}
	}

	public CompletionStage<Result> update(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {

			JsonNode dataConfigUpdateJson = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);

			ObjectNode errorJson = JsonUtils.newJsonObject();

			if (JsonUtils.isValidField(dataConfigUpdateJson, UpdateNames.OP.value())
					&& JsonUtils.isValidField(dataConfigUpdateJson, UpdateNames.PATH.value())
					&& JsonUtils.isValidField(dataConfigUpdateJson, UpdateNames.PATH.value())) {

				if (JsonUpdateOperations.REPLACE.value()
						.equalsIgnoreCase(dataConfigUpdateJson.get(UpdateNames.OP.value()).asText())) {

					return dataConfigurationService.findDataConfigurationById(_id).thenApplyAsync(dataConfig -> {

						if (dataConfig != null) {
							try {
								dataConfig = updateFieldInDataConfig(dataConfig, dataConfigUpdateJson);
								boolean updated = dataConfigurationService.updateDataConfiguration(dataConfig);
								if (updated) {
									return ok(JsonUtils.toEncryptedIdJson(dataConfig, encDecFields));
								} else {
									errorJson.put(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
											DataConfigurationErrorMsg.DATA_CONFIGURATION_NOT_UPDATED.value());
								}

							} catch (ArkaValidationException ave) {
								errorJson.set(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
										JsonUtils.toJson(ave.getErrors()));
								
							}

						} else {
							errorJson.put(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
									DataConfigurationErrorMsg.DATA_CONFIGURATION_NOT_FOUND.value());
						}
						return notFound(errorJson);
					});

				} else {
					errorJson.put(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
							DataConfigurationErrorMsg.PATCH_OPERATION_NOT_SUPPORTED.value());
				}
			} else {
				errorJson.put(DataConfigurationErrorMsg.DATA_CONFIG_ERROR.value(),
						DataConfigurationErrorMsg.DATA_CONFIGURATION_NOT_UPDATED_OP_OR_PATH_MISSING_IN_PATCH_DATA
								.value());
			}

			return CompletableFuture.supplyAsync(() -> {
				return notFound(errorJson);
			});

		} else {
			return CompletableFuture.completedFuture(badRequest("RESOURCE NOT FOUND"));
		}
	}

	private DataConfiguration updateFieldInDataConfig(DataConfiguration dataConfiguration,
			JsonNode dataServiceUpdateJson) throws ArkaValidationException {

		Map<String, List<String>> errorMap = new HashMap<>();

		String path = dataServiceUpdateJson.get(UpdateNames.PATH.value()).asText();
		path = path.substring(1);

		try {
			dataConfiguration = jsonPatchUtils.performSingleJsonPatch(dataServiceUpdateJson, dataConfiguration,
					DataConfiguration.class);
		} catch (IOException | JsonPatchException e) {
			errorMap.put(path, Arrays.asList(e.getMessage()));
		}

		if (MapUtils.isNotEmpty(errorMap)) {
			throw new ArkaValidationException(DataConfigurationErrorMsg.DATA_CONFIGURATION_VALIDATION_FAILED.value(),
					errorMap);
		}
		return dataConfiguration;
	}

	public CompletionStage<Result> put(String _id) {
		_id = CryptUtils.decryptData(_id);
		if (StringUtils.isNotEmpty(_id)) {
			return CompletableFuture.supplyAsync(() -> {
				JsonNode dataConfigJson = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
				DataConfiguration dataConfig = Json.fromJson(dataConfigJson, DataConfiguration.class);
				try {
					dataConfig = dataConfigurationService.updateDataConfigurationfromForm(dataConfig);
				} catch (ArkaValidationException e) {
					return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
				}
				return ok(JsonUtils.toEncryptedIdJson(dataConfig, encDecFields));
			}, executor.current());

		} else {
			return CompletableFuture.completedFuture(badRequest("RESOURCE NOT FOUND"));
		}
	}

	public CompletionStage<Result> checkUniquenessForCollectionName() {
		return CompletableFuture.supplyAsync(() -> {
			String collectionName = request().getQueryString(DataConfigurationConstant.COLLECTION_NAME.value());
			try {
				dataConfigurationService.checkUniquenessForCollectionName(collectionName);
			} catch (ArkaValidationException e) {
				return internalServerError(JsonUtils.errorDataNotFoundJson(JsonUtils.toJson(e.getErrors())));
			}
			return notFound(JsonUtils.newJsonObject());
		}, executor.current());
	}

}