/**
 * 
 */
package com.arka.integrationserv.controllers;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.arka.common.controllers.utils.CriteriaUtils;
import com.arka.common.controllers.utils.MandatoryParams;
import com.arka.common.encrypt.decrypt.constant.VendorIntegrationEncryptDecryptFields;
import com.arka.common.exceptions.ArkaValidationException;
import com.arka.common.utils.CryptUtils;
import com.arka.common.utils.JsonUtils;
import com.arka.common.utils.PropUtils;
import com.arka.common.utils.Validation;
import com.arka.integrationserv.models.collectionmapping.service.CollectionMappingService;
import com.arka.integrationserv.models.dataconfiguration.DataConfiguration;
import com.arka.integrationserv.models.dataconfiguration.service.DataConfigurationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http.Context;
import play.mvc.Result;

/**
 * @author arul.p
 *
 *
 */
public class CollectionMappingController extends Controller {

	@Inject
	DataConfigurationService<String> dataConfigurationService;

	@Inject
	private CollectionMappingService collectionMappingService;

	@Inject
	HttpExecutionContext httpExeContext;

	@Inject
	MandatoryParams mandatoryParams;

	@Inject
	PropUtils propUtils;

	private final List<String> encDecFields = VendorIntegrationEncryptDecryptFields.getEncAndDecFields();

	public CompletionStage<Result> getCollectionDetails(String encId) {
		String id = CryptUtils.decryptData(encId);
		if (StringUtils.isNotEmpty(id)) {

			return dataConfigurationService.findDataConfigurationById(id).thenApplyAsync(dataConfig -> {
				if (dataConfig != null) {
					return ok(JsonUtils.toEncryptedIdJson(dataConfig, encDecFields));
				}
				return notFound(JsonUtils.newJsonObject());
			});

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}

	}

	public CompletionStage<Result> getTempCollections() {
		Map<String, List<String>> mandatQP = CriteriaUtils.queryStringArrayToList(request().queryString());
		if (Validation.queryValidator("id").test(mandatQP)
				&& Validation.queryValidator("collection_name").test(mandatQP)) {
			String id = Validation.queryValueGetter(mandatQP, "id");
			String collection_name = Validation.queryValueGetter(mandatQP, "collection_name");
			return getCollectionDataWithFields(id, collection_name);
		} else if (Validation.queryValidator("id").test(mandatQP)) {
			String id = Validation.queryValueGetter(mandatQP, "id");
			id = CryptUtils.decryptData(id);
			Map<String, List<String>> query = new HashMap<>();
			query.put("product_id", Arrays.asList(id));

			return dataConfigurationService.findDataConfigurations(query).thenApplyAsync(dataConfigList -> {
				if (dataConfigList != null) {
					ArrayNode dataConfigArr = Json.newArray();
					dataConfigList.parallelStream().filter(config -> {
						if (config.isHasMapping()) {
							return true;
						}
						return false;
					}).forEach(dataConfig -> {
						List<String> tempMapCollections = dataConfig.getTempMappingCollections();
						ObjectNode dataConfigObj = Json.newObject();
						dataConfigObj.put("_id", dataConfig.getId());
						dataConfigObj.set("list", Json.toJson(tempMapCollections));
						dataConfigArr.add(dataConfigObj);
					});
					return ok(JsonUtils.toEncryptedIdJson(dataConfigArr, encDecFields));
				} else {
					return notFound(JsonUtils.errorDataNotFoundJson("Record not found"));
				}
			});

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}
	}

	public CompletionStage<Result> getCollectionDataWithFields(String encId, String collection_name) {
		String id = CryptUtils.decryptData(encId);
		if (StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(collection_name)) {

			Map<String, List<String>> query = new HashMap<>();
			query.put("_id", Arrays.asList(id));
			query.put("temp_mapping_collections", Arrays.asList(collection_name));

			Map<String, List<String>> mandatQP = mandatoryParams.getMandatoryQueryParameter();
			Map<String, List<String>> mandatHP = mandatoryParams
					.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

			return dataConfigurationService.findDataConfigurations(query).thenApplyAsync(tempMappingCollection -> {

				if (tempMappingCollection != null) {
					DataConfiguration dataConfiguration = tempMappingCollection.iterator().next();
					String catalogModelRestrictionId = CryptUtils.encryptData(dataConfiguration.getCatalogModelRestrictionId());
					ArrayNode tempCollectionFields = JsonUtils.newJsonArray();
					JsonNode restrictionData = collectionMappingService
							.getRestrictionById(catalogModelRestrictionId, mandatQP, mandatHP).toCompletableFuture()
							.join();
					Collection<String> fields = dataConfigurationService.getCollectionFields(collection_name);

					if (fields != null) {
						fields.remove("_hash");
						fields.remove("for_mapping");

						fields.iterator().forEachRemaining(vendorField -> tempCollectionFields.add(vendorField));


					}

					ObjectNode arkaCollection = Json.newObject();
					String arkaCollectionName = "";
					if (JsonUtils.isValidField(restrictionData, "constraint_value")
							&& restrictionData.get("constraint_value").size() > 0) {
						arkaCollectionName = restrictionData.get("constraint_value").get(0).get("collection").asText();
						if (StringUtils.isNotEmpty(arkaCollectionName)) {
							Map<String, List<String>> dataMap = new LinkedHashMap<>();
							dataMap.put("cname", Arrays.asList(arkaCollectionName));
							dataMap.put("pno", Arrays.asList("1"));
							// dataMap.put("nor", Arrays.asList("1"));
							dataMap.put(CriteriaUtils.NO_OF_RECORDS.toString(),
									Arrays.asList(propUtils.get("nor.one.value")));
							dataMap.putAll(mandatQP);
							
							arkaCollection.setAll((ObjectNode) collectionMappingService
									.getCollectionWithFilter(arkaCollectionName, dataMap, mandatHP)
									.toCompletableFuture().join());

						}

					}
					ArrayNode arkaCollectionFields = Json.newArray();

					if (arkaCollection.has(JsonUtils.JSON_ARRAY_ITEMS_KEY)
							&& arkaCollection.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).has(0)
							&& arkaCollection.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).has("data")) {
						arkaCollection.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).get("data")
								.get(JsonUtils.JSON_ARRAY_ITEMS_KEY).get(0).fieldNames().forEachRemaining(e -> {
									if (!e.equals("_id"))
										arkaCollectionFields.add(e);
								});
					}

					List<String> searchFieldsInArka = new ArrayList<>();

					String[] searchArka = {};
					String[] searchVendor = { "MANUFACTURE", "ITEM_CODE", "SEATING_CAPACITY" };
					searchFieldsInArka.addAll(Arrays.asList(searchArka));
					List<String> searchFieldsInVendor = new ArrayList<>();
					searchFieldsInVendor.addAll(Arrays.asList(searchVendor));
					ObjectNode tableRecords = Json.newObject();
					tableRecords.set("vendorCollectionFields", JsonUtils.toJson(tempCollectionFields));
					tableRecords.set("searchFieldsInArka", JsonUtils.toJson(searchFieldsInArka));
					tableRecords.set("searchFieldsInVendor",
							JsonUtils.toJson(dataConfiguration.getSearchableRestrictionFieldsForMapping()));
					tableRecords.put("vendorCollectionName", collection_name);
					tableRecords.put("tempMappingUpdateTime", String.valueOf(dataConfiguration.getUpdatedTime()));
					tableRecords.put("tempMappingCreateTime", String.valueOf(dataConfiguration.getCreatedTime()));
					tableRecords.put("data_config_id", id);
					tableRecords.put("arkaCollectionName", arkaCollectionName);
					tableRecords.put("product_id", dataConfiguration.getProductId());
					tableRecords.set("arkaCollectionFields", JsonUtils.toJson(arkaCollectionFields));

					return ok(JsonUtils.toEncryptedIdJson(tableRecords, encDecFields));

				}
				return notFound(JsonUtils.errorDataNotFoundJson("Record not found"));
			});

		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}

	}

	public CompletionStage<Result> dataMapping() {
		JsonNode reqJson = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
		Map<String, List<String>> mandatQP = CriteriaUtils.queryStringArrayToList(encDecFields,
				request().queryString());
		Map<String, List<String>> mandatHP = mandatoryParams
				.getMandatoryHeaderFromArgs(Context.current().args.entrySet());

		ObjectNode resp = Json.newObject();
		if (JsonUtils.isValidField(reqJson, "vendor_collection_name")
				&& JsonUtils.isValidField(reqJson, "vendor_collection_id")) {
			String collectionName = reqJson.get("vendor_collection_name").asText();
			String id = CryptUtils.decryptData(reqJson.get("vendor_collection_id").asText());
			Map<String, Object> idFromCollection = dataConfigurationService.findOneByIdFromCollection(collectionName,
					id);

			if (MapUtils.isNotEmpty(idFromCollection) && idFromCollection.containsKey("_id")) {
				Map<String, Object> toBeupdated = new HashMap<>();

				if (JsonUtils.isValidField(reqJson, "source") && reqJson.get("source").asText().equals("map")) {
					JsonNode respFromMap = JsonUtils.newJsonObject();
					respFromMap = collectionMappingService.putProductIdInArkaCollection(reqJson, mandatQP, mandatHP)
							.toCompletableFuture().join();
					toBeupdated.put("product_id", reqJson.get("product_id").asText());
					toBeupdated.put("mapped_id", reqJson.get("arka_collection_id").asText());
					toBeupdated.put("mapping_status", "mapped");
					resp.set("mappedStatus", respFromMap);

				} else {
					if (idFromCollection.get("mapped_id") != null) {
						ObjectNode mappedIdGet = (ObjectNode) reqJson;
						mappedIdGet.put("arka_collection_id", idFromCollection.get("mapped_id").toString());
						JsonNode respFromMap = JsonUtils.newJsonObject();
						respFromMap = collectionMappingService
								.putProductIdInArkaCollection(mappedIdGet, mandatQP, mandatHP).toCompletableFuture()
								.join();
						resp.set("mappedStatus", respFromMap);
					}
					toBeupdated.put("product_id", null);
					toBeupdated.put("mapped_id", null);
					toBeupdated.put("mapping_status", "unmapped");

				}
				toBeupdated.put("version", Instant.now().getNano());

				Map<String, Object> findOneAndUpdate = dataConfigurationService
						.findOneAndUpdateByIdFromCollection(collectionName, id, toBeupdated);

				resp.setAll((ObjectNode) JsonUtils.toJson(findOneAndUpdate));
			} else {
				resp.put("errors", "Record Not found");
			}
		} else {
			resp.put("errors", "Not a valid data");
		}
		return CompletableFuture.completedFuture(ok(JsonUtils.toEncryptedIdJson(resp, encDecFields)));
	}

	public CompletionStage<Result> mergeWithMainCollection() throws ArkaValidationException {

		JsonNode queryString = JsonUtils.toDecryptedIdJson(request().body().asJson(), encDecFields);
		if (JsonUtils.isValidField(queryString, "data_config_id")) {

			return dataConfigurationService.findDataConfigurationById(queryString.get("data_config_id").asText())
					.thenApplyAsync(dataConfig -> {

						List<String> tempcollections = dataConfig.getTempMappingCollections();

						ObjectNode respObj = Json.newObject();

						String collectionName = queryString.get("vendorCollectionName").asText();

						if (tempcollections != null && tempcollections.contains(collectionName)) {
							Map<String, Object> query = new HashMap<>();
							query.put("mapping_status", "mapped");
							long total = dataConfigurationService.getCollectionRecordCount(collectionName, null);
							long merged = dataConfigurationService.getCollectionRecordCount(collectionName, query);

							if (JsonUtils.isValidField(queryString, "mergecheck")
									&& queryString.get("mergecheck") != null) {
								respObj.put("total_records", total);
								respObj.put("merged_records", merged);
								respObj.put("unmerged_records", total - merged);
							} else {

								if (merged > 0) {

									String version = String.valueOf(Instant.now().getNano());

									List<Map<String, Object>> dataFromCollection = dataConfigurationService
											.findFromCollection(collectionName, query).parallelStream().map(data -> {
												data.remove("_id");
												data.replace("version", version);
												return data;
											}).collect(Collectors.toList());

									Map<String, List<String>> mainDataTableQuery = new HashMap<>();
									mainDataTableQuery.put("temp_mapping_collections", Arrays.asList(collectionName));
									List<DataConfiguration> dataConfigList = new ArrayList<>(dataConfigurationService
											.findDataConfigurations(mainDataTableQuery).toCompletableFuture().join());

									if (dataConfigList != null && !dataConfigList.isEmpty()) {
										Optional<DataConfiguration> findAny = dataConfigList.stream()
												.filter(dataConfigCol -> !dataConfigCol.getCollectionName()
														.equals(collectionName))
												.findAny();

										DataConfiguration mainCollectionDataConfig = new DataConfiguration();

										if (findAny.isPresent()) {
											mainCollectionDataConfig = findAny.get();
											dataConfigurationService.insertMany(
													mainCollectionDataConfig.getCollectionName(), dataFromCollection);
											dataConfigurationService.deleteMany(collectionName, query);

										} else {
										}

										long collectionTotal = collectionMappingService.recordCount(collectionName);
												//dbService.getCollection(collectionName).count();

										if (collectionTotal == 0) {
//											dbService.getCollection(collectionName).drop();
											collectionMappingService.dropCollection(collectionName);
											// dataConfigurationService.deleteDataConfigurationById(mainCollectionDataConfig.getId());
										}

									}

									List<String> versionHistory = new ArrayList<>();
									if (dataConfig.getVersionHistory() != null) {
										versionHistory.addAll(dataConfig.getVersionHistory());
									}
									versionHistory.add(version);
									dataConfig.setVersionHistory(versionHistory);
									dataConfig.setCurrentVersion(version);
									try {
										dataConfigurationService.updateDataConfiguration(dataConfig);
									} catch (ArkaValidationException e) {
										return internalServerError(JsonUtils.getJsonResponseInternalServerError(e));
									}
									respObj.put("success", "successfully merged");
								} else {
									respObj.put("errors", "Empty merge");
								}
							}
						} else {
							respObj.put("errors", "Error in merge");
						}
						return ok(JsonUtils.toEncryptedIdJson(respObj, encDecFields));
					});
		} else {
			return CompletableFuture.completedFuture(badRequest(JsonUtils.errorDataNotFoundJson("RESOURCE NOT FOUND")));
		}
	}

}
