/**
 * 
 */
package com.arka.integrationserv.interceptor;

import java.lang.reflect.Method;
import java.util.concurrent.CompletionStage;

import play.http.ActionCreator;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Http.Request;
import play.mvc.Result;

/**
 * @author himagirinathan
 *
 */
public class ArkaActionCreator implements ActionCreator {

	@Override
	public Action<Void> createAction(Request req, Method actionMethod) {
		return new Action.Simple() {
			@Override
			public CompletionStage<Result> call(Context ctx) {
				return delegate.call(ctx);
			}
		};
	}

}
