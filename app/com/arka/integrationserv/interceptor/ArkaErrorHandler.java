/**
 * 
 */
package com.arka.integrationserv.interceptor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.fasterxml.jackson.databind.node.ObjectNode;

import play.Logger;
import play.http.HttpErrorHandler;
import play.libs.Json;
import play.mvc.Http.RequestHeader;
import play.mvc.Http.Status;
import play.mvc.Result;
import play.mvc.Results;

/**
 * @author himagirinathan
 *
 */

public class ArkaErrorHandler implements HttpErrorHandler {

	public CompletionStage<Result> onClientError(RequestHeader request, int statusCode, String message) {
		Logger.error("Client Error : statusCode="+statusCode+", message="+message+" request="+request.uri());
		if (statusCode == Status.NOT_FOUND) {
			ObjectNode objectNode = Json.newObject();
			objectNode.put("message", "Resource Not Found");
			return CompletableFuture.completedFuture(Results.notFound(objectNode));
		}
		return CompletableFuture.completedFuture(Results.status(statusCode, "{\"message\":\"" + message + "\"}"));
	}

	public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception) {
		StringWriter stringWriter = new StringWriter();
		exception.printStackTrace(new PrintWriter(stringWriter));
		ObjectNode objectNode = Json.newObject();
		objectNode.put("message", "A server error occurred");
		objectNode.put("error", stringWriter.toString());
		return CompletableFuture.completedFuture(Results.internalServerError(objectNode));
	}
}
