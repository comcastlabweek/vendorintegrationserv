package religare.health;

/**
 * Created by ganesh on 24/07/17.
 */
public class AdditionalCover {

    private AdditionalCoverType type;
    private double coverageValueInLakhs;
    private boolean optional;
    private double premium;

    public AdditionalCover() {
    }

    public AdditionalCover(AdditionalCoverType type) {
        this.type = type;
    }

    public AdditionalCover(AdditionalCoverType type, double coverageValueInLakhs) {
        this.type = type;
        this.coverageValueInLakhs = coverageValueInLakhs;
    }

    public AdditionalCover(AdditionalCoverType type, boolean optional, double premium) {
        this.type = type;
        this.optional = optional;
        this.premium = premium;
    }

    public AdditionalCover(AdditionalCoverType type, double coverageValue, boolean optional, double premium) {
        this.type = type;
        this.coverageValueInLakhs = coverageValue;
        this.optional = optional;
        this.premium = premium;
    }

    public AdditionalCoverType getType() {
        return type;
    }

    public void setType(AdditionalCoverType type) {
        this.type = type;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public double getCoverageValueInLakhs() {
        return coverageValueInLakhs;
    }

    public void setCoverageValueInLakhs(double coverageValueInLakhs) {
        this.coverageValueInLakhs = coverageValueInLakhs;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }
}
