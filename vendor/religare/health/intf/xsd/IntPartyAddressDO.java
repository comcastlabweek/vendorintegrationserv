
package religare.health.intf.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntPartyAddressDO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntPartyAddressDO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addressLine1Lang1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addressLine2Lang1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addressTypeCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="areaCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cityCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pinCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="stateCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntPartyAddressDO", propOrder = {
    "addressLine1Lang1",
    "addressLine2Lang1",
    "addressTypeCd",
    "areaCd",
    "cityCd",
    "pinCode",
    "stateCd"
})
public class IntPartyAddressDO {

    @XmlElement(nillable = true)
    protected String addressLine1Lang1;
    @XmlElement(nillable = true)
    protected String addressLine2Lang1;
    @XmlElement(nillable = true)
    protected String addressTypeCd;
    @XmlElement(nillable = true)
    protected String areaCd;
    @XmlElement(nillable = true)
    protected String cityCd;
    @XmlElement(nillable = true)
    protected String pinCode;
    @XmlElement(nillable = true)
    protected String stateCd;

    /**
     * Gets the value of the addressLine1Lang1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine1Lang1() {
        return addressLine1Lang1;
    }

    /**
     * Sets the value of the addressLine1Lang1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine1Lang1(String value) {
        this.addressLine1Lang1 = value;
    }

    /**
     * Gets the value of the addressLine2Lang1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressLine2Lang1() {
        return addressLine2Lang1;
    }

    /**
     * Sets the value of the addressLine2Lang1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressLine2Lang1(String value) {
        this.addressLine2Lang1 = value;
    }

    /**
     * Gets the value of the addressTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressTypeCd() {
        return addressTypeCd;
    }

    /**
     * Sets the value of the addressTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressTypeCd(String value) {
        this.addressTypeCd = value;
    }

    /**
     * Gets the value of the areaCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAreaCd() {
        return areaCd;
    }

    /**
     * Sets the value of the areaCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAreaCd(String value) {
        this.areaCd = value;
    }

    /**
     * Gets the value of the cityCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityCd() {
        return cityCd;
    }

    /**
     * Sets the value of the cityCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityCd(String value) {
        this.cityCd = value;
    }

    /**
     * Gets the value of the pinCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPinCode() {
        return pinCode;
    }

    /**
     * Sets the value of the pinCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPinCode(String value) {
        this.pinCode = value;
    }

    /**
     * Gets the value of the stateCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateCd() {
        return stateCd;
    }

    /**
     * Sets the value of the stateCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateCd(String value) {
        this.stateCd = value;
    }

}
