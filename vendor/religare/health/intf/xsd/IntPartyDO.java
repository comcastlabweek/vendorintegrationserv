
package religare.health.intf.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for IntPartyDO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntPartyDO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="birthDt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="genderCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="guid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="partyAddressDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPartyAddressDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="partyContactDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPartyContactDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="partyEmailDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPartyEmailDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="partyIdentityDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPartyIdentityDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="partyQuestionDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPartyQuestionDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="relationCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="roleCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="titleCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntPartyDO", propOrder = {
    "birthDt",
    "firstName",
    "genderCd",
    "guid",
    "lastName",
    "partyAddressDOList",
    "partyContactDOList",
    "partyEmailDOList",
    "partyIdentityDOList",
    "partyQuestionDOList",
    "relationCd",
    "roleCd",
    "titleCd"
})
public class IntPartyDO {

    @XmlElement(nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDt;
    @XmlElement(nillable = true)
    protected String firstName;
    @XmlElement(nillable = true)
    protected String genderCd;
    @XmlElement(nillable = true)
    protected String guid;
    @XmlElement(nillable = true)
    protected String lastName;
    @XmlElement(nillable = true)
    protected List<IntPartyAddressDO> partyAddressDOList;
    @XmlElement(nillable = true)
    protected List<IntPartyContactDO> partyContactDOList;
    @XmlElement(nillable = true)
    protected List<IntPartyEmailDO> partyEmailDOList;
    @XmlElement(nillable = true)
    protected List<IntPartyIdentityDO> partyIdentityDOList;
    @XmlElement(nillable = true)
    protected List<IntPartyQuestionDO> partyQuestionDOList;
    @XmlElement(nillable = true)
    protected String relationCd;
    @XmlElement(nillable = true)
    protected String roleCd;
    @XmlElement(nillable = true)
    protected String titleCd;

    /**
     * Gets the value of the birthDt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDt() {
        return birthDt;
    }

    /**
     * Sets the value of the birthDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDt(XMLGregorianCalendar value) {
        this.birthDt = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the genderCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenderCd() {
        return genderCd;
    }

    /**
     * Sets the value of the genderCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenderCd(String value) {
        this.genderCd = value;
    }

    /**
     * Gets the value of the guid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuid() {
        return guid;
    }

    /**
     * Sets the value of the guid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuid(String value) {
        this.guid = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the partyAddressDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyAddressDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyAddressDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPartyAddressDO }
     * 
     * 
     */
    public List<IntPartyAddressDO> getPartyAddressDOList() {
        if (partyAddressDOList == null) {
            partyAddressDOList = new ArrayList<IntPartyAddressDO>();
        }
        return this.partyAddressDOList;
    }

    /**
     * Gets the value of the partyContactDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyContactDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyContactDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPartyContactDO }
     * 
     * 
     */
    public List<IntPartyContactDO> getPartyContactDOList() {
        if (partyContactDOList == null) {
            partyContactDOList = new ArrayList<IntPartyContactDO>();
        }
        return this.partyContactDOList;
    }

    /**
     * Gets the value of the partyEmailDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyEmailDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyEmailDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPartyEmailDO }
     * 
     * 
     */
    public List<IntPartyEmailDO> getPartyEmailDOList() {
        if (partyEmailDOList == null) {
            partyEmailDOList = new ArrayList<IntPartyEmailDO>();
        }
        return this.partyEmailDOList;
    }

    /**
     * Gets the value of the partyIdentityDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyIdentityDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyIdentityDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPartyIdentityDO }
     * 
     * 
     */
    public List<IntPartyIdentityDO> getPartyIdentityDOList() {
        if (partyIdentityDOList == null) {
            partyIdentityDOList = new ArrayList<IntPartyIdentityDO>();
        }
        return this.partyIdentityDOList;
    }

    /**
     * Gets the value of the partyQuestionDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyQuestionDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyQuestionDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPartyQuestionDO }
     * 
     * 
     */
    public List<IntPartyQuestionDO> getPartyQuestionDOList() {
        if (partyQuestionDOList == null) {
            partyQuestionDOList = new ArrayList<IntPartyQuestionDO>();
        }
        return this.partyQuestionDOList;
    }

    /**
     * Gets the value of the relationCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationCd() {
        return relationCd;
    }

    /**
     * Sets the value of the relationCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationCd(String value) {
        this.relationCd = value;
    }

    /**
     * Gets the value of the roleCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoleCd() {
        return roleCd;
    }

    /**
     * Sets the value of the roleCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoleCd(String value) {
        this.roleCd = value;
    }

    /**
     * Gets the value of the titleCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitleCd() {
        return titleCd;
    }

    /**
     * Sets the value of the titleCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitleCd(String value) {
        this.titleCd = value;
    }

}
