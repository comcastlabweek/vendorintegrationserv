
package religare.health.intf.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntPolicyDataIO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntPolicyDataIO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntIO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="policy" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPolicyDO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntPolicyDataIO", propOrder = {
    "policy"
})
public class IntPolicyDataIO
    extends IntIO
{

    @XmlElement(nillable = true)
    protected IntPolicyDO policy;

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link IntPolicyDO }
     *     
     */
    public IntPolicyDO getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntPolicyDO }
     *     
     */
    public void setPolicy(IntPolicyDO value) {
        this.policy = value;
    }

}
