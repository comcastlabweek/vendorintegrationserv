
package religare.health.intf.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntPolicyAdditionalFieldsDO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntPolicyAdditionalFieldsDO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fieldAgree" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fieldAlerts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fieldTc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntPolicyAdditionalFieldsDO", propOrder = {
    "fieldAgree",
    "fieldAlerts",
    "fieldTc"
})
public class IntPolicyAdditionalFieldsDO {

    @XmlElement(nillable = true)
    protected String fieldAgree;
    @XmlElement(nillable = true)
    protected String fieldAlerts;
    @XmlElement(nillable = true)
    protected String fieldTc;

    /**
     * Gets the value of the fieldAgree property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldAgree() {
        return fieldAgree;
    }

    /**
     * Sets the value of the fieldAgree property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldAgree(String value) {
        this.fieldAgree = value;
    }

    /**
     * Gets the value of the fieldAlerts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldAlerts() {
        return fieldAlerts;
    }

    /**
     * Sets the value of the fieldAlerts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldAlerts(String value) {
        this.fieldAlerts = value;
    }

    /**
     * Gets the value of the fieldTc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFieldTc() {
        return fieldTc;
    }

    /**
     * Sets the value of the fieldTc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFieldTc(String value) {
        this.fieldTc = value;
    }

}
