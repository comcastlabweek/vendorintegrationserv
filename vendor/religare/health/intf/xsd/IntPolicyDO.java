
package religare.health.intf.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntPolicyDO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntPolicyDO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="businessTypeCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="baseProductId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="baseAgentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="coverType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addOns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="inwardDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntInwardDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="partyDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPartyDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="policyAdditionalFieldsDOList" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPolicyAdditionalFieldsDO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="policyNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="proposalNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="quotationReferenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sumInsured" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="term" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/&gt;
 *         &lt;element name="uwDecisionCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isPremiumCalculation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntPolicyDO", propOrder = {
    "businessTypeCd",
    "baseProductId",
    "baseAgentId",
    "coverType",
    "addOns",
    "inwardDOList",
    "partyDOList",
    "policyAdditionalFieldsDOList",
    "policyNum",
    "proposalNum",
    "quotationReferenceNum",
    "sumInsured",
    "term",
    "uwDecisionCd",
    "isPremiumCalculation"
})
public class IntPolicyDO {

    @XmlElement(nillable = true)
    protected String businessTypeCd;
    @XmlElement(nillable = true)
    protected String baseProductId;
    @XmlElement(nillable = true)
    protected String baseAgentId;
    @XmlElement(nillable = true)
    protected String coverType;
    @XmlElement(nillable = true)
    protected String addOns;
    @XmlElement(nillable = true)
    protected List<IntInwardDO> inwardDOList;
    @XmlElement(nillable = true)
    protected List<IntPartyDO> partyDOList;
    @XmlElement(nillable = true)
    protected List<IntPolicyAdditionalFieldsDO> policyAdditionalFieldsDOList;
    @XmlElement(nillable = true)
    protected String policyNum;
    @XmlElement(nillable = true)
    protected String proposalNum;
    @XmlElement(nillable = true)
    protected String quotationReferenceNum;
    @XmlElement(nillable = true)
    protected String sumInsured;
    @XmlElement(nillable = true)
    protected Short term;
    @XmlElement(nillable = true)
    protected String uwDecisionCd;
    @XmlElement(nillable = true)
    protected String isPremiumCalculation;

    /**
     * Gets the value of the businessTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessTypeCd() {
        return businessTypeCd;
    }

    /**
     * Sets the value of the businessTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessTypeCd(String value) {
        this.businessTypeCd = value;
    }

    /**
     * Gets the value of the baseProductId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseProductId() {
        return baseProductId;
    }

    /**
     * Sets the value of the baseProductId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseProductId(String value) {
        this.baseProductId = value;
    }

    /**
     * Gets the value of the baseAgentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseAgentId() {
        return baseAgentId;
    }

    /**
     * Sets the value of the baseAgentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseAgentId(String value) {
        this.baseAgentId = value;
    }

    /**
     * Gets the value of the coverType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverType() {
        return coverType;
    }

    /**
     * Sets the value of the coverType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverType(String value) {
        this.coverType = value;
    }

    /**
     * Gets the value of the addOns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddOns() {
        return addOns;
    }

    /**
     * Sets the value of the addOns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddOns(String value) {
        this.addOns = value;
    }

    /**
     * Gets the value of the inwardDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inwardDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInwardDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntInwardDO }
     * 
     * 
     */
    public List<IntInwardDO> getInwardDOList() {
        if (inwardDOList == null) {
            inwardDOList = new ArrayList<IntInwardDO>();
        }
        return this.inwardDOList;
    }

    /**
     * Gets the value of the partyDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partyDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartyDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPartyDO }
     * 
     * 
     */
    public List<IntPartyDO> getPartyDOList() {
        if (partyDOList == null) {
            partyDOList = new ArrayList<IntPartyDO>();
        }
        return this.partyDOList;
    }

    /**
     * Gets the value of the policyAdditionalFieldsDOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyAdditionalFieldsDOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyAdditionalFieldsDOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntPolicyAdditionalFieldsDO }
     * 
     * 
     */
    public List<IntPolicyAdditionalFieldsDO> getPolicyAdditionalFieldsDOList() {
        if (policyAdditionalFieldsDOList == null) {
            policyAdditionalFieldsDOList = new ArrayList<IntPolicyAdditionalFieldsDO>();
        }
        return this.policyAdditionalFieldsDOList;
    }

    /**
     * Gets the value of the policyNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNum() {
        return policyNum;
    }

    /**
     * Sets the value of the policyNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNum(String value) {
        this.policyNum = value;
    }

    /**
     * Gets the value of the proposalNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNum() {
        return proposalNum;
    }

    /**
     * Sets the value of the proposalNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNum(String value) {
        this.proposalNum = value;
    }

    /**
     * Gets the value of the quotationReferenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuotationReferenceNum() {
        return quotationReferenceNum;
    }

    /**
     * Sets the value of the quotationReferenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuotationReferenceNum(String value) {
        this.quotationReferenceNum = value;
    }

    /**
     * Gets the value of the sumInsured property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSumInsured() {
        return sumInsured;
    }

    /**
     * Sets the value of the sumInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSumInsured(String value) {
        this.sumInsured = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setTerm(Short value) {
        this.term = value;
    }

    /**
     * Gets the value of the uwDecisionCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUwDecisionCd() {
        return uwDecisionCd;
    }

    /**
     * Sets the value of the uwDecisionCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUwDecisionCd(String value) {
        this.uwDecisionCd = value;
    }

    /**
     * Gets the value of the isPremiumCalculation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPremiumCalculation() {
        return isPremiumCalculation;
    }

    /**
     * Sets the value of the isPremiumCalculation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPremiumCalculation(String value) {
        this.isPremiumCalculation = value;
    }

}
