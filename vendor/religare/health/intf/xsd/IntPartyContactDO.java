
package religare.health.intf.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntPartyContactDO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntPartyContactDO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contactNum" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="contactTypeCd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="stdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntPartyContactDO", propOrder = {
    "contactNum",
    "contactTypeCd",
    "stdCode"
})
public class IntPartyContactDO {

    @XmlElement(nillable = true)
    protected Long contactNum;
    @XmlElement(nillable = true)
    protected String contactTypeCd;
    @XmlElement(nillable = true)
    protected String stdCode;

    /**
     * Gets the value of the contactNum property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getContactNum() {
        return contactNum;
    }

    /**
     * Sets the value of the contactNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setContactNum(Long value) {
        this.contactNum = value;
    }

    /**
     * Gets the value of the contactTypeCd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactTypeCd() {
        return contactTypeCd;
    }

    /**
     * Sets the value of the contactTypeCd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactTypeCd(String value) {
        this.contactTypeCd = value;
    }

    /**
     * Gets the value of the stdCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStdCode() {
        return stdCode;
    }

    /**
     * Sets the value of the stdCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStdCode(String value) {
        this.stdCode = value;
    }

}
