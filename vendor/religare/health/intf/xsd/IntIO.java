
package religare.health.intf.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntIO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntIO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="errorLists" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *         &lt;element name="listErrorListList" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntIO", propOrder = {
    "errorLists",
    "listErrorListList"
})
@XmlSeeAlso({
    IntPolicyDataIO.class
})
public class IntIO {

    @XmlElement(nillable = true)
    protected Object errorLists;
    @XmlElement(nillable = true)
    protected Object listErrorListList;

    /**
     * Gets the value of the errorLists property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getErrorLists() {
        return errorLists;
    }

    /**
     * Sets the value of the errorLists property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setErrorLists(Object value) {
        this.errorLists = value;
    }

    /**
     * Gets the value of the listErrorListList property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getListErrorListList() {
        return listErrorListList;
    }

    /**
     * Sets the value of the listErrorListList property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setListErrorListList(Object value) {
        this.listErrorListList = value;
    }

}
