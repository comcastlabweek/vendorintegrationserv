
package religare.health.intf.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IntInwardDO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntInwardDO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="inwardAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="inwardNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntInwardDO", propOrder = {
    "inwardAmount",
    "inwardNum"
})
public class IntInwardDO {

    @XmlElement(nillable = true)
    protected Double inwardAmount;
    @XmlElement(nillable = true)
    protected String inwardNum;

    /**
     * Gets the value of the inwardAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInwardAmount() {
        return inwardAmount;
    }

    /**
     * Sets the value of the inwardAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInwardAmount(Double value) {
        this.inwardAmount = value;
    }

    /**
     * Gets the value of the inwardNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInwardNum() {
        return inwardNum;
    }

    /**
     * Sets the value of the inwardNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInwardNum(String value) {
        this.inwardNum = value;
    }

}
