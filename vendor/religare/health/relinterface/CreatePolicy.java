
package religare.health.relinterface;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import religare.health.intf.xsd.IntPolicyDataIO;





/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="intIO" type="{http://intf.insurance.symbiosys.c2lbiz.com/xsd}IntPolicyDataIO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "intIO"
})
@XmlRootElement(name = "createPolicy")
public class CreatePolicy {

    @XmlElement(nillable = true)
    protected IntPolicyDataIO intIO;

    /**
     * Gets the value of the intIO property.
     * 
     * @return
     *     possible object is
     *     {@link IntPolicyDataIO }
     *     
     */
    public IntPolicyDataIO getIntIO() {
        return intIO;
    }

    /**
     * Sets the value of the intIO property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntPolicyDataIO }
     *     
     */
    public void setIntIO(IntPolicyDataIO value) {
        this.intIO = value;
    }

}
