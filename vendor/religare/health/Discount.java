package religare.health;

/**
 * Created by ganesh on 24/07/17.
 */
public class Discount {

    private DiscountType discountType;
    private double value;

    public Discount() {
    }

    public Discount(DiscountType discountType, double value) {
        this.discountType = discountType;
        this.value = value;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
