package religare.health;

/**
 * Created by ganesh on 24/07/17.
 */
public enum PurchaseType {
    NEW, RENEWAL
}
