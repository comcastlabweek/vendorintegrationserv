package religare.health;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.arka.common.collectionname.constants.VendorCollectionNames;
import com.arka.common.health.constants.HealthAttributeCode;
import com.arka.common.procurement.constants.VendorQuoteConstants;
import com.arka.common.web.interceptor.ArkaRunTimeException;
import com.arka.integrationserv.models.dataconfiguration.repository.DataConfigurationRepository;
import com.arka.integrationserv.models.vendorplan.constants.FetchPlanMessage;
import com.arka.integrationserv.models.vendorplan.constants.ResponsePayLoad;
import com.arka.integrationserv.models.vendorplan.request.AttributeValue;
import com.arka.integrationserv.models.vendorplan.request.InputPolicyParams;
import com.arka.integrationserv.models.vendorplan.request.VendorRequest;
import com.arka.integrationserv.models.vendorplan.request.VendorResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;

import play.libs.Json;

public class PremiumCalculation {

	public static final String FIELD_INSURED_COUNT_CODE = "insuredCountCode";
	public static final String FIELD_MIN_AGE_IN_BAND = "minAgeInBand";
	public static final String FIELD_AGE_BAND = "ageBand";
	private static final int COPAY_20_PERCENT_MIN_AGE = 61;
	private static final double COPAY_20_PERCENT_DISCOUNT = 0.2;
	private static final double PA_PREMIUM_PER_LAKH = 70.0;
//	private static final double PA_MAX_SUM_ASSURED_IN_LAKHS = 300;
//	private static final int PA_MAX_TIMES_SUM_ASSURED = 10;

	private static int MIN_ADULT_AGE = 18;
	private static int MAX_CHILD_AGE = 24;
	DecimalFormat df = new DecimalFormat("############");
	private static List<Integer> mandatoryNCBSumAssuredList = Arrays.asList(new Integer[] { 3, 4 });

	@Inject
	private DataConfigurationRepository dataConfigurationRepository;

	public VendorResponse getPremiumDetails(VendorRequest vendorRequest, VendorResponse vendorResponse) {
		// TODO: VALIDATE ::::::

		// 1. Child age
		// 2. No. of adults

		String strSumAssured = String.valueOf(vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValueAsLong()/100000);

		try {
			ObjectNode insuredMembersJson = formInsuredMembers(vendorRequest);			
	        int adultCount = insuredMembersJson.get("AdultCount").asInt();
	        int childCount = insuredMembersJson.get("ChildCount").asInt();
	        String insuredCountCode = String.format("%dA%dC", adultCount, childCount);
	        int minAgeInBand =  insuredMembersJson.get("MaxAge").asInt();
			System.out.println("InsuredCountCode: " + insuredCountCode + "| minAgeInBand: " + minAgeInBand);

			if(adultCount>2) {
				return getValidationError(vendorRequest.getProductCode(),vendorResponse,
						FetchPlanMessage.VENDOR_RESTRICTION_ID_DOES_NOT_EXISTS.value());
				
			}
			if(childCount>4){
				return getValidationError(vendorRequest.getProductCode(),vendorResponse,
						FetchPlanMessage.VENDOR_RESTRICTION_ID_DOES_NOT_EXISTS.value());
			}
			
			Document searchQuery = new Document();
			searchQuery.put(FIELD_INSURED_COUNT_CODE, insuredCountCode);
			searchQuery.put(FIELD_MIN_AGE_IN_BAND, new BasicDBObject("$lte", minAgeInBand));

			Bson orderBy = Sorts.orderBy(Sorts.descending(FIELD_MIN_AGE_IN_BAND));
			Bson includeFields = Projections.fields(
					Projections.include(FIELD_INSURED_COUNT_CODE, FIELD_MIN_AGE_IN_BAND, FIELD_AGE_BAND, strSumAssured));
			
			Document document = dataConfigurationRepository.searchFirst(VendorCollectionNames.VENDOR_RELIGARE_HEALTH_PREMIUM.value(),searchQuery, orderBy, includeFields);
//			System.out.println(document);
			if (null != document) {
				System.out.println(document.toJson());
			}

			if (null != document && document.containsKey(strSumAssured)) {
				double basePremium = ((Number) document.get(strSumAssured)).doubleValue();
				
				//TODO:Remove this and include as part of the original calculation
				//GST
				basePremium = basePremium * 1.18;
				
			
				vendorResponse.addAttributeValue(
						getAttributeValue(HealthAttributeCode.AH_PREMIUM_AMOUNT.getCode(), df.format(basePremium)));
				vendorResponse.addAttributeValue(
						getAttributeValue(HealthAttributeCode.AH_HOSPITALIZATION_COVER.getCode(), vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValue()));
				System.out.println("Base Premium: " + basePremium);
				// quoteResponse.setBasePremium(basePremium);

				// Additional Covers
				vendorResponse = calculateAdditionalCovers(vendorRequest, basePremium, minAgeInBand,vendorResponse);

				// Discounts
				vendorResponse = calculateDiscounts(vendorRequest, basePremium, minAgeInBand,vendorResponse);
			}else{
				return getValidationError(VendorQuoteConstants.CATEGORY_RESPONSE_ID.value(),vendorResponse,
						FetchPlanMessage.QUOTES_UNATTAINABLE_FROM_VENDOR.value());
				
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
		
		
		
		return vendorResponse;

	}

	private  VendorResponse calculateAdditionalCovers(VendorRequest vendorRequest, double basePremium, int minAgeInBand, VendorResponse vendorResponse) {

		// TODO: Ganesh: Optimize to single db call for getting both NCB and UAR
        String AH_NO_CLAIM_BONUS_DESCRIPTION = null;
		String strSumAssured = String.valueOf(vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SUM_ASSURED.getCode()).getValueAsLong()/100000);

		
		//List<AdditionalCover> reqAdditionalCovers = vendorRequest.getAdditionalCovers();

		Document document;
		Bson orderBy = Sorts.orderBy(Sorts.descending(FIELD_MIN_AGE_IN_BAND));
		Bson includeFields = Projections.fields(Projections.include("optionalCover", FIELD_MIN_AGE_IN_BAND, FIELD_AGE_BAND, strSumAssured));
		Document searchQuery = new Document();
		//if (null != reqAdditionalCovers) {

			// Additional Cover: NCB
			boolean ncbRequested = true;
			boolean ncbMandatory = isNCBMandatory(Integer.parseInt(strSumAssured));
//			if (!ncbMandatory && null != reqAdditionalCovers) {
//				// Check if it was requested:
//				ncbRequested = reqAdditionalCovers.stream().filter(ac -> ac.getType() == AdditionalCoverType.NCB)
//						.findFirst().isPresent();
//			}

			if (ncbMandatory || ncbRequested) {
				searchQuery = new Document();
				searchQuery.put("optionalCover","NCB");
				searchQuery.put(FIELD_MIN_AGE_IN_BAND, new BasicDBObject("$lte", minAgeInBand));

				document = dataConfigurationRepository.searchFirst(VendorCollectionNames.VENDOR_RELIGARE_HEALTH_NCB_UAR.value(),searchQuery, orderBy, includeFields);

				System.out.println(document);
				if (null != document) {
					System.out.println(document.toJson());
				}

				if (null != document && document.containsKey(strSumAssured)) {
					double percentage = ((Number) document.get(strSumAssured)).doubleValue();
					double premium = basePremium * percentage;

					System.out.println("Additional Premium NCB: " + premium);
					//additionalCoverList.add(new AdditionalCover(AdditionalCoverType.NCB, ncbMandatory, premium));
					AH_NO_CLAIM_BONUS_DESCRIPTION = percentage == 0 ? "This plan does not provide no claim bonus"
							: "No claim bonus upto " + percentage + "%";
					vendorResponse.addAttributeValue(
							getAttributeValue(HealthAttributeCode.AH_NO_CLAIM_BONUS.getCode(), String.valueOf(percentage*100)));
					vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_NO_CLAIM_BONUS_DESCRIPTION.getCode(),
							AH_NO_CLAIM_BONUS_DESCRIPTION));
				}

			}

			// Additional Cover: UAR
			boolean uarRequested = true;
//			if (null != reqAdditionalCovers) {
//				// Check if it was requested:
//				uarRequested = reqAdditionalCovers.stream().filter(ac -> ac.getType() == AdditionalCoverType.UAR)
//						.findFirst().isPresent();
//			}

			if (uarRequested) {
				searchQuery = new Document();			
				searchQuery.put("optionalCover", "UAR");
				searchQuery.put(FIELD_MIN_AGE_IN_BAND, new BasicDBObject("$lte", minAgeInBand));

				document = dataConfigurationRepository.searchFirst(VendorCollectionNames.VENDOR_RELIGARE_HEALTH_NCB_UAR.value(),searchQuery, orderBy, includeFields);

				System.out.println(document);
				if (null != document) {
					System.out.println(document.toJson());
				}

				if (null != document && document.containsKey(strSumAssured)) {
					double percentage = ((Number) document.get(strSumAssured)).doubleValue();
					double premium = basePremium * percentage;
					System.out.println("Additional Premium UAR: " + premium);
				//	additionalCoverList.add(new AdditionalCover(AdditionalCoverType.UAR, false, premium));
					vendorResponse.addAttributeValue(getAttributeValue(
							HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode(), "yes"));			
					vendorResponse.addAttributeValue(
								getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_PERCENT.getCode(),String.valueOf(percentage*100)));
					vendorResponse.addAttributeValue(
								getAttributeValue(HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS_CONDITION.getCode(),""));
				}
			}else{
				vendorResponse.addAttributeValue(getAttributeValue(
						HealthAttributeCode.AH_AUTOMATIC_RESTORATION_BENEFITS.getCode(), "no"));
			}

			// Additional Cover: Personal Accident
			AdditionalCover paAdditionalCover = null;
//			if (null != reqAdditionalCovers) {
//				// Check if it was requested:
//				Optional<AdditionalCover> additionalCoverOptional = reqAdditionalCovers.stream()
//						.filter(ac -> ac.getType() == AdditionalCoverType.PA).findFirst();
//				if (additionalCoverOptional.isPresent()) {
//					paAdditionalCover = additionalCoverOptional.get();
//				}
//			}

			if (null != paAdditionalCover) {
				double paPremium = Double.parseDouble(strSumAssured) * PA_PREMIUM_PER_LAKH;
				System.out.println("Additional Premium PA: " + paPremium);
				//additionalCoverList.add(new AdditionalCover(AdditionalCoverType.PA, false, paPremium));
			}
	//	}

		return vendorResponse;

	}

	private VendorResponse calculateDiscounts(VendorRequest vendorRequest, double basePremium, int maxAge, VendorResponse vendorResponse) {
		boolean isCopayOpted = true;
		String purchaseType = "NEW";
		if ((maxAge >= COPAY_20_PERCENT_MIN_AGE)
				&& (purchaseType == PurchaseType.NEW.name() || isCopayOpted)) {

			double discount = -1 * COPAY_20_PERCENT_DISCOUNT * basePremium;
			System.out.println("Discount: " + discount);
			//vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_CO_PAY_PERCENT.getCode(), String.valueOf(discount)));
			vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_CO_PAY.getCode(), "co_pay_based_on_age"));
			vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_CO_PAY_PERCENT.getCode(), String.valueOf(COPAY_20_PERCENT_DISCOUNT*100)));
			vendorResponse.addAttributeValue(getAttributeValue(HealthAttributeCode.AH_CO_PAY_AGE_LIMIT.getCode(), String.valueOf(COPAY_20_PERCENT_MIN_AGE)));
		}

		return vendorResponse;

	}

	private boolean isNCBMandatory(int sumAssuredInLakhs) {

		return mandatoryNCBSumAssuredList.contains(sumAssuredInLakhs);
	}

	private ObjectNode formInsuredMembers(VendorRequest vendorRequest) {
	    int adultCount = 0;
	    int childCount = 0;
	    int maxAge = 0;
		ObjectNode insuredMemebersJson = Json.newObject();
		ObjectNode insuredMemebersObj = insuredMemebersJson.putObject("INSURED_MEMBERS");
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_SELF_AGE.getCode())) {
			 adultCount++;
			int selfAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SELF_AGE.getCode())
					.getValueAsInt();
			maxAge = selfAge;
			ObjectNode selfDetailObj = insuredMemebersObj.putObject("self");
			selfDetailObj.put("age", selfAge);
		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_SPOUSE_AGE.getCode())) {
			 adultCount++;
			int spouseAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_SPOUSE_AGE.getCode())
					.getValueAsInt();
			if(spouseAge>maxAge){
				maxAge = spouseAge;
			}
			ObjectNode selfDetailObj = insuredMemebersObj.putObject("spouse");
			selfDetailObj.put("age", spouseAge);

		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_SON_AGE.getCode())) {
			List<InputPolicyParams> sonAgeList = vendorRequest
					.getInputQuoteForAttribute(HealthAttributeCode.AH_SON_AGE.getCode());
			ObjectNode sonDetailObj = insuredMemebersObj.putObject("son");
			for (InputPolicyParams sonAge : sonAgeList) {
				int sonAgeAsInt = sonAge.getValueAsInt();
				if ((adultCount == 0 && sonAgeAsInt > MIN_ADULT_AGE) || sonAgeAsInt > MAX_CHILD_AGE) {
	                adultCount++;
	            } else {
	                childCount++;
	            }
				List<String> sonTag = sonAge.getTag();
				if (sonTag != null && sonTag.size() > 0) {
					sonTag.stream().filter(
							sonTagElement -> sonTagElement.startsWith("son") && !sonTagElement.equalsIgnoreCase("son"))
							.forEach(sonTagElement -> {
								try {
									int sonIndex = Integer.parseInt(sonTagElement.split("son_")[1]);
									ObjectNode sonIndexObj = sonDetailObj.putObject(String.valueOf(sonIndex));
									sonIndexObj.put("age", sonAge.getValue());

								} catch (Exception e) {
									throw new ArkaRunTimeException(e);
								}

							});
				}

			}
		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_DAUGHTER_AGE.getCode())) {
			List<InputPolicyParams> daughterAgeList = vendorRequest
					.getInputQuoteForAttribute(HealthAttributeCode.AH_DAUGHTER_AGE.getCode());
			ObjectNode daughterDetailObj = insuredMemebersObj.putObject("daughter");
			for (InputPolicyParams daughterAge : daughterAgeList) {
				int daughterAgeAsInt = daughterAge.getValueAsInt();
				if ((adultCount == 0 && daughterAgeAsInt > MIN_ADULT_AGE) || daughterAgeAsInt > MAX_CHILD_AGE) {
	                adultCount++;
	            } else {
	                childCount++;
	            }
				List<String> daughterTag = daughterAge.getTag();
				if (daughterTag != null && daughterTag.size() > 0) {
					daughterTag.stream().filter(daughterTagElement -> daughterTagElement.startsWith("daughter")
							&& !daughterTagElement.equalsIgnoreCase("daughter")).forEach(daughterTagElement -> {
								try {
									int daughterIndex = Integer.parseInt(daughterTagElement.split("daughter_")[1]);
									ObjectNode daughterIndexObj = daughterDetailObj
											.putObject(String.valueOf(daughterIndex));
									daughterIndexObj.put("age", daughterAge.getValue());

								} catch (Exception e) {
									throw new ArkaRunTimeException(e);
								}

							});
				}

			}
		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_FATHER_AGE.getCode())) {
			 adultCount++;
			int fatherAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_FATHER_AGE.getCode())
					.getValueAsInt();
			if(fatherAge > maxAge){
				maxAge = fatherAge;
			}
			ObjectNode fatherDetailObj = insuredMemebersObj.putObject("father");
			fatherDetailObj.put("age", fatherAge);

		}
		if (vendorRequest.isAttributePresent(HealthAttributeCode.AH_MOTHER_AGE.getCode())) {
			 adultCount++;
			int motherAge = vendorRequest.getUniqueInputQuoteForAttribute(HealthAttributeCode.AH_MOTHER_AGE.getCode())
					.getValueAsInt();
			if(motherAge > maxAge){
				maxAge = motherAge;
			}
			ObjectNode motherDetailObj = insuredMemebersObj.putObject("mother");
			motherDetailObj.put("age", motherAge);

		}
		insuredMemebersJson.put("AdultCount", adultCount);
		insuredMemebersJson.put("ChildCount", childCount);
		insuredMemebersJson.put("MaxAge", maxAge);
        return insuredMemebersJson;
	}
	
	private AttributeValue getAttributeValue(String categoryAttributeCode, String value) {
		AttributeValue paramValueObj = new AttributeValue();
		paramValueObj.setCategoryAttributeCode(categoryAttributeCode);
		paramValueObj.setValue(value);
	//	paramValueObj.setTagsAsList(getTagNameListForAttribute(categoryAttributeCode));
		return paramValueObj;
	}
	public VendorResponse getValidationError(String errorKey, VendorResponse vendorResponse, String errorValue) {
		ObjectNode errorJson = Json.newObject();
		errorJson.put(errorKey, errorValue);
		ObjectNode validationErrorJson = Json.newObject();
		validationErrorJson.set(ResponsePayLoad.ERROR, errorJson);
		vendorResponse.setErrorJson(validationErrorJson);
		return vendorResponse;
	}
	
}
