package religare.health;

/**
 * Created by ganesh on 24/07/17.
 */
public class Party {

    private int age;
    private PartyType partyType;

    public Party() {
    }

    public Party(int age, PartyType partyType) {
        this.age = age;
        this.partyType = partyType;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public PartyType getPartyType() {
        return partyType;
    }

    public void setPartyType(PartyType partyType) {
        this.partyType = partyType;
    }
}
