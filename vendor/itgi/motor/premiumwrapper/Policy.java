
package itgi.motor.premiumwrapper;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Policy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Policy"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="agent" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contractType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="inceptionDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="oldVehicle" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="partnerBranch" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="previousPolicyEndDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tieup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vehicle" type="{http://premiumwrapper.motor.itgi.com}Vehicle"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Policy", propOrder = {
    "agent",
    "contractType",
    "expiryDate",
    "inceptionDate",
    "oldVehicle",
    "partnerBranch",
    "previousPolicyEndDate",
    "tieup",
    "vehicle"
})
public class Policy {

    @XmlElement(required = true, nillable = true)
    protected String agent;
    @XmlElement(required = true, nillable = true)
    protected String contractType;
    @XmlElement(required = true, nillable = true)
    protected String expiryDate;
    @XmlElement(required = true, nillable = true)
    protected String inceptionDate;
    @XmlElement(required = true, nillable = true)
    protected String oldVehicle;
    @XmlElement(required = true, nillable = true)
    protected String partnerBranch;
    @XmlElement(required = true, nillable = true)
    protected String previousPolicyEndDate;
    @XmlElement(required = true, nillable = true)
    protected String tieup;
    @XmlElement(required = true, nillable = true)
    protected Vehicle vehicle;

    /**
     * Gets the value of the agent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgent() {
        return agent;
    }

    /**
     * Sets the value of the agent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgent(String value) {
        this.agent = value;
    }

    /**
     * Gets the value of the contractType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractType() {
        return contractType;
    }

    /**
     * Sets the value of the contractType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractType(String value) {
        this.contractType = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the inceptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInceptionDate() {
        return inceptionDate;
    }

    /**
     * Sets the value of the inceptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInceptionDate(String value) {
        this.inceptionDate = value;
    }

    /**
     * Gets the value of the oldVehicle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldVehicle() {
        return oldVehicle;
    }

    /**
     * Sets the value of the oldVehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldVehicle(String value) {
        this.oldVehicle = value;
    }

    /**
     * Gets the value of the partnerBranch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerBranch() {
        return partnerBranch;
    }

    /**
     * Sets the value of the partnerBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerBranch(String value) {
        this.partnerBranch = value;
    }

    /**
     * Gets the value of the previousPolicyEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousPolicyEndDate() {
        return previousPolicyEndDate;
    }

    /**
     * Sets the value of the previousPolicyEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousPolicyEndDate(String value) {
        this.previousPolicyEndDate = value;
    }

    /**
     * Gets the value of the tieup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTieup() {
        return tieup;
    }

    /**
     * Sets the value of the tieup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTieup(String value) {
        this.tieup = value;
    }

    /**
     * Gets the value of the vehicle property.
     * 
     * @return
     *     possible object is
     *     {@link Vehicle }
     *     
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Sets the value of the vehicle property.
     * 
     * @param value
     *     allowed object is
     *     {@link Vehicle }
     *     
     */
    public void setVehicle(Vehicle value) {
        this.vehicle = value;
    }

}
