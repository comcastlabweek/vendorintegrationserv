
package itgi.motor.premiumwrapper;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IDVWebServiceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IDVWebServiceResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="erorMessage" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idv" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="maximumIdvAllowed" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="minimumIdvAllowed" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IDVWebServiceResponse", propOrder = {
    "erorMessage",
    "idv",
    "maximumIdvAllowed",
    "minimumIdvAllowed"
})
public class IDVWebServiceResponse {

    @XmlElement(required = true, nillable = true)
    protected String erorMessage;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal idv;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal maximumIdvAllowed;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal minimumIdvAllowed;

    /**
     * Gets the value of the erorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErorMessage() {
        return erorMessage;
    }

    /**
     * Sets the value of the erorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErorMessage(String value) {
        this.erorMessage = value;
    }

    /**
     * Gets the value of the idv property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIdv() {
        return idv;
    }

    /**
     * Sets the value of the idv property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIdv(BigDecimal value) {
        this.idv = value;
    }

    /**
     * Gets the value of the maximumIdvAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximumIdvAllowed() {
        return maximumIdvAllowed;
    }

    /**
     * Sets the value of the maximumIdvAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximumIdvAllowed(BigDecimal value) {
        this.maximumIdvAllowed = value;
    }

    /**
     * Gets the value of the minimumIdvAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinimumIdvAllowed() {
        return minimumIdvAllowed;
    }

    /**
     * Sets the value of the minimumIdvAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinimumIdvAllowed(BigDecimal value) {
        this.minimumIdvAllowed = value;
    }

}
