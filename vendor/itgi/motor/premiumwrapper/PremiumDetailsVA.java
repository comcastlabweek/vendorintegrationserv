
package itgi.motor.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumDetailsVA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumDetailsVA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://premiumwrapper.motor.itgi.com}PremiumDetails"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="autocoverage" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumDetailsVA", propOrder = {
    "autocoverage"
})
public class PremiumDetailsVA
    extends PremiumDetails
{

    protected boolean autocoverage;

    /**
     * Gets the value of the autocoverage property.
     * 
     */
    public boolean isAutocoverage() {
        return autocoverage;
    }

    /**
     * Sets the value of the autocoverage property.
     * 
     */
    public void setAutocoverage(boolean value) {
        this.autocoverage = value;
    }

}
