
package itgi.motor.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="policyHeader" type="{http://premiumwrapper.motor.itgi.com}PolicyHeader"/&gt;
 *         &lt;element name="policy" type="{http://premiumwrapper.motor.itgi.com}Policy"/&gt;
 *         &lt;element name="partner" type="{http://premiumwrapper.motor.itgi.com}Partner"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "policyHeader",
    "policy",
    "partner"
})
@XmlRootElement(name = "getMotorPremium")
public class GetMotorPremium {

    @XmlElement(required = true)
    protected PolicyHeader policyHeader;
    @XmlElement(required = true)
    protected Policy policy;
    @XmlElement(required = true)
    protected Partner partner;

    /**
     * Gets the value of the policyHeader property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyHeader }
     *     
     */
    public PolicyHeader getPolicyHeader() {
        return policyHeader;
    }

    /**
     * Sets the value of the policyHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyHeader }
     *     
     */
    public void setPolicyHeader(PolicyHeader value) {
        this.policyHeader = value;
    }

    /**
     * Gets the value of the policy property.
     * 
     * @return
     *     possible object is
     *     {@link Policy }
     *     
     */
    public Policy getPolicy() {
        return policy;
    }

    /**
     * Sets the value of the policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Policy }
     *     
     */
    public void setPolicy(Policy value) {
        this.policy = value;
    }

    /**
     * Gets the value of the partner property.
     * 
     * @return
     *     possible object is
     *     {@link Partner }
     *     
     */
    public Partner getPartner() {
        return partner;
    }

    /**
     * Sets the value of the partner property.
     * 
     * @param value
     *     allowed object is
     *     {@link Partner }
     *     
     */
    public void setPartner(Partner value) {
        this.partner = value;
    }

}
