
package itgi.motor.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IDVWebServiceRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IDVWebServiceRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateOfRegistration" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="inceptionDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="makeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rtoCity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IDVWebServiceRequest", propOrder = {
    "dateOfRegistration",
    "inceptionDate",
    "makeCode",
    "rtoCity"
})
public class IDVWebServiceRequest {

    @XmlElement(required = true, nillable = true)
    protected String dateOfRegistration;
    @XmlElement(required = true, nillable = true)
    protected String inceptionDate;
    @XmlElement(required = true, nillable = true)
    protected String makeCode;
    @XmlElement(required = true, nillable = true)
    protected String rtoCity;

    /**
     * Gets the value of the dateOfRegistration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfRegistration() {
        return dateOfRegistration;
    }

    /**
     * Sets the value of the dateOfRegistration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfRegistration(String value) {
        this.dateOfRegistration = value;
    }

    /**
     * Gets the value of the inceptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInceptionDate() {
        return inceptionDate;
    }

    /**
     * Sets the value of the inceptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInceptionDate(String value) {
        this.inceptionDate = value;
    }

    /**
     * Gets the value of the makeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMakeCode() {
        return makeCode;
    }

    /**
     * Sets the value of the makeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMakeCode(String value) {
        this.makeCode = value;
    }

    /**
     * Gets the value of the rtoCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRtoCity() {
        return rtoCity;
    }

    /**
     * Sets the value of the rtoCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRtoCity(String value) {
        this.rtoCity = value;
    }

}
