
package itgi.motor.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoveragePremiumDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoveragePremiumDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="coverageName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="coveragePremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="odPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tpPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoveragePremiumDetails", propOrder = {
    "coverageName",
    "coveragePremium",
    "odPremium",
    "tpPremium"
})
public class CoveragePremiumDetails {

    @XmlElement(required = true, nillable = true)
    protected String coverageName;
    @XmlElement(required = true, nillable = true)
    protected String coveragePremium;
    @XmlElement(required = true, nillable = true)
    protected String odPremium;
    @XmlElement(required = true, nillable = true)
    protected String tpPremium;

    /**
     * Gets the value of the coverageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoverageName() {
        return coverageName;
    }

    /**
     * Sets the value of the coverageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoverageName(String value) {
        this.coverageName = value;
    }

    /**
     * Gets the value of the coveragePremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoveragePremium() {
        return coveragePremium;
    }

    /**
     * Sets the value of the coveragePremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoveragePremium(String value) {
        this.coveragePremium = value;
    }

    /**
     * Gets the value of the odPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOdPremium() {
        return odPremium;
    }

    /**
     * Sets the value of the odPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOdPremium(String value) {
        this.odPremium = value;
    }

    /**
     * Gets the value of the tpPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTpPremium() {
        return tpPremium;
    }

    /**
     * Sets the value of the tpPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTpPremium(String value) {
        this.tpPremium = value;
    }

}
