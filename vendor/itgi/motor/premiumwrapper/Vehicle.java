
package itgi.motor.premiumwrapper;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Vehicle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Vehicle"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aaiExpiryDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="aaiNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="capacity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="engineCpacity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="exShPurPrice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="grossVehicleWeight" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="grossVehicleWt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="itgiRiskOccupationCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="itgiZone" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="make" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="regictrationCity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="registrationDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="registrationState" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="seatingCapacity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vehicleAge" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vehicleBody" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vehicleClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vehicleCoverage" type="{http://premiumwrapper.motor.itgi.com}ArrayOfVehicleCoverage"/&gt;
 *         &lt;element name="vehicleInsuranceCost" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vehicleSubclass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="yearOfManufacture" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="zcover" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Vehicle", propOrder = {
    "aaiExpiryDate",
    "aaiNo",
    "capacity",
    "engineCpacity",
    "exShPurPrice",
    "grossVehicleWeight",
    "grossVehicleWt",
    "itgiRiskOccupationCode",
    "itgiZone",
    "make",
    "regictrationCity",
    "registrationDate",
    "registrationState",
    "seatingCapacity",
    "type",
    "vehicleAge",
    "vehicleBody",
    "vehicleClass",
    "vehicleCoverage",
    "vehicleInsuranceCost",
    "vehicleSubclass",
    "yearOfManufacture",
    "zcover"
})
public class Vehicle {

    @XmlElement(required = true, nillable = true)
    protected String aaiExpiryDate;
    @XmlElement(required = true, nillable = true)
    protected String aaiNo;
    @XmlElement(required = true, nillable = true)
    protected String capacity;
    @XmlElement(required = true, nillable = true)
    protected String engineCpacity;
    @XmlElement(required = true, nillable = true)
    protected String exShPurPrice;
    @XmlElement(required = true, nillable = true)
    protected String grossVehicleWeight;
    @XmlElement(required = true, nillable = true)
    protected String grossVehicleWt;
    @XmlElement(required = true, nillable = true)
    protected String itgiRiskOccupationCode;
    @XmlElement(required = true, nillable = true)
    protected String itgiZone;
    @XmlElement(required = true, nillable = true)
    protected String make;
    @XmlElement(required = true, nillable = true)
    protected String regictrationCity;
    @XmlElement(required = true, nillable = true)
    protected String registrationDate;
    @XmlElement(required = true, nillable = true)
    protected String registrationState;
    @XmlElement(required = true, nillable = true)
    protected String seatingCapacity;
    @XmlElement(required = true, nillable = true)
    protected String type;
    @XmlElement(required = true, nillable = true)
    protected String vehicleAge;
    @XmlElement(required = true, nillable = true)
    protected String vehicleBody;
    @XmlElement(required = true, nillable = true)
    protected String vehicleClass;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfVehicleCoverage vehicleCoverage;
    @XmlElement(required = true, nillable = true)
    protected String vehicleInsuranceCost;
    @XmlElement(required = true, nillable = true)
    protected String vehicleSubclass;
    @XmlElement(required = true, nillable = true)
    protected String yearOfManufacture;
    @XmlElement(required = true, nillable = true)
    protected String zcover;

    /**
     * Gets the value of the aaiExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAaiExpiryDate() {
        return aaiExpiryDate;
    }

    /**
     * Sets the value of the aaiExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAaiExpiryDate(String value) {
        this.aaiExpiryDate = value;
    }

    /**
     * Gets the value of the aaiNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAaiNo() {
        return aaiNo;
    }

    /**
     * Sets the value of the aaiNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAaiNo(String value) {
        this.aaiNo = value;
    }

    /**
     * Gets the value of the capacity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapacity() {
        return capacity;
    }

    /**
     * Sets the value of the capacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapacity(String value) {
        this.capacity = value;
    }

    /**
     * Gets the value of the engineCpacity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEngineCpacity() {
        return engineCpacity;
    }

    /**
     * Sets the value of the engineCpacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEngineCpacity(String value) {
        this.engineCpacity = value;
    }

    /**
     * Gets the value of the exShPurPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExShPurPrice() {
        return exShPurPrice;
    }

    /**
     * Sets the value of the exShPurPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExShPurPrice(String value) {
        this.exShPurPrice = value;
    }

    /**
     * Gets the value of the grossVehicleWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossVehicleWeight() {
        return grossVehicleWeight;
    }

    /**
     * Sets the value of the grossVehicleWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossVehicleWeight(String value) {
        this.grossVehicleWeight = value;
    }

    /**
     * Gets the value of the grossVehicleWt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossVehicleWt() {
        return grossVehicleWt;
    }

    /**
     * Sets the value of the grossVehicleWt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossVehicleWt(String value) {
        this.grossVehicleWt = value;
    }

    /**
     * Gets the value of the itgiRiskOccupationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItgiRiskOccupationCode() {
        return itgiRiskOccupationCode;
    }

    /**
     * Sets the value of the itgiRiskOccupationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItgiRiskOccupationCode(String value) {
        this.itgiRiskOccupationCode = value;
    }

    /**
     * Gets the value of the itgiZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItgiZone() {
        return itgiZone;
    }

    /**
     * Sets the value of the itgiZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItgiZone(String value) {
        this.itgiZone = value;
    }

    /**
     * Gets the value of the make property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMake() {
        return make;
    }

    /**
     * Sets the value of the make property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMake(String value) {
        this.make = value;
    }

    /**
     * Gets the value of the regictrationCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegictrationCity() {
        return regictrationCity;
    }

    /**
     * Sets the value of the regictrationCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegictrationCity(String value) {
        this.regictrationCity = value;
    }

    /**
     * Gets the value of the registrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Sets the value of the registrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationDate(String value) {
        this.registrationDate = value;
    }

    /**
     * Gets the value of the registrationState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationState() {
        return registrationState;
    }

    /**
     * Sets the value of the registrationState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationState(String value) {
        this.registrationState = value;
    }

    /**
     * Gets the value of the seatingCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatingCapacity() {
        return seatingCapacity;
    }

    /**
     * Sets the value of the seatingCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatingCapacity(String value) {
        this.seatingCapacity = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the vehicleAge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleAge() {
        return vehicleAge;
    }

    /**
     * Sets the value of the vehicleAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleAge(String value) {
        this.vehicleAge = value;
    }

    /**
     * Gets the value of the vehicleBody property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleBody() {
        return vehicleBody;
    }

    /**
     * Sets the value of the vehicleBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleBody(String value) {
        this.vehicleBody = value;
    }

    /**
     * Gets the value of the vehicleClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleClass() {
        return vehicleClass;
    }

    /**
     * Sets the value of the vehicleClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleClass(String value) {
        this.vehicleClass = value;
    }

    /**
     * Gets the value of the vehicleCoverage property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfVehicleCoverage }
     *     
     */
    public ArrayOfVehicleCoverage getVehicleCoverage() {
        return vehicleCoverage;
    }

    /**
     * Sets the value of the vehicleCoverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfVehicleCoverage }
     *     
     */
    public void setVehicleCoverage(ArrayOfVehicleCoverage value) {
        this.vehicleCoverage = value;
    }

    /**
     * Gets the value of the vehicleInsuranceCost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleInsuranceCost() {
        return vehicleInsuranceCost;
    }

    /**
     * Sets the value of the vehicleInsuranceCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleInsuranceCost(String value) {
        this.vehicleInsuranceCost = value;
    }

    /**
     * Gets the value of the vehicleSubclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVehicleSubclass() {
        return vehicleSubclass;
    }

    /**
     * Sets the value of the vehicleSubclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVehicleSubclass(String value) {
        this.vehicleSubclass = value;
    }

    /**
     * Gets the value of the yearOfManufacture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYearOfManufacture() {
        return yearOfManufacture;
    }

    /**
     * Sets the value of the yearOfManufacture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYearOfManufacture(String value) {
        this.yearOfManufacture = value;
    }

    /**
     * Gets the value of the zcover property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZcover() {
        return zcover;
    }

    /**
     * Sets the value of the zcover property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZcover(String value) {
        this.zcover = value;
    }

}
