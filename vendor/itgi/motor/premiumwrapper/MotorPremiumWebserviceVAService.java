package itgi.motor.premiumwrapper;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by Apache CXF 3.1.12
 * 2017-10-17T11:19:19.754+05:30
 * Generated source version: 3.1.12
 * 
 */
@WebServiceClient(name = "MotorPremiumWebserviceVAService", 
                  wsdlLocation = "classpath:wsdl/MotorPremiumWebserviceVA.wsdl",
                  targetNamespace = "http://premiumwrapper.motor.itgi.com") 
public class MotorPremiumWebserviceVAService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://premiumwrapper.motor.itgi.com", "MotorPremiumWebserviceVAService");
    public final static QName MotorPremiumWebserviceVA = new QName("http://premiumwrapper.motor.itgi.com", "MotorPremiumWebserviceVA");
    static {
    	 URL url = MotorPremiumWebserviceVAService.class.getClassLoader().getResource("wsdl/MotorPremiumWebserviceVA.wsdl");
         if (url == null) {
             java.util.logging.Logger.getLogger(MotorPremiumWebserviceVAService.class.getName())
                 .log(java.util.logging.Level.INFO, 
                      "Can not initialize the default wsdl from {0}", "classpath:wsdl/MotorPremiumWebserviceVA.wsdl");
         }       
         WSDL_LOCATION = url;
    }

    public MotorPremiumWebserviceVAService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public MotorPremiumWebserviceVAService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public MotorPremiumWebserviceVAService() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public MotorPremiumWebserviceVAService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public MotorPremiumWebserviceVAService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public MotorPremiumWebserviceVAService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns MotorPremiumWebserviceVA
     */
    @WebEndpoint(name = "MotorPremiumWebserviceVA")
    public MotorPremiumWebserviceVA getMotorPremiumWebserviceVA() {
        return super.getPort(MotorPremiumWebserviceVA, MotorPremiumWebserviceVA.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns MotorPremiumWebserviceVA
     */
    @WebEndpoint(name = "MotorPremiumWebserviceVA")
    public MotorPremiumWebserviceVA getMotorPremiumWebserviceVA(WebServiceFeature... features) {
        return super.getPort(MotorPremiumWebserviceVA, MotorPremiumWebserviceVA.class, features);
    }

}
