
package itgi.motor.premiumwrapper;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PremiumDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PremiumDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="coveragePremiumDetail" type="{http://premiumwrapper.motor.itgi.com}CoveragePremiumDetails" maxOccurs="unbounded"/&gt;
 *         &lt;element name="discountLoading" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="discountLoadingAmt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="error" type="{http://premiumwrapper.motor.itgi.com}ErrorDetails" maxOccurs="unbounded"/&gt;
 *         &lt;element name="gst" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="premiumPayable" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceTax" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="totalODPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="totalPremimAfterDiscLoad" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="totalPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="totalTPPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PremiumDetails", propOrder = {
    "coveragePremiumDetail",
    "discountLoading",
    "discountLoadingAmt",
    "error",
    "gst",
    "premiumPayable",
    "serviceTax",
    "totalODPremium",
    "totalPremimAfterDiscLoad",
    "totalPremium",
    "totalTPPremium"
})
@XmlSeeAlso({
    PremiumDetailsVA.class
})
public class PremiumDetails {

    @XmlElement(required = true, nillable = true)
    protected List<CoveragePremiumDetails> coveragePremiumDetail;
    @XmlElement(required = true, nillable = true)
    protected String discountLoading;
    @XmlElement(required = true, nillable = true)
    protected String discountLoadingAmt;
    @XmlElement(required = true, nillable = true)
    protected List<ErrorDetails> error;
    @XmlElement(required = true, nillable = true)
    protected String gst;
    @XmlElement(required = true, nillable = true)
    protected String premiumPayable;
    @XmlElement(required = true, nillable = true)
    protected String serviceTax;
    @XmlElement(required = true, nillable = true)
    protected String totalODPremium;
    @XmlElement(required = true, nillable = true)
    protected String totalPremimAfterDiscLoad;
    @XmlElement(required = true, nillable = true)
    protected String totalPremium;
    @XmlElement(required = true, nillable = true)
    protected String totalTPPremium;

    /**
     * Gets the value of the coveragePremiumDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coveragePremiumDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoveragePremiumDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoveragePremiumDetails }
     * 
     * 
     */
    public List<CoveragePremiumDetails> getCoveragePremiumDetail() {
        if (coveragePremiumDetail == null) {
            coveragePremiumDetail = new ArrayList<CoveragePremiumDetails>();
        }
        return this.coveragePremiumDetail;
    }

    /**
     * Gets the value of the discountLoading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountLoading() {
        return discountLoading;
    }

    /**
     * Sets the value of the discountLoading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountLoading(String value) {
        this.discountLoading = value;
    }

    /**
     * Gets the value of the discountLoadingAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountLoadingAmt() {
        return discountLoadingAmt;
    }

    /**
     * Sets the value of the discountLoadingAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountLoadingAmt(String value) {
        this.discountLoadingAmt = value;
    }

    /**
     * Gets the value of the error property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the error property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getError().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorDetails }
     * 
     * 
     */
    public List<ErrorDetails> getError() {
        if (error == null) {
            error = new ArrayList<ErrorDetails>();
        }
        return this.error;
    }

    /**
     * Gets the value of the gst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGst() {
        return gst;
    }

    /**
     * Sets the value of the gst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGst(String value) {
        this.gst = value;
    }

    /**
     * Gets the value of the premiumPayable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumPayable() {
        return premiumPayable;
    }

    /**
     * Sets the value of the premiumPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumPayable(String value) {
        this.premiumPayable = value;
    }

    /**
     * Gets the value of the serviceTax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceTax() {
        return serviceTax;
    }

    /**
     * Sets the value of the serviceTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceTax(String value) {
        this.serviceTax = value;
    }

    /**
     * Gets the value of the totalODPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalODPremium() {
        return totalODPremium;
    }

    /**
     * Sets the value of the totalODPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalODPremium(String value) {
        this.totalODPremium = value;
    }

    /**
     * Gets the value of the totalPremimAfterDiscLoad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPremimAfterDiscLoad() {
        return totalPremimAfterDiscLoad;
    }

    /**
     * Sets the value of the totalPremimAfterDiscLoad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPremimAfterDiscLoad(String value) {
        this.totalPremimAfterDiscLoad = value;
    }

    /**
     * Gets the value of the totalPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPremium() {
        return totalPremium;
    }

    /**
     * Sets the value of the totalPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPremium(String value) {
        this.totalPremium = value;
    }

    /**
     * Gets the value of the totalTPPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalTPPremium() {
        return totalTPPremium;
    }

    /**
     * Sets the value of the totalTPPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalTPPremium(String value) {
        this.totalTPPremium = value;
    }

}
