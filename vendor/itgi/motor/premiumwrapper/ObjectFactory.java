
package itgi.motor.premiumwrapper;


import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.itgi.motor.premiumwrapper package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.itgi.motor.premiumwrapper
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Main }
     * 
     */
    public Main createMain() {
        return new Main();
    }

    /**
     * Create an instance of {@link MainResponse }
     * 
     */
    public MainResponse createMainResponse() {
        return new MainResponse();
    }

    /**
     * Create an instance of {@link GetMotorPremium }
     * 
     */
    public GetMotorPremium createGetMotorPremium() {
        return new GetMotorPremium();
    }

    /**
     * Create an instance of {@link PolicyHeader }
     * 
     */
    public PolicyHeader createPolicyHeader() {
        return new PolicyHeader();
    }

    /**
     * Create an instance of {@link Policy }
     * 
     */
    public Policy createPolicy() {
        return new Policy();
    }

    /**
     * Create an instance of {@link Partner }
     * 
     */
    public Partner createPartner() {
        return new Partner();
    }

    /**
     * Create an instance of {@link GetMotorPremiumResponse }
     * 
     */
    public GetMotorPremiumResponse createGetMotorPremiumResponse() {
        return new GetMotorPremiumResponse();
    }

    /**
     * Create an instance of {@link PremiumDetailsVA }
     * 
     */
    public PremiumDetailsVA createPremiumDetailsVA() {
        return new PremiumDetailsVA();
    }

    /**
     * Create an instance of {@link VehicleCoverage }
     * 
     */
    public VehicleCoverage createVehicleCoverage() {
        return new VehicleCoverage();
    }

    /**
     * Create an instance of {@link ArrayOfVehicleCoverage }
     * 
     */
    public ArrayOfVehicleCoverage createArrayOfVehicleCoverage() {
        return new ArrayOfVehicleCoverage();
    }

    /**
     * Create an instance of {@link Vehicle }
     * 
     */
    public Vehicle createVehicle() {
        return new Vehicle();
    }

    /**
     * Create an instance of {@link CoveragePremiumDetails }
     * 
     */
    public CoveragePremiumDetails createCoveragePremiumDetails() {
        return new CoveragePremiumDetails();
    }

    /**
     * Create an instance of {@link ErrorDetails }
     * 
     */
    public ErrorDetails createErrorDetails() {
        return new ErrorDetails();
    }

    /**
     * Create an instance of {@link PremiumDetails }
     * 
     */
    public PremiumDetails createPremiumDetails() {
        return new PremiumDetails();
    }

}
