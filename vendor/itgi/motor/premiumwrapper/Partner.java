
package itgi.motor.premiumwrapper;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Partner complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Partner"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partnerBranch" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="partnerCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="partnerSubBranch" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Partner", propOrder = {
    "partnerBranch",
    "partnerCode",
    "partnerSubBranch"
})
public class Partner {

    @XmlElement(required = true, nillable = true)
    protected String partnerBranch;
    @XmlElement(required = true, nillable = true)
    protected String partnerCode;
    @XmlElement(required = true, nillable = true)
    protected String partnerSubBranch;

    /**
     * Gets the value of the partnerBranch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerBranch() {
        return partnerBranch;
    }

    /**
     * Sets the value of the partnerBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerBranch(String value) {
        this.partnerBranch = value;
    }

    /**
     * Gets the value of the partnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerCode() {
        return partnerCode;
    }

    /**
     * Sets the value of the partnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerCode(String value) {
        this.partnerCode = value;
    }

    /**
     * Gets the value of the partnerSubBranch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerSubBranch() {
        return partnerSubBranch;
    }

    /**
     * Sets the value of the partnerSubBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerSubBranch(String value) {
        this.partnerSubBranch = value;
    }

}
