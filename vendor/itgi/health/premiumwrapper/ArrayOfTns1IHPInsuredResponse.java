
package itgi.health.premiumwrapper;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import itgi.health.objects.IHPInsuredResponse;


/**
 * <p>Java class for ArrayOf_tns1_IHPInsuredResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOf_tns1_IHPInsuredResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="item" type="{http://objects.ptnr.itgi.com}IHPInsuredResponse" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOf_tns1_IHPInsuredResponse", propOrder = {
    "ihpInsured"
})
public class ArrayOfTns1IHPInsuredResponse {

    protected List<IHPInsuredResponse> ihpInsured;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IHPInsuredResponse }
     * 
     * 
     */
    public List<IHPInsuredResponse> getIhpInsured() {
        if (ihpInsured == null) {
        	ihpInsured = new ArrayList<IHPInsuredResponse>();
        }
        return this.ihpInsured;
    }

}
