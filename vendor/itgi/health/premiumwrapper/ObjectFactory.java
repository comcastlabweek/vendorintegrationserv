
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.itgi.health.premiumwrapper package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.itgi.health.premiumwrapper
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetFHPPremium }
     * 
     */
    public GetFHPPremium createGetFHPPremium() {
        return new GetFHPPremium();
    }

    /**
     * Create an instance of {@link FHPPolicy }
     * 
     */
    public FHPPolicy createFHPPolicy() {
        return new FHPPolicy();
    }

    /**
     * Create an instance of {@link GetFHPPremiumResponse }
     * 
     */
    public GetFHPPremiumResponse createGetFHPPremiumResponse() {
        return new GetFHPPremiumResponse();
    }

    /**
     * Create an instance of {@link FHPPremiumDetails }
     * 
     */
    public FHPPremiumDetails createFHPPremiumDetails() {
        return new FHPPremiumDetails();
    }

    /**
     * Create an instance of {@link FHPInsured }
     * 
     */
    public FHPInsured createFHPInsured() {
        return new FHPInsured();
    }

    /**
     * Create an instance of {@link ArrayOfFHPInsured }
     * 
     */
    public ArrayOfFHPInsured createArrayOfFHPInsured() {
        return new ArrayOfFHPInsured();
    }

    /**
     * Create an instance of {@link ErrorDetails }
     * 
     */
    public ErrorDetails createErrorDetails() {
        return new ErrorDetails();
    }

    /**
     * Create an instance of {@link ArrayOfErrorDetails }
     * 
     */
    public ArrayOfErrorDetails createArrayOfErrorDetails() {
        return new ArrayOfErrorDetails();
    }

    /**
     * Create an instance of {@link ArrayOfTns1FHPInsuredResponse }
     * 
     */
    public ArrayOfTns1FHPInsuredResponse createArrayOfTns1FHPInsuredResponse() {
        return new ArrayOfTns1FHPInsuredResponse();
    }

}
