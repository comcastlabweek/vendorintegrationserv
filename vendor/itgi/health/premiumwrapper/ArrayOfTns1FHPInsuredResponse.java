
package itgi.health.premiumwrapper;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import itgi.health.objects.FHPInsuredResponse;


/**
 * <p>Java class for ArrayOf_tns1_FHPInsuredResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOf_tns1_FHPInsuredResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="item" type="{http://objects.ptnr.itgi.com}FHPInsuredResponse" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOf_tns1_FHPInsuredResponse", propOrder = {
    "fhpInsured"
})
public class ArrayOfTns1FHPInsuredResponse {

    protected List<FHPInsuredResponse> fhpInsured;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FHPInsuredResponse }
     * 
     * 
     */
    public List<FHPInsuredResponse> getFhpInsured() {
        if (fhpInsured == null) {
        	fhpInsured = new ArrayList<FHPInsuredResponse>();
        }
        return this.fhpInsured;
    }

}
