
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IHPPolicy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IHPPolicy"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="criticalIllness" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="expiryDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ihpInsured" type="{http://premiumwrapper.health.itgi.com}ArrayOfIHPInsured"/&gt;
 *         &lt;element name="inceptionDate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="partnerCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="planCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="promoCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="roomRentWaiver" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="uniqueQuoteId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IHPPolicy", propOrder = {
    "criticalIllness",
    "expiryDate",
    "ihpInsured",
    "inceptionDate",
    "partnerCode",
    "promoCode",
    "roomRentWaiver",
    "uniqueQuoteId"
})
public class IHPPolicy {

    @XmlElement(required = true, nillable = true)
    protected String criticalIllness;
    @XmlElement(required = true, nillable = true)
    protected String expiryDate;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfIHPInsured ihpInsured;
    @XmlElement(required = true, nillable = true)
    protected String inceptionDate;
    @XmlElement(required = true, nillable = true)
    protected String partnerCode;
//    @XmlElement(required = true, nillable = true)
//    protected String planCode;
    @XmlElement(required = true, nillable = true)
    protected String promoCode;
    @XmlElement(required = true, nillable = true)
    protected String roomRentWaiver;
    @XmlElement(required = true, nillable = true)
    protected String uniqueQuoteId;

    /**
     * Gets the value of the criticalIllness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriticalIllness() {
        return criticalIllness;
    }

    /**
     * Sets the value of the criticalIllness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriticalIllness(String value) {
        this.criticalIllness = value;
    }

    /**
     * Gets the value of the expiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * Sets the value of the expiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpiryDate(String value) {
        this.expiryDate = value;
    }

    /**
     * Gets the value of the ihpInsured property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfIHPInsured }
     *     
     */
    public ArrayOfIHPInsured getIhpInsured() {
        return ihpInsured;
    }

    /**
     * Sets the value of the ihpInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfIHPInsured }
     *     
     */
    public void setIhpInsured(ArrayOfIHPInsured value) {
        this.ihpInsured = value;
    }

    /**
     * Gets the value of the inceptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInceptionDate() {
        return inceptionDate;
    }

    /**
     * Sets the value of the inceptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInceptionDate(String value) {
        this.inceptionDate = value;
    }

    /**
     * Gets the value of the partnerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerCode() {
        return partnerCode;
    }

    /**
     * Sets the value of the partnerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerCode(String value) {
        this.partnerCode = value;
    }

    /**
     * Gets the value of the planCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
//    public String getPlanCode() {
//        return planCode;
//    }

    /**
     * Sets the value of the planCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
//    public void setPlanCode(String value) {
//        this.planCode = value;
//    }

    /**
     * Gets the value of the promoCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * Sets the value of the promoCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoCode(String value) {
        this.promoCode = value;
    }

    /**
     * Gets the value of the roomRentWaiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomRentWaiver() {
        return roomRentWaiver;
    }

    /**
     * Sets the value of the roomRentWaiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomRentWaiver(String value) {
        this.roomRentWaiver = value;
    }

    /**
     * Gets the value of the uniqueQuoteId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueQuoteId() {
        return uniqueQuoteId;
    }

    /**
     * Sets the value of the uniqueQuoteId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueQuoteId(String value) {
        this.uniqueQuoteId = value;
    }

}
