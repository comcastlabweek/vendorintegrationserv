
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getIHPPremiumReturn" type="{http://premiumwrapper.health.itgi.com}IHPPremiumDetails"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getIHPPremiumReturn"
})
@XmlRootElement(name = "getIHPPremiumResponse")
public class GetIHPPremiumResponse {

    @XmlElement(required = true)
    protected IHPPremiumDetails getIHPPremiumReturn;

    /**
     * Gets the value of the getIHPPremiumReturn property.
     * 
     * @return
     *     possible object is
     *     {@link IHPPremiumDetails }
     *     
     */
    public IHPPremiumDetails getGetIHPPremiumReturn() {
        return getIHPPremiumReturn;
    }

    /**
     * Sets the value of the getIHPPremiumReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link IHPPremiumDetails }
     *     
     */
    public void setGetIHPPremiumReturn(IHPPremiumDetails value) {
        this.getIHPPremiumReturn = value;
    }

}
