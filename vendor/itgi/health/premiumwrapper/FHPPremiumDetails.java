
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FHPPremiumDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FHPPremiumDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="error" type="{http://premiumwrapper.health.itgi.com}ArrayOfErrorDetails"/&gt;
 *         &lt;element name="fhpInsured" type="{http://premiumwrapper.health.itgi.com}ArrayOf_tns1_FHPInsuredResponse"/&gt;
 *         &lt;element name="grossPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="netPremiumPayable" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="premiumAfterDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="premiumBeforeDiscount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceTax" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FHPPremiumDetails", propOrder = {
    "error",
    "fhpInsured",
    "grossPremium",
    "netPremiumPayable",
    "premiumAfterDiscount",
    "premiumBeforeDiscount",
    "serviceTax"
})
public class FHPPremiumDetails {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfErrorDetails error;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfTns1FHPInsuredResponse fhpInsured;
    @XmlElement(required = true, nillable = true)
    protected String grossPremium;
    @XmlElement(required = true, nillable = true)
    protected String netPremiumPayable;
    @XmlElement(required = true, nillable = true)
    protected String premiumAfterDiscount;
    @XmlElement(required = true, nillable = true)
    protected String premiumBeforeDiscount;
    @XmlElement(required = true, nillable = true)
    protected String serviceTax;

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfErrorDetails }
     *     
     */
    public ArrayOfErrorDetails getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfErrorDetails }
     *     
     */
    public void setError(ArrayOfErrorDetails value) {
        this.error = value;
    }

    /**
     * Gets the value of the fhpInsured property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTns1FHPInsuredResponse }
     *     
     */
    public ArrayOfTns1FHPInsuredResponse getFhpInsured() {
        return fhpInsured;
    }

    /**
     * Sets the value of the fhpInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTns1FHPInsuredResponse }
     *     
     */
    public void setFhpInsured(ArrayOfTns1FHPInsuredResponse value) {
        this.fhpInsured = value;
    }

    /**
     * Gets the value of the grossPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossPremium() {
        return grossPremium;
    }

    /**
     * Sets the value of the grossPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossPremium(String value) {
        this.grossPremium = value;
    }

    /**
     * Gets the value of the netPremiumPayable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetPremiumPayable() {
        return netPremiumPayable;
    }

    /**
     * Sets the value of the netPremiumPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetPremiumPayable(String value) {
        this.netPremiumPayable = value;
    }

    /**
     * Gets the value of the premiumAfterDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumAfterDiscount() {
        return premiumAfterDiscount;
    }

    /**
     * Sets the value of the premiumAfterDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumAfterDiscount(String value) {
        this.premiumAfterDiscount = value;
    }

    /**
     * Gets the value of the premiumBeforeDiscount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremiumBeforeDiscount() {
        return premiumBeforeDiscount;
    }

    /**
     * Sets the value of the premiumBeforeDiscount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremiumBeforeDiscount(String value) {
        this.premiumBeforeDiscount = value;
    }

    /**
     * Gets the value of the serviceTax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceTax() {
        return serviceTax;
    }

    /**
     * Sets the value of the serviceTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceTax(String value) {
        this.serviceTax = value;
    }

}
