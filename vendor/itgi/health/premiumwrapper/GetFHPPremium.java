
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fhpPolicy" type="{http://premiumwrapper.health.itgi.com}FHPPolicy"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fhpPolicy"
})
@XmlRootElement(name = "getFHPPremium")
public class GetFHPPremium {

    @XmlElement(required = true)
    protected FHPPolicy fhpPolicy;

    /**
     * Gets the value of the fhpPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link FHPPolicy }
     *     
     */
    public FHPPolicy getFhpPolicy() {
        return fhpPolicy;
    }

    /**
     * Sets the value of the fhpPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link FHPPolicy }
     *     
     */
    public void setFhpPolicy(FHPPolicy value) {
        this.fhpPolicy = value;
    }

}
