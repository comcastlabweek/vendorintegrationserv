
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IHPPremiumDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IHPPremiumDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="error" type="{http://premiumwrapper.health.itgi.com}ArrayOfErrorDetails"/&gt;
 *         &lt;element name="grossPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ihpInsured" type="{http://premiumwrapper.health.itgi.com}ArrayOf_tns1_IHPInsuredResponse"/&gt;
 *         &lt;element name="netPremiumPayable" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serviceTax" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IHPPremiumDetails", propOrder = {
    "error",
    "grossPremium",
    "ihpInsured",
    "netPremiumPayable",
    "serviceTax"
})
public class IHPPremiumDetails {

    @XmlElement(required = true, nillable = true)
    protected ArrayOfErrorDetails error;
    @XmlElement(required = true, nillable = true)
    protected String grossPremium;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfTns1IHPInsuredResponse ihpInsured;
    @XmlElement(required = true, nillable = true)
    protected String netPremiumPayable;
    @XmlElement(required = true, nillable = true)
    protected String serviceTax;

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfErrorDetails }
     *     
     */
    public ArrayOfErrorDetails getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfErrorDetails }
     *     
     */
    public void setError(ArrayOfErrorDetails value) {
        this.error = value;
    }

    /**
     * Gets the value of the grossPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrossPremium() {
        return grossPremium;
    }

    /**
     * Sets the value of the grossPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrossPremium(String value) {
        this.grossPremium = value;
    }

    /**
     * Gets the value of the ihpInsured property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTns1IHPInsuredResponse }
     *     
     */
    public ArrayOfTns1IHPInsuredResponse getIhpInsured() {
        return ihpInsured;
    }

    /**
     * Sets the value of the ihpInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTns1IHPInsuredResponse }
     *     
     */
    public void setIhpInsured(ArrayOfTns1IHPInsuredResponse value) {
        this.ihpInsured = value;
    }

    /**
     * Gets the value of the netPremiumPayable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetPremiumPayable() {
        return netPremiumPayable;
    }

    /**
     * Sets the value of the netPremiumPayable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetPremiumPayable(String value) {
        this.netPremiumPayable = value;
    }

    /**
     * Gets the value of the serviceTax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceTax() {
        return serviceTax;
    }

    /**
     * Sets the value of the serviceTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceTax(String value) {
        this.serviceTax = value;
    }

}
