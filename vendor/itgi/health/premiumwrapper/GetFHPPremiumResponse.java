
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="getFHPPremiumReturn" type="{http://premiumwrapper.health.itgi.com}FHPPremiumDetails"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFHPPremiumReturn"
})
@XmlRootElement(name = "getFHPPremiumResponse")
public class GetFHPPremiumResponse {

    @XmlElement(required = true)
    protected FHPPremiumDetails getFHPPremiumReturn;

    /**
     * Gets the value of the getFHPPremiumReturn property.
     * 
     * @return
     *     possible object is
     *     {@link FHPPremiumDetails }
     *     
     */
    public FHPPremiumDetails getGetFHPPremiumReturn() {
        return getFHPPremiumReturn;
    }

    /**
     * Sets the value of the getFHPPremiumReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link FHPPremiumDetails }
     *     
     */
    public void setGetFHPPremiumReturn(FHPPremiumDetails value) {
        this.getFHPPremiumReturn = value;
    }

}
