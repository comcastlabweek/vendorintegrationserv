
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FHPInsured complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FHPInsured"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="preExistingDisease" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="relationToInsured" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FHPInsured", propOrder = {
    "dateOfBirth",
    "preExistingDisease",
    "relationToInsured"
})
public class FHPInsured {

    @XmlElement(required = true, nillable = true)
    protected String dateOfBirth;
    @XmlElement(required = true, nillable = true)
    protected String preExistingDisease;
    @XmlElement(required = true, nillable = true)
    protected String relationToInsured;

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfBirth(String value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the preExistingDisease property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreExistingDisease() {
        return preExistingDisease;
    }

    /**
     * Sets the value of the preExistingDisease property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreExistingDisease(String value) {
        this.preExistingDisease = value;
    }

    /**
     * Gets the value of the relationToInsured property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelationToInsured() {
        return relationToInsured;
    }

    /**
     * Sets the value of the relationToInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelationToInsured(String value) {
        this.relationToInsured = value;
    }

}
