
package itgi.health.premiumwrapper;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ihpPolicy" type="{http://premiumwrapper.health.itgi.com}IHPPolicy"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ihpPolicy"
})
@XmlRootElement(name = "getIHPPremium")
public class GetIHPPremium {

    @XmlElement(required = true)
    protected IHPPolicy ihpPolicy;

    /**
     * Gets the value of the ihpPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link IHPPolicy }
     *     
     */
    public IHPPolicy getIhpPolicy() {
        return ihpPolicy;
    }

    /**
     * Sets the value of the ihpPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link IHPPolicy }
     *     
     */
    public void setIhpPolicy(IHPPolicy value) {
        this.ihpPolicy = value;
    }

}
