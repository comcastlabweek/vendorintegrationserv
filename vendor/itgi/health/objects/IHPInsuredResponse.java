
package itgi.health.objects;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IHPInsuredResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IHPInsuredResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="basePremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ciPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="rentWaiver" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="totalPremium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IHPInsuredResponse", propOrder = {
    "basePremium",
    "ciPremium",
    "rentWaiver",
    "totalPremium"
})
public class IHPInsuredResponse {

    @XmlElement(required = true, nillable = true)
    protected String basePremium;
    @XmlElement(required = true, nillable = true)
    protected String ciPremium;
    @XmlElement(required = true, nillable = true)
    protected String rentWaiver;
    @XmlElement(required = true, nillable = true)
    protected String totalPremium;

    /**
     * Gets the value of the basePremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasePremium() {
        return basePremium;
    }

    /**
     * Sets the value of the basePremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasePremium(String value) {
        this.basePremium = value;
    }

    /**
     * Gets the value of the ciPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiPremium() {
        return ciPremium;
    }

    /**
     * Sets the value of the ciPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiPremium(String value) {
        this.ciPremium = value;
    }

    /**
     * Gets the value of the rentWaiver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRentWaiver() {
        return rentWaiver;
    }

    /**
     * Sets the value of the rentWaiver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRentWaiver(String value) {
        this.rentWaiver = value;
    }

    /**
     * Gets the value of the totalPremium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPremium() {
        return totalPremium;
    }

    /**
     * Sets the value of the totalPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPremium(String value) {
        this.totalPremium = value;
    }

}
