package hdfcergo.motor.proposal;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.1.12
 * 2017-11-15T17:58:46.599+05:30
 * Generated source version: 3.1.12
 * 
 */
@WebService(targetNamespace = "http://tempuri.org/", name = "ServiceHttpGet")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ServiceHttpGet {

    @WebMethod(operationName = "HEGICMotorRenewal_Extract")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalExtract(
        @WebParam(partName = "AgentCode", name = "AgentCode", targetNamespace = "http://tempuri.org/")
        java.lang.String agentCode,
        @WebParam(partName = "PrevPolicyNo", name = "PrevPolicyNo", targetNamespace = "http://tempuri.org/")
        java.lang.String prevPolicyNo,
        @WebParam(partName = "VehicleRegistratonNo", name = "VehicleRegistratonNo", targetNamespace = "http://tempuri.org/")
        java.lang.String vehicleRegistratonNo
    );

    @WebMethod(operationName = "HEGICMotorRenewal_GenerateRWTransaction")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalGenerateRWTransaction(
        @WebParam(partName = "RWXMLDetails", name = "RWXMLDetails", targetNamespace = "http://tempuri.org/")
        java.lang.String rwxmlDetails
    );

    @WebMethod(operationName = "HEGICMotorRenewal_Premium")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalPremium(
        @WebParam(partName = "RenewalPremiumInputXml", name = "RenewalPremiumInputXml", targetNamespace = "http://tempuri.org/")
        java.lang.String renewalPremiumInputXml
    );

    @WebMethod(operationName = "GetBreakinProposalDataFinalPremium")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String getBreakinProposalDataFinalPremium(
        @WebParam(partName = "AgentCode", name = "AgentCode", targetNamespace = "http://tempuri.org/")
        java.lang.String agentCode,
        @WebParam(partName = "PGTransNo", name = "PGTransNo", targetNamespace = "http://tempuri.org/")
        java.lang.String pgTransNo
    );

    @WebMethod(operationName = "GetBreakinInspectionStatus")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String getBreakinInspectionStatus(
        @WebParam(partName = "AgentCode", name = "AgentCode", targetNamespace = "http://tempuri.org/")
        java.lang.String agentCode,
        @WebParam(partName = "PGTransNo", name = "PGTransNo", targetNamespace = "http://tempuri.org/")
        java.lang.String pgTransNo
    );

    @WebMethod
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String xmlstring(
        @WebParam(partName = "str", name = "str", targetNamespace = "http://tempuri.org/")
        java.lang.String str
    );

    @WebMethod(operationName = "HEGICTWRenewal_GenerateRWTransaction")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegictwRenewalGenerateRWTransaction(
        @WebParam(partName = "RenewalInputXml", name = "RenewalInputXml", targetNamespace = "http://tempuri.org/")
        java.lang.String renewalInputXml
    );

    @WebMethod(operationName = "HEGICMotorRenewal_GenerateRWTransactionDirect")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalGenerateRWTransactionDirect(
        @WebParam(partName = "RenewalInputXml", name = "RenewalInputXml", targetNamespace = "http://tempuri.org/")
        java.lang.String renewalInputXml
    );

    @WebMethod(operationName = "HEGICTWRenewal_Extract")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegictwRenewalExtract(
        @WebParam(partName = "RenewalInputXml", name = "RenewalInputXml", targetNamespace = "http://tempuri.org/")
        java.lang.String renewalInputXml
    );

    @WebMethod(operationName = "HEGICMotorRenewal")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewal(
        @WebParam(partName = "AgentCode", name = "AgentCode", targetNamespace = "http://tempuri.org/")
        java.lang.String agentCode,
        @WebParam(partName = "PrevPolicyNo", name = "PrevPolicyNo", targetNamespace = "http://tempuri.org/")
        java.lang.String prevPolicyNo
    );

    @WebMethod(operationName = "HEGICMotorRenewal_Dynamic")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalDynamic(
        @WebParam(partName = "AgentCode", name = "AgentCode", targetNamespace = "http://tempuri.org/")
        java.lang.String agentCode,
        @WebParam(partName = "PrevPolicyNo", name = "PrevPolicyNo", targetNamespace = "http://tempuri.org/")
        java.lang.String prevPolicyNo
    );

    @WebMethod(operationName = "HEGICMotorRenewal_GetRenewalPlans")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalGetRenewalPlans(
        @WebParam(partName = "AgentCode", name = "AgentCode", targetNamespace = "http://tempuri.org/")
        java.lang.String agentCode,
        @WebParam(partName = "PrevPolicyNo", name = "PrevPolicyNo", targetNamespace = "http://tempuri.org/")
        java.lang.String prevPolicyNo,
        @WebParam(partName = "VehicleRegistrationNo", name = "VehicleRegistrationNo", targetNamespace = "http://tempuri.org/")
        java.lang.String vehicleRegistrationNo
    );

    @WebMethod(operationName = "HEGICTWRenewal_Premium")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegictwRenewalPremium(
        @WebParam(partName = "RenewalInputXml", name = "RenewalInputXml", targetNamespace = "http://tempuri.org/")
        java.lang.String renewalInputXml
    );

    @WebMethod(operationName = "HEGICMotorRenewal_UpdateAddressDetails")
    @WebResult(name = "string", targetNamespace = "http://tempuri.org/", partName = "Body")
    public java.lang.String hegicMotorRenewalUpdateAddressDetails(
        @WebParam(partName = "RenewalInputAddressDetails", name = "RenewalInputAddressDetails", targetNamespace = "http://tempuri.org/")
        java.lang.String renewalInputAddressDetails
    );
}
