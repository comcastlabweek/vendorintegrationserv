
package usgic.org;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="commBRIDGEFusionHEALTHResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commBRIDGEFusionHEALTHResult"
})
@XmlRootElement(name = "commBRIDGEFusionHEALTHResponse")
public class CommBRIDGEFusionHEALTHResponse {

    @XmlElement(nillable = true)
    protected String commBRIDGEFusionHEALTHResult;

    /**
     * Gets the value of the commBRIDGEFusionHEALTHResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommBRIDGEFusionHEALTHResult() {
        return commBRIDGEFusionHEALTHResult;
    }

    /**
     * Sets the value of the commBRIDGEFusionHEALTHResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommBRIDGEFusionHEALTHResult(String value) {
        this.commBRIDGEFusionHEALTHResult = value;
    }

}
