
package usgic.org;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CommBRIDGEFusionMOTOR }
     * 
     */
    public CommBRIDGEFusionMOTOR createCommBRIDGEFusionMOTOR() {
        return new CommBRIDGEFusionMOTOR();
    }

    /**
     * Create an instance of {@link CommBRIDGEFusionMOTORResponse }
     * 
     */
    public CommBRIDGEFusionMOTORResponse createCommBRIDGEFusionMOTORResponse() {
        return new CommBRIDGEFusionMOTORResponse();
    }

    /**
     * Create an instance of {@link CommBRIDGEFusionHEALTH }
     * 
     */
    public CommBRIDGEFusionHEALTH createCommBRIDGEFusionHEALTH() {
        return new CommBRIDGEFusionHEALTH();
    }

    /**
     * Create an instance of {@link CommBRIDGEFusionHEALTHResponse }
     * 
     */
    public CommBRIDGEFusionHEALTHResponse createCommBRIDGEFusionHEALTHResponse() {
        return new CommBRIDGEFusionHEALTHResponse();
    }

    /**
     * Create an instance of {@link CommBRIDGEFusionTravel }
     * 
     */
    public CommBRIDGEFusionTravel createCommBRIDGEFusionTravel() {
        return new CommBRIDGEFusionTravel();
    }

    /**
     * Create an instance of {@link CommBRIDGEFusionTravelResponse }
     * 
     */
    public CommBRIDGEFusionTravelResponse createCommBRIDGEFusionTravelResponse() {
        return new CommBRIDGEFusionTravelResponse();
    }

}
